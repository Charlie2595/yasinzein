<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 25/03/2018
 * Time: 00.13
 */
use App\Traits\SecurityFilter;
class Retur_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function get($id){
		$id = $this->security->xss_clean($this->sanitize_input($id));

		$sql = "SELECT p.id_masuk, p.document, p.id_sup, p.tgl, p.lunas, p.jumlah, p.id_pegawai, m.nama AS supplier, pe.nama AS pegawai
        FROM masuk p 
        LEFT JOIN master_supplier m ON m.id_sup = p.id_sup 
        INNER JOIN master_pegawai pe ON pe.id_pegawai = p.id_pegawai 
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail penjualan
        $sql = "SELECT p.id as id_masuk_detail, p.id_kue, p.jumlah, p.retur, p.id_pegawai, mk.nama_kue, mp.nama AS pegawai, mk.barcode_kue, p.harga_beli
        FROM masuk_detail p INNER JOIN master_kue mk ON mk.id_kue = p.id_kue
        LEFT JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['detail'] = $result;
		
		return $hasil;
	}

	public function getList(){
		$sql = "SELECT p.id_masuk, p.document, pe.tgl_retur as tgl, mp.nama AS pegawai, m.nama AS supplier, p.lunas
        FROM masuk p 
        LEFT JOIN masuk_detail pe ON pe.id_masuk = p.id_masuk 
        INNER JOIN master_pegawai mp ON pe.id_pegawai = mp.id_pegawai
        INNER JOIN master_supplier m ON m.id_sup = p.id_sup
        WHERE pe.retur > 0
        GROUP BY p.id_masuk, p.document, m.nama, pe.tgl_retur, mp.nama ";

        $exectutedQuery = $this->db->query($sql);
        $list = $exectutedQuery->result_array();

        return $list;
	}

	public function searchBarangMasuk($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));

        $sql = "SELECT p.id_masuk, p.document, p.tgl, p.jumlah, mp.nama AS pegawai, p.id_pegawai
            FROM masuk p
            INNER JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
            WHERE p.id_sup = ?";

        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        
        return $result;
    }

    public function detailBarangMasuk($id){
        $sql = "SELECT p.id AS id, p.id_kue, k.nama_kue AS nama, p.jumlah, p.retur, k.barcode_kue, p.harga_beli
        FROM masuk_detail p 
        INNER JOIN master_kue k ON k.id_kue = p.id_kue
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        return $result;
    }

    public function createRetur($idSupplier, $tgl, $idPegawai, $idBarangMasuk, $detail, $totalBarang){
    	$idSupplier = $this->security->xss_clean($this->sanitize_input($idSupplier));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idBarangMasuk = $this->security->xss_clean($this->sanitize_input($idBarangMasuk));
        $totalBarang = $this->security->xss_clean($this->sanitize_input($totalBarang));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
        $cekError = false;
        $total = 0;
        if($detail != null){
            $sqlDetail = "UPDATE masuk_detail SET tgl_retur = ?, retur = retur + ?, id_pegawai = ? WHERE id = ?";
            for ($i=0; $i < count($detail); $i++)
            {
                $idDetail = $this->security->xss_clean($this->sanitize_input($detail[$i]['idDetail']));
                $jumlahRetur = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlahRetur']));
                $paramBarang = [$datetime, $jumlahRetur, $idPegawai, $idDetail];
                $this->db->query($sqlDetail, $paramBarang);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                        $cekError = true;
                }
                $total = $total + ($jumlahRetur*1);
                
                $sqlStok = "SELECT id_kue FROM masuk_detail WHERE id = ?";
                $param = [$idDetail];
                $exectutedQuery = $this->db->query($sqlStok, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                $sqlUpdateStok = "UPDATE master_kue SET stok = stok - ? WHERE id_kue = ?";
                $paramUpdate = [$total, $result['id_kue']];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        $sql = "UPDATE masuk SET jumlah = jumlah - ? WHERE id_masuk = ?";
        $bindParam = [$total, $idBarangMasuk];
        $this->db->query($sql,$bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal membuat retur barang masuk'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil membuat retur barang masuk'];
        }
    }

    public function update($idMasuk, $tgl, $idPegawai, $grandtotal, $total, $detail, $totalItem)
    {
        $cekError = false;
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idMasuk = $this->security->xss_clean($this->sanitize_input($idMasuk));
        $grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
        $sql = "UPDATE masuk SET jumlah = ? WHERE id_masuk = ?";
        $bindParam = [$totalItem, $idMasuk];
        $this->db->query($sql,$bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "SELECT * FROM masuk_detail where id_masuk = ?";
        $bindParam = [$idMasuk];
        $result = $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $result->result_array();

        for($i = 0; $i<count($result); $i++){
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok + ? WHERE id_kue = ?";
            $paramUpdate = [(int)$result[$i]['retur'], $result[$i]['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        $total = 0;
        if($detail != null){
            $sqlDetail = "UPDATE masuk_detail SET retur = ?, id_pegawai = ? WHERE id = ?";
            for ($i=0; $i < count($detail); $i++)
            {
                $idDetail = $this->security->xss_clean($this->sanitize_input($detail[$i]['idDetail']));
                $idBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['idBarang']));
                $jumlahRetur = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlahRetur']));
                $paramBarang = [$jumlahRetur, $idPegawai, $idDetail];
                $this->db->query($sqlDetail, $paramBarang);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                
                $sqlUpdateStok = "UPDATE master_kue SET stok = stok - ? WHERE id_kue = ?";
                $paramUpdate = [$jumlahRetur, $idBarang];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }
}
