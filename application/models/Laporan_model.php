<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 23.47
 */
use App\Traits\SecurityFilter;
class Laporan_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function getPembayaran($id, $tglAwal, $tglAkhir){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == 'all'){
			$sql = "SELECT (sum(p.total) - sum(a.kembalian)) AS total, sum(a.kembalian) as kembalian, m.nama, p.jenis_trx FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 AND m.id_masterbayar = 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx
			UNION SELECT sum(p.total) AS total, '0' as kembalian, m.nama, p.jenis_trx FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 and m.id_masterbayar <> 1  and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx";
			$bindParam = [$tglAwal, $tglAkhir, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else{
			$sql = "SELECT (sum(p.total) - sum(a.kembalian)) AS total, sum(a.kembalian) as kembalian, m.nama, p.jenis_trx FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.id_tipe_bayar = ? AND p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 AND m.id_masterbayar = 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx UNION SELECT sum(p.total) AS total, '0' as kembalian, m.nama, p.jenis_trx FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.id_tipe_bayar = ? AND p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 and m.id_masterbayar <> 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx";
			$bindParam = [$id, $tglAwal, $tglAkhir, $id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		return $exectutedQuery->result_array();
	}

	public function getPembayaranDetail($id, $tglAwal, $tglAkhir){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == 'all'){
			$sql = "SELECT (sum(p.total) - sum(a.kembalian)) AS grandtotal, sum(p.total) as total, sum(a.kembalian) as kembalian, m.nama, a.id_jual, a.document, a.tgl, a.keterangan as keteranganPenjualan, p.keterangan, a.last_document, (a.total - a.diskon_pembulatan) as totalTransaksi
			FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 AND m.id_masterbayar = 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, a.id_jual, p.keterangan
			UNION SELECT sum(p.total) AS grandtotal, sum(p.total) as total, '0' as kembalian, m.nama, a.id_jual, a.document, a.tgl, a.keterangan as keteranganPenjualan, p.keterangan, a.last_document 
			FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 and m.id_masterbayar <> 1  and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, a.id_jual, p.keterangan";
			$bindParam = [$tglAwal, $tglAkhir, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else{
			$sql = "SELECT (sum(p.total) - sum(a.kembalian)) AS grandtotal, sum(p.total) as total, sum(a.kembalian) as kembalian, m.nama, a.id_jual, a.document, a.tgl, a.keterangan as keteranganPenjualan, p.keterangan, a.last_document FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.id_tipe_bayar = ? AND p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 AND m.id_masterbayar = 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, a.id_jual, p.keterangan
			UNION SELECT sum(p.total) AS grandtotal, sum(p.total) as total, '0' as kembalian, m.nama, a.id_jual, a.document, a.tgl, a.keterangan as keteranganPenjualan, p.keterangan, a.last_document FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.id_tipe_bayar = ? AND p.tgl >= ? AND  ? >= p.tgl AND a.lunas = 1 and m.id_masterbayar <> 1 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, a.id_jual, p.keterangan";
			$bindParam = [$id, $tglAwal, $tglAkhir, $id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		return $exectutedQuery->result_array();
	}

	public function getPembayaranDP($id, $tglAwal, $tglAkhir){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == 'all'){
			$sql = "SELECT sum(p.total) AS total, m.nama, p.jenis_trx FROM pembayaran_detail p
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE a.tgl >= ? AND  ? >= a.tgl AND a.lunas = 0 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx";
			$bindParam = [$tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else{
			$sql = "SELECT sum(p.total) AS total, p.tgl, p.keterangan, m.nama, p.jenis_trx FROM pembayaran_detail p 
			INNER JOIN penjualan a ON a.id_jual = p.id_trx
			INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
			WHERE p.id_tipe_bayar = ? AND a.tgl >= ? AND  ? >= a.tgl AND a.lunas = 0 and a.hapus = 0
			GROUP BY p.id_tipe_bayar, m.id_masterbayar, p.jenis_trx, p.tgl, p.keterangan, m.nama";
			$bindParam = [$id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}

		return $exectutedQuery->result_array();
	}

	public function getPembayaranByYear($year){
		$sql = "SELECT * FROM master_pembayaran";
		$pembayaran = $this->db->query($sql)->result_array();
		$monthlySalesArray =[];
		for($i=1; $i<=12; $i++){
            $monthList[] = str_pad($i, 2, "0", STR_PAD_LEFT);
		}
		
		$count = 0;
		foreach ($pembayaran as $value) {
			$result[] = $value['nama'];
			foreach ($monthList as $month) {
				$sql = "SELECT (sum(p.total) - sum(a.kembalian)) as total FROM pembayaran_detail p 
				INNER JOIN penjualan a ON a.id_jual = p.id_trx
				INNER JOIN master_pembayaran m ON p.id_tipe_bayar = m.id_masterbayar 
				WHERE MONTH(p.tgl) = ? AND YEAR(p.tgl) = ? and m.id_masterbayar = ? and a.hapus = 0
				GROUP BY p.id_tipe_bayar";
				$bindParam = [$month, $year, $value['id_masterbayar']];
				$exectutedQuery = $this->db->query($sql, $bindParam);
				$hasil = $exectutedQuery->row_array();
				if($hasil['total'] == null){
					$hasil['total'] = 0;
				}
				$result[] = (float)$hasil['total'];
			}
			$monthlySalesArray[] = $result;
			$result = [];
			$count++;
		}
		return $monthlySalesArray;
	}

	public function getDepartemen($tglAwal, $tglAkhir, $departemen){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		$sql = "SELECT distinct(departemen) as departemen FROM master_kue WHERE departemen like '%" . $departemen. "%' GROUP BY departemen";
		$allDepartemen = $this->db->query($sql)->result_array();
		foreach ($allDepartemen as $value) {
			$result['departemen'] = $value['departemen'];
			$sql = "SELECT sum(p.jumlah) as jumlah, p.id_kue, m.nama_kue, sum(p.retur) as retur, m.stok, m.departemen
			FROM penjualan_detail p 
			INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
			INNER JOIN master_kue m ON p.id_kue = m.id_kue 
			WHERE ph.tgl BETWEEN ? AND ? and m.departemen like '%" . $value['departemen'] . "%' and ph.hapus = 0
			GROUP BY p.id_kue, m.departemen";
			$bindParam = [$tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);

			$result['detail'] = $exectutedQuery->result_array();
			$getDepartemen[] = $result;
			$result = [];
		}
		return $getDepartemen;
	}

	public function getSupplier($tglAwal, $tglAkhir, $supplier){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == 'all'){
			$sql = "SELECT a.transaksi, b.masuk, b.retur, d.nama_kue, d.barcode_kue FROM 
			kue_supplier k
			INNER JOIN master_kue d ON d.id_kue = k.id_kue
			LEFT JOIN (SELECT SUM(pd.jumlah)- SUM(pd.retur) AS transaksi, pd.id_kue FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual WHERE p.hapus = 0 and pd.id_kue in (SELECT id_kue FROM kue_supplier k WHERE id_sup = ?) AND p.tgl >= ? AND  ? >= p.tgl GROUP BY pd.id_kue) AS a ON a.id_kue = k.id_kue
			LEFT JOIN (SELECT SUM(cd.jumlah) AS masuk, SUM(cd.retur) AS retur, cd.id_kue FROM masuk c INNER JOIN masuk_detail cd ON cd.id_masuk = c.id_masuk WHERE cd.id_kue in (SELECT id_kue FROM kue_supplier k WHERE id_sup = ?) AND c.tgl >= ? AND  ? >= c.tgl GROUP BY cd.id_kue) AS b ON b.id_kue = k.id_kue
			WHERE k.id_sup = ?";
			// $sql = "SELECT d.nama_kue, d.barcode_kue, d.departemen, SUM(bd.jumlah) - SUM(bd.retur) as transaksi, SUM(cd.jumlah) AS masuk, SUM(cd.retur) AS retur FROM kue_supplier a
			// LEFT JOIN penjualan_detail bd ON bd.id_kue = a.id_kue
			// LEFT JOIN penjualan b ON b.id_jual = bd.id_jual
			// INNER JOIN master_kue d ON d.id_kue = a.id_kue
			// LEFT JOIN masuk c ON c.id_sup = a.id_sup
			// LEFT JOIN masuk_detail cd ON cd.id_masuk = c.id_masuk
			// WHERE c.tgl >= ? AND  ? >= c.tgl AND b.tgl >= ? AND ? >= b.tgl AND a.id_sup = ?
			// GROUP BY a.id_kue, a.id_sup";
			$bindParam = [$supplier, $tglAwal, $tglAkhir, $supplier, $tglAwal, $tglAkhir, $supplier];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else{
			$sql = "SELECT d.nama_kue, d.barcode_kue, d.departemen, SUM(bd.jumlah) - SUM(bd.retur) as transaksi, SUM(cd.jumlah) AS jumlah, SUM(cd.retur) AS retur FROM kue_supplier a
			LEFT JOIN penjualan_detail bd ON bd.id_kue = a.id_kue
			LEFT JOIN penjualan b ON b.id_jual = bd.id_jual
			INNER JOIN master_kue d ON d.id_kue = a.id_kue
			INNER JOIN masuk c ON c.id_sup = a.id_sup
			INNER JOIN masuk_detail cd ON cd.id_masuk = c.id_masuk
			WHERE c.tgl >= ? AND  ? >= c.tgl AND b.tgl >= ? AND  ? >= b.tgl AND a.id_sup AND b.hapus = 0
			GROUP BY a.id_kue, a.id_sup";
			$bindParam = [$tglAwal, $tglAkhir, $tglAwal, $tglAkhir, $supplier];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		return $exectutedQuery->result_array();
	}

	public function getBarang($id, $tglAwal, $tglAkhir, $departemen){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == ""){
			$sql = "SELECT sum(p.jumlah) as jumlah, sum(p.retur) as retur, m.stok, p.id_kue, m.nama_kue, m.departemen
			FROM penjualan_detail p 
			INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
			INNER JOIN master_kue m ON p.id_kue = m.id_kue 
			WHERE ph.tgl BETWEEN ? AND ? and m.departemen like '%" . $departemen. "%' AND p.hapus = 0
			GROUP BY p.id_kue";
			$bindParam = [$tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else{
			$sql = "SELECT sum(p.jumlah) as jumlah, p.id_kue, m.nama_kue, sum(p.retur) as retur, m.stok, m.departemen
			FROM penjualan_detail p 
			INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
			INNER JOIN master_kue m ON p.id_kue = m.id_kue 
			WHERE p.id_kue = ? and ph.tgl BETWEEN ? AND ? and m.departemen like '%" . $departemen. "%' AND p.hapus = 0
			GROUP BY p.id_kue ";
			$bindParam = [$id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}

		return $exectutedQuery->result_array();
	}

	public function getBarangDetail($tglAwal, $tglAkhir, $departemen, $id_kue){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id_kue == null || $id_kue == "" || $id_kue == 0){
			$sql = "SELECT distinct(p.id_kue) as id, m.nama_kue, m.departemen
				FROM penjualan_detail p 
				INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
				INNER JOIN master_kue m ON p.id_kue = m.id_kue 
				WHERE ph.tgl BETWEEN ? AND ? and m.departemen like '%" . $departemen . "%' and ph.hapus = 0
				GROUP BY p.id_kue";
				
			$bindParam = [$tglAwal, $tglAkhir];
		}
		else{
			$sql = "SELECT distinct(p.id_kue) as id, m.nama_kue, m.departemen
			FROM penjualan_detail p 
			INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
			INNER JOIN master_kue m ON p.id_kue = m.id_kue 
			WHERE ph.tgl BETWEEN ? AND ? and m.departemen like '%" . $departemen . "%' and m.id_kue = ? AND ph.hapus = 0
			GROUP BY p.id_kue ";
			$bindParam = [$tglAwal, $tglAkhir, $id_kue];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		$allBarang = $this->db->query($sql, $bindParam)->result_array();

		foreach ($allBarang as $value) {
			$result['idBarang'] = $value['id'];
			$result['departemen'] = $value['departemen'];
			$result['name'] = $value['nama_kue'];
			$sql = "SELECT sum(p.jumlah) as jumlah, p.id_kue, m.nama_kue, sum(p.retur) as retur, m.stok, m.departemen, ph.document, ph.tgl, ph.id_jual
			FROM penjualan_detail p 
			INNER JOIN penjualan ph ON ph.id_jual = p.id_jual 
			INNER JOIN master_kue m ON p.id_kue = m.id_kue 
			WHERE ph.tgl BETWEEN ? AND ? and m.id_kue = ? and ph.hapus = 0
			GROUP BY p.id_kue, m.departemen, ph.id_jual";
			$bindParam = [$tglAwal, $tglAkhir, $value['id']];
			$exectutedQuery = $this->db->query($sql, $bindParam);

			$result['detail'] = $exectutedQuery->result_array();
			$getBarang[] = $result;
			$result = [];
		}

		return $getBarang;
	}

	public function getStokBarang($id, $tglAwal, $tglAkhir, $departemen){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";

		if ($id != null && $id != ""){
			$sql = "SELECT m.id_kue, m.nama_kue, a.transaksi, b.masuk, b.retur, m.departemen, m.stok FROM master_kue m 
			LEFT JOIN (SELECT SUM(pd.jumlah) as masuk, SUM(pd.retur) AS retur, pd.id_kue FROM masuk p INNER JOIN masuk_detail pd ON pd.id_masuk = p.id_masuk WHERE p.tgl BETWEEN ? AND ? GROUP BY pd.id_kue) AS b ON b.id_kue = m.id_kue
			LEFT JOIN (SELECT SUM(pd.jumlah)- SUM(pd.retur) AS transaksi, pd.id_kue FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual WHERE p.hapus = 0 and p.tgl BETWEEN ? AND ? GROUP BY pd.id_kue) AS a ON a.id_kue = m.id_kue where m.departemen like '%" . $departemen. "%'";
			$bindParam = [$id, $tglAwal, $tglAkhir, $id, $tglAwal, $tglAkhir];
		} else {
			$sql = "SELECT m.id_kue, m.nama_kue, a.transaksi, b.masuk, b.retur, m.departemen, m.stok FROM master_kue m 
			LEFT JOIN (SELECT SUM(pd.jumlah) as masuk, SUM(pd.retur) AS retur, pd.id_kue FROM masuk p INNER JOIN masuk_detail pd ON pd.id_masuk = p.id_masuk WHERE p.tgl BETWEEN ? AND ? GROUP BY pd.id_kue) AS b ON b.id_kue = m.id_kue
			LEFT JOIN (SELECT SUM(pd.jumlah)- SUM(pd.retur) AS transaksi, pd.id_kue FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual WHERE p.hapus = 0 and p.tgl BETWEEN ? AND ? GROUP BY pd.id_kue) AS a ON a.id_kue = m.id_kue where m.departemen like '%" . $departemen. "%'";
			$bindParam = [$tglAwal, $tglAkhir, $tglAwal, $tglAkhir];
		}
		$exectutedQuery = $this->db->query($sql, $bindParam);

		return $exectutedQuery->result_array();
	}

	public function getSalesMember($id, $tglAwal, $tglAkhir){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == ""){
			$sql = "SELECT a.document, a.tgl, a.id_jual as id, a.diskon_pembulatan as pemotongan, a.diskon_member, a.total, a.keterangan, b.nama, a.tipe_trx, sum(pd.jumlah - pd.retur) as totalQty, (a.total - a.diskon_member - a.diskon_pembulatan) AS grandtotal
			FROM penjualan a 
			INNER JOIN penjualan_detail pd ON a.id_jual = pd.id_jual 
			LEFT JOIN master_member b ON a.id_member = b.id_member 
			WHERE a.lunas = 1 and a.tgl >= ? AND  ? >= a.tgl AND a.hapus = 0
			GROUP BY a.id_jual, b.id_member, b.nama";
			$bindParam = [$tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else {
			$sql = "SELECT a.document, a.tgl, a.id_jual as id, a.diskon_pembulatan as pemotongan, a.diskon_member, a.total, a.keterangan, b.nama, a.tipe_trx, sum(pd.jumlah - pd.retur) as totalQty, (a.total - a.diskon_member - a.diskon_pembulatan) AS grandtotal
			FROM penjualan a 
			INNER JOIN penjualan_detail pd ON a.id_jual = pd.id_jual 
			LEFT JOIN master_member b ON a.id_member = b.id_member 
			WHERE a.lunas = 1 and a.id_member = ? AND a.tgl >= ? AND  ? >= a.tgl AND a.hapus = 0
			GROUP BY a.id_jual, b.id_member, b.nama";
			$bindParam = [$id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		return $exectutedQuery->result_array();
	}

	public function getPiutangMember($id, $tglAwal, $tglAkhir){
		$tglAwal = date('Y-m-d', strtotime($tglAwal)) . " 00:00:00";
		$tglAkhir = date('Y-m-d', strtotime($tglAkhir)). " 23:59:59";
		if($id == null || $id == ""){
			$sql = "SELECT a.document, a.tgl, a.id_jual as id, a.diskon_pembulatan as pemotongan, a.diskon_member, a.total, a.keterangan, b.nama, if(tipe_trx = 3, 'B2B', 'Cabang') as type, sum(pd.jumlah - pd.retur) as totalQty, (a.total - a.diskon_member - a.diskon_pembulatan) AS grandtotal
			FROM penjualan a 
			INNER JOIN penjualan_detail pd ON a.id_jual = pd.id_jual 
			LEFT JOIN master_member b ON a.id_member = b.id_member 
			WHERE a.lunas = 0 and a.tgl >= ? AND  ? >= a.tgl AND a.hapus = 0 AND (a.tipe_trx = 3 OR a.tipe_trx = 4) 
			GROUP BY a.id_jual, b.id_member, b.nama";
			$bindParam = [$tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		else {
			$sql = "SELECT a.document, a.tgl, a.id_jual as id, a.diskon_pembulatan as pemotongan, a.diskon_member, a.total, a.keterangan, b.nama, if(tipe_trx = 3, 'B2B', 'Cabang') as type, sum(pd.jumlah - pd.retur) as totalQty, (a.total - a.diskon_member - a.diskon_pembulatan) AS grandtotal
			FROM penjualan a 
			INNER JOIN penjualan_detail pd ON a.id_jual = pd.id_jual 
			LEFT JOIN master_member b ON a.id_member = b.id_member 
			WHERE a.lunas = 0 and a.id_member = ? AND a.tgl >= ? AND  ? >= a.tgl AND a.hapus = 0 AND (a.tipe_trx = 3 OR a.tipe_trx = 4)
			GROUP BY a.id_jual, b.id_member, b.nama";
			$bindParam = [$id, $tglAwal, $tglAkhir];
			$exectutedQuery = $this->db->query($sql, $bindParam);
		}
		return $exectutedQuery->result_array();
	}
}
