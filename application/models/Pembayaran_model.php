<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.26
 */
use App\Traits\SecurityFilter;
class Pembayaran_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function getPembayaran(){
		$sql = "SELECT * FROM master_pembayaran";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getPesanan(){
		$sql = "SELECT p.id_pesan AS id, p.tgl, p.tgl_pesan, m.nama AS member, p.lunas, p.dikirim, pe.nama AS pegawai FROM pesanan p LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function get($id)
	{
		$sql = "SELECT * FROM master_pembayaran WHERE id_masterbayar = ?";
		$bindParam = [$id];
		return $this->db->query($sql, $bindParam)->row_array();
	}

	public function create($nama){
		$this->db->trans_begin();
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		
		$sql = "INSERT INTO master_pembayaran (nama) VALUES (?)";
		$bindParam = [$nama];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

    	if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
            	return ['status' => 'error', 'description' => 'Gagal membuat master pembayaran'];
            } else {
            	return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil membuat master pembayaran'];
        }
		

		// $sql = "INSERT INTO master_pembayaran(nama) VALUES (?)";
		// $bindParam= [$nama];
		// $this->db->query($sql, $bindParam);
	}

	public function edit($id, $nama){
		$cekError = false;
		$this->db->trans_begin();
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$id = $this->security->xss_clean($this->sanitize_input($id));

		$sql = "UPDATE master_pembayaran SET nama = ? WHERE id_masterbayar = ?";
		$bindParam= [$nama, $id];
		$this->db->query($sql, $bindParam);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

		if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
            	return ['status' => 'error', 'description' => 'Gagal mengubah master pembayaran'];
            } else {
            	return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil mengubah master pembayaran'];
        }
	}
}
