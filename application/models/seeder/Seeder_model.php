<?php

/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 04/04/2017
 * Time: 22:37
 */
class Seeder_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->dbforge();
    }

    public function seed()
    {
        //$this->dbforge->drop_database('gereja');
        $this->userTableSeeder();
    }

    protected function userTableSeeder()
    {
        $data = [
            ['username' => 'Owner', 'password' => password_hash('owner', PASSWORD_DEFAULT), 'nama' => 'Owner', 'jabatan' => 'Owner'],
            ['username' => 'Kasir', 'password' => password_hash('kasir', PASSWORD_DEFAULT), 'nama' => 'Kasir', 'jabatan' => 'Kasir'],
            ['username' => 'Dapur', 'password' => password_hash('dapur', PASSWORD_DEFAULT), 'nama' => 'Dapur', 'jabatan' => 'Dapur']
        ];
        $this->db->insert_batch('master_pegawai', $data);
    }
}
