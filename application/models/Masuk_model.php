<?php
use App\Traits\SecurityFilter;
class Masuk_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

    public function generateCode($date, $code){
        $period = date('ym', strtotime($date));
        //INV/M/1801/001
        //S201S1801001
        $sql = "SELECT max(document) as document FROM masuk where SUBSTR(document,5,6) = ? limit 1";
        $bindParam = [$code."/".$period];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $lastDoc = $exectutedQuery->row_array();
        if($lastDoc['document'] != null){
            $lastNum = substr($lastDoc['document'],11);
            $lastPeriod = substr($lastDoc['document'],0,10);
            $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
            $firstOfLastNum = $lastNum[0];
            if(in_array($firstOfLastNum,$alphabet)){
                $getTwoLastNum = substr($lastNum,1);
                foreach($alphabet as $key => $alpha){
                    if($firstOfLastNum == $alpha){
                        if($getTwoLastNum < 99){
                            $number = $alpha. str_pad( (int)($getTwoLastNum) + 1, 2,'0',STR_PAD_LEFT);
                        } else {
                            $nextAlpha = $alphabet[$key+1];
                            $number = $nextAlpha.'01';
                        }
                        break;
                    }
                }
                $document = $lastPeriod.'/'.$number;

            } else {
                $document = $lastPeriod.'/'.str_pad( (int)($lastNum) + 1, 3,'0',STR_PAD_LEFT);
            }
        } else {
            $document = 'INV/'. $code . '/' . $period . '/001';
        }
        return $document;
    }

	public function create($idPegawai, $idSupplier, $tgl, $detail, $total, $pemotongan){
		$cekError = false;
		$idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idSupplier = $this->security->xss_clean($this->sanitize_input($idSupplier));
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
		$totalItem = 0;
		$sql = "SELECT id_masuk FROM masuk ORDER BY id_masuk DESC LIMIT 1";
		$executedQuery = $this->db->query($sql);
		$id = $executedQuery->row_array();
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        if($id['id_masuk'] == null){
        	$idMasuk = 0;
        }
        $idMasuk = ($id['id_masuk']*1) + 1;
        $document = $this->generateCode($tanggal, 'M');
        
		$sql = "INSERT INTO masuk(id_masuk, document, tgl, lunas, id_sup, id_pegawai, total, pemotongan)
                VALUES(?,?,?,0,?,?,?,?)";
        $bindParam= [$idMasuk, $document, $datetime, $idSupplier, $idPegawai, $total, $pemotongan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($detail != null){
            // Insert Barang Masuk Detail
            $sqlDetail = "INSERT INTO masuk_detail(id_masuk, id_kue, jumlah, harga_jual, harga_beli) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?,?,?)";
                else
                    $sqlDetail .= "(?,?,?,?,?),";

                $idKue = $this->security->xss_clean($this->sanitize_input($detail[$i]['idKue']));
                $hargaJual = $this->security->xss_clean($this->sanitize_input($detail[$i]['hargaJual']));
                $hargaBeli = $this->security->xss_clean($this->sanitize_input($detail[$i]['hargaBeli']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $paramBarang[] = $idMasuk;
                $paramBarang[] = $idKue;
                $paramBarang[] = $jumlah;
                $paramBarang[] = $hargaJual;
                $paramBarang[] = $hargaBeli;
                
                // Mengambil stok terakhir
                $sqlStok = "SELECT stok FROM master_kue WHERE id_kue = ?";
                $totalItem = $totalItem + $jumlah*1;
                $param = [$idKue];
                $exectutedQuery = $this->db->query($sqlStok, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                // Update Stock
                $stok = $result['stok'] + $jumlah;
                $sqlUpdateStok = "UPDATE master_kue SET stok = ?, harga = ? WHERE id_kue = ?";
                $paramUpdate = [$stok, $hargaJual, $idKue];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }

                // Cek apakah kue yang diinput sudah join sama supplier atau belum
                $sqlGet = "SELECT * FROM kue_supplier WHERE id_kue = ? and id_sup = ?";
                $param = [$idKue, $idSupplier];
                $exectutedQuery = $this->db->query($sqlGet, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                // Update relasi supplier dan kue
                if(count($result) < 1){
                    $sql = "INSERT INTO kue_supplier(id_kue, id_sup)
                    VALUES(?,?)";
                    $bindParam = [$idKue, $idSupplier];
                    $this->db->query($sql, $bindParam);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                }
                
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        $sql = "UPDATE masuk SET jumlah = ? WHERE id_masuk = ?";
        $param = [$totalItem, $idMasuk];
        $this->db->query($sql, $param);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        if($cekError === true){
            $this->db->trans_rollback();
           if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan barang masuk'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => "Berhasil menyimpan barang masuk"];
        }
    }
    
    public function update($idMasuk, $idPegawai, $idSupplier, $tgl, $detail, $total, $pemotongan){
		$cekError = false;
		$idMasuk = $this->security->xss_clean($this->sanitize_input($idMasuk));
		$idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idSupplier = $this->security->xss_clean($this->sanitize_input($idSupplier));
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
        $totalItem = 0;
        
        // Mengambil data Masuk Detail
        $sql = "SELECT * FROM masuk_detail where id_masuk = ?";
        $bindParam = [$idMasuk];
        $result = $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $result->result_array();

        // Mengurangi stok master
        for($i = 0; $i<count($result); $i++){
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok - (?-?) WHERE id_kue = ?";
            $paramUpdate = [(int)$result[$i]['jumlah'], (int)$result[$i]['retur'], $result[$i]['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        // Delete Masuk Detail
        $sqlDelete = "DELETE FROM masuk_detail where id_masuk = ?";
        $paramDelete = [$idMasuk];
        $this->db->query($sqlDelete, $paramDelete);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        
        if($detail != null){
            // Insert Barang Masuk Detail
            $sqlDetail = "INSERT INTO masuk_detail(id_masuk, id_kue, jumlah, harga_jual, harga_beli) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?,?,?)";
                else
                    $sqlDetail .= "(?,?,?,?,?),";

                $idKue = $this->security->xss_clean($this->sanitize_input($detail[$i]['idKue']));
                $hargaJual = $this->security->xss_clean($this->sanitize_input($detail[$i]['hargaJual']));
                $hargaBeli = $this->security->xss_clean($this->sanitize_input($detail[$i]['hargaBeli']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $paramBarang[] = $idMasuk;
                $paramBarang[] = $idKue;
                $paramBarang[] = $jumlah;
                $paramBarang[] = $hargaJual;
                $paramBarang[] = $hargaBeli;
                
                // Mengambil stok terakhir
                $sqlStok = "SELECT stok FROM master_kue WHERE id_kue = ?";
                $totalItem = $totalItem + $jumlah*1;
                $param = [$idKue];
                $exectutedQuery = $this->db->query($sqlStok, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                // Update Stock
                $stok = (int)$result['stok'] + (int)$jumlah;
                $sqlUpdateStok = "UPDATE master_kue SET stok = ?, harga = ? WHERE id_kue = ?";
                $paramUpdate = [$stok, $hargaJual, $idKue];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }

                // Cek apakah kue yang diinput sudah join sama supplier atau belum
                $sqlGet = "SELECT * FROM kue_supplier WHERE id_kue = ? and id_sup = ?";
                $param = [$idKue, $idSupplier];
                $exectutedQuery = $this->db->query($sqlGet, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                // Update relasi supplier dan kue
                if(count($result) < 1){
                    $sql = "INSERT INTO kue_supplier(id_kue, id_sup)
                    VALUES(?,?)";
                    $bindParam = [$idKue, $idSupplier];
                    $this->db->query($sql, $bindParam);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                }
                
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        // Update DB Barang Masuk
        $sql = "UPDATE masuk SET tgl = ?, total = ?, pemotongan = ?, id_sup = ?, jumlah = ? WHERE id_masuk = ?";
        $bindParam = [$datetime, $total, $pemotongan, $idSupplier, $totalItem, $idMasuk];
		$this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($cekError === true){
            $this->db->trans_rollback();
           if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal mengubah barang masuk'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => "Berhasil mengubah barang masuk"];
        }
	}

	public function create_payment($tgl, $idPegawai, $idMasuk, $pemotongan, $total, $grandtotal, $kembalian, $detail){
		$cekError = false;
		$idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
		$idMasuk = $this->security->xss_clean($this->sanitize_input($idMasuk));
		$pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
		$total = $this->security->xss_clean($this->sanitize_input($total));
		$grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
		$kembalian = $this->security->xss_clean($this->sanitize_input($kembalian));
		$time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;

		$sql = "UPDATE masuk SET lunas = 1, total = ?, pemotongan = ?, kembalian = ? WHERE id_masuk = ?";
        $bindParam= [$total, $pemotongan, $kembalian, $idMasuk];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($detail != null){
            $sqlDetail = "INSERT INTO masuk_pembayaran(id_masuk, id_tipe_bayar, jumlah, tgl, id_pegawai, keterangan) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?,?,?,?)";
                else
                    $sqlDetail .= "(?,?,?,?,?,?),";

                $idPembayaran = $this->security->xss_clean($this->sanitize_input($detail[$i]['idPembayaran']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $keterangan = $this->security->xss_clean($this->sanitize_input($detail[$i]['keterangan']));
                $paramBarang[] = $idMasuk;
                $paramBarang[] = $idPembayaran;
                $paramBarang[] = $jumlah;
                $paramBarang[] = $datetime;
                $paramBarang[] = $idPegawai;
                $paramBarang[] = $keterangan;
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
           if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan pembayaran barang masuk'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => "Berhasil menyimpan pembayaran barang masuk"];
        }
	}

	public function getBarangMasuk($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));

        $sql = "SELECT p.id_masuk, p.document, p.id_sup, p.tgl, p.lunas, p.jumlah, p.id_pegawai, m.nama AS supplier, pe.nama AS pegawai, p.pemotongan
        FROM masuk p
        LEFT JOIN master_supplier m ON m.id_sup = p.id_sup
        INNER JOIN master_pegawai pe ON pe.id_pegawai = p.id_pegawai
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail masuk
        $sql = "SELECT p.id_masuk, p.id_kue, p.jumlah, p.retur, p.id_pegawai, mk.nama_kue, mp.nama AS pegawai, p.harga_beli, p.harga_jual, mk.barcode_kue
        FROM masuk_detail p INNER JOIN master_kue mk ON mk.id_kue = p.id_kue
        LEFT JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['detail'] = $result;

        //Mengambil Detail Pembayaran
        $sql = "SELECT p.jumlah, j.nama, p.tgl, p.keterangan, p.id_tipe_bayar
        FROM masuk_pembayaran p
        INNER JOIN master_pembayaran j ON j.id_masterbayar = p.id_tipe_bayar
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql,$bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['payment'] = $result;

        return $hasil;
    }

    public function getAll()
    {
    	$sql = "SELECT b.id_masuk, b.document, b.tgl, b.lunas, b.total, b.jumlah, p.nama As pegawai, s.nama AS supplier
    	FROM masuk b
    	INNER JOIN master_pegawai p ON b.id_pegawai = p.id_pegawai
    	INNER JOIN master_supplier s ON s.id_sup = b.id_sup ORDER BY b.id_masuk desc";
		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
    }

    public function searchHistory($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));
        $sql = "SELECT b.id_masuk
                FROM masuk b
                WHERE b.id_sup = ? ORDER BY b.id_masuk desc limit 1";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $sql = "SELECT p.id_kue, p.jumlah, p.harga_beli, p.harga_jual, k.nama_kue, k.barcode_kue
        FROM masuk_detail p
        INNER JOIN master_kue k ON k.id_kue = p.id_kue
        WHERE p.id_masuk = ?";
        $bindParam = [$result['id_masuk']];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        return $result;
    }

    public function getTagihan()
    {
        $sql = "SELECT a.document, a.tgl, a.id_masuk, b.nama as supplier, a.total, a.jumlah FROM masuk a INNER JOIN master_supplier b ON a.id_sup = b.id_sup WHERE a.lunas = 0";
        $executedQuery = $this->db->query($sql);
        $result = $executedQuery->result_array();

        return $result;
    }
}
