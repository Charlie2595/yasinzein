<?php

use App\Traits\SecurityFilter;

class User_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function login($username, $password){
		$username = $this->sanitize_input($username);
		$sql = "select u.id_pegawai AS id, u.nama, u.password, u.jabatan from master_pegawai u
                where u.username = ?";
		$bindParam = array($username);
		$executedQuery = $this->db->query($sql, $bindParam);
		$result = $executedQuery->row_array();

		if($result === NULL)
		{
			$user = NULL;
		}
		else
		{
			$password_hash = $result['password'];
			if(password_verify($password, $password_hash))
			{
				$user = $result;
			}
			else
			{
				$user = NULL;
			}
		}
		return $user;
	}

	public function getActiveUsers()
	{
		$sql = "SELECT u.id_pegawai, u.nama, u.jabatan, u.nohp, u.ktp
                FROM master_pegawai u";

		$exectutedQuery = $this->db->query($sql);
		$users = $exectutedQuery->result_array();

		return $users;
	}

	public function createUser($username,$jabatan,$nama,$password,$kode)
	{
		$username = $this->security->xss_clean($this->sanitize_input($username));
		$hashedPassword = password_hash($password, PASSWORD_DEFAULT);
		$jabatan = $this->security->xss_clean($this->sanitize_input($jabatan));
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$kode = $this->security->xss_clean($this->sanitize_input($kode));

		$sql = "INSERT INTO user(username, jabatan, nama, password, hapus, karyawan_kode) VALUES(?,?,?,?,0,?) ";
		$bindParam= [$username, $jabatan, $nama, $hashedPassword, $kode];
		$this->db->query($sql, $bindParam);
	}

	public function updateUser($id,$username, $providedPassword ,$kode, $fullUpdate = FALSE)
	{
		$kode = $this->security->xss_clean($this->sanitize_input($kode));
		$username = $this->security->xss_clean($this->sanitize_input($username));
		$id = $this->security->xss_clean($this->sanitize_input($id));

		if($fullUpdate)
		{
			$sql = "UPDATE user SET username = ?, password = ?, karyawan_kode = ? WHERE id = ?";
			$hashedPassword = password_hash($providedPassword, PASSWORD_DEFAULT);
			$bindParam = [$username, $hashedPassword, $kode,$id];
		}
		else {
			$sql = "UPDATE user SET username = ?, karyawan_kode = ? WHERE id = ?";
			$bindParam = [$username, $kode, $id];
		}
		$this->db->query($sql, $bindParam);
	}

	// public function getPengirim()
	// {
	// 	$sql = "SELECT id_pegawai, nama, jabatan, nohp, ktp
 //                FROM master_pegawai";

	// 	$exectutedQuery = $this->db->query($sql);
	// 	$users = $exectutedQuery->result_array();

	// 	return $users;
	// }
}
