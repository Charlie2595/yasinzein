<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.26
 */
use App\Traits\SecurityFilter;
class Penjualan_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function generateCode($date, $code){
		$period = date('ym', strtotime($date));
        //Inv/S/1801/001
        if($code == 'N'){
            $sql = "SELECT max(document) as document FROM tagihan where SUBSTR(document,5,6) = ? limit 1";
        } else {
            $sql = "SELECT max(document) as document FROM penjualan where SUBSTR(document,5,6) = ? limit 1";
        }
		$bindParam = [$code."/".$period];
		$exectutedQuery = $this->db->query($sql, $bindParam);
        $lastDoc = $exectutedQuery->row_array();

        $sql = "SELECT max(last_document) as document FROM penjualan where SUBSTR(last_document,5,6) = ? and tipe_trx = 1 limit 1";
		$bindParam = [$code."/".$period];
		$exectutedQuery = $this->db->query($sql, $bindParam);
        $lastDocPesanan = $exectutedQuery->row_array();
        if($lastDoc['document'] != null){
            if($lastDoc['document'] && $code == 'N'){
                $lastNum = substr($lastDoc['document'],11);
                $lastPeriod = substr($lastDoc['document'],0,10);
                $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                $firstOfLastNum = $lastNum[0];
                if(in_array($firstOfLastNum,$alphabet)){
                    $getTwoLastNum = substr($lastNum,1);
                    foreach($alphabet as $key => $alpha){
                        if($firstOfLastNum == $alpha){
                            if($getTwoLastNum < 99){
                                $number = $alpha. str_pad( (int)($getTwoLastNum) + 1, 2,'0',STR_PAD_LEFT);
                            } else {
                                $nextAlpha = $alphabet[$key+1];
                                $number = $nextAlpha.'01';
                            }
                            break;
                        }
                    }
                    $document = $lastPeriod.'/'.$number;
                } else {
                    
                    if($lastNum < 999){
                        $document = $lastPeriod.'/'.str_pad( (int)($lastNum) + 1, 3,'0',STR_PAD_LEFT);
                    } else {
                        $number = 'A01';
                        $document = $lastPeriod.'/'.$number;
                    }
                }
            } else if($lastDoc['document'] > $lastDocPesanan['document']){
                $lastNum = substr($lastDoc['document'],11);
                $lastPeriod = substr($lastDoc['document'],0,10);
                $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                $firstOfLastNum = $lastNum[0];
                if(in_array($firstOfLastNum,$alphabet)){
                    $getTwoLastNum = substr($lastNum,1);
                    foreach($alphabet as $key => $alpha){
                        if($firstOfLastNum == $alpha){
                            if($getTwoLastNum < 99){
                                $number = $alpha. str_pad( (int)($getTwoLastNum) + 1, 2,'0',STR_PAD_LEFT);
                            } else {
                                $nextAlpha = $alphabet[$key+1];
                                $number = $nextAlpha.'01';
                            }
                            break;
                        }
                    }
                    $document = $lastPeriod.'/'.$number;
                } else {
                    
                    if($lastNum < 999){
                        $document = $lastPeriod.'/'.str_pad( (int)($lastNum) + 1, 3,'0',STR_PAD_LEFT);
                    } else {
                        $number = 'A01';
                        $document = $lastPeriod.'/'.$number;
                    }
                }
            } else {
                $lastNum = substr($lastDocPesanan['document'],11);
                $lastPeriod = substr($lastDocPesanan['document'],0,10);
                $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                $firstOfLastNum = $lastNum[0];
                if(in_array($firstOfLastNum,$alphabet)){
                    $getTwoLastNum = substr($lastNum,1);
                    foreach($alphabet as $key => $alpha){
                        if($firstOfLastNum == $alpha){
                            if($getTwoLastNum < 99){
                                $number = $alpha. str_pad( (int)($getTwoLastNum) + 1, 2,'0',STR_PAD_LEFT);
                            } else {
                                $nextAlpha = $alphabet[$key+1];
                                $number = $nextAlpha.'01';
                            }
                            break;
                        }
                    }
                    $document = $lastPeriod.'/'.$number;
                } else {
                    
                    if($lastNum < 999){
                        $document = $lastPeriod.'/'.str_pad( (int)($lastNum) + 1, 3,'0',STR_PAD_LEFT);
                    } else {
                        $number = 'A01';
                        $document = $lastPeriod.'/'.$number;
                    }
                }
            }
        } else {
            if($lastDocPesanan['document'] != null and $code != 'N'){
                $lastNum = substr($lastDocPesanan['document'],11);
                $lastPeriod = substr($lastDocPesanan['document'],0,10);
                $alphabet = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
                $firstOfLastNum = $lastNum[0];
                if(in_array($firstOfLastNum,$alphabet)){
                    $getTwoLastNum = substr($lastNum,1);
                    foreach($alphabet as $key => $alpha){
                        if($firstOfLastNum == $alpha){
                            if($getTwoLastNum < 99){
                                $number = $alpha. str_pad( (int)($getTwoLastNum) + 1, 2,'0',STR_PAD_LEFT);
                            } else {
                                $nextAlpha = $alphabet[$key+1];
                                $number = $nextAlpha.'01';
                            }
                            break;
                        }
                    }
                    $document = $lastPeriod.'/'.$number;
                } else {
                    
                    if($lastNum < 999){
                        $document = $lastPeriod.'/'.str_pad( (int)($lastNum) + 1, 3,'0',STR_PAD_LEFT);
                    } else {
                        $number = 'A01';
                        $document = $lastPeriod.'/'.$number;
                    }
                }
            } else {
                $document = 'INV/'. $code . '/' . $period . '/001';
            }
        }
		return $document;
	}

	public function get($id){
		$id = $this->security->xss_clean($this->sanitize_input($id));

		$sql = "SELECT p.id_jual, p.document, m.nama AS member, pe.nama AS pegawai, p.diskon_pembulatan, p.diskon_member, p.lunas, p.dikirim, p.tgl_pesanan, p.tipe_trx, pk.nama AS pengirim, p.id_nota, p.total, (p.total - p.diskon_pembulatan - p.diskon_member) as grandtotal, p.tgl, p.id_member, p.tipe_trx, m.saldo, m.poin, m.use_poin, m.nohp, m.alamat_rumah, p.status, m.diskon_barang, p.kembalian, p.jam_pesanan, p.tgl_kirim, p.jam_kirim, p.alamat_kirim, p.tgl_terima, p.jam_terima, p.keterangan, p.jam_pesanan AS jam, p.id_pegawai_kirim
        FROM penjualan p
        LEFT JOIN master_member m ON m.id_member = p.id_member
        INNER JOIN master_pegawai pe ON pe.id_pegawai = p.id_pegawai
        LEFT JOIN master_pegawai pk ON pk.id_pegawai = p.id_pegawai_kirim
        WHERE p.id_jual = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail penjualan
        $sql = "SELECT p.id_jualdetail, mk.nama_kue, mk.id_kue, p.jumlah, p.harga, p.diskon, mk.barcode_kue, p.retur, mp.nama AS pegawai
        FROM penjualan_detail p INNER JOIN master_kue mk ON mk.id_kue = p.id_kue
        LEFT JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
        WHERE p.id_jual = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['detail'] = $result;

        //Mengambil Detail Pembayaran
        $sql = "SELECT p.total, j.nama, p.tgl, p.keterangan, p.id_tipe_bayar
        FROM pembayaran_detail p
        INNER JOIN master_pembayaran j ON j.id_masterbayar = p.id_tipe_bayar
        WHERE p.id_trx = ? and jenis_trx <> 4";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql,$bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['payment'] = $result;

		return $hasil;
    }
    
    public function getPenjualanPending(){
		$sql = "SELECT p.id_jual AS id, p.document, p.tgl, m.nama AS member, p.lunas, pe.nama AS pegawai, p.status, sum(pd.jumlah) - sum(pd.retur) as totalQty, p.total, (p.total - p.diskon_pembulatan) as grandtotal, p.keterangan, p.last_document FROM penjualan p LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN penjualan_detail pd on pd.id_jual = p.id_jual INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '1' and p.status = '0' and p.hapus = 0 group by p.id_jual, m.nama order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getPOS(){
		$sql = "SELECT p.id_jual AS id, p.document, p.tgl, m.nama AS member, p.lunas, pe.nama AS pegawai, p.status, sum(pd.jumlah) - sum(pd.retur) as totalQty, p.total, (p.total - p.diskon_pembulatan) as grandtotal, p.keterangan, p.last_document FROM penjualan p LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN penjualan_detail pd on pd.id_jual = p.id_jual INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '1' and p.hapus = 0 group by p.id_jual, m.nama order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getPesanan(){
		$sql = "SELECT p.id_jual AS id, p.tgl, p.document, p.id_pegawai_kirim, p.status, p.tgl_pesanan, m.nama AS member, p.lunas, p.dikirim, pe.nama AS pegawai, tgl_pesanan, jam_pesanan, p.tgl_terima, p.jam_terima, p.keterangan, sum(pd.jumlah) - sum(pd.retur) as totalQty, p.total FROM penjualan p LEFT JOIN penjualan_detail pd on pd.id_jual = p.id_jual LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '2' and p.hapus = 0 and p.status = 0 group by p.id_jual, m.nama order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
    }
    
    public function getHistoryPesanan(){
		$sql = "SELECT p.id_jual AS id, p.tgl, p.document, p.id_pegawai_kirim, p.status, p.tgl_pesanan, m.nama AS member, p.lunas, p.dikirim, pe.nama AS pegawai, tgl_pesanan, jam_pesanan, p.tgl_terima, p.jam_terima, p.keterangan, sum(pd.jumlah) - sum(pd.retur) as totalQty, p.total FROM penjualan p LEFT JOIN penjualan_detail pd on pd.id_jual = p.id_jual LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '2' and p.hapus = 0 and p.status = 1 group by p.id_jual, m.nama order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getb2b(){
		$sql = "SELECT p.id_jual AS id, p.tgl, p.document, p.id_nota, p.tgl_pesanan, m.nama AS member, p.lunas, p.dikirim, pe.nama AS pegawai FROM penjualan p LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '3' order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getCabang(){
		$sql = "SELECT p.id_jual AS id, p.tgl, p.document, p.tgl_pesanan, m.nama AS member, p.lunas, p.dikirim, pe.nama AS pegawai FROM penjualan p LEFT JOIN master_member m ON p.id_member = m.id_member INNER JOIN master_pegawai pe ON p.id_pegawai = pe.id_pegawai WHERE p.tipe_trx = '4' order by p.id_jual desc";

		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

    public function getNota(){
        $sql = "SELECT t.document, t.id_nota, t.diskon, t.tgl, t.lunas, p.nama AS pegawai, m.nama AS member, t.total
        FROM tagihan t
        LEFT JOIN master_member m ON t.id_member = m.id_member
        INNER JOIN master_pegawai p ON p.id_pegawai = t.id_pegawai";

        $exectutedQuery = $this->db->query($sql);
        $list = $exectutedQuery->result_array();

        return $list;
    }

    public function getDetailNota($id){
        $sql = "SELECT t.id_nota, t.tgl, t.lunas, p.nama AS pegawai, m.nama AS member, t.document, t.total, t.kembalian, t.diskon
        FROM tagihan t LEFT JOIN master_member m ON t.id_member = m.id_member INNER JOIN master_pegawai p ON p.id_pegawai = t.id_pegawai where t.id_nota = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql,$bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail penjualan
        $sql = "SELECT p.id_jual, p.tgl, p.total, p.diskon_pembulatan, p.document
        FROM penjualan p INNER JOIN tagihan t ON t.id_nota = p.id_nota WHERE p.id_nota = ?  order by p.id_jual desc";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $hasil = $exectutedQuery->result_array();
        $result['detail'] = $hasil;

        $sql = "SELECT p.total, j.nama, p.tgl, p.keterangan, p.id_tipe_bayar
        FROM pembayaran_detail p INNER JOIN tagihan t ON t.id_nota = p.id_trx INNER JOIN master_pembayaran j ON j.id_masterbayar = p.id_tipe_bayar WHERE t.id_nota = ? and p.jenis_trx = 4 order by p.id_pembayaran desc";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $hasil = $exectutedQuery->result_array();
        $result['payment'] = $hasil;

        return $result;
    }

    public function getRetur(){
        $sql = "SELECT p.id_jual, p.document, pe.tgl_retur as tgl, mp.nama AS pegawai, m.nama AS member, p.lunas, p.dikirim
        FROM penjualan p
        LEFT JOIN penjualan_detail pe ON pe.id_jual = p.id_jual
        INNER JOIN master_pegawai mp ON p.id_pegawai = mp.id_pegawai
        INNER JOIN master_member m ON m.id_member = p.id_member
        WHERE pe.retur > 0
        GROUP BY p.id_jual, p.document, m.nama, pe.tgl_retur";

        $exectutedQuery = $this->db->query($sql);
        $list = $exectutedQuery->result_array();

        return $list;
    }

    public function getNotaById($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));

        $sql = "SELECT t.id_nota, t.id_member, t.tgl, t.lunas, t.id_pegawai, m.nama AS member, pe.nama AS pegawai, m.saldo, m.poin, m.use_poin, t.diskon
        FROM tagihan t
        LEFT JOIN master_member m ON m.id_member = t.id_member
        INNER JOIN master_pegawai pe ON pe.id_pegawai = t.id_pegawai
        WHERE t.id_nota = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail penjualan
        $sql = "SELECT p.id_jual, p.tgl, p.total, p.diskon_pembulatan, p.diskon_member, p.document
        FROM penjualan p
        WHERE p.id_nota = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['detail'] = $result;

        return $hasil;
    }

	public function create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $type, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat)
	{
        $this->db->trans_begin();
		$idMember = $this->security->xss_clean($this->sanitize_input($idMember));
        $diskonMember = $this->security->xss_clean($this->sanitize_input($diskonMember));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
        $pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
        $kembalian = $this->security->xss_clean($this->sanitize_input($kembalian));
        $jamKirim = $this->security->xss_clean($this->sanitize_input($jamKirim));
        $alamat = $this->security->xss_clean($this->sanitize_input($alamat));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
        $tglKirim = date('Y-m-d', strtotime($tglKirim));
        $cekError = false;
        if($idMember == ""){
            $idMember = null;
        }
        if($alamat != null){
            $dikirim = 1;
        } else {
            $dikirim = 0;
        }

        if($type == "POS"){
			$document = $this->generateCode($tanggal, 'S');
            $sql = "INSERT INTO penjualan(document, tgl, id_member, diskon_member, id_pegawai, total, tipe_trx, diskon_pembulatan, kembalian, status)
                VALUES(?,?,?,?,?,?,?,?,?,0)";
            $bindParam= [$document, $datetime, $idMember, $diskonMember, $idPegawai, $total, "1", $pemotongan, $kembalian];
        }
        elseif($type == "Pesanan") {
			$document = $this->generateCode($tanggal, 'P');
            $sql = "INSERT INTO penjualan(document, tgl, id_member, diskon_member, id_pegawai, total, tipe_trx, tgl_pesanan, diskon_pembulatan, kembalian, status, jam_pesanan, tgl_kirim, jam_kirim, alamat_kirim, dikirim)
                VALUES(?,now(),?,?,?,?,?,?,?,?,0,?,?,?,?,?)";
            $bindParam = [$document, $idMember, $diskonMember, $idPegawai, $total, "2", $tanggal, $pemotongan, $kembalian, $jam, $tglKirim, $jamKirim, $alamat, $dikirim];
        }
        elseif($type == "B2B"){
			$document = $this->generateCode($tanggal, 'B');
        	$sql = "INSERT INTO penjualan(document, tgl, id_member, diskon_member, id_pegawai, total, tipe_trx, diskon_pembulatan, kembalian, status)
                VALUES(?,?,?,?,?,?,?,?,?,0)";
            $bindParam = [$document, $datetime, $idMember, $diskonMember, $idPegawai, $total, "3", $pemotongan, $kembalian];
        }
        elseif($type == "Cabang"){
			$document = $this->generateCode($tanggal, 'C');
        	$sql = "INSERT INTO penjualan(document, tgl, id_member, diskon_member, id_pegawai, total, tipe_trx, diskon_pembulatan, kembalian, status)
                VALUES(?,?,?,?,?,?,?,?,?,0)";
            $bindParam = [$document, $datetime, $idMember, $diskonMember, $idPegawai, $total, "4", $pemotongan, $kembalian];
        }
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }
        $sqlGetId = "SELECT id_jual FROM penjualan ORDER BY id_jual DESC LIMIT 1";
        $executedQuery = $this->db->query($sqlGetId);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $id = $executedQuery->row_array();

        if($detail != null){
            $sqlDetail = "INSERT INTO penjualan_detail(id_jual, id_kue, jumlah, harga,diskon) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?,?,?)";
                else
                    $sqlDetail .= "(?,?,?,?,?),";

                $idBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['idBarang']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $harga = $this->security->xss_clean($this->sanitize_input($detail[$i]['harga']));
                $diskonBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['diskonBarang']));
                $paramBarang[] = $id['id_jual'];
                $paramBarang[] = $idBarang;
                $paramBarang[] = $jumlah;
                $paramBarang[] = $harga;
                $paramBarang[] = $diskonBarang;

                if($type != "Pesanan"){
                    $sqlStok = "SELECT stok FROM master_kue WHERE id_kue = ?";
                    $param = [$idBarang];
                    $exectutedQuery = $this->db->query($sqlStok, $param);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                    $result = $exectutedQuery->row_array();

                    $stok = $result['stok'] - $jumlah;

                    $sqlUpdateStok = "UPDATE master_kue SET stok = ? WHERE id_kue = ?";
                    $paramUpdate = [$stok, $idBarang];
                    $this->db->query($sqlUpdateStok, $paramUpdate);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                }
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($idMember != null || $idMember != 0){
            $sql = "update master_member set tgl_transaksi = ? where id_member = ?";
            $bindParam = [$tanggal, $idMember];
            $this->db->query($sql, $bindParam);

            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($detailPembayaran != NULL){
            if($type == "POS"){
                $status = 1;
            }
            elseif($type == "Pesanan"){
                $status = 0;
            }
        	$this->create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detailPembayaran, $id['id_jual'], $type, "", $kembalian, $status);
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan '. $type];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $id['id_jual']];
        }
	}

	public function create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detail, $idPenjualan, $type, $deposit, $kembalian, $status){
		$diskonMember = $this->security->xss_clean($this->sanitize_input($diskonMember));
		$pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
		$grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
		$total = $this->security->xss_clean($this->sanitize_input($total));
		$idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
		$totalPembayaran = 0;
		$jumlahDeposit = 0;
		$jumlahPoin = 0;
        $cekError = false;

		$this->db->select('m.id_member');
		$this->db->from('master_member as `m`');
		$this->db->join('penjualan as `p`', 'p.id_member = m.id_member');
		$this->db->where('p.id_jual', $idPenjualan);
		$result = $this->db->get()->row_array();

		if($type == "POS"){
			$trx = 1;
		}
		elseif($type == "Pesanan") {
			$trx = 2;
		}
		elseif($type == "cabang"){
			$trx = 3;
		}
        $this->db->trans_begin();

        $this->db->query("DELETE FROM pembayaran_detail WHERE id_trx = " .$idPenjualan);
        $error = $this->db->error();
        if($error['message']){
		    $messageError = $error['message'];
            $cekError = true;
        }

		if($detail != NULL){
			for ($i=0; $i < count($detail); $i++)
			{
				$idBayar = $this->security->xss_clean($this->sanitize_input($detail[$i]['idPembayaran']));
				$jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
				$keterangan = $this->security->xss_clean($this->sanitize_input($detail[$i]['keterangan']));

				$data = array(
					'jenis_trx'     => $trx,
					'id_trx'        => $idPenjualan,
					'total'         => $jumlah,
					'id_tipe_bayar' => $idBayar,
					'keterangan'    => $keterangan,
					'tgl'           => date('Y-m-d H:i:s'),
					'id_pegawai'    => $_SESSION['idLogin']
				);

				$this->db->insert('pembayaran_detail', $data);
                if($this->db->affected_rows() < 0){
                    $messageError = $error['message'];
                    $cekError = true;
                }

				$totalPembayaran += $jumlah;

				if($idBayar == 6){
					$jumlahDeposit = $jumlah;
                    $this->db->query("UPDATE master_member SET saldo = saldo-".$jumlah." WHERE id_member = ". $result['id_member']);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
				}else if($idBayar == 5){
					$jumlahPoin = $jumlah;
					$jumlah = $jumlah/1000;
                    $this->db->query("UPDATE master_member SET poin = poin-".$jumlah." WHERE id_member = ". $result['id_member']);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
				}
			}
            $lunas = 0;
			if($totalPembayaran >= $grandtotal*1){
                $lunas = 1;
				if($deposit == "SIMPAN"){
					$kembalian = 0;
				} else {
                    $kembalian = $totalPembayaran - $grandtotal;
                }
                $this->db->query("UPDATE penjualan SET diskon_pembulatan = ".$pemotongan.", diskon_member = ". $diskonMember .", lunas = '".$lunas."', total = ".$total.", kembalian = ".$kembalian.", status = '".$status."' WHERE id_jual = ". $idPenjualan);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
			}
			else{
                $this->db->query("UPDATE penjualan SET diskon_pembulatan = ".$pemotongan.", diskon_member = ". $diskonMember .", lunas = '".$lunas."', total = ".$total.", kembalian = ".$kembalian." WHERE id_jual = ". $idPenjualan);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
			}
			if($deposit == "SIMPAN"){
				$result = $this->db->query("SELECT m.id_member, m.saldo, m.poin FROM master_member m INNER JOIN penjualan p ON p.id_member = m.id_member WHERE p.id_jual = ".$idPenjualan);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                $cekError = true;
                } else {
                    $result = $result->row_array();
                }
				$jumlahDeposit = ($totalPembayaran-$jumlahDeposit) - $grandtotal;
				if($jumlahDeposit < 0){
					$jumlahDeposit = 0;
				}
				$jumlahPoin = ($totalPembayaran - $jumlahPoin) / 1000;
				if($jumlahPoin < 0){
					$jumlahPoin = 0;
				}

                if($status == 1){
                    $lunas = 1;
                }

                $this->db->query("UPDATE penjualan SET diskon_pembulatan = ".$pemotongan.", diskon_member = ". $diskonMember .", lunas = '".$lunas."', total = ".$total.", kembalian = ".$kembalian." WHERE id_jual = ". $idPenjualan);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                $cekError = true;
                }

                $this->db->query("UPDATE master_member SET saldo = ".($result['saldo'] + $jumlahDeposit). ", poin = ". ($result['poin'] + $jumlahPoin). " WHERE id_member = ".$result['id_member']);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                $cekError = true;
                }
			}
		}

        if($cekError === true){
			$this->db->trans_rollback();
			if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

		} else {
			$this->db->trans_commit();
			return ['status' => 'success', 'description' => $idPenjualan];
		}
    }
    
    public function selesai($id)
    {
        $tanggal = date('Y-m-d');
        $document = $this->generateCode($tanggal, 'S');

        //Mengambil detail penjualan
        $sql = "SELECT p.document, p.keterangan
        FROM penjualan p
        WHERE p.id_jual = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $this->db->query("UPDATE penjualan SET status = 1, document = '".$document."', tgl = now(), tipe_trx = 1, last_document = '".$result['document']."' WHERE id_jual = ".$id);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($cekError === true){
			$this->db->trans_rollback();
			if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal mengubah pesanan'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
		} else {
			$this->db->trans_commit();
			return ['status' => 'success', 'description' => 'Pesanan berhasil diubah'];
		}
    }

    public function update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $type, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat){
        $this->db->trans_begin();
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $diskonMember = $this->security->xss_clean($this->sanitize_input($diskonMember));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $tglKirim = date('Y-m-d', strtotime($tglKirim));
        $grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
        $pemotongan = $this->security->xss_clean($this->sanitize_input($pemotongan));
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $jam = $this->security->xss_clean($this->sanitize_input($jam));
        $jamKirim = $this->security->xss_clean($this->sanitize_input($jamKirim));
        $alamat = $this->security->xss_clean($this->sanitize_input($alamat));
        $kembalian = 0;
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
        if($alamat != null){
            $dikirim = 1;
        } else {
            $dikirim = 0;
        }

        if($type == "POS"){
            $sql = "UPDATE penjualan SET total = ?, lunas = ?, diskon_pembulatan = ? where id_jual = ?";
            $bindParam= [$total, 0, $pemotongan, $idPenjualan];
        }
        elseif($type == "Pesanan") {
            $tglPesanan =  date('Y-m-d', strtotime($tgl));
            $sql = "UPDATE penjualan SET total = ?, lunas = ?, jam_pesanan = ?, tgl_pesanan = ?, diskon_pembulatan = ?, status = ?, tgl_kirim = ?, jam_kirim = ?, alamat_kirim = ?, dikirim = ? where id_jual = ?";
            $bindParam= [$total, 0, $jam, $tglPesanan, $pemotongan, 0, $tglKirim, $jamKirim, $alamat, $dikirim, $idPenjualan];
        }
        elseif($type == "B2B"){
            $sql = "UPDATE penjualan SET total = ?, lunas = ?, tgl = ?, diskon_pembulatan = ? where id_jual = ?";
            $bindParam= [$total, 0, $datetime, $pemotongan, $idPenjualan];
        }
        elseif($type == "Cabang"){
            $sql = "UPDATE penjualan SET total = ?, lunas = ?, tgl_pesanan = ? where id_jual = ?";
            $bindParam= [$total, 0, $tgl, $idPenjualan];
        }
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($type != "Pesanan"){
            $sql = "SELECT * FROM penjualan_detail where id_jual = ?";
            $bindParam = [$idPenjualan];
            $result = $this->db->query($sql, $bindParam);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
            $result = $result->result_array();


            for($i = 0; $i<count($result); $i++){
                $sqlUpdateStok = "UPDATE master_kue SET stok = stok + ? WHERE id_kue = ?";
                $paramUpdate = [$result[$i]['jumlah'], $result[$i]['id_kue']];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        $sqlDelete = "DELETE FROM penjualan_detail where id_jual = ?";
        $paramDelete = [$idPenjualan];
        $this->db->query($sqlDelete, $paramDelete);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($detail != null){
            $sqlDetail = "INSERT INTO penjualan_detail(id_jual, id_kue, jumlah, harga, id_pegawai, diskon) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?,?,?,?)";
                else
                    $sqlDetail .= "(?,?,?,?,?,?),";

                $idBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['idBarang']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $harga = $this->security->xss_clean($this->sanitize_input($detail[$i]['harga']));
                $diskonBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['diskonBarang']));
                $paramBarang[] = $idPenjualan;
                $paramBarang[] = $idBarang;
                $paramBarang[] = $jumlah;
                $paramBarang[] = $harga;
                $paramBarang[] = $idPegawai;
                $paramBarang[] = $diskonBarang;

                if($type != "Pesanan"){
                    $sqlStok = "SELECT stok FROM master_kue WHERE id_kue = ?";
                    $param = [$idBarang];
                    $exectutedQuery = $this->db->query($sqlStok, $param);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                    $result = $exectutedQuery->row_array();

                    $stok = $result['stok'] - $jumlah;

                    $sqlUpdateStok = "UPDATE master_kue SET stok = ? WHERE id_kue = ?";
                    $paramUpdate = [$stok, $idBarang];
                    $this->db->query($sqlUpdateStok, $paramUpdate);
                    $error = $this->db->error();
                    if($error['message']){
                        $messageError = $error['message'];
                        $cekError = true;
                    }
                }
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($detailPembayaran != NULL){
            if($type == "POS"){
                $status = 1;
            }
            elseif($type == "Pesanan"){
                $status = 0;
            }
            $this->create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detailPembayaran, $idPenjualan, $type, "", $kembalian, $status);
        }
        
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal mengubah ' . $type];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function searchHistory($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));
        $sql = "SELECT *
                FROM penjualan p
                WHERE p.id_member = ? AND status = 1 ORDER BY id_jual desc limit 10";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        return $result;
    }

    public function searchDetailHistory($id){
        $sql = "SELECT p.id_kue, p.jumlah, p.harga, k.nama_kue, k.barcode_kue, k.diskon
        FROM penjualan_detail p
        INNER JOIN master_kue k ON k.id_kue = p.id_kue
        WHERE p.id_jual = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        return $result;
    }

    public function searchPenjualan($id,$type){
        $id = $this->security->xss_clean($this->sanitize_input($id));
        if($type == 'b2b'){
            $sql = "SELECT p.id_jual, p.tgl, p.document, p.total, mp.nama AS pegawai, p.id_pegawai, p.diskon_member, p.diskon_pembulatan
                FROM penjualan p
                INNER JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
                WHERE p.id_member = ? AND p.id_nota IS NULL AND p.tipe_trx = 3";
        } elseif ($type == 'cabang') {
            $sql = "SELECT p.id_jual, p.tgl, p.document, p.total, mp.nama AS pegawai, p.id_pegawai, p.diskon_member, p.diskon_pembulatan
                FROM penjualan p
                INNER JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
                WHERE p.id_member = ? AND p.tipe_trx = 4 AND lunas = 0";
        } elseif ($type == 'cabangRetur') {
            $sql = "SELECT p.id_jual, p.tgl, p.document, p.total, mp.nama AS pegawai, p.id_pegawai, p.diskon_member, p.diskon_pembulatan
                FROM penjualan p
                INNER JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
                WHERE p.id_member = ? AND p.tipe_trx = 4 AND lunas = 0";
        }
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();

        return $result;
    }

    public function detailPenjualan($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));
        $sql = "SELECT * FROM penjualan p WHERE p.id_jual = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $sql = "SELECT k.nama_kue, k.barcode_kue, p.id_kue, p.jumlah, p.harga, p.retur, p.id_jualdetail
            FROM penjualan_detail p
            INNER JOIN penjualan pe ON pe.id_jual = p.id_jual
            INNER JOIN master_kue k ON k.id_kue = p.id_kue
            WHERE p.id_jual = ? AND pe.status = 0";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result['detail'] = $exectutedQuery->result_array();

        return $result;
    }

    public function createNota($idMember, $tgl, $idPegawai, $total, $detail, $detailPembayaran, $kembalian, $diskon)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idMember = $this->security->xss_clean($this->sanitize_input($idMember));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $diskon = $this->security->xss_clean($this->sanitize_input($diskon));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $document = $this->generateCode($tanggal, 'N');
        $datetime = $tanggal . " " .$time;
        $penjualan = [];

        $sql = "INSERT INTO tagihan(document,id_member, tgl, lunas, id_pegawai, diskon, total)
                VALUES(?,?,?,?,?,?,?)";
        $bindParam= [$document, $idMember, $datetime, 0, $idPegawai, $diskon, $total];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sqlGetId = "SELECT id_nota FROM tagihan ORDER BY id_nota DESC LIMIT 1";
        $executedQuery = $this->db->query($sqlGetId);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $id = $executedQuery->row_array();
        
        if($detail != null){
            $sqlDetail = "UPDATE penjualan SET id_nota = ? WHERE id_jual = ?";
            for ($i=0; $i < count($detail); $i++)
            {
                $idPenjualan = $this->security->xss_clean($this->sanitize_input($detail[$i]['idPenjualan']));
                $paramBarang = [$id['id_nota'], $idPenjualan];
                $penjualan[] = $idPenjualan;
                $this->db->query($sqlDetail, $paramBarang);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        if($detailPembayaran != NULL){
        	$this->create_payment_nota($id['id_nota'], $grandtotal, $penjualan, $detailPembayaran, $kembalian);
        }

        if($cekError === true){
            $this->db->trans_rollback();
           if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function createRetur($idMember, $tgl, $idPegawai, $detail, $idPenjualan, $grandtotal)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idMember = $this->security->xss_clean($this->sanitize_input($idMember));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
        $time = date('H:i:s');
        $tanggal = date('Y-m-d', strtotime($tgl));
        $datetime = $tanggal . " " .$time;
        $sql = "UPDATE penjualan SET total = ? WHERE id_jual = ?";
        $bindParam = [$grandtotal, $idPenjualan];
        $this->db->query($sql,$bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        
        $total = 0;
        if($detail != null){
            $sqlDetail = "UPDATE penjualan_detail SET retur = (retur + ?), id_pegawai = ?, tgl_retur = ? WHERE id_jualdetail = ?";
            for ($i=0; $i < count($detail); $i++)
            {
                $idDetail = $this->security->xss_clean($this->sanitize_input($detail[$i]['idDetail']));
                $jumlahRetur = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlahRetur']));
                $paramBarang = [$jumlahRetur, $idPegawai, $datetime, $idDetail];
                $this->db->query($sqlDetail, $paramBarang);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }

                $sqlStok = "SELECT id_kue FROM penjualan_detail WHERE id_jualdetail = ?";
                $param = [$idDetail];
                $exectutedQuery = $this->db->query($sqlStok, $param);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                $result = $exectutedQuery->row_array();

                $sqlUpdateStok = "UPDATE master_kue SET stok = stok + ? WHERE id_kue = ?";
                $paramUpdate = [$jumlahRetur, $result['id_kue']];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function updateRetur($idPenjualan, $tgl, $idPegawai, $grandtotal, $total, $tipetrans, $detail)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $grandtotal = $this->security->xss_clean($this->sanitize_input($grandtotal));
        $sql = "UPDATE penjualan SET total = ? WHERE id_jual = ?";
        $bindParam = [$grandtotal, $idPenjualan];
        $this->db->query($sql,$bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "SELECT * FROM penjualan_detail where id_jual = ?";
        $bindParam = [$idPenjualan];
        $result = $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $result->result_array();

        for($i = 0; $i<count($result); $i++){
            // var_dump($result[$i]);die();
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok - ? WHERE id_kue = ?";
            $paramUpdate = [(int)$result[$i]['retur'], $result[$i]['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        $total = 0;
        if($detail != null){
            $sqlDetail = "UPDATE penjualan_detail SET retur = ?, id_pegawai = ? WHERE id_jualdetail = ?";
            for ($i=0; $i < count($detail); $i++)
            {
                $idDetail = $this->security->xss_clean($this->sanitize_input($detail[$i]['idDetail']));
                $idBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['idBarang']));
                $jumlahRetur = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlahRetur']));
                $paramBarang = [$jumlahRetur, $idPegawai, $idDetail];
                $this->db->query($sqlDetail, $paramBarang);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
                
                $sqlUpdateStok = "UPDATE master_kue SET stok = stok + ? WHERE id_kue = ?";
                $paramUpdate = [$jumlahRetur, $idBarang];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function create_payment_nota($idNota, $total, $idPenjualan, $detail, $kembalian, $diskon){
        $cekError = false;
        $total = $this->security->xss_clean($this->sanitize_input($total));
        $idNota = $this->security->xss_clean($this->sanitize_input($idNota));
        $kembalian = $this->security->xss_clean($this->sanitize_input($kembalian));
        $totalPembayaran = 0;
        $this->db->trans_begin();

        for ($i=0; $i < count($idPenjualan); $i++) {
            $sql = "UPDATE penjualan SET lunas = ?, status = ? WHERE id_jual = ?";
            $bindParam = [1, 1, $idPenjualan[$i]];

            $this->db->query($sql, $bindParam);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        $sqlDetail = "INSERT INTO pembayaran_detail(jenis_trx, id_trx, total, id_tipe_bayar, keterangan, tgl, id_pegawai) VALUES ";

        $param = [];
        for ($i=0; $i < count($detail); $i++)
        {
            if($i == count($detail) - 1)
                $sqlDetail .= "(?,?,?,?,?,?,?)";
            else
                $sqlDetail .= "(?,?,?,?,?,?,?),";

            $idBayar = $this->security->xss_clean($this->sanitize_input($detail[$i]['idPembayaran']));
            $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
            $keterangan = $this->security->xss_clean($this->sanitize_input($detail[$i]['keterangan']));
            $param[] = 4;
            $param[] = $idNota;
            $param[] = $jumlah;
            $param[] = $idBayar;
            $param[] = $keterangan;
            $param[] = date('Y-m-d H:i:s');
            $param[] = $_SESSION['idLogin'];
            $totalPembayaran += $jumlah;

            if($idBayar == 6){
                $sql = "UPDATE master_member SET saldo = saldo - ? WHERE id_member = ?";
                $bindParam = [$jumlah, $result['id_member']];
                $this->db->query($sql, $bindParam);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                    $cekError = true;
                }
            }
        }
        $this->db->query($sqlDetail, $param);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "UPDATE tagihan SET lunas = ?, kembalian = ?, total = ?, diskon = ? WHERE id_nota = ?";
        $bindParam = [1, $kembalian, $total, $diskon, $idNota];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function create_pengiriman($idPenjualan, $tgl, $jam, $pegawai, $nama, $nohp, $alamat, $typeBayar, $typetrans)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($pegawai));
        $nama = $this->security->xss_clean($this->sanitize_input($nama));
        $nohp = $this->security->xss_clean($this->sanitize_input($nohp));
        $alamat = $this->security->xss_clean($this->sanitize_input($alamat));
        $typeBayar = $this->security->xss_clean($this->sanitize_input($typeBayar));
        $jam = $this->security->xss_clean($this->sanitize_input($jam));
        $tanggal = date('Y-m-d', strtotime($tgl));

        $sql = "UPDATE penjualan SET dikirim = ?, id_pegawai_kirim = ?, tgl_kirim = ?, jam_kirim = ?, jenis_bayar = ?, alamat_kirim = ? WHERE id_jual = ?";
        $bindParam= [1, $idPegawai, $tanggal, $jam, $typeBayar, $alamat, $idPenjualan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "SELECT * FROM penjualan_detail WHERE id_jual = ?";
        $param = [$idPenjualan];
        $exectutedQuery = $this->db->query($sqlStok, $param);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $exectutedQuery->result_array();

        foreach($result as $value){
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok - ? WHERE id_kue = ?";
            $paramUpdate = [$value['jumlah'], $value['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }
    public function create_receive($idPenjualan, $tgl, $jam, $keterangan, $typetrans)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));
        $jam = $this->security->xss_clean($this->sanitize_input($jam));
        $tanggal = date('Y-m-d', strtotime($tgl));

        $sql = "UPDATE penjualan SET tgl_terima = ?, jam_terima = ?, keterangan = ? WHERE id_jual = ?";
        $bindParam= [$tanggal, $jam, $keterangan, $idPenjualan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "SELECT * FROM penjualan_detail WHERE id_jual = ?";
        $param = [$idPenjualan];
        $exectutedQuery = $this->db->query($sqlStok, $param);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $exectutedQuery->result_array();

        foreach($result as $value){
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok - ? WHERE id_kue = ?";
            $paramUpdate = [$value['jumlah'], $value['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menyimpan document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }

    public function hapus_transaksi($idPenjualan, $keterangan, $typetrans)
    {
        $cekError = false;
        $this->db->trans_begin();
        $idPenjualan = $this->security->xss_clean($this->sanitize_input($idPenjualan));
        $keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));

        $sql = "UPDATE penjualan SET hapus = 1, tgl_hapus = now(), alasan_hapus = ? WHERE id_jual = ?";
        $bindParam= [$keterangan, $idPenjualan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "UPDATE penjualan_detail SET hapus = 1 WHERE id_jual = ?";
        $bindParam= [$idPenjualan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "UPDATE pembayaran_detail SET hapus = 1 WHERE id_trx = ?";
        $bindParam= [$idPenjualan];
        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

        $sql = "SELECT * FROM penjualan_detail where id_jual = ?";
        $bindParam = [$idPenjualan];
        $result = $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        $result = $result->result_array();

        for($i = 0; $i<count($result); $i++){
            $sqlUpdateStok = "UPDATE master_kue SET stok = stok + ? WHERE id_kue = ?";
            $paramUpdate = [$result[$i]['jumlah'], $result[$i]['id_kue']];
            $this->db->query($sqlUpdateStok, $paramUpdate);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                $cekError = true;
            }
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menghapus document'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }

        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => $idPenjualan];
        }
    }
}
