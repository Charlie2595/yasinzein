<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 23.47
 */
use App\Traits\SecurityFilter;
class Member_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function getMember($type){
		$sql = "SELECT * FROM master_member where jenis_member = ? and hapus = 0";
		$bindParam = [$type];
		$exectutedQuery = $this->db->query($sql, $bindParam);
		return $exectutedQuery->result_array();
	}

	public function getAllMember(){
		$sql = "SELECT * FROM master_member where hapus = 0";
		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function getAllMemberCabangB2B(){
		$sql = "SELECT * FROM master_member where hapus = 0 AND jenis_member <> 0";
		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function get($id){
		$sql = "SELECT * FROM master_member WHERE id_member = ? and hapus = 0";
		$bindParam = [$id];
		$exectutedQuery = $this->db->query($sql, $bindParam);
		$result = $exectutedQuery->row_array();
		return $result;
	}

	public function create($kode, $nama, $email, $rumah, $hp, $ktp, $diskon, $instansi, $kantor, $keterangan, $jenis, $diskonBarang, $usepoin){
        $cekError = false;
		try {
			$kode = $this->security->xss_clean($this->sanitize_input($kode));
			$nama = $this->security->xss_clean($this->sanitize_input($nama));
			$email = $this->security->xss_clean($this->sanitize_input($email));
			$rumah = $this->security->xss_clean($this->sanitize_input($rumah));
			$hp = $this->security->xss_clean($this->sanitize_input($hp));
			$ktp = $this->security->xss_clean($this->sanitize_input($ktp));
			$diskon = $this->security->xss_clean($this->sanitize_input($diskon));
			$instansi = $this->security->xss_clean($this->sanitize_input($instansi));
			$kantor = $this->security->xss_clean($this->sanitize_input($kantor));
			$keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));
			$jenis = $this->security->xss_clean($this->sanitize_input($jenis));
			$diskonBarang = $this->security->xss_clean($this->sanitize_input($diskonBarang));
			$usepoin = $this->security->xss_clean($this->sanitize_input($usepoin));

			$data = array(
				'nama'			=> $nama,
				'barcode_member'=> $kode,
				'email'			=> $email,
				'nohp' 			=> $hp,
				'saldo'			=> 0,
				'poin'			=> 0,
				'ktp' 			=> $ktp,
				'use_poin' 		=> $usepoin,
				'diskon' 		=> $diskon,
				'alamat_rumah' 	=> $rumah,
				'alamat_kantor' => $kantor,
				'instansi' 		=> $instansi,
				'ket'			=> $keterangan,
				'diskon_barang' => $diskonBarang,
				'jenis_member'  => $jenis,
				'tgl_buat' 		=> date('Y-m-d'),
			);

			$this->db->insert('master_member', $data);
			$error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                    $cekError = true;
            }
	        //check if transaction status TRUE or FALSE
	        if($cekError === true){
	            $this->db->trans_rollback();
	            if(SHOW_ERROR === FALSE){
	                return ['status' => 'error', 'description' => 'Gagal membuat master member'];
	            } else {
	                return ['status' => 'error', 'description' => $messageError];
	            }
	        } else {
	            $this->db->trans_commit();
	            return ['status' => 'success', 'description' => 'Berhasil membuat master member'];
	        }
			/*$sql = "INSERT INTO master_member(barcode_member, nama, nohp, email, ktp, saldo, poin, use_poin, diskon, alamat_rumah, alamat_kantor, instansi, ket, jenis_member, diskon_barang) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			$bindParam= [$kode, $nama, $hp, $email, $ktp, 0, 0, $usepoin, $diskon, $rumah, $kantor, $instansi, $keterangan, $jenis, $diskonBarang];
			$this->db->query($sql, $bindParam);*/
		} catch (Exception $e) {
			$this->db->trans_rollback();
			return ['status' => 'error', 'description' => 'Error '. $e->getMessage()];
		}
	}

	public function edit($id, $nama, $email, $rumah, $hp, $ktp, $diskon, $instansi, $kantor, $keterangan, $usepoin, $diskonBarang, $jenisMember){
		$this->db->trans_begin();
        $cekError = false;
		try {
			$nama = $this->security->xss_clean($this->sanitize_input($nama));
			$email = $this->security->xss_clean($this->sanitize_input($email));
			$rumah = $this->security->xss_clean($this->sanitize_input($rumah));
			$hp = $this->security->xss_clean($this->sanitize_input($hp));
			$ktp = $this->security->xss_clean($this->sanitize_input($ktp));
			$diskon = $this->security->xss_clean($this->sanitize_input($diskon));
			$instansi = $this->security->xss_clean($this->sanitize_input($instansi));
			$kantor = $this->security->xss_clean($this->sanitize_input($kantor));
			$keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));
			$usepoin = $this->security->xss_clean($this->sanitize_input($usepoin));
			$id = $this->security->xss_clean($this->sanitize_input($id));
			$diskonBarang = $this->security->xss_clean($this->sanitize_input($diskonBarang));
			$jenisMember = $this->security->xss_clean($this->sanitize_input($jenisMember));

			$data = array(
				'nama'			=> $nama,
				'email'			=> $email,
				'nohp' 			=> $hp,
				'ktp' 			=> $ktp,
				'use_poin' 		=> $usepoin,
				'diskon' 		=> $diskon,
				'alamat_rumah' 	=> $rumah,
				'alamat_kantor' => $kantor,
				'instansi' 		=> $instansi,
				'ket'			=> $keterangan,
				'diskon_barang' => $diskonBarang,
				'jenis_member'  => $jenisMember,
			);

			$this->db->where('id_member', $id);
			$this->db->update('master_member', $data);

			$error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                    $cekError = true;
            }
	        //check if transaction status TRUE or FALSE
	        if($cekError === true){
	            $this->db->trans_rollback();
	            if(SHOW_ERROR === FALSE){
	                return ['status' => 'error', 'description' => 'Gagal mengubah master member'];
	            } else {
	                return ['status' => 'error', 'description' => $messageError];
	            }
	        } else {
	            $this->db->trans_commit();
	            return ['status' => 'success', 'description' => 'Berhasil mengubah master member'];
	        }
			// $sql = "UPDATE master_member SET nama = ?, nohp = ?, email = ?, ktp = ?, use_poin = ?, diskon = ?, alamat_rumah = ?, alamat_kantor = ?, instansi = ?, ket = ?, diskon_barang = ? WHERE id_member = ?";
			// $bindParam= [$nama, $hp, $email, $ktp, $usepoin, $diskon, $rumah, $kantor, $instansi, $keterangan, $diskonBarang, $id];
			// $this->db->query($sql, $bindParam);
		} catch (Exception $e) {
			$this->db->trans_rollback();
			throw new Exception('Error saving member ' . $e->getMessage());
		}
	}

	public function isAvailableHP() {
		$hp = $_POST["hp"];
		//var_dump($_POST['username']);

		$sql = "SELECT * FROM master_member WHERE hp = ?";
		$bindParam = [$hp];
		$result = $this->db->query($sql,$bindParam);
		if(isset($_POST["oldHP"])){
			if($hp == $_POST["oldHP"]){
				return 0;
			}
			else{
				return $result->num_rows();
			}
		}
		else{
			return $result->num_rows();
		}
	}

	public function delete($id)
	{
		$this->db->trans_begin();
        $cekError = false;
		$sql = "UPDATE master_member SET hapus = 1 WHERE id_member = ?";
		$bindParam = [$id];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }
        //check if transaction status TRUE or FALSE
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menghapus master member'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil menghapus master member'];
        }
	}
}
