<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 23.47
 */
use App\Traits\SecurityFilter;
class Supplier_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
		$this->db->trans_begin();
	}

	public function getsupplier(){
		$sql = "SELECT * FROM master_supplier where hapus = 0";
		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
	}

	public function get($id){
		$sql = "SELECT * FROM master_supplier WHERE id_sup = ? and hapus = 0";
		$bindParam = [$id];
		$exectutedQuery = $this->db->query($sql, $bindParam);
		$result = $exectutedQuery->row_array();
		return $result;
	}

	public function create($nama, $email, $rumah, $hp, $keterangan, $kue){
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$email = $this->security->xss_clean($this->sanitize_input($email));
		$rumah = $this->security->xss_clean($this->sanitize_input($rumah));
		$hp = $this->security->xss_clean($this->sanitize_input($hp));
		$keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));

		$sql = "SELECT id_sup FROM master_supplier ORDER BY id_sup DESC LIMIT 1";
		$executedQuery = $this->db->query($sql);
		$id = $executedQuery->row_array();
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }
        if($id['id_sup'] == null){
        	$idSup = 0;
        }
        $idSup = ($id['id_sup']*1) + 1;

		$sql = "INSERT INTO master_supplier(id_sup, nama, alamat, nohp, email, ket, hapus) VALUES (?,?,?,?,?,?,?)";
		$bindParam= [$idSup, $nama, $rumah, $hp, $email, $keterangan, 0];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        $sqlDetail = "INSERT INTO kue_supplier(id_kue, id_sup) VALUES";
        $paramKue = [];
        for ($i=0; $i < count($kue); $i++)
        {
            if($i == count($kue) - 1)
                $sqlDetail .= "(?,?)";
            else
                $sqlDetail .= "(?,?),";

            $id_kue = $this->security->xss_clean($this->sanitize_input($kue[$i]));
            $paramBarang[] = $id_kue;
            $paramBarang[] = $idSup;
        }

        $this->db->query($sqlDetail, $paramBarang);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

    	if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
            	return ['status' => 'error', 'description' => 'Gagal membuat master supplier'];
            } else {
            	return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil membuat master supplier'];
        }
	}

	public function edit($id, $nama, $email, $rumah, $hp, $keterangan, $kue){
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$email = $this->security->xss_clean($this->sanitize_input($email));
		$rumah = $this->security->xss_clean($this->sanitize_input($rumah));
		$hp = $this->security->xss_clean($this->sanitize_input($hp));
		$keterangan = $this->security->xss_clean($this->sanitize_input($keterangan));
		$id = $this->security->xss_clean($this->sanitize_input($id));

		$sql = "UPDATE master_supplier SET nama = ?, nohp = ?, email = ?, alamat = ?, ket = ? WHERE id_sup = ?";
		$bindParam= [$nama, $hp, $email, $rumah, $keterangan, $id];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        $sql = "DELETE FROM kue_supplier WHERE id_sup = ?";
        $bindParam = [$id];
        $this->db->query($sql, $bindParam);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        $sqlDetail = "INSERT INTO kue_supplier(id_kue, id_sup) VALUES";
        $paramKue = [];
        for ($i=0; $i < count($kue); $i++)
        {
            if($i == count($kue) - 1)
                $sqlDetail .= "(?,?)";
            else
                $sqlDetail .= "(?,?),";

            $id_kue = $this->security->xss_clean($this->sanitize_input($kue[$i]));
            $paramBarang[] = $id_kue;
            $paramBarang[] = $id;
        }

         $this->db->query($sqlDetail, $paramBarang);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
            $cekError = true;
        }

    	if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
            	return ['status' => 'error', 'description' => 'Gagal mengubah master supplier'];
            } else {
            	return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil mengubah master supplier'];
        }
	}

	public function delete($id){

		$id = $this->security->xss_clean($this->sanitize_input($id));

		$sql = "UPDATE master_supplier SET hapus = ? WHERE id_sup = ?";
		$bindParam= [1, $id];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

    	if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
            	return ['status' => 'error', 'description' => 'Gagal menghapus master supplier'];
            } else {
            	return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil menghapus master supplier'];
        }
	}

	public function getKueSupplier($id){
		$sql = "SELECT id_kue FROM kue_supplier WHERE id_sup = ?";
		$bindParam = [$id];
		$exectutedQuery = $this->db->query($sql, $bindParam);

		return $exectutedQuery->result_array();
	}
}
