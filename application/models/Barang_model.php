<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 25/03/2018
 * Time: 00.13
 */
use App\Traits\SecurityFilter;
class Barang_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function getBarang($idSup){
        if($idSup != null){
            $sql = "SELECT k.* FROM master_kue k left join kue_supplier ks ON ks.id_kue = k.id_kue where k.hapus = 0 AND ks.id_sup = ?";
            $bindParam = [$idSup];
            $exectutedQuery = $this->db->query($sql, $bindParam);
        } else {
    		$sql = "SELECT * FROM master_kue where hapus = 0";
    		$exectutedQuery = $this->db->query($sql);
        }
		$list = $exectutedQuery->result_array();

		return $list;
	}

    public function getDepartemen(){
        $sql = "SELECT DISTINCT departemen as nama FROM master_kue";
        $exectutedQuery = $this->db->query($sql);
        $list = $exectutedQuery->result_array();

        return $list;
    }

    public function getBarangById($id){
        $sql = "SELECT * FROM master_kue where id_kue = ? and hapus = 0";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list = $exectutedQuery->row_array();

        return $list;
    }

	public function get($search){
		$sql = "SELECT * FROM master_kue WHERE (barcode_kue LIKE '%" . $search. "%' OR nama_kue LIKE '%" . $search. "%') AND hapus = 0 ";
		$bindParam = [$search, $search];
		$result = $this->db->query($sql);
        return $result->result_array();
	}

    public function create($kode, $nama, $jenis, $departemen, $produksi, $harga, $stok, $diskon){
        $sql = "SELECT * FROM master_kue WHERE barcode_kue = ?";
        $bindParam = [$kode];
        $result = $this->db->query($sql, $bindParam)->row_array();

        if(count($result) > 0){
            $sql = "UPDATE master_kue SET nama_kue = ?, jenis_kue = ?, departemen = ?, harga = ?, produksi = ?, stok = ?, hapus = ? WHERE id_kue = ?";
            $bindParam= [$nama, $jenis, $departemen, $harga, $produksi, $stok, 0, $result['id_kue']];
            $this->db->query($sql, $bindParam);
        } else {
            $sql = "INSERT INTO master_kue(barcode_kue, jenis_kue, departemen, nama_kue, harga, produksi, stok, hapus) VALUES (?,?,?,?,?,?,?,?)";
            $bindParam= [$kode, $jenis, $departemen, $nama, $harga, $produksi, $stok, 0];
            $this->db->query($sql, $bindParam);
        }

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }
        
        //check if transaction status TRUE or FALSE
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal membuat master kue'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil membuat master kue'];
        }
    }

    public function edit($id, $nama, $jenis, $departemen, $harga, $produksi){
        $cekError = false;
        
        $sql = "UPDATE master_kue SET nama_kue = ?, jenis_kue = ?, departemen = ?, harga = ?, produksi = ? WHERE id_kue = ?";
        $bindParam= [$nama, $jenis, $departemen, $harga, $produksi, $id];
        $this->db->query($sql, $bindParam);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }
        //check if transaction status TRUE or FALSE
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal mengubah master kue'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil mengubah master kue'];
        }
    }

	public function cekKodeBarang($kode){
        $kode = $this->security->xss_clean($this->sanitize_input($kode));
        $sql = "SELECT *
                FROM master_kue WHERE barcode_kue = ? and hapus = 0";
        $bindParam = [$kode];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();
        return $result;
    }

    public function getProduksi($tgl)
    {
    	$tanggal = date('Y-m-d', strtotime($tgl));
        $sql = "SELECT k.nama_kue as nama, k.id_kue
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual 
        INNER JOIN master_kue k ON k.id_kue = pd.id_kue WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? GROUP BY k.id_kue";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam)->result_array();
        $list['barang']['list'] = $exectutedQuery;


        /*$sql = "SELECT k.nama_kue as nama, k.id_kue
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual 
        INNER JOIN master_kue k ON k.id_kue = pd.id_kue WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '00:00:00' AND '11:59:00') GROUP BY k.id_kue";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list['pagi']['barang'] = $exectutedQuery->result_array();

        $sql = "SELECT m.nama, m.id_member
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual 
        INNER JOIN master_member m ON m.id_member = p.id_member
        WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '00:00:00' AND '11:59:00') GROUP BY m.id_member, m.nama";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list['pagi']['member'] = $exectutedQuery->result_array();*/
        
    	$sql = "SELECT k.nama_kue as nama, if(SUM(pd.jumlah) = null,0,sum(pd.jumlah)) as jumlah, k.id_kue
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual INNER JOIN master_member m ON m.id_member = p.id_member
        INNER JOIN master_kue k ON k.id_kue = pd.id_kue WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '00:00:00' AND '11:59:00') GROUP BY k.id_kue";
    	$bindParam = [$tanggal];
    	$exectutedQuery = $this->db->query($sql, $bindParam);
		$list['barang']['pagi'] = $exectutedQuery->result_array();

        /*$sql = "SELECT k.nama_kue as nama, k.id_kue
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual 
        INNER JOIN master_kue k ON k.id_kue = pd.id_kue WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '12:00:00' AND '23:59:00') GROUP BY k.id_kue";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list['malam']['barang'] = $exectutedQuery->result_array();

        $sql = "SELECT m.nama, m.id_member
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual 
        INNER JOIN master_member m ON m.id_member = p.id_member
        WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '12:00:00' AND '23:59:00') GROUP BY m.id_member, m.nama";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list['malam']['member'] = $exectutedQuery->result_array();*/

        $sql = "SELECT k.nama_kue as nama, if(SUM(pd.jumlah) = null,0,sum(pd.jumlah)) as jumlah, k.id_kue
        FROM penjualan p INNER JOIN penjualan_detail pd ON pd.id_jual = p.id_jual INNER JOIN master_member m ON m.id_member = p.id_member
        INNER JOIN master_kue k ON k.id_kue = pd.id_kue WHERE p.tipe_trx = 2 AND p.status != 1 AND p.tgl_pesanan = ? AND (p.jam_pesanan between '12:00:00' AND '23:59:00') GROUP BY k.id_kue";
        $bindParam = [$tanggal];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $list['barang']['malam'] = $exectutedQuery->result_array();

		return $list;
    }

    public function getBarangMasuk()
    {
    	$sql = "SELECT b.id_masuk, b.tgl, b.lunas, b.jumlah, p.nama As pegawai, s.nama AS supplier 
    	FROM barang_masuk b
    	INNER JOIN master_pegawai p ON b.id_pegawai = p.id_pegawai 
    	INNER JOIN master_supplier s ON s.id_sup = b.id_sup";
		$exectutedQuery = $this->db->query($sql);
		$list = $exectutedQuery->result_array();

		return $list;
    }

    public function searchHistory($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));
        $sql = "SELECT b.id_masuk
                FROM barang_masuk b
                WHERE b.id_sup = ? ORDER BY b.id_masuk desc limit 1";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $sql = "SELECT p.id_kue, p.jumlah, p.harga_beli, p.harga_jual, k.nama_kue, k.barcode_kue
        FROM barang_masuk_detail p 
        INNER JOIN master_kue k ON k.id_kue = p.id_kue
        WHERE p.id_masuk = ?";
        $bindParam = [$result['id_masuk']];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        return $result;
    }

    public function isAvailableKode() {
        $kode = $_POST["kode"];
        //var_dump($_POST['username']);

        $sql = "SELECT * FROM master_kue WHERE barcode_kue = ?";
        $bindParam = [$kode];
        $result = $this->db->query($sql,$bindParam);
        return $result->num_rows();
    }

    public function createBarangMasuk($tanggal, $idSupplier, $idPegawai, $detail)
    {
        $this->db->trans_begin();
        $cekError = false;
        $idSupplier = $this->security->xss_clean($this->sanitize_input($idSupplier));
        $idPegawai = $this->security->xss_clean($this->sanitize_input($idPegawai));
        $tanggal = date('Y-m-d', strtotime($tanggal));

        $sql = "INSERT INTO barang_masuk(tgl, id_sup, id_pegawai)
            VALUES(?,?,?)";
        $bindParam= [$tanggal, $idSupplier, $idPegawai];

        $this->db->query($sql, $bindParam);
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        $total = 0;

        $sqlGetId = "SELECT id_masuk FROM barang_masuk ORDER BY id_masuk DESC LIMIT 1";
        $executedQuery = $this->db->query($sqlGetId);
        $id = $executedQuery->row_array();
        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        if($detail != null){
            $sqlDetail = "INSERT INTO barang_masuk_detail(id_masuk, id_kue, jumlah) VALUES ";
            $paramBarang = [];
            for ($i=0; $i < count($detail); $i++)
            {
                if($i == count($detail) - 1)
                    $sqlDetail .= "(?,?,?)";
                else
                    $sqlDetail .= "(?,?,?),";

                $idBarang = $this->security->xss_clean($this->sanitize_input($detail[$i]['idBarang']));
                $jumlah = $this->security->xss_clean($this->sanitize_input($detail[$i]['jumlah']));
                $paramBarang[] = $id['id_masuk'];
                $paramBarang[] = $idBarang;
                $paramBarang[] = $jumlah;
                $total = $total + $jumlah*1;
                $sqlStok = "SELECT stok FROM master_kue WHERE id_kue = ?";
                $param = [$idBarang];
                $exectutedQuery = $this->db->query($sqlStok, $param);
                $result = $exectutedQuery->row_array();
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                        $cekError = true;
                }

                $stok = $result['stok'] + $jumlah;

                $sqlUpdateStok = "UPDATE master_kue SET stok = ? WHERE id_kue = ?";
                $paramUpdate = [$stok, $idBarang];
                $this->db->query($sqlUpdateStok, $paramUpdate);
                $error = $this->db->error();
                if($error['message']){
                    $messageError = $error['message'];
                        $cekError = true;
                }
            }
            $this->db->query($sqlDetail, $paramBarang);
            $error = $this->db->error();
            if($error['message']){
                $messageError = $error['message'];
                    $cekError = true;
            }
        }

        $sql = "UPDATE barang_masuk SET jumlah = ? WHERE id_masuk = ?";
        $param = [$total, $id['id_masuk']];
        $this->db->query($sql, $param);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }
        //check if transaction status TRUE or FALSE
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal mengubah master kue'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil mengubah master kue'];
        }
    }

    public function delete($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));

        $sql = "UPDATE master_kue SET hapus = ? WHERE id_kue = ?";
        $bindParam= [1, $id];
        $this->db->query($sql, $bindParam);

        $error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menghapus master kue'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil menghapus master kue'];
        }
    }

    public function getDetailBarangMasuk($id){
        $id = $this->security->xss_clean($this->sanitize_input($id));

        $sql = "SELECT p.id_masuk, p.id_sup, p.tgl, p.lunas, p.jumlah, p.id_pegawai, m.nama AS supplier, pe.nama AS pegawai
        FROM barang_masuk p 
        LEFT JOIN master_supplier m ON m.id_sup = p.id_sup 
        INNER JOIN master_pegawai pe ON pe.id_pegawai = p.id_pegawai 
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->row_array();

        $hasil = $result;
        //Mengambil detail penjualan
        $sql = "SELECT p.id_masuk_detail, p.id_kue, p.jumlah, p.retur, p.id_pegawai, mk.nama_kue, mp.nama AS pegawai, p.harga_beli, p.harga_jual
        FROM barang_masuk_detail p INNER JOIN master_kue mk ON mk.id_kue = p.id_kue
        LEFT JOIN master_pegawai mp ON mp.id_pegawai = p.id_pegawai
        WHERE p.id_masuk = ?";
        $bindParam = [$id];
        $exectutedQuery = $this->db->query($sql, $bindParam);
        $result = $exectutedQuery->result_array();
        $hasil['detail'] = $result;
        
        return $hasil;
    }
}
