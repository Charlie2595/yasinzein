<?php

use App\Traits\SecurityFilter;

class Pegawai_model extends CI_Model
{
	use SecurityFilter;

	public function __construct()
	{
		parent::__construct();
	}

	public function login($username, $password){
		$username = $this->sanitize_input($username);
		$sql = "select u.id_pegawai AS id, u.nama, u.password, u.jabatan from master_pegawai u
		where u.username = ? and u.hapus = 0";
		$bindParam = array($username);
		$executedQuery = $this->db->query($sql, $bindParam);
		$result = $executedQuery->row_array();
		
		if($result === NULL)
		{
			$user = NULL;
		}
		else
		{
			$password_hash = $result['password'];
			if(password_verify($password, $password_hash))
			{
				$user = $result;
			}
			else
			{
				$user = NULL;
			}
		}
		return $user;
	}

	public function getActiveUsers()
	{
		$sql = "SELECT u.id_pegawai AS id, u.nama, u.jabatan, u.nohp, u.ktp, u.hapus, u.username
                FROM master_pegawai u where hapus = 0";

		$exectutedQuery = $this->db->query($sql);
		$users = $exectutedQuery->result_array();

		return $users;
	}

	public function getPengirim()
	{
		$sql = "SELECT u.id_pegawai AS id, u.nama, u.jabatan, u.nohp, u.ktp, u.hapus, u.username
                FROM master_pegawai u WHERE jabatan = 'Driver' and hapus = 0";

		$exectutedQuery = $this->db->query($sql);
		$users = $exectutedQuery->result_array();

		return $users;
	}

	/**
	 * @param $idUser
	 * @return User
	 */
	public function getActiveUser($idUser)
	{
		$idUser = $this->security->xss_clean($this->sanitize_input($idUser));
		$sql = "SELECT id_pegawai As id, nama, username, jabatan, nohp, ktp
                FROM master_pegawai
                WHERE hapus = 0 AND id_pegawai = ?";
		$bindParam = [$idUser];
		$exectutedQuery = $this->db->query($sql, $bindParam);
		$user = $exectutedQuery->row_array();

		return $user;
	}

	public function isAvailableUsername() {
		$username = $_POST["username"];
		$sql = "SELECT * FROM master_pegawai WHERE username = ?";
		$bindParam = [$username];
		$result = $this->db->query($sql,$bindParam);
		if(isset($_SESSION['lastUsername'])){
			if($username == $_SESSION['lastUsername']){
				return 0;
			}
			else{
				return $result->num_rows();
			}
		} else {
			return $result->num_rows();
		}
	}

	public function verifikasiPassword() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$sql = "select u.id_pegawai AS id, u.nama, u.password, u.jabatan from master_pegawai u
                where u.username = ? and u.hapus = 0 and (u.jabatan = 'Owner' OR u.jabatan = 'Supervisor')";
		$bindParam = array($username);
		$executedQuery = $this->db->query($sql, $bindParam);
		$result = $executedQuery->row_array();

		if($result === NULL)
		{
			$user = 1;
		}
		else
		{
			$password_hash = $result['password'];
			if(password_verify($password, $password_hash))
			{
				$user = 0;
			}
			else
			{
				$user = 1;
			}
		}
		return $user;
	}

	public function createUser($username, $nama, $jabatan, $password, $notelp, $ktp)
	{
		$username = $this->security->xss_clean($this->sanitize_input($username));
		if($password != null){
			$hashedPassword = password_hash($password, PASSWORD_DEFAULT);	
		}else{
			$hashedPassword = $password;
		}
		$jabatan = $this->security->xss_clean($this->sanitize_input($jabatan));
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$notelp = $this->security->xss_clean($this->sanitize_input($notelp));
		$ktp = $this->security->xss_clean($this->sanitize_input($ktp));
		//var_dump($username, $hashedPassword, $jabatan, $nama, $notelp, $ktp);die();
		$sql = "INSERT INTO master_pegawai(nama, username, password, jabatan, nohp, ktp, hapus) VALUES(?,?,?,?,?,?,?) ";
		$bindParam= [$nama, $username, $hashedPassword, $jabatan, $notelp, $ktp, 0];
		$this->db->query($sql, $bindParam);
	}

	public function updateUser($id, $nama, $username, $providedPassword ,$jabatan, $notelp, $ktp, $fullEdit)
	{
		$id = $this->security->xss_clean($this->sanitize_input($id));
		$username = $this->security->xss_clean($this->sanitize_input($username));
		$nama = $this->security->xss_clean($this->sanitize_input($nama));
		$providedPassword = $this->security->xss_clean($this->sanitize_input($providedPassword));
		$jabatan = $this->security->xss_clean($this->sanitize_input($jabatan));
		$notelp = $this->security->xss_clean($this->sanitize_input($notelp));
		$ktp = $this->security->xss_clean($this->sanitize_input($ktp));

		if($fullEdit)
		{
			$sql = "UPDATE master_pegawai SET nama = ?, username = ?, password = ?, jabatan = ?, nohp = ?, ktp = ? WHERE id_pegawai = ?";
			$hashedPassword = password_hash($providedPassword, PASSWORD_DEFAULT);
			$bindParam = [$nama, $username, $hashedPassword, $jabatan, $notelp, $ktp, $id];
		}
		else {
			$sql = "UPDATE master_pegawai SET nama = ?, username = ?, jabatan = ?, nohp = ?, ktp = ? WHERE id_pegawai = ?";
			$bindParam = [$nama, $username, $jabatan, $notelp, $ktp, $id];
		}
		$this->db->query($sql, $bindParam);
	}

	public function deactivateUser($user)
	{
		$this->db->trans_begin();
        $cekError = false;

		$user->id = $this->security->xss_clean($this->sanitize_input($user['id']));
		$sql = "UPDATE master_pegawai SET hapus = 1 WHERE id_pegawai = ?";
		$bindParam = [$user['id']];
		$this->db->query($sql, $bindParam);

		$error = $this->db->error();
        if($error['message']){
            $messageError = $error['message'];
                $cekError = true;
        }

        //check if transaction status TRUE or FALSE
        if($cekError === true){
            $this->db->trans_rollback();
            if(SHOW_ERROR === FALSE){
                return ['status' => 'error', 'description' => 'Gagal menghapus master pegawai'];
            } else {
                return ['status' => 'error', 'description' => $messageError];
            }
        } else {
            $this->db->trans_commit();
            return ['status' => 'success', 'description' => 'Berhasil menghapus master pegawai'];
        }
	}

	public function cekKode($kode){
		$kode = $this->security->xss_clean($this->sanitize_input($kode));
		$sql = "select kode, nama from karyawan
                WHERE status = 0 AND kode = ?";
		$bindParam = [$kode];
		$exectutedQuery = $this->db->query($sql, $bindParam);
		$user = $exectutedQuery->row_array();

		if($user != null){
			$sql = "SELECT id FROM user WHERE karyawan_kode = ?";
			$bindParam = [$kode];
			$result = $this->db->query($sql,$bindParam);
			$user['available'] = $result->num_rows();
		}
		return $user;
	}

}
