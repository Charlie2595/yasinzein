<?php
/**
 * Indonesian Translation for CI Form Validation
 *
 * @author: Charlie
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Kolom {field} harus diisi.';
$lang['form_validation_isset']			= 'Kolom {field} harus memiliki nilai.';
$lang['form_validation_valid_email']	= 'Kolom {field} harus diisi alamat email dalam format yang benar.';
$lang['form_validation_valid_emails']	= 'Kolom {field} harus diisi semua alamat email dalam format yang benar.';
$lang['form_validation_valid_url']		= 'Kolom {field} harus diisi URL dengan format yang benar.';
$lang['form_validation_valid_ip']		= 'Kolom {field} harus diisi alamat IP dalamt format yang benar.';
$lang['form_validation_min_length']		= 'Kolom {field} harus diisi minimal {param} karakter.';
$lang['form_validation_max_length']		= 'Kolom {field} tidak boleh diisi melebihi {param} karakter.';
$lang['form_validation_exact_length']	= 'Kolom {field} harus diisi tepat {param} karakter.';
$lang['form_validation_alpha']			= 'Kolom {field} hanya bisa diisi karakter abjad.';
$lang['form_validation_alpha_numeric']	= 'Kolom {field} hanya bisa diisi karakter abjad atau angka.';
$lang['form_validation_alpha_numeric_spaces']	= 'Kolom {field} hanya bisa diisi karakter abjad, angka, atau spasi.';
$lang['form_validation_alpha_dash']		= 'Kolom {field} hanya bisa diisi karakter abjad, angka, garis bawah (underscores), dan garis sambung (dashes).';
$lang['form_validation_numeric']		= 'Kolom {field} harus diisi angka/numerik.';
$lang['form_validation_is_numeric']		= 'Kolom {field} harus diisi karakter angka.';
$lang['form_validation_integer']		= 'Kolom {field} harus diisi bilangan bulat.';
$lang['form_validation_regex_match']	= 'Kolom {field} tidak dalam format yang sesuai.';
$lang['form_validation_matches']		= 'Kolom {field} tidak cocok dengan isi kolom {param}.';
$lang['form_validation_differs']		= 'Kolom {field} harus berbeda dengan isi kolom {param}.';
$lang['form_validation_is_unique'] 		= 'Nilai kolom {field} sudah ada. Silahkan isi dengan nilai yang lain';
$lang['form_validation_is_natural']		= 'Kolom {field} hanya boleh diisi bilangan natural ( 0 dan bilangan positif).';
$lang['form_validation_is_natural_no_zero']	= 'Kolom {field} hanya boleh diisi bilangan positif.';
$lang['form_validation_decimal']		= 'Kolom {field} hanya boleh diisi bilangan desimal.';
$lang['form_validation_less_than']		= 'Kolom {field} harus berisi angka  kurang dari {param}.';
$lang['form_validation_less_than_equal_to']	= 'Kolom {field} harus berisi angka yang kurang dari atau sama dengan {param}.';
$lang['form_validation_greater_than']		= 'Kolom {field} harus berisi angka lebih besar dari {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Kolom {field} harus berisi angka yang lebih besar atau sama dengan {param}.';
$lang['form_validation_error_message_not_set']	= 'Tidak dapat mengakses pesan error yang sesuai terhadap kolom {field}.';
$lang['form_validation_in_list']		= 'Kolom {field} harus salah satu dari: {param}.';
