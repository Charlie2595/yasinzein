<?php

namespace App\Traits;

defined('BASEPATH') OR exit('No direct script access allowed');
trait Currency {

    /**
     * Terbilang hingga skala juta
     * @param $number
     * @param string $bahasa
     * @return string
     */
    public function terbilang($number, $bahasa = INA_LANG)
    {
        $number = (int) $number;
        return $this->terbilangIndonesia($number);
    }

    private function terbilangIndonesia($x)
    {
        $angka = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
        if ($x < 12)
            return $angka[$x] . " ";
        elseif ($x < 20)
            return $this->terbilangIndonesia($x - 10) . "belas ";
        elseif ($x < 100)
            return $this->terbilangIndonesia($x / 10) . "puluh " . $this->terbilangIndonesia($x % 10);
        elseif ($x < 200)
            return "seratus " . $this->terbilangIndonesia($x - 100);
        elseif ($x < 1000)
            return $this->terbilangIndonesia($x / 100) . "ratus " . $this->terbilangIndonesia($x % 100);
        elseif ($x < 2000)
            return "seribu " . $this->terbilangIndonesia($x - 1000);
        elseif ($x < 1000000)
            return $this->terbilangIndonesia($x / 1000) . "ribu " . $this->terbilangIndonesia($x % 1000);
        elseif ($x < 1000000000)
            return $this->terbilangIndonesia($x / 1000000) . "juta " . $this->terbilangIndonesia($x % 1000000);
    }
}
