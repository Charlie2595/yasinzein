<?php

namespace App\Traits;

defined('BASEPATH') OR exit('No direct script access allowed');
trait SecurityFilter {

	public function sanitize_input($var)
    {
        $var = htmlspecialchars(stripslashes($var));
        $var = str_replace("script", "blocked", $var);
        return $var;
    }

    /**
     * perform xss_clean() & htmlentities()
     * @param $var
     * @return mixed
     */
    public function sanitize_output($var)
    {
        $var = $this->security->xss_clean(htmlentities($var));
        return $var;
    }
}
