<?php

namespace App\Traits;

defined('BASEPATH') OR exit('No direct script access allowed');
trait Periode {
    private $monthsName = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni','Juli', 'Agustus', 'September', 'Oktober','November', 'Desember' ];
    private $monthsNumber = ['01','02','03','04','05','06','07','08','09','10','11','12'];

    /**
     * mengembalikan array berisi daftar nama bulan dlm bhsa Indo dlm bentuk 'angka' (mm) dan 'nama' panjang
     * @return mixed array keys: nama, angka
     */
    public function getMonths()
    {
        $months['angka'] = $this->monthsNumber;
        $months['nama'] = $this->monthsName;
        return $months;
    }

    public function getMonthText($monthNumber)
    {
        if($monthNumber <= 0 || $monthNumber > 12)
            return false;
        else
            return $this->monthsName[$monthNumber-1];
    }
}
