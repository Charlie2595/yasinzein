<?php
namespace App\Traits;

defined('BASEPATH') OR exit('No direct script access allowed');
trait CheckAccess {
	
	function isOwner() {
		if(isset($_SESSION['idLogin']) && isset($_SESSION['jabatan'])) {
			if($_SESSION['jabatan'] === 'Owner') {
				return true;
			}
			else {
				return false;
			}
		}
		else
			return false;
	}

    /**
     * @param $jabatanUser
     * @return bool
     */
	function hasAccess($jabatanUser) {
		if(isset($_SESSION['idLogin']) && isset($_SESSION['jabatan'])) {
			if($_SESSION['jabatan'] === $jabatanUser) {
				return true;
			}
			else {
				return false;
			}
		}
		else
			return false;	
	}

	/**
	 * Cek apakah user saat ini sudah login
	 * @return bool
	 */
	function hasLogin() {
		if(isset($_SESSION['idLogin']) && isset($_SESSION['jabatan'])) {
			$jabatanTersedia = ['Owner', 'Kasir', 'Dapur', 'Supervisor'];
			$sudahLogin = false;
			foreach ($jabatanTersedia as $value) {
				if($value === $_SESSION['jabatan']) {
					$sudahLogin = true;
					break;
				}
			}
			return $sudahLogin;
		}
		else {
			return false;
		}
	}
	
}
