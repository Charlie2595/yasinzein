<?php
namespace App\Core\Controllers;


class Regular_Controller extends \CI_Controller
{

    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->data['pageTitle'] = 'YASINZEIN';
        $this->data['js_source'] = array();
        $this->data['css_source'] = array();
        /*$this->data['js_source'] = ['jquery' => base_url() . 'assets/js/jquery-1.12.4.min.js',
                    'bootstrap' => base_url() . 'assets/bootstrap/js/bootstrap.min.js'
                ]; */
    }

    /**
     * @param string $viewFileName isi dengan nama view yang mau ditampilkan. jika berada di subfolder, sertakan juga subfoldernya
     * Contoh: view login berada di subfolder web. maka penulisannya "web/login"
     * @param string $template isi dengan nama template tempat si $viewFileName mau disisipkan. jika tidak pakai template, /
     * isi dengan NULL. Khusus untuk kasus AJAX request, jika ingin return JSON , isi dengan "json".
     * Default template adalah tidak ada (NULL).
     * @param string $viewForJS nama view yg berisi file javascript yang terkait/diperlukan oleh view pada parameter pertama ($viewFileName).
     */
    protected function render_view($viewFileName, $template = NULL, $viewForJS = NULL)
    {
        if($this->input->is_ajax_request())
        {
            if($template == 'json')
            {
                /*
                header('Content-Type: application/json');
                echo json_encode($this->data);

                OR

                $this->output->set_output(json_encode($this->data));

                */
                $this->output->set_output(json_encode($this->data));
            }
            else
            {
                $this->load->view($viewFileName,$this->data);
            }
        }
        else {
            if(is_null($template)) {
                $this->load->view($viewFileName,$this->data);
            }
            else {
                // load isi dari $viewFileName tanpa header footer beserta nilai variabel pd script PHP,
                // kemudian hasilnya berupa string disimpan di field "alldata['content']".
                $this->alldata['content'] = $this->load->view($viewFileName,$this->data,TRUE);

                if(!is_null($viewForJS))
                {
                    $this->alldata['js'] = $this->load->view($viewForJS, $this->data,TRUE);
                }

                $this->load->view( $template , $this->alldata);
            }
        }
    }
}
?>
