<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//format:  $route['polaURL'] = 'namaController/namaMethod/parameterMethod';
$route['login'] = 'auth_controller/index';
$route['authenticate']['POST'] = 'auth_controller/login';
$route['authenticate']['GET'] = 'auth_controller/login';
$route['logout'] = 'auth_controller/logout';

$route['event/birthday'] = 'event_controller/birthday';

$route['pegawai'] = 'user_controller/index';
$route['pegawai/(:num)'] = 'user_controller/show/$1';
$route['pegawai/create']['get'] = 'user_controller/create';
$route['pegawai/create']['post'] = 'user_controller/store';
$route['pegawai/(:num)/edit'] = 'user_controller/edit/$1';
$route['pegawai/edit']['post'] = 'user_controller/update';
$route['pegawai/(:num)/deactivate'] = 'user_controller/deactivateConfirmation/$1';
$route['pegawai/(:num)/deactivate'] = 'user_controller/deactivate/$1';

$route['getNamaJemaat/(:any)'] = 'jemaat_controller/getNamaJemaatByJenis/$1';
$route['getAkunPengeluaran'] = 'kegiatan_controller/getAkunPengeluaran';
$route['getAkunPemasukan'] = 'kegiatan_controller/getAkunPemasukan';

$route['customer'] = 'customer_controller/index';
$route['customer/create']['GET'] = 'customer_controller/create';
$route['customer/create']['POST'] = 'customer_controller/store';
$route['customer/edit']['POST'] = 'customer_controller/update';
$route['customer/(:any)'] = 'customer_controller/show/$1';
$route['customer/(:any)/edit'] = 'customer_controller/edit/$1';

//Barang
$route['barang'] = 'barang_controller/index';
$route['barang/(:num)'] = 'barang_controller/show/$1';
$route['barang/create']['get'] = 'barang_controller/create';
$route['barang/create']['post'] = 'barang_controller/store';
$route['barang/(:num)/edit'] = 'barang_controller/edit/$1';
$route['barang/edit']['post'] = 'barang_controller/update/$1';
$route['barang/import'] = 'barang_controller/import';
$route['barang/(:num)/delete'] = 'barang_controller/delete/$1';
$route['import/view']['post'] = 'barang_controller/read';
$route['import/save']['post'] = 'barang_controller/upload';

$route['masuk'] = 'masuk_controller/index';
$route['masuk/create']['GET'] = 'masuk_controller/create';
$route['masuk/create']['POST'] = 'masuk_controller/store';
$route['masuk/(:num)/edit'] = 'masuk_controller/edit/$1';
$route['masuk/edit']['post'] = 'masuk_controller/update/$1';
$route['masuk/(:num)'] = 'masuk_controller/detail/$1';
$route['masuk/(:num)/payment'] = 'masuk_controller/payment/$1';
$route['masuk/store_payment']['post'] = 'masuk_controller/store_payment';

//retur cabang
$route['retur'] = 'penjualan_controller/index_retur';
$route['retur/create']['GET'] = 'penjualan_controller/create_retur';
$route['retur/create']['POST'] = 'penjualan_controller/store_retur';
$route['retur/(:num)'] = 'penjualan_controller/detail_retur/$1';
$route['retur/(:num)/edit'] = 'penjualan_controller/edit_retur/$1';
$route['retur/edit']['post'] = 'penjualan_controller/update_retur/$1';

//Pembayaran
$route['pembayaran'] = 'pembayaran_controller/index';
$route['pembayaran/(:num)'] = 'pembayaran_controller/show/$1';
$route['pembayaran/create']['get'] = 'pembayaran_controller/create';
$route['pembayaran/create']['post'] = 'pembayaran_controller/store';
$route['pembayaran/(:num)/edit'] = 'pembayaran_controller/edit/$1';
$route['pembayaran/edit']['post'] = 'pembayaran_controller/update/$1';
$route['pembayaran/(:num)/delete'] = 'pembayaran_controller/deactivate/$1';

//Supplier
$route['supplier'] = 'supplier_controller/index';
$route['supplier/(:num)'] = 'supplier_controller/show/$1';
$route['supplier/create']['get'] = 'supplier_controller/create';
$route['supplier/create']['post'] = 'supplier_controller/store';
$route['supplier/(:num)/delete'] = 'supplier_controller/delete/$1';
$route['supplier/(:num)/edit'] = 'supplier_controller/edit/$1';
$route['supplier/edit']['post'] = 'supplier_controller/update/$1';
$route['supplier/getKueSupplier'] = 'supplier_controller/getKueSupplier';

//Member
$route['member'] = 'member_controller/index';
$route['member/(:num)'] = 'member_controller/show/$1';
$route['member/create']['get'] = 'member_controller/create';
$route['member/create']['post'] = 'member_controller/store';
$route['member/(:num)/edit'] = 'member_controller/edit/$1';
$route['member/edit']['post'] = 'member_controller/update/$1';
$route['member/(:num)/delete']['get'] = 'member_controller/delete/$1';

//POS (Point Of Sale)
$route['pos'] = 'penjualan_controller/index_pos';
$route['pos/(:num)'] = 'penjualan_controller/detail_pos/$1';
$route['pos/create']['GET'] = 'penjualan_controller/create_pos';
$route['pos/create']['POST'] = 'penjualan_controller/store_pos';
$route['pos/(:num)/payment'] = 'penjualan_controller/payment_pos/$1';
$route['pos/store_payment']['post'] = 'penjualan_controller/store_payment_pos';
$route['pos/(:num)/edit'] = 'penjualan_controller/edit_pos/$1';
$route['pos/edit']['POST'] = 'penjualan_controller/update_pos';
$route['pos/(:num)/print'] = 'penjualan_controller/print_pos/$1';
$route['pos/(:num)/invoice'] = 'penjualan_controller/invoice_pos/$1';
$route['pos/hapus']['post'] = 'penjualan_controller/hapus_pesanan';

//Pesanan
$route['pesanan'] = 'penjualan_controller/index_pesanan';
$route['pesanan/history'] = 'penjualan_controller/index_history_pesanan';
$route['pesanan/(:num)'] = 'penjualan_controller/detail_pesanan/$1';
$route['pesanan/create']['GET'] = 'penjualan_controller/create_pesanan';
$route['pesanan/create']['POST'] = 'penjualan_controller/store_pesanan';
$route['pesanan/(:num)/selesai'] = 'penjualan_controller/pesanan_selesai/$1';
$route['pesanan/(:num)/edit'] = 'penjualan_controller/edit_pesanan/$1';
$route['pesanan/edit']['POST'] = 'penjualan_controller/update_pesanan';
$route['pesanan/(:num)/payment'] = 'penjualan_controller/payment_pesanan/$1';
$route['pesanan/store_payment']['post'] = 'penjualan_controller/store_payment_pesanan';
$route['pesanan/(:num)/send'] = 'penjualan_controller/pengiriman_pesanan/$1';
$route['pesanan/store_send']['post'] = 'penjualan_controller/store_pengiriman_pesanan';
$route['pesanan/receive']['post'] = 'penjualan_controller/store_receive_pesanan';
$route['pesanan/hapus']['post'] = 'penjualan_controller/hapus_pesanan';
$route['pesanan/(:num)/print'] = 'penjualan_controller/print_pesanan/$1';
$route['pesanan/(:num)/invoice'] = 'penjualan_controller/invoice_pesanan/$1';

//b2b
$route['b2b'] = 'penjualan_controller/index_b2b';
$route['b2b/(:num)'] = 'penjualan_controller/detail_b2b/$1';
$route['b2b/create']['GET'] = 'penjualan_controller/create_b2b';
$route['b2b/create']['POST'] = 'penjualan_controller/store_b2b';
$route['b2b/(:num)/edit'] = 'penjualan_controller/edit_b2b/$1';
$route['b2b/edit']['POST'] = 'penjualan_controller/update_b2b';

//cabang
$route['cabang'] = 'penjualan_controller/index_cabang';
$route['cabang/(:num)'] = 'penjualan_controller/detail_cabang/$1';
$route['cabang/create']['GET'] = 'penjualan_controller/create_cabang';
$route['cabang/create']['POST'] = 'penjualan_controller/store_cabang';
$route['cabang/edit']['POST'] = 'penjualan_controller/update_cabang';
$route['cabang/(:num)/payment'] = 'penjualan_controller/payment_cabang/$1';
$route['cabang/store_payment']['post'] = 'penjualan_controller/store_payment_cabang';
$route['cabang/(:num)/send'] = 'penjualan_controller/pengiriman_cabang/$1';
$route['cabang/store_send']['post'] = 'penjualan_controller/store_pengiriman_cabang';

//nota
$route['nota'] = 'penjualan_controller/index_nota';
$route['nota/create']['GET'] = 'penjualan_controller/create_nota';
$route['nota/create']['POST'] = 'penjualan_controller/store_nota';
$route['nota/(:num)/payment'] = 'penjualan_controller/payment_nota/$1';
$route['nota/store_payment']['post'] = 'penjualan_controller/store_payment_nota';
$route['nota/(:num)'] = 'penjualan_controller/detail_nota/$1';

//Retur Dapur
$route['retur_barang'] = 'retur_controller/index';
$route['retur_barang/create']['GET'] = 'retur_controller/create';
$route['retur_barang/create']['POST'] = 'retur_controller/store';
$route['retur_barang/(:num)'] = 'retur_controller/detail/$1';
$route['retur_barang/(:num)/edit'] = 'retur_controller/edit/$1';
$route['retur_barang/edit']['POST'] = 'retur_controller/update';

//Ajax
$route['searchBarang'] = 'penjualan_controller/searchBarang';
$route['cekKodeBarang/(:any)'] = 'penjualan_controller/cekKodeBarang/$1';
$route['searchHistory/(:num)'] = 'penjualan_controller/searchHistory/$1';
$route['searchDetailHistory/(:num)'] = 'penjualan_controller/searchDetailHistory/$1';
$route['searchPenjualan/(:num)/(:any)'] = 'penjualan_controller/searchPenjualan/$1/$2';
$route['searchBarangMasuk'] = 'retur_controller/searchBarangMasuk';
$route['detailBarangMasuk/(:num)'] = 'retur_controller/detailBarangMasuk/$1';
$route['detailPenjualan/(:num)'] = 'penjualan_controller/detailPenjualan/$1';
$route['searchHistoryBarangMasuk/(:num)'] = 'masuk_controller/searchHistoryBarangMasuk/$1';
$route['searchPembayaran/(:any)'] = 'laporan_controller/searchPembayaranByYear/$1';
$route['searchPembayaranByDate'] = 'laporan_controller/searchPembayaranByDate';
$route['searchPembayaranDetailByDate'] = 'laporan_controller/searchPembayaranDetailByDate';
$route['searchPembayaranDPByDate'] = 'laporan_controller/searchPembayaranDPByDate';
$route['searchLaporanBarang'] = 'laporan_controller/searchLaporanBarang';
$route['searchLaporanBarangDetail'] = 'laporan_controller/searchLaporanBarangDetail';
$route['searchLaporanStokBarang'] = 'laporan_controller/searchLaporanStokBarang';
$route['searchPenjualanMember'] = 'laporan_controller/searchPenjualanMember';
$route['searchPiutangMember'] = 'laporan_controller/searchPiutangMember';
$route['searchSupplier'] = 'laporan_controller/searchSupplier';
$route['searchPesanan'] = 'laporan_controller/searchPesanan';
$route['searchLaporanDepartemen'] = 'laporan_controller/searchLaporanDepartemen';
$route['getTagihan'] = 'dashboard_controller/getTagihan';
$route['getPenjualanPending'] = 'dashboard_controller/getPenjualanPending';

$route['laporan/pembayaran'] = 'laporan_controller/pembayaran';
$route['laporan/barang'] = 'laporan_controller/barang';
$route['laporan/departemen'] = 'laporan_controller/departemen';
$route['laporan/penjualan'] = 'laporan_controller/penjualan';
$route['laporan/pesanan'] = 'laporan_controller/pesanan';
$route['laporan/pembayaranDP'] = 'laporan_controller/pembayaranDP';
$route['laporan/detailPembayaran'] = 'laporan_controller/pembayaranDetail';
$route['laporan/supplier'] = 'laporan_controller/supplier';
$route['laporan/piutang'] = 'laporan_controller/piutang';
$route['laporan/stokBarang'] = 'laporan_controller/stokBarang';
$route['laporan/barangDetail'] = 'laporan_controller/barangDetail';

$route['verifikasiPassword'] = 'user_controller/verifikasiPassword';
$route['cekUsername'] = 'user_controller/cekUsername';
$route['cekHPMember'] = 'member_controller/cekHPMember';
$route['cekKodeKue'] = 'barang_controller/cekKodeKue';
$route['getMemberPesanan'] = 'member_controller/getMemberPesanan';
$route['getSupplier'] = 'supplier_controller/getSupplier';
$route['getBarang'] = 'barang_controller/getBarang';
$route['getMember/(:num)'] = 'member_controller/getMember/$1';
$route['openRegister'] = 'penjualan_controller/openRegister';

$route['getAllMember'] = 'member_controller/getAll';
$route['getAllKue'] = 'barang_controller/getBarang';
$route['getAllBarangMasuk'] = 'masuk_controller/getAllBarangMasuk';
$route['getAllSupplier'] = 'supplier_controller/getSupplier';
$route['getAllPegawai'] = 'user_controller/getAllPegawai';
$route['getAllPOS'] = 'penjualan_controller/getALlPOS';
$route['getAllPesanan'] = 'penjualan_controller/getAllPesanan';
$route['getAllHistoryPesanan'] = 'penjualan_controller/getAllHistoryPesanan';
$route['getAllB2B'] = 'penjualan_controller/getAllB2B';
$route['getAllCabang'] = 'penjualan_controller/getAllCabang';
$route['getAllRetur'] = 'penjualan_controller/getAllRetur';
$route['getAllNota'] = 'penjualan_controller/getAllNota';
$route['getAllReturBM'] = 'retur_controller/getAllReturBM';

$route['notfound'] = 'error_controller/error404';
$route['home'] = 'dashboard_controller/index';

$route['seeder']['GET'] = 'seeder/seeder/index';
$route['seeder']['POST'] = 'seeder/seeder/run';
