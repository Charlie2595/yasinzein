<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Laporan_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}
		if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') && !$this->hasAccess('Supervisor'))
		{
			redirect('home');
		}

        $this->load->model('barang_model', 'barangModel');
        $this->load->model('supplier_model', 'supplierModel');
        $this->load->model('member_model', 'memberModel');
        $this->load->model('laporan_model', 'laporanModel');
        $this->load->model('pembayaran_model', 'pembayaranModel');
		$this->load->model('penjualan_model', 'penjualanModel');
		$this->load->helper('form_helper');
		$this->data['laporanActive'] = 'active';
	}

    public function pembayaran()
    {
        $this->data['reportPembayaranActive'] = 'active';
        if(!$this->hasAccess('Owner'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $this->data['payment'] = $this->pembayaranModel->getPembayaran();
        $this->data['result'] = $this->laporanModel->getPembayaran($payment, $tglAwal, $tglAkhir);
        if($this->input->get('payment') !== NULL)
        {
            $result = $this->laporanModel->getPembayaran($payment, $tglAwal, $tglAkhir);
            $this->data['result'] = $result;
            $this->data['idPayment'] = $payment;
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/pembayaran','master/owner_sidebar','laporan/pembayaran_js');
        }
    }

    public function pembayaranDetail()
    {
        $this->data['reportPembayaranDetailActive'] = 'active';
        if(!$this->hasAccess('Owner'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $this->data['payment'] = $this->pembayaranModel->getPembayaran();
        $this->data['result'] = $this->laporanModel->getPembayaranDetail($payment, $tglAwal, $tglAkhir);
        if($this->input->get('payment') !== NULL)
        {
            $result = $this->laporanModel->getPembayaranDetail($payment, $tglAwal, $tglAkhir);
            $this->data['result'] = $result;
            $this->data['idPayment'] = $payment;
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/pembayaran_detail','master/owner_sidebar','laporan/pembayaran_detail_js');
        }
    }

    public function pembayaranDP()
    {
        $this->data['reportPembayaranDPActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $this->data['payment'] = $this->pembayaranModel->getPembayaran();
        $this->data['result'] = $this->laporanModel->getPembayaranDP($payment, $tglAwal, $tglAkhir);
        if($this->input->get('payment') !== NULL)
        {
            $result = $this->laporanModel->getPembayaranDP($payment, $tglAwal, $tglAkhir);
            $this->data['result'] = $result;
            $this->data['idPayment'] = $payment;
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/pembayaran_dp','master/owner_sidebar','laporan/pembayaran_dp_js');
        } 
        else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/pembayaran_dp','master/kasir_sidebar','laporan/pembayaran_dp_js');
        }
    }

    public function barang()
    {
        $this->data['reportBarangActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['barang'] = $this->barangModel->getBarang("");
        $this->data['departemen'] = $this->barangModel->getDepartemen();

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/penjualan_barang','master/owner_sidebar','laporan/penjualan_barang_js');
        }
        else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/penjualan_barang','master/kasir_sidebar','laporan/penjualan_barang_js');
        }
    }

    public function barangDetail()
    {
        $this->data['reportBarangDetailActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['barang'] = $this->barangModel->getBarang("");
        $this->data['departemen'] = $this->barangModel->getDepartemen();

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/barang','master/owner_sidebar','laporan/barang_js');
        }
        else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/barang','master/kasir_sidebar','laporan/barang_js');
        }
    }

    public function departemen()
    {
        $this->data['reportDepartemenActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['departemen'] = $this->barangModel->getDepartemen();

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/departemen','master/owner_sidebar','laporan/departemen_js');
        }
        else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/departemen','master/kasir_sidebar','laporan/departemen_js');
        }
    }

    public function penjualan()
    {
        $this->data['reportPenjualanActive'] = 'active';
        if(!$this->hasAccess('Owner'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['member'] = $this->memberModel->getAllMember();
        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/penjualan','master/owner_sidebar','laporan/penjualan_js');
        }
    }

    public function stokBarang()
    {
        $this->data['reportStokBarangActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['barang'] = $this->barangModel->getBarang("");
        $this->data['departemen'] = $this->barangModel->getDepartemen();

        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/stok_barang','master/owner_sidebar','laporan/stok_barang_js');
        }
        else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/stok_barang','master/kasir_sidebar','laporan/stok_barang_js');
        }
    }

    public function supplier()
    {
        $this->data['reportSupplierActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['supplier'] = $this->supplierModel->getsupplier();
        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/supplier','master/owner_sidebar','laporan/supplier_js');
        } else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/supplier','master/kasir_sidebar','laporan/supplier_js');
        }
    }

    public function pesanan()
    {
        $this->data['reportPesananActive'] = 'active';
        if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['member'] = $this->memberModel->getAllMember();
        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/pesanan','master/owner_sidebar','laporan/pesanan_js');
        } else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor'))
        {
            parent::render_view('laporan/pesanan','master/kasir_sidebar','laporan/pesanan_js');
        }
    }

    public function piutang()
    {
        $this->data['reportPiutangActive'] = 'active';
        if(!$this->hasAccess('Owner'))
        {
            redirect(base_url("notfound"));
        }
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $this->data['tglAwal'] = $tglAwal;
        $this->data['tglAkhir'] = $tglAkhir;
        $this->data['member'] = $this->memberModel->getAllMemberCabangB2B();
        if($this->hasAccess('Owner'))
        {
            parent::render_view('laporan/piutang','master/owner_sidebar','laporan/piutang_js');
        }
    }

    public function searchLaporanBarang(){
        $barang = $this->input->get('barang');
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        $departemen = $this->input->get('departemen');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $result = $this->laporanModel->getBarang($barang, $tglAwal, $tglAkhir, $departemen);
        echo json_encode($result);
    }

    public function searchLaporanBarangDetail(){
        $barang = $this->input->get('barang');
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        $departemen = $this->input->get('departemen');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $result = $this->laporanModel->getBarangDetail($tglAwal, $tglAkhir, $departemen, $barang);
        echo json_encode($result);
    }

    public function searchLaporanStokBarang(){
        $barang = $this->input->get('barang');
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        $departemen = $this->input->get('departemen');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $result = $this->laporanModel->getStokBarang($barang, $tglAwal, $tglAkhir, $departemen);
        echo json_encode($result);
    }

    public function searchLaporanDepartemen(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        $departemen = $this->input->get('departemen');
        
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $result = $this->laporanModel->getDepartemen($tglAwal, $tglAkhir, $departemen);
        echo json_encode($result);
    }

    public function searchSupplier(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        $supplier = $this->input->get('supplier');
        
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $result = $this->laporanModel->getSupplier($tglAwal, $tglAkhir, $supplier);
        echo json_encode($result);
    }

    public function searchPembayaranByYear($year){
        $result = $this->laporanModel->getPembayaranByYear($year);
        echo json_encode($result);
    }

    public function searchPembayaranByDate(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $result = $this->laporanModel->getPembayaran($payment, $tglAwal, $tglAkhir);
        echo json_encode($result);
    }

    public function searchPembayaranDetailByDate(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $result = $this->laporanModel->getPembayaranDetail($payment, $tglAwal, $tglAkhir);
        echo json_encode($result);
    }

    public function searchPembayaranDPByDate(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $payment = $this->input->get('payment');
        $result = $this->laporanModel->getPembayaranDP($payment, $tglAwal, $tglAkhir);
        echo json_encode($result);
    }

    public function searchPenjualanMember(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $member = $this->input->get('member');
        $result = $this->laporanModel->getSalesMember($member, $tglAwal, $tglAkhir);
        echo json_encode($result);
    }

    public function searchPiutangMember(){
        $tglAwal = $this->input->get('tglAwal');
        $tglAkhir = $this->input->get('tglAkhir');
        if($tglAwal == null || $tglAwal == ""){
            $tglAwal = date("Y-m-d");
        }
        if($tglAkhir == null || $tglAkhir == ""){
            $tglAkhir = date("Y-m-d");
        }
        $member = $this->input->get('member');
        $result = $this->laporanModel->getPiutangMember($member, $tglAwal, $tglAkhir);
        echo json_encode($result);
    }
}
