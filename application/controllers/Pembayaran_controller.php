<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Pembayaran_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}
		if(!$this->hasAccess('Owner') && !$this->hasAccess('Kasir') && !$this->hasAccess('Supervisor'))
		{
			redirect('home');
		}

		$this->load->model('Pembayaran_model', 'pembayaranModel');
		$this->load->helper('form_helper');
        $this->data['page'] = "pembayaran";
		$this->data['masterActive'] = 'active';
		$this->data['pembayaranActive'] = 'active';
	}

	public function index(){
		$this->data['list'] = $this->pembayaranModel->getPembayaran();
		if ($this->hasAccess('Owner')) {
			parent::render_view('pembayaran/list', 'master/owner_sidebar', 'pembayaran/list_js');
		} elseif($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('pembayaran/list', 'master/kasir_sidebar', 'pembayaran/list_js');
        }
	}

	public function create(){

		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		
		if ($this->hasAccess('Owner')) {
			parent::render_view('pembayaran/create', 'master/owner_sidebar', 'pembayaran/create_js');
		} elseif($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('pembayaran/list', 'master/kasir_sidebar', 'pembayaran/list_js');
        }
	}

	/**
     * menyimpan data barang baru
     */
    public function store()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('pembayaran/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $nama = $this->input->post('nama');

        $result = $this->pembayaranModel->create($nama);
        echo json_encode($result);
    }

    public function edit($id)
    {
        $detail = $this->pembayaranModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['detail'] = $detail;
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if($this->hasAccess('Owner')){
            parent::render_view('pembayaran/edit', 'master/owner_sidebar', 'pembayaran/edit_js');
        } elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('pembayaran/edit', 'master/kasir_sidebar', 'pembayaran/edit_js');
        }
    }

    public function update()
    {
        if($this->input->post('id') === NULL)
        {
            redirect('pembayaran');
        }
        $this->config->set_item('language', 'indonesia');

        $id = $this->input->post('id');
        $nama = $this->input->post('nama');

        $result = $this->pembayaranModel->edit($id, $nama);
        echo json_encode($result);
    }
}