<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 13.22
 */

class Auth_controller extends Regular_Controller
{
	use CheckAccess;
	//use FormError;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form_helper');
	}

	public function index()
	{
		if($this->hasLogin())
		{
			redirect(base_url('home'));
		}
		else
		{
			if(isset($_SESSION['formValues'])) {
				$_POST = $_SESSION['formValues'];
			}
			//$this->data['rf'] = $this->agent->referrer();
			$this->data['pageTitle'] = "YASINZEIN - LOGIN";
			$this->data['csrf'] = [
				'name' => $this->security->get_csrf_token_name(),
				'hash' => $this->security->get_csrf_hash()
			];
			parent::render_view('auth/login','master/main', 'auth/login_js');
		}
	}

	public function login()
	{
		$this->lang->load('form_validation_lang', 'indonesia');
		$this->config->set_item('language', 'indonesia');
		$this->load->model('pegawai_model','pegawaiModel');
		$this->load->library('form_validation');
		if($this->input->post('btnLogin') !== NULL)
		{
			$username = $this->input->post('txtUsername');
			$password = $this->input->post('txtPassword');
			$login = $this->pegawaiModel->login($username, $password);
			if($login === NULL)
			{
				$_SESSION['formError'] = ['errMsg' => 'Username / password anda salah'];
				$this->session->mark_as_flash('formError');
				redirect(base_url('login'));
			}
			else
			{
				$_SESSION['idLogin'] = $login['id'];
				$_SESSION['nama'] = $login['nama'];
				$_SESSION['jabatan'] = $login['jabatan'];
				
				$this->session->sess_regenerate();
				redirect(base_url('home'));
			}
		}
		else
		{
			redirect(base_url('login'));
		}
	}

	public function logout()
	{
		if(isset($_SESSION['idLogin']))
		{
			unset($_SESSION['idLogin']);
			unset($_SESSION['nama']);
			unset($_SESSION['jabatan']);
			unset($_SESSION['level']);

			$this->session->sess_regenerate(TRUE);

			$_SESSION['logoutMsg'] = "Anda berhasil logout dari sistem";
			$this->session->mark_as_flash('logoutMsg');
			redirect(base_url('login'));
		}
		else
		{
			redirect(base_url("login"));
		}
	}
}
