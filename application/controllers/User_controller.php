<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.07
 */

class User_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}
		
		$this->load->model('pegawai_model', 'pegawaiModel');
		$this->load->helper('form_helper');
		$this->data['masterActive'] = 'active';
		$this->data['pegawaiActive'] = 'active';
		$this->data['page'] = 'pegawai';
	}

	/**
	 *  Menampilkan semua user
	 */
	public function index()
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('user/list', 'master/owner_sidebar', 'user/list_js');
		}
	}

	public function getAllPegawai(){
		$result = $this->pegawaiModel->getActiveUsers();
		echo json_encode($result);
	}

	/**
	 * Menampilkan detail user tertentu berdasarkan $idUser
	 * @param $idUser
	 */
	public function show($idUser)
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		$user = $this->pegawaiModel->getActiveUser($idUser);
		if(!isset($user))
		{
			parent::render_view('errors/html/custom_error404', 'master/main');
		}
		else
		{
			$this->data['activeUser'] = $user;
			if ($this->hasAccess('Owner')) {
				parent::render_view('user/user_profile', 'master/owner_sidebar', 'user/user_profile_js');
			}
			
		}
	}

	/**
	 *  Menampilkan form untuk membuat user account baru
	 */
	public function create()
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		if(isset($_SESSION['formValues'])) {
			$_POST = $_SESSION['formValues'];
		}
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		$this->data['page'] = "pegawai";
		if ($this->hasAccess('Owner')) {
			parent::render_view('user/create', 'master/owner_sidebar', 'user/create_js');
		}
		
	}

	/**
	 * menyimpan user account baru
	 */
	public function store()
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		if($this->input->post() === NULL)
		{
			redirect(base_url('user/create'));
		}
		$this->lang->load('form_validation_lang', 'indonesia');
		$this->config->set_item('language', 'indonesia');
		$this->load->library('form_validation');

		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$jabatan = $this->input->post('jabatan');
		$password = $this->input->post('password');
		$notelp = $this->input->post('notelp');
		$ktp = $this->input->post('ktp');

		$this->pegawaiModel->createUser($username, $nama, $jabatan, $password, $notelp, $ktp);
		$_SESSION['createSucceedMsg'] = "User Account ". $this->sanitize_output($username). " berhasil dibuat";
		$this->session->mark_as_flash('createSucceedMsg');

		redirect(base_url('pegawai'));
	}

	public function username_available($username)
	{
		$this->form_validation->set_message('username_available','{field} tidak tersedia');
		return $this->pegawaiModel->isAvailableUsername($username);
	}

	public function editusername_callable($username){
		$this->form_validation->set_message('editusername_callable', '{field} tidak dapat dipakai karena sudah ada.');
		return $this->pegawaiModel->isEditUsernameAvailable($username);
	}

	public function kodeAnggota_available($kode)
	{
		$this->form_validation->set_message('idAnggota_available','{field} tidak tersedia');
		return $this->pegawaiModel->isAvailableIdAnggota($kode);
	}

	public function editkodeanggota_callable($kode){
		$this->form_validation->set_message('editkodeanggota_callable', '{field} tidak tersedia');
		return $this->pegawaiModel->isEditKodeAvailable($kode);
	}

	/**
	 * Menampilkan form untuk mengubah user account
	 * @param $idUser
	 */
	public function edit($idUser)
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		$user = $this->pegawaiModel->getActiveUser($idUser);
		$this->data['page'] = "pegawai";
		if(!isset($user))
		{
			parent::render_view('errors/html/custom_error404', 'master/main');
		}
		else
		{
			// belum selesai
			if(isset($_SESSION['formValues'])) {
				$_POST = $_SESSION['formValues'];
			}
			$_SESSION['lastUsername'] = $user['username'];
			$_SESSION['idEditedUser'] = $user['id'];

			$this->data['activeUser'] = $user;
			$this->data['csrf'] = [
				'name' => $this->security->get_csrf_token_name(),
				'hash' => $this->security->get_csrf_hash()
			];
			if ($this->hasAccess('Owner')) {
				parent::render_view('user/edit', 'master/owner_sidebar', 'user/edit_js');
			}
		}
	}

	public function update()
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		if($this->input->post('idUser') === NULL)
		{
			redirect('users');
		}
		$this->lang->load('form_validation_lang', 'indonesia');
		$this->config->set_item('language', 'indonesia');
		$this->load->library('form_validation');
		$id = $this->sanitize_output($this->input->post('idUser'));
		$username = $this->input->post('username');
		$nama = $this->input->post('nama');
		$providedPassword = $this->input->post('password');
		$jabatan = $this->input->post('jabatan');
		$notelp = $this->input->post('notelp');
		$ktp = $this->input->post('ktp');

		$fullEdit = strlen($providedPassword) > 0 ? TRUE : FALSE;

		$this->pegawaiModel->updateUser($id, $nama, $username, $providedPassword ,$jabatan, $notelp, $ktp, $fullEdit);
		$_SESSION['createSucceedMsg'] = "Data ". $nama ." berhasil diubah";
		$this->session->mark_as_flash('createSucceedMsg');
		redirect(base_url('pegawai'));
	}

	public function deactivate($idUser)
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		$user = $this->pegawaiModel->getActiveUser($idUser);
		$result = $this->pegawaiModel->deactivateUser($user);

		echo json_encode($result);
	}

	public function deactivateConfirmation($idUser)
	{
		if(!$this->hasAccess('Owner'))
		{
			redirect('home');
		}
		$user = $this->pegawaiModel->getActiveUser($idUser);
		if($user === NULL)
		{
			redirect(base_url('notfound'));
		}

		$this->data['user'] = $user;
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		if ($this->hasAccess('Owner')) {
			parent::render_view('user/user_deactivate', 'master/superuser_sidebar');
		}
	}

	public function cekUsername(){
		$cek = $this->pegawaiModel->isAvailableUsername();
		if($cek == 0)
		{
			echo "true";  //good to register
		}
		else
		{
			echo "false"; //already registered
		}
	}

	public function verifikasiPassword(){
		$cek = $this->pegawaiModel->verifikasiPassword();
		if($cek == 0)
		{
			echo "true"; 
		}
		else
		{
			echo "false";
		}
	}
}
