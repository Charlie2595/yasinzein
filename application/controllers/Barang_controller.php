<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
require_once APPPATH."/third_party/PHPExcel/Classes/PHPExcel.php";
ini_set('memory_limit','1024M');
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Barang_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}

        $this->load->model('barang_model', 'barangModel');
		$this->load->model('supplier_model', 'supplierModel');
		$this->load->helper('form_helper');
        $this->data['page'] = "barang";
        //$this->load->library('excel');
        $config['upload_path'] = 'assets/propic/';
        $config['allowed_types'] = 'xls,xlsx';
        $config['max_size'] = '10000';
        $this->load->library('upload', $config);
        $this->load->library('excel');
	}

	public function index(){
        $this->data['masterActive'] = 'active';
        $this->data['barangActive'] = 'active';
		if ($this->hasAccess('Owner')) {
			parent::render_view('barang/list', 'master/owner_sidebar', 'barang/list_js');
		} else if ($this->hasAccess('Dapur')){
            parent::render_view('barang/list', 'master/dapur_sidebar', 'barang/list_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang/list', 'master/kasir_sidebar', 'barang/list_js');
        }
	}

	public function create(){
		if(!$this->hasAccess('Owner') && !$this->hasAccess('Dapur'))
		{
			redirect('home');
		}
        $this->data['masterActive'] = 'active';
        $this->data['barangActive'] = 'active';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		
		if ($this->hasAccess('Owner')) {
			parent::render_view('barang/create', 'master/owner_sidebar', 'barang/create_js');
		} else if ($this->hasAccess('Dapur')){
            parent::render_view('barang/create', 'master/dapur_sidebar', 'barang/create_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang/create', 'master/kasir_sidebar', 'barang/create_js');
        }
	}

	/**
     * menyimpan data barang baru
     */
    public function store()
    {
		if(!$this->hasAccess('Owner') && !$this->hasAccess('Dapur'))
		{
			redirect('home');
		}
        if($this->input->post() === NULL)
        {
            redirect(base_url('barang/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $jenis = $this->input->post('jenis');
        $departemen = $this->input->post('departemen');
        $harga = $this->input->post('harga');
        $produksi = $this->input->post('produksi');
        $stok = 0;
        $diskon = 0;
        
        $result = $this->barangModel->create($kode, $nama, $jenis, $departemen, $produksi, $harga, $stok, $diskon);
        echo json_encode($result);
    }

    /**
     * Menampilkan form untuk mengubah data barang
     * @param $id
     */
    public function edit($id)
    {   
        $this->data['masterActive'] = 'active';
        $this->data['barangActive'] = 'active';
        $detail = $this->barangModel->getBarangById($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }
            $this->data['detail'] = $detail;
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang/edit', 'master/owner_sidebar', 'barang/edit_js');
        } else if ($this->hasAccess('Dapur')){
            parent::render_view('barang/edit', 'master/dapur_sidebar', 'barang/edit_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang/edit', 'master/kasir_sidebar', 'barang/edit_js');
        }
    }

    public function update()
    {
		if(!$this->hasAccess('Owner') && !$this->hasAccess('Dapur'))
		{
			redirect('home');
		}
        if($this->input->post('id') === NULL)
        {
            redirect('barang');
        }
        $this->config->set_item('language', 'indonesia');

        $id = $this->sanitize_output($this->input->post('id'));
        $nama = $this->input->post('nama');
        $jenis = $this->input->post('jenis');
        $departemen = $this->input->post('departemen');
        $harga = $this->input->post('harga');
        $produksi = $this->input->post('produksi');

        $result = $this->barangModel->edit($id, $nama, $jenis, $departemen, $harga, $produksi);
        echo json_encode($result);
    }

    public function delete($id){
        $this->data['masterActive'] = 'active';
        $this->data['barangActive'] = 'active';
        $detail = $this->barangModel->getBarangById($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else{
            $result = $this->barangModel->delete($id);
            echo json_encode($result);
        }
    }

    /**
     *  Menampilkan form untuk import barang
     */
    public function import() {
        $this->data['masterActive'] = 'active';
        $this->data['barangActive'] = 'active';
        if(isset($_SESSION['formValues'])) {
            $_POST = $_SESSION['formValues'];
        }
        $this->data['csrf'] = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        ];
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang/import', 'master/owner_sidebar', 'barang/import_js');
        } else if ($this->hasAccess('Dapur')){
            parent::render_view('barang/import', 'master/dapur_sidebar', 'barang/import_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang/import', 'master/kasir_sidebar', 'barang/import_js');
        }
    }

    public function read() {
        if(isset($_FILES["file"]["name"]))
        {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);

            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $jenis = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $departemen = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $kode = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $nama = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $produksi = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $harga = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $stok = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $diskon = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $data[] = array(
                        'kode'       =>  $kode,
                        'nama'       =>  $nama,
                        'jenis'      =>  $jenis,
                        'departemen' =>  $departemen,
                        'harga'      =>  $harga,
                        'stok'       =>  $stok,
                        'produksi'   =>  $produksi,
                        'diskon'     =>  $diskon,
                    );
                    $this->barangModel->create($kode, $nama, $jenis, $departemen, $produksi, $harga, $stok, $diskon);
                }
            }
            echo json_encode($data);
        }
    }

    public function index_barang_masuk()
    {
        $this->data['barangMasukActive'] = 'active';
        $this->data['page'] = "barang_masuk";
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang_masuk/list', 'master/owner_sidebar', 'barang_masuk/list_js');
        } else if ($this->hasAccess('Dapur')){
            parent::render_view('barang_masuk/list', 'master/dapur_sidebar', 'barang_masuk/list_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/list', 'master/kasir_sidebar', 'barang_masuk/list_js');
        }
    }

    public function cekKodeKue(){
        $cek = $this->barangModel->isAvailableKode();
        if($cek == 0)
        {
            echo "true";  //good to register
        }
        else
        {
            echo "false"; //already registered
        }
    }

    public function create_barang_masuk(){
        $this->data['barangMasukActive'] = 'active';
        $this->data['csrf'] = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        ];
        $this->data['collapse'] = "sidebar-collapse";
        $this->data['page'] = "barang_masuk";
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang_masuk/create', 'master/owner_sidebar', 'barang_masuk/create_js');
        } else if ($this->hasAccess('Dapur')){
            parent::render_view('barang_masuk/create', 'master/dapur_sidebar', 'barang_masuk/create_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/create', 'master/kasir_sidebar', 'barang_masuk/create_js');
        }
    }

    public function searchHistoryBarangMasuk($id){
        $result = $this->barangMasukModel->searchHistory($id);
        echo json_encode($result);
    }

    public function getBarang(){
        $idSup = $this->input->get('supplier');
        $result = $this->barangModel->getBarang($idSup);
        echo json_encode($result);
    }

    public function getAllBarangMasuk(){
        $result = $this->barangMasukModel->getBarangMasuk();
        echo json_encode($result);
    }

    /**
     * menyimpan data barang baru
     */
    public function store_barang_masuk()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('barang_masuk/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $tanggal = $this->input->post('tgl');
        $idSupplier = $this->input->post('idSupplier');
        $idPegawai = $this->input->post('idPegawai');

        //Barang
        $barang = $this->input->post('barang');

        $detail = [];
        foreach ($barang as $value) {
            $detail[] = array(
                'idBarang' => $value['id'],
                'jumlah' => $value['jumlah'],
                'harga_beli' => $value['hargaBeli'],
                'harga_jual' => $value['hargaJual']
            );
        }
        $result = $this->barangMasukModel->createBarangMasuk($tanggal, $idSupplier, $idPegawai, $detail);
        echo json_encode($result);
    }

    public function detail_barang_masuk($id)
    {
        $this->data['barangMasukActive'] = 'active';
        $detail = $this->barangMasukModel->getDetailBarangMasuk($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            if ($this->hasAccess('Owner')) {
                parent::render_view('barang_masuk/detail', 'master/owner_sidebar', 'barang_masuk/detail_js');
            } else if ($this->hasAccess('Dapur')){
                parent::render_view('barang_masuk/detail', 'master/dapur_sidebar', 'barang_masuk/detail_js');
            } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
                parent::render_view('barang_masuk/detail', 'master/kasir_sidebar', 'barang_masuk/detail_js');
            }
        }
    }
}
