<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.04
 */

class Dashboard_controller extends Regular_Controller
{
	use CheckAccess;

	public function __construct()
	{
		parent::__construct();
		if(!$this->hasLogin()){
			redirect(base_url('login'));
		}
		$this->data['pageTitle'] = 'YASINZEIN - Home';

		// nantinya nilai active disisipkan ke class link sidebar utk menandai menu mana yg aktif.
		$this->data['homeActive'] = 'active';
		$this->load->model('barang_model', 'barangModel');
		$this->load->model('masuk_model', 'masukModel');
		$this->load->model('penjualan_model', 'penjualanModel');
	}

	public function index()
	{
		if($this->hasAccess('Owner'))
		{
			$this->owner();
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			$this->kasir();
		}
		elseif ($this->hasAccess('Dapur')){
			$this->dapur();
		}
		else
		{
			redirect('login');
		}
	}

	public function owner(){
		if(!$this->hasAccess('Owner'))
        {
            redirect(base_url("notfound"));
        }
        $tgl = date("d-m-Y");
        $this->data['result'] = $this->barangModel->getProduksi($tgl);

        if($this->input->get('tgl') !== NULL)
        {
            $tgl = $this->input->get('tgl');

            $result = $this->barangModel->getProduksi($tgl);
            $this->data['result'] = $result;
        }
        $this->data['tgl'] = $tgl;
		$this->data['totalPenjualanPending'] = count($this->penjualanModel->getPenjualanPending());
		$this->data['totalTagihan'] = count($this->masukModel->getTagihan());
		
        if($this->hasAccess('Owner')){
            parent::render_view('dashboard/owner','master/owner_sidebar', 'dashboard/owner_js');
        } elseif($this->hasAccess('Dapur')){
        	parent::render_view('dashboard/dapur','master/dapur_sidebar', 'dashboard/dapur_js');
        }
	}

	public function dapur(){
		parent::render_view('dashboard/dapur','master/dapur_sidebar');
	}

	public function kasir(){
		$this->data['totalPenjualanPending'] = count($this->penjualanModel->getPenjualanPending());
		parent::render_view('dashboard/kasir','master/kasir_sidebar', 'dashboard/kasir_js');
	}

	public function getTagihan(){
		$result = $this->masukModel->getTagihan();
		echo json_encode($result);
	}

	public function getPenjualanPending(){
		$result = $this->penjualanModel->getPenjualanPending();
		echo json_encode($result);
	}
}
