<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Retur_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * Retur_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}

        $this->load->model('barang_model', 'barangModel');
		$this->load->model('supplier_model', 'supplierModel');
		$this->load->model('retur_model', 'returModel');
		$this->load->helper('form_helper');
        $this->data['page'] = "retur_barang";
	}

	public function index(){
        $this->data['returActive'] = 'active';
        $this->data['returCabangActive'] = 'active';
		if ($this->hasAccess('Owner')) {
			parent::render_view('retur/list', 'master/owner_sidebar', 'retur/list_js');
		} else if ($this->hasAccess('Dapur')) {
			parent::render_view('retur/list', 'master/dapur_sidebar', 'retur/list_js');
		} else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('retur/list', 'master/kasir_sidebar', 'retur/list_js');
		}
	}

    public function getAllReturBM(){
        $result = $this->returModel->getList();
        echo json_encode($result);
    }

	public function create(){
        $this->data['returActive'] = 'active';
        $this->data['returCabangActive'] = 'active';
        $this->data['collapse'] = "sidebar-collapse";
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		$this->data['barang'] = $this->barangModel->getBarang("");
        $this->data['supplier'] = $this->supplierModel->getSupplier();
		
		if ($this->hasAccess('Owner')) {
			parent::render_view('retur/create', 'master/owner_sidebar', 'retur/create_js');
		} else if ($this->hasAccess('Dapur')) {
			parent::render_view('retur/create', 'master/dapur_sidebar', 'retur/create_js');
		} else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('retur/create', 'master/kasir_sidebar', 'retur/create_js');
		}
	}

	/**
     * menyimpan data retur baru
     */
    public function store()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('barang_masuk/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idSupplier = $this->input->post('idSupplier');
        $tgl = $this->input->post('tgl');
        $idPegawai = $this->input->post('idPegawai');
        $idBarangMasuk = $this->input->post('idBarangMasuk');
        $totalBarang = $this->input->post('grandtotal');
        //Barang
        $barang = $this->input->post('barang');
        $detail = [];
        foreach ($barang as $value) {
            $detail[] = array(
                'idDetail' => $value['idDetail'],
                'idBarang' => $value['idBarang'],
                'jumlahRetur' => $value['retur']
            );
        }
        $result = $this->returModel->createRetur($idSupplier, $tgl, $idPegawai, $idBarangMasuk, $detail, $totalBarang);
        echo json_encode($result);
    }

    public function searchBarangMasuk(){
        $id = $_GET['idSupplier'];
        $result = $this->returModel->searchBarangMasuk($id);
        echo json_encode($result);
    }

    public function detailBarangMasuk($id){
        $result = $this->returModel->detailBarangMasuk($id);
        echo json_encode($result);
    }

    /**
     * Menampilkan detail Penjualan POS tertentu berdasarkan $id
     * @param $id
     */
    public function detail($id)
    {
    	$this->data['returActive'] = 'active';
        $this->data['returCabangActive'] = 'active';
        $detail = $this->returModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
			if ($this->hasAccess('Owner')) {
				parent::render_view('retur/detail', 'master/owner_sidebar', 'retur/detail_js');
			}
			elseif ($this->hasAccess('Dapur')){
				parent::render_view('retur/detail', 'master/dapur_sidebar', 'retur/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('retur/detail', 'master/kasir_sidebar', 'retur/detail_js');
			}
        }
    }

    public function edit($id){
		$this->data['b2bActive'] = 'active';
		$this->data['page'] = 'retur_barang';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->returModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['data'] = $detail;
			$this->data['barang'] = $this->barangModel->getBarang("");
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
            parent::render_view('retur/edit', 'master/owner_sidebar', 'retur/edit_js');
        }
        elseif ($this->hasAccess('Dapur')){
            parent::render_view('retur/edit', 'master/dapur_sidebar', 'retur/edit_js');
        }
        elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('retur/edit', 'master/kasir_sidebar', 'retur/edit_js');
        }
	}

	public function update(){
		if($this->input->post('idMasuk') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
			$idSup = $this->input->post('idSup');
			$tgl = $this->input->post('tgl');
			$idPegawai = $this->input->post('idPegawai');
			$type = $this->input->post('type');
			$grandtotal = $this->input->post('grandtotal');
			$totalItem = $this->input->post('totalItem');
			$total = $this->input->post('total');
		
	        $idMasuk = $this->input->post('idMasuk');

			//Barang
			$barang = $this->input->post('barang');
			$detail = [];
	        foreach ($barang as $value) {
				$detail[] = array(
					'idDetail' => $value['id_masukdetail'],
					'idBarang' => $value['id'],
					'jumlahRetur' => $value['retur'],
				);
			}
			$result = $this->returModel->update($idMasuk, $tgl, $idPegawai, $grandtotal, $total, $detail, $totalItem);
			
	        echo json_encode($result);
	    }
	}
}
