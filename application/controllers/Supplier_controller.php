<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Supplier_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}

		$this->load->model('supplier_model', 'supplierModel');
        $this->load->model('barang_model', 'barangModel');
		$this->load->helper('form_helper');
        $this->data['page'] = "supplier";
        $this->data['masterActive'] = 'active';
		$this->data['supplierActive'] = 'active';
	}

	public function index(){
		$this->data['list'] = $this->supplierModel->getSupplier();
		if ($this->hasAccess('Owner')) {
			parent::render_view('supplier/list', 'master/owner_sidebar', 'supplier/list_js');
		} else if($this->hasAccess('Dapur')){
            parent::render_view('supplier/list', 'master/dapur_sidebar', 'supplier/list_js');
        } else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('supplier/list', 'master/kasir_sidebar', 'supplier/list_js');
        }
	}

	public function create(){

		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
        $idSup = "";
		$this->data['barang'] = $this->barangModel->getBarang($idSup);
		if ($this->hasAccess('Owner')) {
			parent::render_view('supplier/create', 'master/owner_sidebar', 'supplier/create_js');
		} elseif($this->hasAccess('Dapur')){
            parent::render_view('supplier/create', 'master/dapur_sidebar', 'supplier/create_js');
        } else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('supplier/create', 'master/kasir_sidebar', 'supplier/create_js');
        }
	}

	/**
     * menyimpan data barang baru
     */
    public function store()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('supplier/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $rumah = $this->input->post('rumah');
        $hp = $this->input->post('hp');
        $keterangan = $this->input->post('keterangan');
        $kue = $this->input->post('kue');

        $result = $this->supplierModel->create($nama, $email, $rumah, $hp, $keterangan, $kue);
        echo json_encode($result);
    }

    /**
     * Menampilkan form untuk mengubah data barang
     * @param $id
     */
    public function edit($id)
    {
        $detail = $this->supplierModel->get($id);
        $idSup = "";
        $this->data['barang'] = $this->barangModel->getBarang($idSup);
        $this->data['kueSupplier'] = $this->supplierModel->getKueSupplier($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['detail'] = $detail;
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
            parent::render_view('supplier/edit', 'master/owner_sidebar', 'supplier/edit_js');
        } elseif($this->hasAccess('Dapur')){
            parent::render_view('supplier/edit', 'master/dapur_sidebar', 'supplier/edit_js');
        } else if($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('supplier/edit', 'master/kasir_sidebar', 'supplier/edit_js');
        }
    }

    public function update()
    {
        if($this->input->post('id') === NULL)
        {
            redirect('supplier');
        }
        $this->config->set_item('language', 'indonesia');

        $id = $this->sanitize_output($this->input->post('id'));
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $rumah = $this->input->post('rumah');
        $hp = $this->input->post('hp');
        $keterangan = $this->input->post('keterangan');
        $kue = $this->input->post('kue');

        $result = $this->supplierModel->edit($id, $nama, $email, $rumah, $hp, $keterangan, $kue);
        echo json_encode($result);
    }

    public function delete($id){
        $detail = $this->supplierModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else{
            $result = $this->supplierModel->delete($id);
            echo json_encode($result);
        }
    }

    public function getSupplier(){
        $result = $this->supplierModel->getsupplier();
        echo json_encode($result);
    }

    public function getKueSupplier(){
        $id = $this->input->get('id');
        $result = $this->supplierModel->getKueSupplier($id);
        echo json_encode($result);
    }
}
