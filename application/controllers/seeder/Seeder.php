<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use App\Core\Controllers\Regular_Controller;
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 15/03/2017
 * Time: 18:17
 */
class Seeder extends Regular_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['csrf'] = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        ];
        parent::render_view('seeder/index','master/main');
    }

    public function run()
    {
        if($this->input->post('submitSeed'))
        {
            $this->load->model('seeder/seeder_model','seederModel');
            $this->seederModel->seed();
            //$this->seederAlexModel->seed();

            $_SESSION['seederMsg'] = 'seeder succeed';
            $this->session->mark_as_flash('seederMsg');
            redirect(base_url('seeder'));
        }
        else
        {
            echo "NOT ALLOWED";
        }
    }
}