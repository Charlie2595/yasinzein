<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Penjualan_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}

		$this->load->model('penjualan_model', 'penjualanModel');
		$this->load->model('member_model', 'memberModel');
		$this->load->model('pegawai_model', 'pegawaiModel');
		$this->load->model('barang_model', 'barangModel');
		$this->load->model('pembayaran_model', 'pembayaranModel');
		$this->load->helper('form_helper');
	}

	public function index_pos(){
		$this->data['penjualanActive'] = 'active';
		$this->data['page'] = 'pos';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pos/list', 'master/owner_sidebar', 'penjualan/pos/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pos/list', 'master/kasir_sidebar', 'penjualan/pos/list_js');
		}
	}

	public function getAllPOS(){
		$result = $this->penjualanModel->getPOS();
		echo json_encode($result);
	}

	public function create_pos(){
		$this->data['penjualanActive'] = 'active';
		$this->data['page'] = 'pos';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";
		$this->data['member'] = $this->memberModel->getMember(0);
        $this->data['payment'] = $this->pembayaranModel->getPembayaran();
		$this->data['barang'] = $this->barangModel->getBarang("");
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pos/create', 'master/owner_sidebar', 'penjualan/pos/create_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pos/create', 'master/kasir_sidebar', 'penjualan/pos/create_js');
		}
	}

	public function store_pos(){
		if($this->input->post() === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $diskonMember = $this->input->post('diskonMember');
        $idPegawai = $this->input->post('idPegawai');
        $type = $this->input->post('type');
        $pemotongan = $this->input->post('pemotongan');
        $total = $this->input->post('totalPenjualan');
        $grandtotal = $this->input->post('grandtotal');
        $kembalian = $this->input->post('kembalian');
        $jam = "";
        $tipetrans = "POS";

        $detail = [];
        $barang = $this->input->post('barang');
        // var_dump($barang);die();
		foreach ($barang as $value) {
			$detail[] = array(
		        'idBarang' => $value['id'],
		        'harga' => $value['harga'],
		        'diskonBarang' => 0,
		        'jumlah' => $value['jumlah']
		    );
		}
        $detailPembayaran = [];
        if($type == "simpan"){
        	$result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);
        }
        elseif ($type == "payment") {
	        //Payment
	        $payment = $this->input->post('payment');

			foreach ($payment as $value) {
				$detailPembayaran[] = array(
			        'idPembayaran' => $value['id'],
			        'jumlah' => $value['jumlah'],
			        'keterangan' => $value['keterangan']
			    );
			}
	        $result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);
        }
        echo json_encode($result);
	}

	public function payment_pos($id) {
		$this->data['penjualanActive'] = 'active';
    	$this->data['page'] = 'pos';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pos/pembayaran', 'master/owner_sidebar', 'penjualan/pos/pembayaran_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pos/pembayaran', 'master/kasir_sidebar', 'penjualan/pos/pembayaran_js');
		}
    }

    public function store_payment_pos() {
			if($this->input->post('idPenjualan') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	        $diskonMember = $this->input->post('diskonMember');
	        $total = $this->input->post('total');
	        $pemotongan = $this->input->post('pemotongan');
	        $grandtotal = $this->input->post('grandtotal');
	        $kembalian = $this->input->post('kembalian');
	        $tipetrans = "POS";
	        $status = 1;

	        $deposit = null;
	        $detail = [];
			$payment = $this->input->post('payment');
			foreach ($payment as $value) {
				$detail[] = array(
			        'idPembayaran' => $value['id'],
			        'jumlah' => $value['jumlah'],
			        'keterangan' => $value['keterangan']
			    );
			}
	        $result = $this->penjualanModel->create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detail, $idPenjualan, $tipetrans, $deposit, $kembalian, $status);
			echo json_encode($result);
	    }
	}

	public function edit_pos($id){
		$this->data['penjualanActive'] = 'active';
		$this->data['page'] = 'pos';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id, "Pesanan");
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
			$this->data['barang'] = $this->barangModel->getBarang("");
			$this->data['member'] = $this->memberModel->getMember(0);
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pos/edit', 'master/owner_sidebar', 'penjualan/pos/edit_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pos/edit', 'master/kasir_sidebar', 'penjualan/pos/edit_js');
		}
	}

	public function update_pos(){
		if($this->input->post('idPenjualan') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
	        $idPenjualan = $this->input->post('idPenjualan');
	        $tgl = $this->input->post('tgl');
	        $diskonMember = $this->input->post('diskonMember');
	        $idPegawai = $this->input->post('idPegawai');
	        $type = $this->input->post('type');
	        $pemotongan = $this->input->post('pemotongan');
	        $grandtotal = $this->input->post('grandtotal');
	        $total = $this->input->post('total');
	        $jam = "";
	        $tipetrans = "POS";
	        $tglKirim = null;
	        $jamKirim = null;
	        $alamat = null;

	        //Barang
	        $detailPembayaran = [];
	        $detail = [];
	        $barang = $this->input->post('barang');
			foreach ($barang as $value) {
				$detail[] = array(
			        'idBarang' => $value['id'],
			        'harga' => $value['harga'],
			        'diskonBarang' => $value['diskonBarang'],
			        'jumlah' => $value['jumlah']
			    );
			}

	        if($type == "simpan"){
	        	$result = $this->penjualanModel->update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);
	        }
	        elseif ($type == "payment") {
		        //Payment
		        $payment = $this->input->post('payment');

				foreach ($payment as $value) {
					$detailPembayaran[] = array(
				        'idPembayaran' => $value['id'],
				        'jumlah' => $value['jumlah'],
				        'keterangan' => $value['keterangan']
				    );
				}
		        $result = $this->penjualanModel->update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);
	        }
	        echo json_encode($result);
	    }
	}

	/**
     * Menampilkan detail Penjualan POS tertentu berdasarkan $id
     * @param $id
     */
    public function detail_pos($id)
    {
    	$this->data['penjualanActive'] = 'active';
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'POS';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/detail', 'master/owner_sidebar', 'penjualan/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/detail', 'master/kasir_sidebar', 'penjualan/detail_js');
			}
        }
    }

    public function print_pos($id){
    	$this->data['penjualanActive'] = 'active';
    	$detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'pos';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/print', 'master/owner_sidebar', 'penjualan/print_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/print', 'master/kasir_sidebar', 'penjualan/print_js');
			}
        }
    }

    public function invoice_pos($id){
    	$this->data['penjualanActive'] = 'active';
    	$detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'POS';
			parent::render_view('penjualan/invoice');
        }
    }

	//Pesanan Penjualan
	public function index_pesanan(){
		$this->data['pesananActive'] = 'active';
		$this->data['listPesananActive'] = 'active';
		$this->data['page'] = 'pesanan';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/list', 'master/owner_sidebar', 'penjualan/pesanan/list_js');
		}
		elseif ($this->hasAccess('Dapur')){
			parent::render_view('penjualan/pesanan/list', 'master/dapur_sidebar', 'penjualan/pesanan/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pesanan/list', 'master/kasir_sidebar', 'penjualan/pesanan/list_js');
		}
		else {
			redirect('home');
		}
	}

	//Pesanan Penjualan
	public function index_history_pesanan(){
		$this->data['pesananActive'] = 'active';
		$this->data['historyPesananActive'] = 'active';
		$this->data['page'] = 'pesanan';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/history_list', 'master/owner_sidebar', 'penjualan/pesanan/history_list_js');
		}
		elseif ($this->hasAccess('Dapur')){
			parent::render_view('penjualan/pesanan/history_list', 'master/dapur_sidebar', 'penjualan/pesanan/history_list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/pesanan/history_list', 'master/kasir_sidebar', 'penjualan/pesanan/history_list_js');
		}
		else {
			redirect('home');
		}
	}
	
	public function pesanan_selesai($id)
	{
		$this->data['pesananActive'] = 'active';
		$this->data['page'] = 'pesanan';
        $detail = $this->penjualanModel->get($id, "Pesanan");
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else{
            $result = $this->penjualanModel->selesai($id);
            echo json_encode($result);
        }
	}

	public function getAllPesanan(){
		$result = $this->penjualanModel->getPesanan();
		echo json_encode($result);
	}

	public function getAllHistoryPesanan(){
		$result = $this->penjualanModel->getHistoryPesanan();
		echo json_encode($result);
	}

	public function create_pesanan(){
		$this->data['pesananActive'] = 'active';
		$this->data['page'] = 'pesanan';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";
		$this->data['payment'] = $this->pembayaranModel->getPembayaran();
		$this->data['member'] = $this->memberModel->getMember(0);
		$this->data['barang'] = $this->barangModel->getBarang("");
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/create', 'master/owner_sidebar', 'penjualan/pesanan/create_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/pesanan/create', 'master/kasir_sidebar', 'penjualan/pesanan/create_js');
		} else {
			redirect('home');
		}
		// elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
		// 	parent::render_view('penjualan/pesanan/create', 'master/kasir_sidebar', 'penjualan/pesanan/create_js');
		// }
	}

	public function store_pesanan(){
		if($this->input->post() === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $diskonMember = $this->input->post('diskonMember');
        $idPegawai = $this->input->post('idPegawai');
        $type = $this->input->post('type');
        $pemotongan = $this->input->post('pemotongan');
        $grandtotal = $this->input->post('grandtotal');
        $total = $this->input->post('totalPenjualan');
        $jam = $this->input->post('jam');
        $tipetrans = "Pesanan";
        $kembalian = 0;
        $cekKirim = $this->input->post('kirim');
        $tglKirim = null;
        $jamKirim = null;
        $alamat = null;
        if($cekKirim == 'kirim'){
        	$tglKirim = $this->input->post('tglKirim');
	        $jamKirim = $this->input->post('jamKirim');
	        $alamat = $this->input->post('alamat');
        }

        //Barang
        $barang = $this->input->post('barang');
        $detail = [];
	    foreach ($barang as $value) {
        	$detail[] = array(
                'idBarang' => $value['id'],
                'harga' => $value['harga'],
                'diskonBarang' => $value['diskonBarang'],
                'jumlah' => $value['jumlah']
            );
        }
        $detailPembayaran = [];
        if($type == "simpan"){
        	$result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);
        }
        elseif ($type == "payment") {
	        //Payment
	        $payment = $this->input->post('payment');

	        foreach ($payment as $value) {
	        	$detailPembayaran[] = array(
	                'idPembayaran' => $value['id'],
	                'jumlah' => $value['jumlah'],
	                'keterangan' => $value['keterangan']
	            );
	        }
	        $result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);
        }
        echo json_encode($result);
        // $_SESSION['createSucceedMsg'] = "Pesanan ". $this->sanitize_output($tgl). " berhasil dibuat";
        // $this->session->mark_as_flash('createSucceedMsg');

        // redirect(base_url('pesanan'));
	}

	public function edit_pesanan($id){
		$this->data['pesananActive'] = 'active';
		$this->data['page'] = 'pesanan';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id, "Pesanan");
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
			$this->data['barang'] = $this->barangModel->getBarang("");
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/edit', 'master/owner_sidebar', 'penjualan/pesanan/edit_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/pesanan/edit', 'master/kasir_sidebar', 'penjualan/pesanan/edit_js');
		} else {
			redirect('home');
		}
	}

	public function update_pesanan(){
		if($this->input->post('idPenjualan') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
	        $idPenjualan = $this->input->post('idPenjualan');
	        $tgl = $this->input->post('tgl');
	        $diskonMember = $this->input->post('diskonMember');
	        $idPegawai = $this->input->post('idPegawai');
	        $type = $this->input->post('type');
	        $pemotongan = $this->input->post('pemotongan');
	        $grandtotal = $this->input->post('grandtotal');
	        $total = $this->input->post('total');
	        $jam = $this->input->post('jam');
	        $cekKirim = $this->input->post('kirim');
	        $tglKirim = null;
	        $jamKirim = null;
	        $alamat = null;
	        if($cekKirim == 'kirim'){
	        	$tglKirim = $this->input->post('tglKirim');
		        $jamKirim = $this->input->post('jamKirim');
		        $alamat = $this->input->post('alamat');
	        }
	        $tipetrans = "Pesanan";

			//Barang
			$barang = $this->input->post('barang');
	        $detailPembayaran = [];
	        $detail = [];
	        foreach ($barang as $value) {
	        	$detail[] = array(
	                'idBarang' => $value['id'],
	                'harga' => $value['harga'],
	                'diskonBarang' => $value['diskonBarang'],
	                'jumlah' => $value['jumlah']
	            );
	        }
			// var_dump($barang);die();?
	        if($type == "simpan"){
	        	$result = $this->penjualanModel->update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);
	        }
	        elseif ($type == "payment") {
		        //Payment
		        $payment = $this->input->post('payment');

		        foreach ($payment as $value) {
		        	$detailPembayaran[] = array(
		                'idPembayaran' => $value['id'],
		                'jumlah' => $value['jumlah'],
		                'keterangan' => $value['keterangan']
		            );
		        }
		        //var_dump($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);die();
		        $result = $this->penjualanModel->update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);
	        }
	        echo json_encode($result);
	        // $_SESSION['createSucceedMsg'] = "Pesanan ". $this->sanitize_output($tgl). " berhasil diubah";
	        // $this->session->mark_as_flash('createSucceedMsg');

	        // redirect(base_url('pesanan'));
	    }
	}

	public function payment_pesanan($id)
    {
    	$this->data['pesananActive'] = 'active';
    	$this->data['page'] = 'pesanan';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        //var_dump($detail);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/pembayaran', 'master/owner_sidebar', 'penjualan/pesanan/pembayaran_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/pesanan/pembayaran', 'master/kasir_sidebar', 'penjualan/pesanan/pembayaran_js');
		} else {
			redirect('home');
		}
    }

    public function store_payment_pesanan(){
    	// var_dump($this->input->post('idPenjualan'));die();
        if($this->input->post('idPenjualan') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	        $diskonMember = $this->input->post('diskonMember');
	        $pemotongan = $this->input->post('pemotongan');
	        $grandtotal = $this->input->post('grandtotal');
	        $total = $this->input->post('total');
	        $deposit = $this->input->post('deposit');
	        $tipetrans = "Pesanan";
	        $kembalian = $this->input->post('kembalian');
	        $status = 1;

	        $payment = $this->input->post('payment');

	        $detail = [];
	        foreach ($payment as $value) {
	        	$detail[] = array(
	                'idPembayaran' => $value['id'],
	                'jumlah' => $value['jumlah'],
	                'keterangan' => $value['keterangan']
	            );
	        }

	        $result = $this->penjualanModel->create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detail, $idPenjualan, $tipetrans, $deposit, $kembalian, $status);
	        echo json_encode($result);
	    }
	}

	/**
     * Menampilkan detail Penjualan Pesanan tertentu berdasarkan $id
     * @param $id
     */
    public function detail_pesanan($id)
    {
    	$this->data['pesananActive'] = 'active';
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'pesanan';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/detail', 'master/owner_sidebar', 'penjualan/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/detail', 'master/kasir_sidebar', 'penjualan/detail_js');
			}
        }
    }

    public function pengiriman_pesanan($id){
    	$this->data['pesananActive'] = 'active';
    	$this->data['page'] = 'pesanan';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['pegawai'] = $this->pegawaiModel->getPengirim();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pesanan/pengiriman', 'master/owner_sidebar', 'penjualan/pesanan/pengiriman_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/pesanan/pengiriman', 'master/kasir_sidebar', 'penjualan/pesanan/pengiriman_js');
		} else {
			redirect('home');
		}
    }

    public function store_pengiriman_pesanan() {
        if($this->input->post('idPenjualan') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	    	$tgl = $this->input->post('tgl');
	    	$jam = $this->input->post('jam');
	    	$pegawai = $this->input->post('idPegawai');
	    	$nama = $this->input->post('nama');
	    	$nohp = $this->input->post('notelp');
	    	$alamat = $this->input->post('alamat');
	    	$tipeBayar = $this->input->post('tipeBayar');
	        $tipetrans = "pesanan";

	        $result = $this->penjualanModel->create_pengiriman($idPenjualan, $tgl, $jam, $pegawai, $nama, $nohp, $alamat, $tipeBayar, $tipetrans);
	        echo json_encode($result);
	    }
	}

	public function store_receive_pesanan(){
		if($this->input->post('idPenjualan') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	    	$tgl = $this->input->post('tglTerima');
	    	$jam = $this->input->post('jamTerima');
	    	$keterangan = $this->input->post('keterangan');
	        $tipetrans = "pesanan";

	        //var_dump($idPenjualan, $tgl, $jam, $keterangan, $tipetrans);die();

	        $result = $this->penjualanModel->create_receive($idPenjualan, $tgl, $jam, $keterangan, $tipetrans);
	        echo json_encode($result);
	    }
	}

	public function hapus_pesanan(){
		if($this->input->post('idPenjualanHapus') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualanHapus');
	    	$keterangan = $this->input->post('alasan');
	        $tipetrans = "pesanan";

	        //var_dump($idPenjualan, $tgl, $jam, $keterangan, $tipetrans);die();

			$result = $this->penjualanModel->hapus_transaksi($idPenjualan, $keterangan, $tipetrans);
			// var_dump("asd");die();
	        echo json_encode($result);
	    }
	}

	public function print_pesanan($id){
    	$this->data['penjualanActive'] = 'active';
    	$detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'pesanan';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/print', 'master/owner_sidebar', 'penjualan/print_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/print', 'master/kasir_sidebar', 'penjualan/print_js');
			}
        }
    }

    public function invoice_pesanan($id){
    	$this->data['penjualanActive'] = 'active';
    	$detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'pesanan';
			parent::render_view('penjualan/invoice');
        }
    }

	//B2B
	public function index_b2b(){
		$this->data['inputKueActive'] = 'active';
		$this->data['b2bActive'] = 'active';
		$this->data['page'] = 'b2b';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/b2b/list', 'master/owner_sidebar', 'penjualan/b2b/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/b2b/list', 'master/kasir_sidebar', 'penjualan/b2b/list_js');
		}
	}

	public function getAllB2B(){
		$result = $this->penjualanModel->getb2b();
		echo json_encode($result);
	}

	/**
     * Menampilkan detail Penjualan POS tertentu berdasarkan $id
     * @param $id
     */
    public function detail_b2b($id)
    {
    	$this->data['inputKueActive'] = 'active';
    	$this->data['b2bActive'] = 'active';
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'b2b';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/detail', 'master/owner_sidebar', 'penjualan/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/detail', 'master/kasir_sidebar', 'penjualan/detail_js');
			}
        }
    }

	public function create_b2b(){
		$this->data['inputKueActive'] = 'active';
		$this->data['b2bActive'] = 'active';
		$this->data['page'] = 'b2b';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";
		$this->data['payment'] = $this->pembayaranModel->getPembayaran();
		$this->data['member'] = $this->memberModel->getMember(1);
		$this->data['barang'] = $this->barangModel->getBarang("");
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/b2b/create', 'master/owner_sidebar', 'penjualan/b2b/create_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/b2b/create', 'master/kasir_sidebar', 'penjualan/b2b/create_js');
		}
	}

	public function store_b2b(){
		if($this->input->post() === NULL)
        {
            redirect(base_url('b2b/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $diskonMember = $this->input->post('diskonMember');
        $idPegawai = $this->input->post('idPegawai');
        $type = $this->input->post('type');
        $pemotongan = $this->input->post('pemotongan');
        $grandtotal = $this->input->post('grandtotal');
        $total = $this->input->post('total');
        $jam = "";
        $kembalian = 0;
        $tipetrans = "B2B";
        $tglKirim = null;
        $jamKirim = null;
        $alamat = null;

        //Barang
        $detail = [];
        $barang = $this->input->post('barang');
		foreach ($barang as $value) {
			$detail[] = array(
		        'idBarang' => $value['id'],
		        'harga' => $value['harga'],
		        'diskonBarang' => $value['diskonBarang'],
		        'jumlah' => $value['jumlah']
		    );
		}
        $detailPembayaran = [];

        //var_dump($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);die();
        $result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);
        echo json_encode($result);
        // $_SESSION['createSucceedMsg'] = "Penjualan ". $this->sanitize_output($tgl). " berhasil dibuat";
        // $this->session->mark_as_flash('createSucceedMsg');

        // redirect(base_url('b2b'));
	}

	public function edit_b2b($id){
		$this->data['b2bActive'] = 'active';
		$this->data['page'] = 'b2b';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id, "b2b");
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
			$this->data['barang'] = $this->barangModel->getBarang("");
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/b2b/edit', 'master/owner_sidebar', 'penjualan/b2b/edit_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/b2b/edit', 'master/kasir_sidebar', 'penjualan/b2b/edit_js');
		} else {
			redirect('home');
		}
	}

	public function update_b2b(){
		if($this->input->post('idPenjualan') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
			$idMember = $this->input->post('idMember');
			$tgl = $this->input->post('tgl');
			$diskonMember = $this->input->post('diskonMember');
			$idPegawai = $this->input->post('idPegawai');
			$type = $this->input->post('type');
			$pemotongan = $this->input->post('pemotongan');
			$grandtotal = $this->input->post('grandtotal');
			$total = $this->input->post('total');
			$jam = "";
			$kembalian = 0;
			$tipetrans = "B2B";
			$tglKirim = null;
			$jamKirim = null;
			$alamat = null;
		
	        $idPenjualan = $this->input->post('idPenjualan');

			//Barang
			$barang = $this->input->post('barang');
	        $detailPembayaran = [];
	        $detail = [];
	        foreach ($barang as $value) {
	        	$detail[] = array(
	                'idBarang' => $value['id'],
	                'harga' => $value['harga'],
	                'diskonBarang' => 0,
	                'jumlah' => $value['jumlah']
	            );
			}

			$result = $this->penjualanModel->update($idPenjualan, $tgl, $diskonMember, $idPegawai, $pemotongan, $grandtotal, $total, $tipetrans, $detail, $detailPembayaran, $jam, $tglKirim, $jamKirim, $alamat);
			
	        echo json_encode($result);
	    }
	}

	//Cabang
	public function index_cabang(){
		$this->data['inputKueActive'] = 'active';
		$this->data['cabangActive'] = 'active';
		$this->data['page'] = 'cabang';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/cabang/list', 'master/owner_sidebar', 'penjualan/cabang/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/cabang/list', 'master/kasir_sidebar', 'penjualan/cabang/list_js');
		}
	}

	public function getAllCabang(){
		$result = $this->penjualanModel->getCabang();
		echo json_encode($result);
	}

	public function create_cabang(){
		$this->data['inputKueActive'] = 'active';
		$this->data['cabangActive'] = 'active';
		$this->data['page'] = 'cabang';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";

		$this->data['member'] = $this->memberModel->getMember(2);
		$this->data['barang'] = $this->barangModel->getBarang("");
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/cabang/create', 'master/owner_sidebar', 'penjualan/cabang/create_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/cabang/create', 'master/kasir_sidebar', 'penjualan/cabang/create_js');
		}
	}

	public function store_cabang(){
		if($this->input->post() === NULL)
        {
            redirect(base_url('b2b/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $diskonMember = 0;
        $idPegawai = $this->input->post('idPegawai');
        $type = $this->input->post('type');
        $total = $this->input->post('total');
        $grandtotal = $this->input->post('grandtotal');
        $tipetrans = "Cabang";
        $jam = "";
        $kembalian = 0;
        $pemotongan = $this->input->post('pemotongan');

        //Barang
        $detail = [];
        $barang = $this->input->post('barang');
		foreach ($barang as $value) {
			$detail[] = array(
		        'idBarang' => $value['id'],
		        'harga' => $value['harga'],
		        'diskonBarang' => $value['diskonBarang'],
		        'jumlah' => $value['jumlah']
		    );
		}
        $detailPembayaran = [];

        $result = $this->penjualanModel->create($idMember, $tgl, $jam, $diskonMember, $idPegawai, $detail, $total, $pemotongan, $tipetrans, $detailPembayaran, $kembalian, $grandtotal, $tglKirim, $jamKirim, $alamat);

        echo json_encode($result);
	}

	/**
     * Menampilkan detail Penjualan Cabang tertentu berdasarkan $id
     * @param $id
     */
    public function detail_cabang($id)
    {
    	$this->data['cabangActive'] = 'active';
    	$this->data['inputKueActive'] = 'active';
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'cabang';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/detail', 'master/owner_sidebar', 'penjualan/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/detail', 'master/kasir_sidebar', 'penjualan/detail_js');
			}
        }
    }

    public function payment_cabang($id) {
    	$this->data['cabangActive'] = 'active';
    	$this->data['inputKueActive'] = 'active';
    	$this->data['page'] = 'cabang';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/cabang/pembayaran', 'master/owner_sidebar', 'penjualan/cabang/pembayaran_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/cabang/pembayaran', 'master/kasir_sidebar', 'penjualan/cabang/pembayaran_js');
		} else {
			redirect('home');
		}
    }

    public function store_payment_cabang() {
        if($this->input->post('idPenjualan') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	        $diskonMember = 0;
	        $pemotongan = $this->input->post('pemotongan');
	        $grandtotal = $this->input->post('grandtotal');
	        $total = $this->input->post('total');
	        $kembalian = $this->input->post('kembalian');
	        $tipetrans = "cabang";
	        $status = 1;

	        //Payment
	        $deposit = null;
	        $detailPembayaran = [];
	        $payment = $this->input->post('payment');

			foreach ($payment as $value) {
				$detailPembayaran[] = array(
			        'idPembayaran' => $value['id'],
			        'jumlah' => $value['jumlah'],
			        'keterangan' => $value['keterangan']
			    );
			}

	        $result = $this->penjualanModel->create_payment($total, $diskonMember, $pemotongan, $grandtotal, $detailPembayaran, $idPenjualan, $tipetrans, $deposit, $kembalian, $status);
	        echo json_encode($result);
	    }
	}

	public function pengiriman_cabang($id){
		$this->data['cabangActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
    	$this->data['page'] = 'cabang';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['pegawai'] = $this->pegawaiModel->getPengirim();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/pengiriman', 'master/owner_sidebar', 'penjualan/pengiriman_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/pengiriman', 'master/kasir_sidebar', 'penjualan/pengiriman_js');
		} else {
			redirect('home');
		}
    }

    public function store_pengiriman_cabang() {
        if($this->input->post('idPenjualan') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idPenjualan = $this->input->post('idPenjualan');
	    	$tgl = $this->input->post('tgl');
	    	$jam = $this->input->post('jam');
	    	$pegawai = $this->input->post('idPegawai');
	    	$nama = $this->input->post('nama');
	    	$nohp = $this->input->post('notelp');
	    	$alamat = $this->input->post('alamat');
	    	$tipeBayar = $this->input->post('tipeBayar');
	        $tipetrans = "cabang";

	        //var_dump($idPenjualan, $tgl, $jam, $pegawai, $nama, $nohp, $alamat, $tipeBayar, $tipetrans);die();

	        $result = $this->penjualanModel->create_pengiriman($idPenjualan, $tgl, $jam, $pegawai, $nama, $nohp, $alamat, $tipeBayar, $tipetrans);
	        echo json_encode($result);
	    }
	}

	//Nota
	public function index_nota(){
		$this->data['notaActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
		$this->data['page'] = 'nota';

		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/nota/list', 'master/owner_sidebar', 'penjualan/nota/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/nota/list', 'master/kasir_sidebar', 'penjualan/nota/list_js');
		}
	}

	public function getAllNota(){
		$result = $this->penjualanModel->getNota();
		echo json_encode($result);
	}

	/**
     * Menampilkan detail Penjualan POS tertentu berdasarkan $id
     * @param $id
     */
    public function detail_nota($id)
    {
    	$this->data['notaActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
        $detail = $this->penjualanModel->getDetailNota($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'nota';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/nota/detail', 'master/owner_sidebar', 'penjualan/nota/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/nota/detail', 'master/kasir_sidebar', 'penjualan/nota/detail_js');
			}
        }
    }

	public function create_nota(){
		$this->data['notaActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
		$this->data['page'] = 'nota';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";

		$this->data['member'] = $this->memberModel->getMember(1);
		$this->data['payment'] = $this->pembayaranModel->getPembayaran();

		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/nota/create', 'master/owner_sidebar', 'penjualan/nota/create_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/nota/create', 'master/kasir_sidebar', 'penjualan/nota/create_js');
		}
	}

	public function store_nota(){
		if($this->input->post() === NULL)
        {
            redirect(base_url('nota/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $idPegawai = $this->input->post('idPegawai');
        $total = $this->input->post('total');
        $type = $this->input->post('type');
        $diskon = $this->input->post('diskon');
        $kembalian = 0;

        //Penjualan
        $penjualan = $this->input->post('penjualan');
        $$detailPembayaran = [];
        $detail = [];
		foreach ($penjualan as $value) {
			$detail[] = array(
		        'idPenjualan' => $value['id'],
		        'total' => $value['total']
		    );
		}

		if($type == 'payment'){
			//Payment
	        $payment = $this->input->post('payment');
			$kembalian = $this->input->post('kembalian');
			foreach ($payment as $value) {
				$detailPembayaran[] = array(
			        'idPembayaran' => $value['id'],
			        'jumlah' => $value['jumlah'],
			        'keterangan' => $value['keterangan']
			    );
			}
			$result = $this->penjualanModel->createNota($idMember, $tgl, $idPegawai, $total, $detail, $detailPembayaran, $kembalian, $diskon);
		} else {
			$result = $this->penjualanModel->createNota($idMember, $tgl, $idPegawai, $total, $detail, $detailPembayaran, $kembalian, $diskon);
		}

        echo json_encode($result);
	}

	public function payment_nota($id) {
		$this->data['notaActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
    	$this->data['page'] = 'nota';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->getNotaById($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
			}
			
            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/nota/pembayaran', 'master/owner_sidebar', 'penjualan/nota/pembayaran_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/nota/pembayaran', 'master/kasir_sidebar', 'penjualan/nota/pembayaran_js');
		} else {
			redirect('home');
		}
    }

    public function store_payment_nota() {
        if($this->input->post('idNota') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	    	$idNota = $this->input->post('idNota');
	        $total = $this->input->post('total');
	        $diskon = $this->input->post('diskon');
	        $idPenjualan = $this->input->post('idPenjualan');
	        $kembalian = $this->input->post('kembalian');

	        $penjualan = $this->input->post('penjualan');
	        $idPenjualan = [];
			foreach ($penjualan as $value) {
				$idPenjualan[] = $value['id'];
			}

	        $deposit = null;
	        $detail = [];
	        $payment = $this->input->post('payment');
			foreach ($payment as $value) {
				$detail[] = array(
			        'idPembayaran' => $value['id'],
			        'jumlah' => $value['jumlah'],
			        'keterangan' => $value['keterangan']
			    );
			}

	        $result = $this->penjualanModel->create_payment_nota($idNota, $total, $idPenjualan, $detail, $kembalian, $diskon);
	        echo json_encode($result);
	    }
	}

	//Retur
	public function index_retur(){
		$this->data['returActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
		$this->data['page'] = 'retur';
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/retur/list', 'master/owner_sidebar', 'penjualan/retur/list_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/retur/list', 'master/kasir_sidebar', 'penjualan/retur/list_js');
		}
	}

	public function getAllRetur(){
		$result = $this->penjualanModel->getRetur();
		echo json_encode($result);
	}

	public function create_retur(){
		$this->data['returActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
		$this->data['page'] = 'retur';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];

		$this->data['collapse'] = "sidebar-collapse";

		$this->data['member'] = $this->memberModel->getMember(2);
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/retur/create', 'master/owner_sidebar', 'penjualan/retur/create_js');
		}
		elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
			parent::render_view('penjualan/retur/create', 'master/kasir_sidebar', 'penjualan/retur/create_js');
		}
	}

	public function store_retur(){
		if($this->input->post() === NULL)
        {
            //redirect(base_url('b2b/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idMember = $this->input->post('idMember');
        $tgl = $this->input->post('tgl');
        $idPegawai = $this->input->post('idPegawai');
        $idPenjualan = $this->input->post('idPenjualan');
        $grandtotal = $this->input->post('grandtotal');
        $tipetrans = "Cabang";

        //Barang
        $idDetail = $this->input->post('idDetail');
        $idBarang = $this->input->post('idBarang');
        $jumlahBarang = count($idBarang);
        $jumlahRetur = $this->input->post('jumlahRetur');

        $detail = [];
        $barang = $this->input->post('barang');
		foreach ($barang as $value) {
			$detail[] = array(
		        'idDetail' => $value['idDetail'],
		        'idBarang' => $value['idBarang'],
		        'jumlahRetur' => $value['jumlahRetur'],
		    );
		}

        //var_dump($idMember, $tgl, $idPegawai, $detail, $idPenjualan, $grandtotal);die();
        $result = $this->penjualanModel->createRetur($idMember, $tgl, $idPegawai, $detail, $idPenjualan, $grandtotal);
        echo json_encode($result);
        // $_SESSION['createSucceedMsg'] = "Retur ". $this->sanitize_output($tgl). " berhasil dibuat";
        // $this->session->mark_as_flash('createSucceedMsg');

        // redirect(base_url('retur'));
	}

	/**
     * Menampilkan detail Penjualan Retur tertentu berdasarkan $id
     * @param $id
     */
    public function detail_retur($id)
    {
    	$this->data['returActive'] = 'active';
		$this->data['inputKueActive'] = 'active';
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            $this->data['page'] = 'retur';
			if ($this->hasAccess('Owner')) {
				parent::render_view('penjualan/retur/detail', 'master/owner_sidebar', 'penjualan/retur/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('penjualan/retur/detail', 'master/kasir_sidebar', 'penjualan/retur/detail_js');
			}
        }
	}
	
	public function edit_retur($id){
		$this->data['b2bActive'] = 'active';
		$this->data['page'] = 'retur';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->penjualanModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
			$this->data['barang'] = $this->barangModel->getBarang("");
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
		}
		if ($this->hasAccess('Owner')) {
			parent::render_view('penjualan/retur/edit', 'master/owner_sidebar', 'penjualan/retur/edit_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('penjualan/retur/edit', 'master/kasir_sidebar', 'penjualan/retur/edit_js');
		} else {
			redirect('home');
		}
	}

	public function update_retur(){
		if($this->input->post('idPenjualan') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
			$idMember = $this->input->post('idMember');
			$tgl = $this->input->post('tgl');
			$idPegawai = $this->input->post('idPegawai');
			$type = $this->input->post('type');
			$grandtotal = $this->input->post('grandtotal');
			$total = $this->input->post('total');
			$jam = "";
			$kembalian = 0;
			$tipetrans = "Cabang";
			$tglKirim = null;
			$jamKirim = null;
			$alamat = null;
		
	        $idPenjualan = $this->input->post('idPenjualan');

			//Barang
			$barang = $this->input->post('barang');
	        $detailPembayaran = [];
			$detail = [];
	        foreach ($barang as $value) {
				$detail[] = array(
					'idDetail' => $value['id_jualdetail'],
					'idBarang' => $value['id'],
					'jumlahRetur' => $value['retur'],
				);
			}

			$result = $this->penjualanModel->updateRetur($idPenjualan, $tgl, $idPegawai, $grandtotal, $total, $tipetrans, $detail);
			
	        echo json_encode($result);
	    }
	}

	//Function Global
	public function searchBarang(){
		if(isset($_GET['term'])){
			$result = $this->barangModel->get($_GET['term']);
			if(count($result) > 0){
				foreach ($result as $result) {
					$arr_result[] = $result['barcode_kue'] . "_" . $result['nama_kue'] . "_" . $result['harga'];
				}
				echo json_encode($arr_result);
			}
		}
	}

	public function cekKodeBarang($kode){
        $result = $this->barangModel->cekKodeBarang($kode);
        echo json_encode($result);
    }

    public function searchHistory($id){
        $result = $this->penjualanModel->searchHistory($id);
        echo json_encode($result);
    }

    public function searchDetailHistory($id){
        $result = $this->penjualanModel->searchDetailHistory($id);
        echo json_encode($result);
    }

    public function searchPenjualan($id,$type){
        $result = $this->penjualanModel->searchPenjualan($id,$type);
        echo json_encode($result);
    }

    public function detailPenjualan($id){
        $result = $this->penjualanModel->detailPenjualan($id);
        echo json_encode($result);
    }

    public function openRegister(){
    	$program_path = exec('start /B C:\xampp\htdocs\yasinzein\public\assets\opendrw.bat');
    }
}
