<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;
use App\Traits\SecurityFilter;
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.20
 */

class Member_controller extends Regular_Controller
{
	use CheckAccess;
	use SecurityFilter;
	/**
	 * User_controller constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
		}

		$this->load->model('member_model', 'memberModel');
		$this->load->helper('form_helper');
        $this->data['page'] = "member";
        $this->data['masterActive'] = 'active';
		$this->data['memberActive'] = 'active';
	}

	public function index(){
		if ($this->hasAccess('Owner')) {
			parent::render_view('member/list', 'master/owner_sidebar', 'member/list_js');
		} else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('member/list', 'master/kasir_sidebar', 'member/list_js');
		} else if ($this->hasAccess('Dapur')) {
			parent::render_view('member/list', 'master/dapur_sidebar', 'member/list_js');
		}
    }
    
    public function show($id)
    {
        $detail = $this->memberModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
			if ($this->hasAccess('Owner')) {
				parent::render_view('member/detail', 'master/owner_sidebar', 'member/detail_js');
			}
			elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
				parent::render_view('member/detail', 'master/kasir_sidebar', 'member/detail_js');
			}elseif ($this->hasAccess('Dapur')){
				parent::render_view('member/detail', 'master/dapur_sidebar', 'member/detail_js');
			}
        }
    }

	public function create(){

		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		
		if ($this->hasAccess('Owner')) {
			parent::render_view('member/create', 'master/owner_sidebar', 'member/create_js');
		} elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('member/create', 'master/kasir_sidebar', 'member/create_js');
		} elseif ($this->hasAccess('Dapur')) {
			parent::render_view('member/create', 'master/dapur_sidebar', 'member/create_js');
		}
	}

	/**
     * menyimpan data member baru
     */
    public function store()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('member/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $rumah = $this->input->post('rumah');
        $hp = $this->input->post('hp');
        $ktp = $this->input->post('ktp');
        $diskon = $this->input->post('diskon');
        $diskonBarang = $this->input->post('diskonBarang');
        $instansi = $this->input->post('instansi');
        $kantor = $this->input->post('kantor');
        $keterangan = $this->input->post('keterangan');
        $jenisMember = $this->input->post('jenisMember');
        $usepoin = $this->input->post('usepoin');
        
        $this->db->trans_start();
        $result = $this->memberModel->create($kode, $nama, $email, $rumah, $hp, $ktp, $diskon, $instansi, $kantor, $keterangan, $jenisMember, $diskonBarang, $usepoin);
        echo json_encode($result);
    }

    /**
     * Menampilkan form untuk mengubah data barang
     * @param $id
     */
    public function edit($id)
    {
        $detail = $this->memberModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['detail'] = $detail;
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
            parent::render_view('member/edit', 'master/owner_sidebar', 'member/edit_js');
        } elseif ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')) {
			parent::render_view('member/edit', 'master/kasir_sidebar', 'member/edit_js');
		} elseif ($this->hasAccess('Dapur')) {
			parent::render_view('member/edit', 'master/dapur_sidebar', 'member/edit_js');
		}
    }

    public function update()
    {
        if($this->input->post('id') === NULL)
        {
            redirect('member');
        }
        $this->config->set_item('language', 'indonesia');

        $id = $this->sanitize_output($this->input->post('id'));
        $nama = $this->input->post('nama');
        $email = $this->input->post('email');
        $rumah = $this->input->post('rumah');
        $hp = $this->input->post('hp');
        $ktp = $this->input->post('ktp');
        $diskon = $this->input->post('diskon');
        $instansi = $this->input->post('instansi');
        $kantor = $this->input->post('kantor');
        $keterangan = $this->input->post('keterangan');
        $jenisMember = $this->input->post('jenisMember');
        $usepoin = $this->input->post('usepoin');
        $diskonBarang = $this->input->post('diskonBarang');

        $this->memberModel->edit($id, $nama, $email, $rumah, $hp, $ktp, $diskon, $instansi, $kantor, $keterangan, $usepoin, $diskonBarang, $jenisMember);
        $_SESSION['createSucceedMsg'] = "Member ". $this->sanitize_output($nama). " berhasil diubah";
        $this->session->mark_as_flash('createSucceedMsg');

        redirect(base_url('member'));
    }

    public function delete($id){
        $detail = $this->memberModel->get($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else{
            $result = $this->memberModel->delete($id);
            echo json_encode($result);
        }
    }

    public function cekHPMember(){
        $cek = $this->memberModel->isAvailableHP();
        if($cek == 0)
        {
            echo "true";  //good to register
        }
        else
        {
            echo "false"; //already registered
        }
    }

    // public function getMemberPesanan(){
    //     $list = $this->memberModel->getMember(0);  
    //     $data = array();  
    //     foreach($list as $row)  
    //     {  
    //         $sub_array = array();  
    //         $sub_array[] = '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember('.$row['id_member'].')"><i class="fa fa-plus"></i></button>
    //                         <input type="hidden" id="namaMemberModal'.$row['id_member'].'" value="'.$row['nama'].'">
    //                         <input type="hidden" id="diskonMember'.$row['id_member'].'" value="'.$row['diskon'].'">
    //                         <input type="hidden" id="userPoin'.$row['id_member'].'" value="'.$row['use_poin'].'">
    //                         <input type="hidden" id="saldo'.$row['id_member'].'" value="'.$row['saldo'].'">
    //                         <input type="hidden" id="poin'.$row['id_member'].'" value="'.$row['poin'].'">
    //                         <input type="hidden" id="diskonBarang'.$row['id_member'].'" value="'.$row['diskon_barang'].'">';  
    //         $sub_array[] = $row['barcode_member'];  
    //         $sub_array[] = $row['nama'];   
    //         $sub_array[] = $row['nohp'];   
    //         $sub_array[] = $row['ket'];
    //         $data[] = $sub_array;  
    //     }  
    //     $output = array(  
    //         "data" => $data
    //     );  
    //     echo json_encode($output);  
    // }

    public function getAll(){
        $result = $this->memberModel->getAllMember();
        echo json_encode($result);
    }

    public function getMember($id){
        $result = $this->memberModel->getMember((int)$id);
        echo json_encode($result);
    }

    
}
