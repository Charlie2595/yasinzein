<?php
use App\Core\Controllers\Regular_Controller;
use App\Traits\CheckAccess;

class Masuk_controller extends Regular_Controller
{
	use CheckAccess;
	public function __construct()
	{
		parent::__construct();

		if(!$this->hasLogin())
		{
			redirect('home');
        }
        
        $this->load->model('barang_model', 'barangModel');
        $this->load->model('masuk_model', 'masukModel');
        $this->load->model('supplier_model', 'supplierModel');
        $this->load->model('pembayaran_model', 'pembayaranModel');
        $this->load->helper('form_helper');
        $this->data['page'] = "masuk";
	}

	public function index()
    {
        $this->data['barangMasukActive'] = 'active';
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang_masuk/list', 'master/owner_sidebar', 'barang_masuk/list_js');
        } else if ($this->hasAccess('Dapur')){
            parent::render_view('barang_masuk/list', 'master/dapur_sidebar', 'barang_masuk/list_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/list', 'master/kasir_sidebar', 'barang_masuk/list_js');
        }
    }

	public function create(){
        $this->data['barangMasukActive'] = 'active';
		$this->data['csrf'] = [
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		];
		$this->data['collapse'] = "sidebar-collapse";
		if ($this->hasAccess('Owner')) {
			parent::render_view('barang_masuk/create', 'master/owner_sidebar', 'barang_masuk/create_js');
		} else if ($this->hasAccess('Dapur')){
            parent::render_view('barang_masuk/create', 'master/dapur_sidebar', 'barang_masuk/create_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/create', 'master/kasir_sidebar', 'barang_masuk/create_js');
        }
	}

    public function store()
    {
        if($this->input->post() === NULL)
        {
            redirect(base_url('masuk/create'));
        }
        $this->config->set_item('language', 'indonesia');

        $idPegawai = $this->input->post('idPegawai');
        $idSupplier = $this->input->post('idSupplier');
        $total = $this->input->post('total');
        $pemotongan = $this->input->post('pemotongan');
        $tgl = $this->input->post('tgl');
        $barang = $this->input->post('barang');
        $detail = [];
        foreach ($barang as $value) {
			$detail[] = array(
		        'idKue' => $value['id'],
		        'hargaJual' => $value['hargaJual'],
		        'hargaBeli' => $value['hargaBeli'],
		        'jumlah' => $value['jumlah']
		    );
        }
        $result = $this->masukModel->create($idPegawai, $idSupplier, $tgl, $detail, $total, $pemotongan);
        echo json_encode($result);
    }

    public function edit($id){
		$this->data['barangMasukActive'] = 'active';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->masukModel->getBarangMasuk($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['data'] = $detail;
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
			parent::render_view('barang_masuk/edit', 'master/owner_sidebar', 'barang_masuk/edit_js');
		} else if ($this->hasAccess('Dapur')){
            parent::render_view('barang_masuk/edit', 'master/dapur_sidebar', 'barang_masuk/edit_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/edit', 'master/kasir_sidebar', 'barang_masuk/edit_js');
        }
	}

	public function update(){
		if($this->input->post('idMasuk') === NULL)
        {
            echo json_encode(['status' => 'error', 'description' => 'Please refresh the page']);
        }
        else
	    {
	        $idMasuk = $this->input->post('idMasuk');
            $idPegawai = $this->input->post('idPegawai');
            $idSupplier = $this->input->post('idSupplier');
            $total = $this->input->post('total');
            $pemotongan = $this->input->post('pemotongan');
            $tgl = $this->input->post('tgl');
            $barang = $this->input->post('barang');
            $detail = [];
            foreach ($barang as $value) {
                $detail[] = array(
                    'idKue' => $value['id'],
                    'hargaJual' => $value['hargaJual'],
                    'hargaBeli' => $value['hargaBeli'],
                    'jumlah' => $value['jumlah']
                );
            }
            $result = $this->masukModel->update($idMasuk, $idPegawai, $idSupplier, $tgl, $detail, $total, $pemotongan);
            echo json_encode($result);
	    }
	}

    public function getAllBarangMasuk(){
        $result = $this->masukModel->getAll();
        echo json_encode($result);
    }

    public function searchHistoryBarangMasuk($id){
        $result = $this->masukModel->searchHistory($id);
        echo json_encode($result);
    }

    public function detail($id)
    {
        $this->data['barangMasukActive'] = 'active';
        $detail = $this->masukModel->getBarangMasuk($id);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            $this->data['detail'] = $detail;
            if ($this->hasAccess('Owner')) {
                parent::render_view('barang_masuk/detail', 'master/owner_sidebar', 'barang_masuk/detail_js');
            } else if ($this->hasAccess('Dapur')) {
                parent::render_view('barang_masuk/detail', 'master/dapur_sidebar', 'barang_masuk/detail_js');
            } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
                parent::render_view('barang_masuk/detail', 'master/kasir_sidebar', 'barang_masuk/detail_js');
            }
        }
    }

    public function payment($id)
    {
    	$this->data['barangMasukActive'] = 'active';
    	$this->data['collapse'] = "sidebar-collapse";
        $detail = $this->masukModel->getBarangMasuk($id);
        //var_dump($detail);
        if(!isset($detail))
        {
            parent::render_view('errors/html/custom_error404', 'master/main');
        }
        else
        {
            // belum selesai
            if(isset($_SESSION['formValues'])) {
                $_POST = $_SESSION['formValues'];
            }

            $this->data['data'] = $detail;
            $this->data['payment'] = $this->pembayaranModel->getPembayaran();
            $this->data['csrf'] = [
                'name' => $this->security->get_csrf_token_name(),
                'hash' => $this->security->get_csrf_hash()
            ];
        }
        if ($this->hasAccess('Owner')) {
            parent::render_view('barang_masuk/pembayaran', 'master/owner_sidebar', 'barang_masuk/pembayaran_js');
        } else if ($this->hasAccess('Dapur')) {
            parent::render_view('barang_masuk/pembayaran', 'master/dapur_sidebar', 'barang_masuk/pembayaran_js');
        } else if ($this->hasAccess('Kasir') || $this->hasAccess('Supervisor')){
            parent::render_view('barang_masuk/pembayaran', 'master/kasir_sidebar', 'barang_masuk/pembayaran_js');
        }
    }

    public function store_payment(){
        if($this->input->post('idMasuk') === NULL)
        {
            echo ['status' => 'error', 'description' => 'Please refresh your browser'];
        }
        else
	    {
	        $tgl = $this->input->post('tgl');
	        $idPegawai = $this->input->post('idPegawai');
	    		$idMasuk = $this->input->post('idMasuk');
	        $pemotongan = $this->input->post('pemotongan');
	        $total = $this->input->post('total');
	        $grandtotal = $this->input->post('grandtotal');
	        $kembalian = $this->input->post('kembalian');

	        $payment = $this->input->post('payment');
	        $detail = [];
	        foreach ($payment as $value) {
	        	$detail[] = array(
	                'idPembayaran' => $value['id'],
	                'jumlah' => $value['jumlah'],
	                'keterangan' => $value['keterangan']
	            );
	        }

	        $result = $this->masukModel->create_payment($tgl, $idPegawai, $idMasuk, $pemotongan, $total, $grandtotal, $kembalian, $detail);
	        echo json_encode($result);
	    }
	}

}
