<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Master
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master <?php echo strtoupper($page); ?> </li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> List <?php echo strtoupper($page); ?></h3>
						<a href="<?php echo base_url($page . '/create') ?>" class="btn btn-success pull-right">
							<i class="fa fa-plus"></i> Buat Account</a>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Nama</th>
								<th>Jabatan</th>
								<th>No HP</th>
								<th>ktp</th>
								<th>Aksi</th>
							</tr>
							</thead>
						</table>
					</div>
				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
