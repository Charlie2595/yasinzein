<script>
	var username = "";
	var error = true;

	$(function () {
		$('#confirmSubmit').click(function () {
			$('#createForm').submit();
		});

		$("#buttonModal").click(function () {
			if ($("#createForm").valid()) {
				$('#confirm-edit-modal').modal('show');
			}
		});

		if ($("#user").is(':checked')) {
			$("#createUser").show();
		} else {
			$("#createUser").hide();
		}

		$("#user").on("click", function () {
			if ($("#user").is(':checked')) {
				$("#createUser").show("slow");
			} else {
				$("#createUser").hide("slow");
			}
		});

		$('#createForm').validate({
			rules: {
				nama: {
					required: true,
				},
				jabatan: {
					required: true,
				},

				username: {
					required: true,
					remote: {
						url: "<?php echo base_url();?>cekUsername",
						type: "post",
						data:
							{
								username: function () {
									return $("#username").val();
								}
							}
					}
				},

				password: {
					required: true,
					minlength: 5
				},

				ulangPassword: {
					required: true,
					equalTo: "#password"
				}
			},
			messages: {
				kode: {
					required: "Kode harus diisi",
					minlength: "Kode harus diisi minimal 6 karakter.",
					//remote: jQuery.validator.format("{0} sudah digunakan/tidak ditemukan.")
				},
				username: {
					required: "Username harus diisi",
					remote: jQuery.validator.format("{0} sudah digunakan.")
				},
				password: {
					required: "Password harus diisi",
					minlength: "Password harus diisi minimum 5 karakter"
				},
				ulangPassword: {
					required: "Ulang Password harus diisi",
					equalTo: "Password Tidak Sama"
				},
				nama: {
					required: "Nama harus diisi"
				},
				jabatan: {
					required: "jabatan harus diisi"
				}
			}
		});
	});


</script>
