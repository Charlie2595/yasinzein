<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			BUAT
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">BUAT <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo strtoupper($page); ?> FORM</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/create'); ?>">
						<div class="box-body">

							<div class="form-group">
								<div class="col-md-8">
									<label class="col-sm-3">Nama :</label>
									<div class="col-sm-8">
										<input type="text" class="form-control pull-right" id="nama" name="nama" placeholder="Nama">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<div class="checkbox">
										<label> <input type="checkbox" id="user"> Buat User ? </label>
									</div>
								</div>
							</div>
							<div id="createUser">
								<div class="form-group">
									<div class="col-md-8">
										<label class="col-sm-3">Username :</label>
										<div class="col-sm-8">
											<input type="text" class="form-control pull-right" id="username" name="username" placeholder="Username">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-8">
										<label class="col-sm-3">Password :</label>
										<div class="col-sm-8">
											<input type="password" class="form-control pull-right" id="password" name="password" placeholder="Password">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-8">
										<label class="col-sm-3">Ulang Password :</label>
										<div class="col-sm-8">
											<input type="password" class="form-control pull-right" name="ulangPassword" id="ulangPassword" placeholder="Ulang Password">
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-8">
									<label class="col-sm-3">No Telp :</label>
									<div class="col-sm-8">
										<input type="text" class="form-control pull-right" id="notelp" name="notelp" placeholder="No Telp">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-8">
									<label class="col-sm-3">KTP :</label>
									<div class="col-sm-8">
										<input type="text" class="form-control pull-right" id="ktp" name="ktp" placeholder="KTP">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-8">
									<label class="col-sm-3">Jabatan</label>
									<div class="col-sm-8">
										<select id="jabatan" name="jabatan" class="form-control select2" style="width: 100%;">
											<option selected="selected" disabled=""></option>
											<option value="Owner">Owner</option>
											<option value="Supervisor">Supervisor</option>
											<option value="Kasir">Kasir</option>
											<option value="Dapur">Dapur</option>
											<option value="Driver">Driver</option>
										</select>
									</div>
									<!-- /.input group -->
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10  pull-right" id="buttonModal">
									<i class="fa fa-save"></i> SIMPAN
								</button>
								<div id="confirm-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Pembuatan <?php echo($page); ?></h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin membuat <?php echo($page); ?> ini?</p>
											</div>
											<div class="modal-footer">
												<button type="button" id="confirmSubmit" class="btn btn-success waves-effect">
													<i class="fa fa-check"></i> Ya
												</button>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
