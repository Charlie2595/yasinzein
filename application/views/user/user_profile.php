<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-xs-12 col-sm-4 col-lg-3">
			<h4 class=""> Profil User</h4>
		</div>
		<div class="col-xs-12 col-sm-8 col-lg-9">
			<ol class="breadcrumb">
				<li><a href="<?php echo base_url('home'); ?>"> Beranda </a></li>
				<li><a href="<?php echo base_url('users'); ?>"> Master User </a></li>
				<li class="active"> Profil User</li>
			</ol>
		</div>
	</div>
	<?php if (isset($_SESSION['editSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['editSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="row">
		<div class="col-xs-12 col-lg-8">
			<div class="panel">
				<div class="cust-panel-head">
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<form class="form-horizontal" role="form">
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label col-sm-3 col-lg-4">Nama :</label>
										<div class="col-sm-9 col-lg-8">
											<p class="form-control-static"><?php echo $activeUser['nama']; ?></p></div>
									</div>
								</div>
							</div>
							<?php if ($activeUser['kode'] != null) : ?>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Kode Karyawan :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $activeUser['kode']; ?></p>
											</div>
										</div>
									</div>
								</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label col-sm-3 col-lg-4">Username :</label>
										<div class="col-sm-9 col-lg-8">
											<p class="form-control-static"><?php echo $activeUser['username']; ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label class="control-label col-sm-3 col-lg-4">Jabatan :</label>
										<div class="col-sm-9 col-lg-8">
											<p class="form-control-static"><?php echo $activeUser['jabatan']; ?></p>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-sm-offset-3 col-sm-9 col-lg-offset-4 col-lg-8">
												<a class="btn btn-info" href="<?php echo base_url('user/' . $activeUser['id'] . '/edit') ?>">
													<i class="fa fa-pencil fa-lg"></i> Ubah </a>
												<a class="btn btn-danger" href="<?php echo base_url('user/' . $activeUser['id'] . '/deactivate') ?>">
													<i class="fa fa-trash fa-lg"></i> Hapus </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
