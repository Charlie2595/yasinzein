<style type="text/css">
	th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #4CAF50;
		color: white;
	}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			DASHBOARD
		</h1>
	</section>

	<section class="content">
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">DAFTAR PESANAN <?php echo $tgl ?></h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form class="form-horizontal" id="searchBarang" method="get" action="">
					<section>
						<div class="col-sm-2">
							<div class="form-group">
								<input type="text" class="form-control tgl pull-right input-sm" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tgl)); ?>">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<button type="button" class="btn btn-block btn-warning btn-sm" id="searchBtn">
									<i class="fa fa-search"> Cari </i></button>
							</div>
						</div>
					</section>
				</form>
			</div>
			<?php $barang = $result['barang']; ?>
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">SHIFT 1</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i></button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<div class="box-body">
								<table id="listTable" class="table table-bordered table-striped" width="100%">
									<thead>
									<tr>
										<th>Kue</th>
										<?php foreach ($barang['list'] as $list) : ?>
											<?php $idKue = $list['id_kue']; ?>
										<?php endforeach; ?>
										<th>PAGI</th>
										<th>MALAM</th>
									</tr>
									</thead>
									<tbody>
									<?php $counter = 1; ?>
									<?php foreach ($barang['list'] as $list) : $cekPagi = 0;
										$cekMalam = 0; ?>
										<tr>
											<th style="background-color: #ff6600"><?php echo $list['nama']; ?></th>
											<?php foreach ($barang['pagi'] as $value) : ?>
												<?php if ($list['id_kue'] == $value['id_kue']) : ?>
													<?php $cekPagi = 1; ?>
													<td><?php echo $value['jumlah']; ?></td>
												<?php endif; ?>
											<?php endforeach; ?>

											<?php if ($cekPagi == 0) : ?>
												<td><?php echo 0 ?></td>
											<?php endif; ?>

											<?php foreach ($barang['malam'] as $value) : ?>
												<?php if ($list['id_kue'] == $value['id_kue']) : ?>
													<?php $cekMalam = 1; ?>
													<td><?php echo $value['jumlah']; ?></td>
												<?php endif; ?>
											<?php endforeach; ?>

											<?php if ($cekMalam == 0) : ?>
												<td><?php echo 0 ?></td>
											<?php endif; ?>
											<!-- <?php for ($i = 0; $i < count($member); $i++) {
												$cek = 0; ?> 
                                                <?php for ($j = 0; $j < count($pagi['detail']); $j++) { ?>
                                                <?php if (($member[$i] == $pagi['detail'][$j]['id_member']) && ($list['id_kue'] == $pagi['detail'][$j]['id_kue'])):
													$cek = 1; ?>
                                                    <td><?php echo $pagi['detail'][$j]['jumlah'];
													$total += $pagi['detail'][$j]['jumlah'] * 1; ?></td>
                                                    <?php endif; ?>
                                                <?php } ?>
                                                <?php if ($cek == 0): ?>
                                                    <td><?php echo 0 ?></td>
                                                <?php endif; ?>
                                            <?php } ?> -->
											<!-- <th style="background-color: #ff6600"><?php echo $total; ?> -->
										</tr>
										<?php $counter++; ?>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">SHIFT 2</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
			<!-- <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box-body">
                                <table id="listTable" class="table table-bordered table-striped" width="100%">
                                    <thead>
                                    <?php $counter = 0; ?>
                                    <tr>
                                        <th>Kue</th>
                                        <?php foreach ($malam['member'] as $list) : ?>
                                            <th><?php echo $list['nama']; ?></th>
                                            <?php $member[$counter] = $list['id_member'];
				$counter++; ?>
                                        <?php endforeach; ?>
                                        <th>Total</th>
                                    </tr>
                                    </thead>
                                    <?php if (isset($malam)) : ?>
                                    <tbody>
                                    <?php $counter = 1; ?>
                                    <?php foreach ($malam['barang'] as $list) : $total = 0; ?>
                                        <tr>
                                            <th style="background-color: #ff6600"><?php echo $list['nama']; ?></th>
                                            <?php for ($i = 0; $i < count($member); $i++) {
				$cek = 0; ?> 
                                                <?php for ($j = 0; $j < count($malam['detail']); $j++) { ?>
                                                <?php if (($member[$i] == $malam['detail'][$j]['id_member']) && ($list['id_kue'] == $malam['detail'][$j]['id_kue'])):
					$cek = 1; ?>
                                                    <td><?php echo $malam['detail'][$j]['jumlah'];
					$total += $malam['detail'][$j]['jumlah'] * 1; ?></td>
                                                    <?php endif; ?>
                                                <?php } ?>
                                                <?php if ($cek == 0): ?>
                                                    <td><?php echo 0 ?></td>
                                                <?php endif; ?>
                                            <?php } ?>
                                            <th style="background-color: #ff6600"><?php echo $total; ?>
                                        </tr>
                                        <?php $counter++; ?>
                                    <?php endforeach; ?>
                                    </tbody>
                                    <?php endif; ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
		</div>
	</section>
</div>
