<script type="text/javascript">
	$(function () {
		$('#searchBtn').click(function () {
			$('#searchBarang').submit();
		});
	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('#tgl').datepicker({
		autoclose: true
	});

	$('.table').DataTable({
		"scrollX": true,
		"paging": false,
		"scrollY": 300,
		"searching": false,
	});

</script>
