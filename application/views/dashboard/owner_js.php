<script type="text/javascript">
	$(function () {
		$('#searchBtn').click(function () {
			$('#searchBarang').submit();
		});
	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('#tgl').datepicker({
		autoclose: true
	});

	$('#listTable').DataTable({
		"scrollX": true,
		"paging": false,
		"scrollY": 300,
		"searching": false,
	});

	$('#tagihanTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getTagihan'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<a href="<?php echo base_url();?>masuk/' + row['id_masuk'] + '/payment" data-toggle="tooltip" title="Pembayaran" class="btn btn-sm btn-warning"><i class="fa fa-money fa-lg"></i></a> &nbsp;';
			}
		}, {
			field: 'document',
			title: 'Document',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="document' + row['id'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'supplier',
			title: 'Supplier',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'total',
			title: 'Total Tagihan',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	function deleted(id) {
		$.ajax({
			url: "<?php echo base_url('pos/hapus'); ?>",
			method: 'post',
			data: {
				idPenjualanHapus: $('#idPenjualanHapus').val(),
				alasan: $('#alasan').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Penjualan berhasil dihapus', 'success', function () {
						$('#penjualanModalHapus').modal('hide');
						$('#penjualanTable').bootstrapTable('refresh', {
							silent: true
						});
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	};

	$('#penjualanModalHapus').on('hidden.bs.modal', function (event) {
		$('#idPenjualanHapus').val("");
		$('#alasan').val("");
	});

	$('#penjualanHapusForm').validate({
		errorElement: 'div',
		rules: {
			alasan: {
				required: true,
			}
		},

		messages: {
			alasan: {
				required: "Alasan harus diisi",
			}
		}
	});

	function hapusPesanan(event, value, row, index) {
		$('#idPenjualanHapus').val(row.id);
		$('#penjualanHapusForm').data('id', row.id);
		$('#penjualanModalHapus').modal('show');
	}

	$('#penjualanTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getPenjualanPending'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				var btn = "";

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['status'] == 1) {
						btn += '<a class="btn btn-sm btn-default" disable ><i class="fa fa-trash fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a href="javascript:void(0)" data-toggle="tooltip" title="Hapus Penjualan" class="btn btn-sm btn-danger hapus"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';
					}
				}

				btn += '<a href="<?php echo base_url();?>pos/' + row['id'] + '" data-toggle="tooltip" title="Detail POS" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';

				if (row['status'] == 0) {
					btn += '<a href="<?php echo base_url();?>pos/' + row['id'] + '/edit" data-toggle="tooltip" title="Edit POS" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
				} else {
					btn += '<a class="btn btn-default" disable ><i class="fa fa-pencil"></i></a> &nbsp;';
				}

				return btn;
			},
			events: {
				'click .hapus': hapusPesanan,
			}
		}, {
			field: 'document',
			title: 'Document',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="document' + row['id'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'grandtotal',
			title: 'Grand Total',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'totalQty',
			title: 'Total Item',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'keterangan',
			title: 'Keterangan',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (row['last_document']) {
					return value + " " + row['last_document'];
				} else {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});
</script>
