<style type="text/css">
	th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #4CAF50;
		color: white;
	}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			DASHBOARD
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-aqua">
					<div class="inner">
						<h2>Tagihan (<?php echo $totalTagihan; ?>)</h2>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-tagihan">More info
						<i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>

			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h2>Penjualan Pending (<?php echo $totalPenjualanPending; ?>)</h2>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-penjualan-pending">More info
						<i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">DAFTAR PESANAN <?php echo $tgl ?></h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<form class="form-horizontal" id="searchBarang" method="get" action="">
						<section>
							<div class="col-sm-2">
								<div class="form-group">
									<input type="text" class="form-control tgl pull-right input-sm" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tgl)); ?>">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<button type="button" class="btn btn-block btn-warning btn-sm" id="searchBtn">
										<i class="fa fa-search"> Cari </i></button>
								</div>
							</div>
						</section>
					</form>
				</div>
				<?php $barang = $result['barang']; ?>
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">SHIFT 1</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse">
								<i class="fa fa-minus"></i></button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="box-body">
									<table id="listTable" class="table table-bordered table-striped" width="100%">
										<thead>
										<tr>
											<th>Kue</th>
											<?php foreach ($barang['list'] as $list) : ?>
												<?php $idKue = $list['id_kue']; ?>
											<?php endforeach; ?>
											<th>PAGI</th>
											<th>MALAM</th>
										</tr>
										</thead>
										<tbody>
										<?php $counter = 1; ?>
										<?php foreach ($barang['list'] as $list) : $cekPagi = 0;
											$cekMalam = 0; ?>
											<tr>
												<th style="background-color: #ff6600"><?php echo $list['nama']; ?></th>
												<?php foreach ($barang['pagi'] as $value) : ?>
													<?php if ($list['id_kue'] == $value['id_kue']) : ?>
														<?php $cekPagi = 1; ?>
														<td><?php echo $value['jumlah']; ?></td>
													<?php endif; ?>
												<?php endforeach; ?>

												<?php if ($cekPagi == 0) : ?>
													<td><?php echo 0 ?></td>
												<?php endif; ?>

												<?php foreach ($barang['malam'] as $value) : ?>
													<?php if ($list['id_kue'] == $value['id_kue']) : ?>
														<?php $cekMalam = 1; ?>
														<td><?php echo $value['jumlah']; ?></td>
													<?php endif; ?>
												<?php endforeach; ?>

												<?php if ($cekMalam == 0) : ?>
													<td><?php echo 0 ?></td>
												<?php endif; ?>
											</tr>
											<?php $counter++; ?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="modal-tagihan">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">List Tagihan</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="tagihanTable" style="width:100%">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-penjualan-pending">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">List Penjualan Pending</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="penjualanTable" style="width:100%">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="penjualanModalHapus" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="hapusModalLabel">Hapus Penjualan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-4 control-label">Alasan :</label>
					<div class="col-sm-8 input-group">
						<textarea class="form-control" id="alasan" name="alasan" required></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="idPenjualanHapus" id="idPenjualanHapus">
				<button class="btn btn-info pull-right" onclick="deleted();"><i class="fa fa-save"></i> Simpan</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Tidak</button>
			</div>
		</div>
	</div>
</div>
