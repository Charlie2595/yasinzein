<style type="text/css">
	th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #4CAF50;
		color: white;
	}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			DASHBOARD
		</h1>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-lg-3 col-xs-6">
				<div class="small-box bg-yellow">
					<div class="inner">
						<h2>Penjualan Pending (<?php echo $totalPenjualanPending; ?>)</h2>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="#" class="small-box-footer" data-toggle="modal" data-target="#modal-penjualan-pending">More info
						<i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		</div>
	</section>
</div>

<div class="modal fade" id="modal-penjualan-pending">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">List Penjualan Pending</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="penjualanTable" style="width:100%">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="penjualanModalHapus" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="hapusModalLabel">Hapus Penjualan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-4 control-label">Alasan :</label>
					<div class="col-sm-8 input-group">
						<textarea class="form-control" id="alasan" name="alasan" required></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="idPenjualanHapus" id="idPenjualanHapus">
				<button class="btn btn-info pull-right" onclick="deleted();"><i class="fa fa-save"></i> Simpan</button>
				<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Tidak</button>
			</div>
		</div>
	</div>
</div>
