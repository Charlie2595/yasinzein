<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	};

	function deleteSupplier(id) {
		var page = '/' + id + '/deactivate';
		$('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page); ?>" + page,
			method: 'post',
			data: {
				id: id,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				console.log(data['status']);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					swal('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					swal('', data['description'], 'success').then((value) => {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					swal('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	};

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		url: "<?php echo base_url('getAllSupplier'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				btn += '<a href="<?php echo base_url();?>supplier/' + row['id_sup'] + '/edit" data-toggle="tooltip" title="Edit Supplier" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';

				btn += '<a href="#" class="btn btn-sm btn-danger waves-effect waves-light delete" data-toggle="tooltip" title="Delete Supplier"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';

				return btn;
			},
			events: {
				'click .delete': function (e, value, row, index) {
					showConfirm('', 'Are you sure you want to delete this supplier?', 'warning', function () {
						$.ajax({
							url: "<?php echo base_url();?>supplier/" + row['id_sup'] + "/delete",
							success: function (data) {
								var data = $.parseJSON(data);
								if (data['status'] == 'error') {
									showAlert('', data['description'], 'error');
								} else {
									showAlert('', data['description'], 'success', function (e) {
										$('#listTable').bootstrapTable('refresh');
									});
								}
							},
							error: function (xhr, text, status) {
								if (xhr.status == 422) {
									showAlert('', xhr.responseJSON.join(' '), 'error');
								}
							}
						});
					});
				}
			}
		}, {
			field: 'nama',
			title: 'Nama',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'email',
			title: 'Email',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat',
			title: 'Alamat',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'ket',
			title: 'Keterangan',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

</script>
