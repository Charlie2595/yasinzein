<script type="text/javascript">
	$('#confirmSubmit').click(function () {
		$('#createForm').submit();
	});

	$("#buttonModal").click(function () {
		if ($("#createForm").valid()) {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/edit'); ?>",
				method: 'post',
				data: {
					id: $('#id').val(),
					nama: $('#nama').val(),
					email: $('#email').val(),
					hp: $('#hp').val(),
					rumah: $('#rumah').val(),
					keterangan: $('#keterangan').val(),
					kue: $('#kue').val(),
				},
				success: function (data) {
					var data = $.parseJSON(data);
					console.log(data['status']);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		}
	});

	$('.multiple').select2();

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			nama: {
				required: true,
			},
			hp: {
				required: true,
			},
			'kue[]': {
				required: true,
			},
		},

		messages: {
			nama: {
				required: "Nama harus diisi",
			},
			hp: {
				required: "No Telp harus diisi",
			},
			'kue[]': {
				required: "Barang harus diisi",
			},
		}
	});

</script>
