<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			UBAH
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">UBAH <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo strtoupper($page); ?> FORM</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post">
						<div class="box-body">
							<div class="col-md-4">
								<div class="form-group">
									<label><h4>Nama</h4></label>
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="nama" name="nama" placeholder="Nama" value="<?php echo $detail['nama']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
                    </span>
									</div>
									<!-- /.input group -->
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label><h4>Email</h4></label>
									<div class="input-group">
										<input type="email" class="form-control pull-right" id="email" name="email" placeholder="Email" value="<?php echo $detail['email']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-envelope"></i></button>
                    </span>
									</div>
									<!-- /.input group -->
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group">
									<label><h4>No HP</h4></label>
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="hp" name="hp" placeholder="No Handphone" value="<?php echo $detail['nohp']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-phone"></i></button>
                    </span>
									</div>
									<!-- /.input group -->
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label><h4>Alamat</h4></label>
									<div class="input-group">
										<textarea id="rumah" name="rumah" class="form-control pull-right" placeholder="Alamat Rumah" rows="2" cols="100"><?php echo $detail['alamat']; ?></textarea>
									</div>
									<!-- /.input group -->
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label><h4>Keterangan</h4></label>
									<div class="input-group">
										<textarea id="keterangan" name="keterangan" class="form-control pull-right" placeholder="Keterangan" rows="2" cols="100"><?php echo $detail['ket']; ?></textarea>
									</div>
									<!-- /.input group -->
								</div>
							</div>
							<?php foreach ($kueSupplier as $value) {
								$kueList[] = $value['id_kue'];
							} ?>
							<div class="col-sm-12">
								<div class="form-group">
									<label><h4>Barang</h4></label>
									<select class="form-control multiple" name="kue[]" id="kue" multiple="multiple" style="width: 100%" required>
										<?php foreach ($barang as $barang) : ?>
											<?php if ($kueList) : ?>
												<option <?php echo in_array($barang['id_kue'], $kueList) ? 'selected' : ''; ?> value="<?php echo $barang['id_kue'] ?>"><?php echo $barang['barcode_kue'] . " - " . $barang['nama_kue'] ?></option>
											<?php else : ?>
												<option value="<?php echo $barang['id_kue'] ?>"><?php echo $barang['barcode_kue'] . " - " . $barang['nama_kue'] ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="buttonModal">
									<i class="fa fa-save"></i> Simpan
								</button>
								<div id="confirm-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Perubahan Data <?php echo($page); ?></h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin mengubah <?php echo($page); ?> ini?</p>
											</div>
											<div class="modal-footer">
												<button type="button" id="confirmSubmit" class="btn btn-success waves-effect">
													<i class="fa fa-check"></i> Ya
												</button>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<input type="hidden" name="id" id="id" value="<?php echo $detail['id_sup']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
