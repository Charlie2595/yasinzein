<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Master
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master <?php echo $page; ?> </li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<form id="formUpload" class="form-horizontal" enctype="multipart/form-data">
							<div class="col-sm-12">
								<p><label>Select Excel File</label>
									<input type="file" name="file" id="file" required accept=".xls, .xlsx"/></p>
								<br/>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<button type="button" class="btn btn-info btn-lg btn-block" id="import">
										<i class="fa fa-save"></i> IMPORT
									</button>
								</div>
								<!-- <div class="col-sm-4">
                                    <button type="button" class="btn btn-info btn-lg btn-block" id="btnSimpan"> <i class="fa fa-save"></i> SIMPAN</button>
                                </div> -->
							</div>
						</form>
						<div class="progress" style="display:none;">
							<div id="progress-bar" class="progress-bar progress-bar-success progress-bar-striped " role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 30%;">
								20%
							</div>
						</div>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Kode</th>
								<th>Nama</th>
								<th>Jenis</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Departemen</th>
								<th>Produksi</th>
							</tr>
							</thead>
							<tbody id="bodyList">

							</tbody>
						</table>
					</div>

				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
