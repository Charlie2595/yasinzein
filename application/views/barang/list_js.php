<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		url: "<?php echo base_url('getAllKue'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				btn += '<a href="<?php echo base_url();?>barang/' + row['id_kue'] + '/edit" data-toggle="tooltip" title="Edit Barang" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';

				btn += '<a href="#" class="btn btn-sm btn-danger waves-effect waves-light delete" data-toggle="tooltip" title="Delete Barang"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';

				return btn;
			},
			events: {
				'click .delete': function (e, value, row, index) {
					showConfirm('', 'Are you sure you want to delete this product?', 'warning', function () {
						$.ajax({
							url: "<?php echo base_url();?>barang/" + row['id_kue'] + "/delete",
							success: function (data) {
								var data = $.parseJSON(data);
								if (data['status'] == 'error') {
									showAlert('', data['description'], 'error');
								} else {
									showAlert('', data['description'], 'success', function (e) {
										$('#listTable').bootstrapTable('refresh');
									});
								}
							},
							error: function (xhr, text, status) {
								if (xhr.status == 422) {
									showAlert('', xhr.responseJSON.join(' '), 'error');
								}
							}
						});
					});
				}
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'jenis_kue',
			title: 'Jenis Kue',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'departemen',
			title: 'Departemen',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return "YA";
				} else {
					return "TIDAK";
				}
			}
		}]
	});

</script>
