<div class="content-wrapper">
	<section class="content-header">
		<h1>
			MASTER
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master <?php echo $page; ?> </li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> LIST <?php echo strtoupper($page); ?></h3>
						<?php if ($_SESSION['jabatan'] === 'Owner') : ?>
							<div class="col-sm-2 pull-right">
								<a href="<?php echo base_url($page . '/create') ?>" class="btn btn-success pull-right">
									<i class="fa fa-plus"></i> Barang Baru</a>
							</div>
							<div class="col-sm-2 pull-right">
								<a href="<?php echo base_url($page . '/import') ?>" class="btn btn-info pull-right">
									<i class="fa fa-plus"></i> Import <?php echo $page; ?></a>
							</div>
						<?php endif; ?>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped text-nowrap">
							<thead>
							<tr>
								<th>Kode Barcode</th>
								<th>Nama</th>
								<th>Jenis Kue</th>
								<th>Departemen</th>
								<th>Harga</th>
								<th>Stok</th>
								<th>Produksi</th>
								<?php if ($_SESSION['jabatan'] === 'Owner') : ?>
									<th>Aksi</th>
								<?php endif; ?>
							</tr>
							</thead>
							<tbody>
							<?php $counter = 1; ?>
							<?php foreach ($list as $list) : ?>
								<tr>
									<td><?php echo $list['barcode_kue']; ?></td>
									<td><?php echo $list['nama_kue']; ?></td>
									<td><?php echo $list['id_jenis_kue']; ?></td>
									<td><?php echo $list['departemen']; ?></td>
									<td><?php echo $list['harga']; ?></td>
									<td><?php echo $list['stok']; ?></td>
									<td><?php echo $list['produksi']; ?></td>
									<?php if ($_SESSION['jabatan'] === 'Owner') : ?>
										<td class="p-t-8 p-b-6">
											<a href="<?php echo base_url($page . '/' . $list['id_kue'] . '/edit'); ?>" class="btn btn-success"><i class="fa fa-pencil"></i>
											</a>
											<!-- <a type="button" class="btn btn-danger waves-effect waves-light" id="buttonModal<?php echo $counter; ?>" onclick="showModal(<?php echo $counter; ?>);"> <i class="fa fa-trash"></i></a> -->
										</td>
									<?php endif; ?>
								</tr>
								<div id="confirm-edit-modal<?php echo $counter; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Menghapus <?php echo $page; ?></h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin membuat Mengapus ini?</p>
											</div>
											<div class="modal-footer">
												<a href="<?php echo base_url($page . '/' . $list['id_kue'] . '/deactivate'); ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Ya
												</a>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
								<?php $counter++; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
