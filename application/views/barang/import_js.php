<script>
	$(document).ready(function () {
		var barang = [];


		function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
			var num = parseFloat(amount); //convert to float
			//default values
			decimalSeparator = decimalSeparator || ',';
			thousandsSeparator = thousandsSeparator || '.';
			nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

			var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
			//separate begin [$1], middle [$2] and decimal digits [$4]
			var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

			if (parts) { //num >= 1000 || num < = -1000
				return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
			} else {
				return fixed.replace('.', decimalSeparator);
			}
		};

		$('#formUpload').on('submit', function (event) {
			$('#pleaseWaitDialog').modal('show');
			event.preventDefault();
			$.ajax({
				url: "<?php echo base_url(); ?>import/view",
				method: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Master barang berhasil disimpan', 'success');
					$('#file').val('');
					$("#bodyList").html("");
					barang = [];
					var mydata = $.parseJSON(data);
					for (var i = 0; i < mydata.length; i++) {
						var obj = new Object();
						obj.kode = mydata[i].kode;
						obj.harga = mydata[i].harga;
						obj.nama = mydata[i].nama;
						obj.jenis = mydata[i].jenis;
						obj.departemen = mydata[i].departemen;
						obj.produksi = mydata[i].produksi;
						obj.diskon = mydata[i].diskon;
						obj.stok = mydata[i].stok;
						barang.push(obj);
						idx = barang.length - 1;
						var newRow = "<tr id='baris" + idx + "'>" +
							"<td><input type='hidden' name='kode[]' id='kode" + idx + "' value='" + mydata[i].kode + "'>" + mydata[i].kode + "</td>" +
							"<td><input type='hidden' name='nama[]' id='nama" + idx + "' value='" + mydata[i].nama + "'>" + mydata[i].nama + "</td>" +
							"<td><input type='hidden' name='jenis[]' id='jenis" + idx + "' value='" + mydata[i].jenis + "'>" + mydata[i].jenis + "</td>" +
							"<td><input type='hidden' name='harga[]' id='harga" + idx + "' value='" + mydata[i].harga + "'>" + toRp(mydata[i].harga) + "</td>" +
							"<td><input type='hidden' name='stok[]' id='stok" + idx + "' value='" + mydata[i].stok + "'>" + mydata[i].stok + "</td>" +
							"<td><input type='hidden' name='departemen[]' id='departemen" + idx + "' value='" + mydata[i].departemen + "'>" + mydata[i].departemen + "</td>" +
							"<td><input type='hidden' name='produksi[]' id='produksi" + idx + "' value='" + mydata[i].produksi + "'>" + mydata[i].produksi + "</td></tr>";
						$("#bodyList").append(newRow);
					}
					$("#btnSimpan").removeAttr("disabled");
				}
			})
		});

		$("#import").click(function () {
			var file = $('#file').val();
			if (file == '') {
				showAlert('', 'Pilih file terlebih dahulu', 'error');
			} else {
				$("#formUpload").submit();
			}
		});
	});
</script>
