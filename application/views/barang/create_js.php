<script type="text/javascript">
	$('#btnSimpan').click(function () {
		// $('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page . '/create'); ?>",
			method: 'post',
			data: {
				kode: $('#kode').val(),
				nama: $('#nama').val(),
				jenis: $('#jenis').val(),
				departemen: $('#departemen').val(),
				harga: $('#harga').val(),
				produksi: $('#produksi').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				console.log(data['status']);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$("#buttonModal").click(function () {
		if ($("#createForm").valid()) {
			$('#confirm-edit-modal').modal('show');
		}
	});

	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	})

	//Date picker
	$('#tgllahir').datepicker({
		autoclose: true
	})

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			kode: {
				required: true,
				remote: {
					url: "<?php echo base_url();?>cekKodeKue",
					type: "post",
					data:
						{
							kode: function () {
								return $("#kode").val();
							}
						}
				}
			},
			nama: {
				required: true,
			},
			jenis: {
				required: true,
			},
			departemen: {
				required: true,
			},
			harga: {
				required: true,
			},
		},

		messages: {
			kode: {
				required: "Kode harus diisi",
				remote: jQuery.validator.format("{0} sudah digunakan.")
			},
			jenis: {
				required: "Jenis harus diisi",
			},
			nama: {
				required: "Nama harus diisi",
			},
			departemen: {
				required: "Departemen harus diisi",
			},
			harga: {
				required: "Harga harus diisi",
			},
		}
	});

</script>
