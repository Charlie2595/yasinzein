<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Ubah
			<small>Barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url('barang'); ?>">Barang</a></li>
			<li class="active">Ubah Barang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Barang Form</h3>
					</div>
					<form class="form-horizontal" id="editForm" method="post">
						<div class="box-body">
							<div class="form-group">
								<label for="inputKode" class="col-sm-4 control-label">Kode Barcode :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="kode" placeholder="Kode" disabled name="kode" value="<?php echo $detail['barcode_kue']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputNama" class="col-sm-4 control-label">Nama :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="<?php echo $detail['nama_kue']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputJenis" class="col-sm-4 control-label">Jenis :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="jenis" name="jenis" value="<?php echo $detail['jenis_kue']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputDepartemen" class="col-sm-4 control-label">KLASIFIKASI :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="departemen" name="departemen" value="<?php echo $detail['departemen']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputDepartemen" class="col-sm-4 control-label">Harga :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="harga" name="harga" value="<?php echo $detail['harga']; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Produksi :</label>
								<input type="radio" name="produksi" id="produksi" value="1" class="minimal" <?php echo $detail['produksi'] == '1' ? 'checked' : ''; ?>> YA
								<input type="radio" name="produksi" id="produksi" value="0" class="minimal" <?php echo $detail['produksi'] == '0' ? 'checked' : ''; ?>> TIDAK
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="btnSimpan">
									<i class="fa fa-save"></i> Simpan
								</button>
								<div id="confirm-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Ubah Barang</h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin mengubah barang ini?</p>
											</div>
											<div class="modal-footer">
												<button type="button" id="btnSimpan" class="btn btn-success waves-effect">
													<i class="fa fa-check"></i> Ya
												</button>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<input type="hidden" name="idKue" id="idKue" value="<?php echo $detail['id_kue']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
