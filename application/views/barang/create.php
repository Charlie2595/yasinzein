<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Buat
			<small>Barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url('barang'); ?>">Barang</a></li>
			<li class="active">Buat Barang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Barang Form</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url('barang/create'); ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="inputKode" class="col-sm-4 control-label">Kode Barcode :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="kode" placeholder="Kode" name="kode">
								</div>
							</div>
							<div class="form-group">
								<label for="inputNama" class="col-sm-4 control-label">Nama :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama">
								</div>
							</div>
							<div class="form-group">
								<label for="inputJenis" class="col-sm-4 control-label">Jenis :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="jenis" name="jenis">
								</div>
							</div>
							<div class="form-group">
								<label for="inputDepartemen" class="col-sm-4 control-label">Departemen :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="departemen" name="departemen">
								</div>
							</div>
							<div class="form-group">
								<label for="inputDepartemen" class="col-sm-4 control-label">Harga :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="harga" name="harga">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Produksi :</label>
								<input type="radio" name="produksi" value="1" class="minimal" checked> YA
								<input type="radio" name="produksi" value="0" class="minimal"> TIDAK
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="btnSimpan">
									<i class="fa fa-save"></i> Simpan
								</button>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
