<script type="text/javascript">
	$("#btnPrint").on('click', function () {
		window.frames["invoiceFrame"].focus();
		window.frames["invoiceFrame"].print();

		$.ajax({
			url: "<?php echo base_url('openRegister')?>",
			method: 'post',
			success: function (response) {
				if (response.status == 'error') {
					showAlert('', response.description, 'error');
				}
			}, error: function (xhr, text, status) {
				$('#pleaseWaitDialog').modal('hide');
				if (xhr.status == 422) {
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});
</script>
