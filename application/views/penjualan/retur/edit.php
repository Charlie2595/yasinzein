<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<!--  <style>
 .ui-autocomplete {
   max-height: 300px;
   overflow-y: auto;
   /* prevent horizontal scrollbar */
   overflow-x: hidden;
 }
 /* IE 6 doesn't support max-height
  * we use height instead, but this forces the menu to always be this tall
  */
 * html .ui-autocomplete {
   height: 400px;
 }
.fixed_header{
   width: 100%;
   border-collapse: collapse;
}

.fixed_header tbody{
 display:block;
 width: 100%;
 overflow: auto;
 height: 350px;
}

.fixed_header thead tr {
  display: block;
}

.fixed_header th, .fixed_header td {
 padding: 5px;
 text-align: left;
 width: 200px;
}
 </style> -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Ubah Pesanan
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">Pembayaran <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/edit'); ?>">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Tanggal :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control pull-right tgl" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($data['tgl'])); ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Member :</h4></label>
											<div class="input-group">
												<input type="hidden" name="idMember" id="idMember" value="<?php echo $data['id_member'] ?>">
												<input type="hidden" name="diskonBarang" id="diskonBarang" value="<?php echo $data['diskon_barang'] ?>">
												<input type="text" class="form-control" id="member" placeholder="Member" name="member" readonly value="<?php echo $data['member']; ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label><h4>Pegawai :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" value="<?php echo $_SESSION['nama'] ?>" readonly>
												<input type="hidden" name="idPegawai" id="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
									</span>
											</div>
										</div>
									</div>
									<ul class="nav nav-tabs">
									</ul>
									<div class="box-body">
										<table id="detailTable">
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2>
													<label id="viewTotalItem">(0 Item)</label></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right"><?php echo number_format($data['total'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="total" id="total" value="<?php echo $data['total']; ?>">
												<input type="hidden" name="diskonMember" id="diskonMember" value="<?php echo $data['diskon_member']; ?>">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h3 class="list-group-item-heading">RETUR</h3>
													<label id="viewTotalItemRetur">(0 Item)</label></div>
												<div class="col-sm-6"><h3 class="list-group-item-heading pull-right">
														<span id="viewRetur" class="pull-right">0</span></h3></div>
												<input type="hidden" name="totalRetur" id="totalRetur">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h2 class="list-group-item-heading">GRAND TOTAL</h2></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right"><?php echo number_format($data['total'] - ($data['total'] * $data['diskon_member'] / 100) - $data['diskon_pembulatan'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="grandtotal" value="<?php echo($data['total'] - ($data['total'] * $data['diskon_member'] / 100) - $data['diskon_pembulatan']) ?>" id="grandtotal">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> SIMPAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>
								</section>
							</div>
						</div>
						<input type="hidden" name="type" id="type" value="B2B">
						<input type="hidden" name="idPenjualan" id="idPenjualan" value="<?php echo $data['id_jual']; ?>">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
					</form>
				</div>
		</section>
	</div>
</div>
