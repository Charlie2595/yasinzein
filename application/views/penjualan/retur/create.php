<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Buat
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">BUAT <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/create'); ?>">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Date:</h4></label>
											<div class="input-group date">
												<input type="text" class="form-control pull-right" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y'); ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
											<!-- /.input group -->
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Member :</h4></label>
											<div class="input-group">
												<input type="hidden" id="idMember" name="idMember">
												<input type="hidden" id="diskonMember" name="diskonMember">
												<input type="text" class="form-control" id="member" placeholder="Member" name="member" data-toggle="modal" data-target="#modal-member" readonly>
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-member"><i class="fa fa-search"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label><h4>Pegawai :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" value="<?php echo $_SESSION['nama'] ?>" readonly>
												<input type="hidden" name="idPegawai" id="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
									</span>
											</div>
										</div>
									</div>

									<!-- Detail -->
									<section>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" id="searchPenjualan">
													<i class="fa fa-search"> Cari Penjualan </i></button>
											</div>
										</div>
									</section>

									<!-- /.box-body -->
									<div class="box-body">
										<table class="table table-bordered" id="detailTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th width="35%" style="text-align: center">Nama Barang</th>
												<th width="20%" style="text-align: center">Harga</th>
												<th width="20%" style="text-align: center">Jumlah</th>
												<th width="20%" style="text-align: center">Jumlah Retur</th>
											</tr>
											</thead>
											<tbody id="bodyNota">
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2><label id="viewTotalItem">(0 Item)</label>
												</div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right">0</span></h2></div>
												<input type="hidden" name="totalPenjualan" value="" id="totalPenjualan">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h3 class="list-group-item-heading">RETUR</h3>
												<label id="viewTotalItemRetur">(0 Item)</label></div>
												<div class="col-sm-6"><h3 class="list-group-item-heading pull-right">
														<span id="viewRetur" class="pull-right">0</span></h3></div>
												<input type="hidden" name="totalRetur" id="totalRetur">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h2 class="list-group-item-heading">GRAND TOTAL</h2></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right">0</span></h2></div>
												<input type="hidden" name="grandtotal" value="" id="grandtotal">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> SIMPAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>

								</section>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<input type="hidden" name="idPenjualan" id="idPenjualan"/>
						<!-- /.box-footer -->
					</form>
					<!-- /.box -->
				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
	</div>
	<!-- /.content -->
</div>

<div class="modal fade modal-lg" id="modal-member" style="width: 100%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Member</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="memberTable">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-nota">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Nota</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="detailTable" style="width:100%">
					<thead class="alert">
					<tr>
						<th width="3%"></th>
						<th width="25%" style="text-align: center">Document</th>
						<th width="25%" style="text-align: center">Tanggal</th>
						<th width="20%" style="text-align: center">Pegawai</th>
						<th width="20%" style="text-align: center">Total</th>
					</tr>
					</thead>
					<tbody id="bodyPenjualan">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
