<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var penjualan = []; //array penjualan
	var kode = "";
	var nama = "";
	var harga = "";
	var jumlah = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var barang = []; //array barang

	$(document).ready(function () {
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
	});

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		$('#diskonMember').val(diskon);
		$('#member').val(nama);
		$('#modal-member').modal('hide');
	}

	//Function untuk mencari history transaksi member
	function searchPenjualan() {
		var idMember = $('#idMember').val();
		$.ajax({
			url: "<?php echo base_url();?>searchPenjualan/" + idMember + "/cabangRetur",
			success: function (data) {
				var mydata = $.parseJSON(data);
				var newRow = "";
				for (var i = 0; i < mydata.length; i++) {
					newRow += "<tr><td width='5%'><button type='button' class='btn btn-xs btn-primary' onclick='choosePenjualan(" + i + ");'><i class='fa fa-plus'></button></td>" +
						"<td><input type='hidden' id='id_penjualan" + i + "' value='" + mydata[i].id_jual + "'><input type='hidden' id='tgl" + i + "' value='" + mydata[i].tgl + "'>" + mydata[i].document + "</td>" +
						"<td>" + mydata[i].tgl + "</td>" +
						"<td>" + mydata[i].pegawai + "</td>" +
						"<td><input type='hidden' id='total" + i + "' value='" + (mydata[i].total * 1 - ((mydata[i].diskon_member * mydata[i].total / 100) * 1 + mydata[i].diskon_pembulatan * 1)) + "'>" + toRp(mydata[i].total * 1 - ((mydata[i].diskon_member * mydata[i].total / 100) * 1 + mydata[i].diskon_pembulatan * 1)) + "</td></tr>";
				}
				$("#bodyPenjualan").html(newRow);
			}
		});
	}

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/2'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'email',
			title: 'Email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	function choosePenjualan(id) {
		var idPenjualan = $('#id_penjualan' + id).val();
		detailPenjualan(idPenjualan);
		$('#modal-nota').modal('hide');
	}

	function detailPenjualan(id) {
		$.ajax({
			url: "<?php echo base_url();?>detailPenjualan/" + id,
			success: function (data) {
				var mydata = $.parseJSON(data);
				var newRow = "";
				var detail = mydata['detail'];
				var totalItem = 0;
				$('#totalPenjualan').val(mydata['total']);
				$('#viewTotalBayar').html(toRp(mydata['total']));
				$('#viewGrandtotal').html(toRp(mydata['total']));
				$('#grandtotal').val(mydata['total']);
				$('#idPenjualan').val(mydata['id_jual']);
				for (var i = 0; i < detail.length; i++) {
					var obj = new Object();
					obj.idDetail = detail[i].id_jualdetail;
					obj.idBarang = detail[i].id_kue;
					obj.jumlah = detail[i].jumlah;
					obj.jumlahRetur = 0;
					barang.push(obj);
					totalItem += parseInt(detail[i].jumlah);
					newRow += "<tr><td><input type='hidden' name='idDetail[]' value='" + detail[i].id_jualdetail + "'><input type='hidden' name='idBarang[]' id='idBarang" + i + "' value='" + detail[i].id_kue + "'>" + detail[i].barcode_kue + " - " + detail[i].nama_kue + "</td>" +
						"<td><input type='hidden' id='harga" + i + "' value='" + detail[i].harga + "' >" + toRp(detail[i].harga) + "</td>" +
						"<td>" + (detail[i].jumlah * 1 - detail[i].retur * 1) + "</td>" +
						"<td><input type='number' style='width=100%' name='jumlahRetur[]' id='jumlahRetur" + i + "' value='0' min='0' max='" + (detail[i].jumlah * 1 - detail[i].retur * 1) + "' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max='Jumlah retur tidak boleh melebihi jumlah beli.' onchange='hitungRetur(" + i + ");'></td></tr>";
				}
				$("#viewTotalItem").html("(" + totalItem + " Item)");
				$("#bodyNota").html(newRow);
			}
		});
	}

	function hitungRetur(idx) {
		var grandtotal = $('#grandtotal').val() * 1;
		var harga = $('#harga' + idx).val() * 1;
		var jumlahRetur = $('#jumlahRetur' + idx).val() * 1;
		barang[idx].jumlahRetur = jumlahRetur;

		var diskon = 0;
		var totalItemRetur = 0;
		for (var i = 0; i < barang.length; i++) {
			totalItemRetur += parseInt(barang[i].jumlahRetur);
		}

		var totalRetur = harga * jumlahRetur;

		$('#grandtotal').val(grandtotal - totalRetur);
		$('#viewRetur').html(toRp(totalRetur));
		$('#viewGrandtotal').html(toRp(grandtotal - totalRetur));
		$("#viewTotalItemRetur").html("(" + totalItemRetur + " Item)");
	}

	//Payment Submit
	$('#btnSimpan').click(function () {
		if (totalbayar >= grandtotal) {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/create'); ?>",
				method: 'post',
				data: {
					idMember: $('#idMember').val(),
					tgl: $('#tgl').val(),
					idPegawai: $('#idPegawai').val(),
					idPenjualan: $('#idPenjualan').val(),
					grandtotal: $('#grandtotal').val(),
					type: 'payment',
					barang: barang,
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Retur berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		} else {
			showAlert('', "Pembayaran kurang", 'error');
		}
	});

	$('#btnSubmit').click(function () {
		if ($('#createForm').valid()) {
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			var cek = 0;
			for (var i = 0; i < jumlahrow; i++) {
				if ($('#jumlahRetur' + i).val() > 0) {
					cek++
				}
			}
			if (cek > 0) {
				console.log($('#idPegawai').val());
				$('#pleaseWaitDialog').modal('show');
				$.ajax({
					url: "<?php echo base_url($page . '/create'); ?>",
					method: 'post',
					data: {
						idMember: $('#idMember').val(),
						tgl: $('#tgl').val(),
						idPegawai: $('#idPegawai').val(),
						idPenjualan: $('#idPenjualan').val(),
						grandtotal: $('#grandtotal').val(),
						type: 'payment',
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', 'Retur berhasil disimpan', 'success', function () {
								window.location = "<?php echo base_url($page); ?>"
							});
						}
					}, error: function (xhr, text, status) {
						console.log(status);
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			} else {
				showAlert('', 'Isi jumlah barang yang ingin diretur', 'error');
			}
		}
	});

	$('#searchPenjualan').click(function () {
		var idMember = $('#idMember').val();
		if (idMember == "") {
			showAlert('', "Pilih member terlebih dahulu", 'error');
		} else {
			searchPenjualan();
			$('#modal-nota').modal({
				show: 'true'
			});
		}
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('#tgl').datepicker({
		autoclose: true
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
			member: {
				required: true,
			}
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
			member: {
				required: "Member harus diisi",
			}
		}
	});
</script>
