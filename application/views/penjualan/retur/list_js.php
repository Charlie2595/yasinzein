<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'tgl',
		url: "<?php echo base_url('getAllRetur'); ?>",
		columns: [{
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal Retur',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="<?php echo base_url();?>retur/' + row['id_jual'] + '" data-toggle="tooltip" title="Detail" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';
				}
				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['lunas'] == 0) {
						btn += '<a href="<?php echo base_url();?>retur/' + row['id_jual'] + '/edit" data-toggle="tooltip" title="Ubah Retur Cabang" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-pencil"></i></a> &nbsp;';
					}
				}

				return btn;
			}
		}]
	});
</script>
