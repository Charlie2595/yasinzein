<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 13/07/2017
 * Time: 14.13
 */ ?>
<style>
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}

	th, td {
		text-align: center;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">DETAIL <?php echo strtoupper($page); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-lg-12">
				<div class="box">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<form class="form-horizontal" role="form">
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Tanggal :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['tgl']; ?></p></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Member :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['member']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Pegawai :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['pegawai']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-8">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">LUNAS :</label>
											<div class="col-sm-9 col-lg-8"><p class="form-control-static">
													<?php if ($detail['lunas'] == 1) : ?>
														LUNAS
													<?php else: ?>
														BELUM LUNAS
													<?php endif; ?>
												</p></div>
										</div>
									</div>
								</div>
								<?php if ($detail['tipe_trx'] == 3) : ?>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">NOTA :</label>
												<div class="col-sm-9 col-lg-8"><p class="form-control-static">
														<?php echo $detail['id_nota']; ?>
													</p></div>
											</div>
										</div>
									</div>
								<?php endif; ?>
								<?php if ($detail['tipe_trx'] != 1) : ?>
									<div class="row">
										<div class="col-md-8">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Dikirim :</label>
												<div class="col-sm-9 col-lg-8"><p class="form-control-static">
														<?php if ($detail['dikirim'] == 1) : ?>
															SUDAH DIKIRIM
														<?php else: ?>
															BELUM DIKIRIM
														<?php endif; ?>
													</p></div>
											</div>
										</div>
									</div>
								<?php endif; ?>
								<div class="row">
									<div class="col-xs-12 col-lg-12">
										<div class="table-responsive">
											<table id="detailTable" class="table table-bordered table-striped users">
												<thead>
												<tr>
													<th width="10%">No</th>
													<th width="35%" style="text-align: center">Nama Barang</th>
													<th width="20%" style="text-align: center">Harga</th>
													<th width="20%" style="text-align: center">Jumlah</th>
													<th width="20%" style="text-align: center">Subtotal</th>
												</tr>
												</thead>
												<tbody>
												<?php $counter = 1; ?>
												<?php foreach ($detailBarang as $detailBarang) : ?>
													<tr>
														<td><?php echo $counter ?></td>
														<td style="text-align: left"><?php echo $detailBarang['nama_kue']; ?></td>
														<td style="text-align: right"><?php echo $detailBarang['jumlah']; ?></td>
														<td style="text-align: right"><?php echo "Rp. " . number_format($detailBarang['harga'], 0, ",", ","); ?></td>
														<td style="text-align: right"><?php echo "Rp. " . number_format(($detailBarang['jumlah'] * $detailBarang['harga']), 0, ",", ","); ?></td>
													</tr>
													<?php $counter++; ?>
												<?php endforeach; ?>
												</tbody>
												<tfoot>
												<tr>
													<th colspan="7" style="text-align: right;" ;>Grandtotal</th>
													<th style="text-align: right;">
														<?php echo "Rp. " . number_format($detail['total'], 0, ",", ","); ?>
													</th>
												</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
