<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	function deleted(id) {
		$.ajax({
			url: "<?php echo base_url($page . '/hapus'); ?>",
			method: 'post',
			data: {
				idPenjualanHapus: $('#idPenjualanHapus').val(),
				alasan: $('#alasan').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Penjualan berhasil dihapus', 'success', function () {
						$('#penjualanModalHapus').modal('hide');
						$('#listTable').bootstrapTable('refresh', {
							silent: true
						});
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	};

	$('#penjualanModalHapus').on('hidden.bs.modal', function (event) {
		$('#idPenjualanHapus').val("");
		$('#alasan').val("");
	});

	$('#penjualanHapusForm').validate({
		errorElement: 'div',
		rules: {
			alasan: {
				required: true,
			}
		},

		messages: {
			alasan: {
				required: "Alasan harus diisi",
			}
		}
	});

	function hapusPesanan(event, value, row, index) {
		$('#idPenjualanHapus').val(row.id);
		$('#penjualanHapusForm').data('id', row.id);
		$('#penjualanModalHapus').modal('show');
	}

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'document',
		url: "<?php echo base_url('getAllPOS'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="javascript:void(0)" data-toggle="tooltip" title="Hapus Penjualan" class="btn btn-sm btn-danger hapus"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';
				}

				btn += '<a href="<?php echo base_url();?>pos/' + row['id'] + '" data-toggle="tooltip" title="Detail POS" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';

				if (row['status'] == 0) {
					btn += '<a href="<?php echo base_url();?>pos/' + row['id'] + '/edit" data-toggle="tooltip" title="Edit POS" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
				} else {
					btn += '<a class="btn btn-default" disable ><i class="fa fa-pencil"></i></a> &nbsp;';
				}

				if (row['lunas'] == 1 || row['lunas'] == 2) {
					btn += '<a href="<?php echo base_url();?>pos/' + row['id'] + '/print" data-toggle="tooltip" title="Print POS" class="btn btn-sm btn-primary"><i class="fa fa-print fa-lg"></i></a> &nbsp;';
				} else {
					btn += '<a class="btn btn-default" disable ><i class="fa fa-print"></i></a> &nbsp;';
				}

				return btn;
			},
			events: {
				'click .hapus': hapusPesanan,
			}
		}, {
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'grandtotal',
			title: 'Grand Total',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'totalQty',
			title: 'Total Item',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'keterangan',
			title: 'Keterangan',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (row['last_document']) {
					return (value == null ? "" : value == " " ? "" : value) + " " + row['last_document'];
				} else {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

</script>
