<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Master
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master <?php echo $page; ?> </li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> List <?php echo strtoupper($page); ?></h3>
						<a href="<?php echo base_url($page . '/create') ?>" class="btn btn-success pull-right">
							<i class="fa fa-plus"></i> Transaksi Baru </a>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">

						</table>
					</div>

				</div><!--end of whitebox -->
			</div>
		</div>
	</section>

	<div class="modal fade" id="penjualanModalHapus" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="hapusModalLabel">Hapus Penjualan</h4>
				</div>
				<div class="modal-body">
					<div class="col-sm-12 form-group">
						<label class="col-sm-4 control-label">Alasan :</label>
						<div class="col-sm-8 input-group">
							<textarea class="form-control" id="alasan" name="alasan" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idPenjualanHapus" id="idPenjualanHapus">
					<button class="btn btn-info pull-right" onclick="deleted();"><i class="fa fa-save"></i> Simpan
					</button>
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Tidak</button>
				</div>
			</div>
		</div>
	</div>
</div>
