<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var barang = []; //array barang
	var kode = "";
	var nama = "";
	var harga = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran
	var cekUsePoin = "";

	$(document).ready(function () {
		$("#searchKodeBarang").focus();
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();

		$('[class="row statusMember col-md-12"').hide();
		cekSubmit();
		$("#btnSimpan").attr("disabled", "disabled");

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
	});

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/0'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	function chooseBarang(kode) {
		$('#searchKodeBarang').val(kode);
		$('#addLineBtn').trigger('click');
		$('#modal-barang').modal('hide');
	}

	$('#barangTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getBarang'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseBarang(`' + row['barcode_kue'] + '`)"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="namasupplier' + row['id_sup'] + '" value="' + row['nama'] + '">'
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode Kue',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="barcode_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="nama_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="harga' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	shortcut.add("f1", function () {
		$("#searchKodeBarang").focus();
	});

	shortcut.add("f2", function () {
		$('#modal-barang').modal({show: 'true'});
	});

	shortcut.add("f3", function() {
	    $('#modal-pemotongan').modal({show: 'true'});
	});

	shortcut.add("f4", function () {
		$('#modal-payment').modal({show: 'true'});
	});

	shortcut.add("f8", function () {
		$('#btnSubmit').trigger('click');
	});

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;
		var diskon = 0;
		var totalItem = 0;
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			totalItem += item.qty * 1;
			var jumlah = item.qty * 1;
			var harga = item.harga * 1;
			var diskonBarang = 0;
			grandtotal += ((jumlah * harga) - diskonBarang);
		});

		$("#viewTotalBayar").html(toRp(grandtotal));
		$("#viewTotalItem").html("(" + totalItem + " Item)");
		$("#totalPenjualan").val(grandtotal); //Total Penjualan
		$("#calDiskonTotal").val(toRp(grandtotal)); //Total Penjualan

		diskon = grandtotal * (($('#diskonMember').val() * 1) / 100);
		$('#viewDiskon').html(toRp(diskon));

		var pemotongan = $('#pemotongan').val();
		grandtotal = grandtotal - diskon - pemotongan;
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#viewGrandtotal").html(toRp(grandtotal));
		$("#grandtotal").val(grandtotal);
		$("#calDiskonGrandtotal").val(toRp(grandtotal));
		$("#grandtotalPembelian").val(toRp(grandtotal));
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlah" + id).val());
		var harga = barang[id].harga * 1;
		var diskonBarang = barang[id].diskonBarang * 1;
		barang[id].jumlah = jumlah;

		var subtotal = (jumlah * harga) - diskonBarang;
		$("#subtotal" + id).html(toRp(subtotal));
		calculatetotal();
	}

	//hitung subtotal
	function hitungtotalPembayaran(id) {
		id = id * 1;
		var jumlah = eval($("#jumlahPembayaran" + id).val());
		bayar[id].jumlah = jumlah;

		calculatetotalPembayaran();
		hitungKembalian();
		cekSubmit();
	}

	//hitung total pembayaran
	function calculatetotalPembayaran() {
		totalbayar = 0;

		for (var i = 0; i < bayar.length; i++) {
			var jumlah = bayar[i].jumlah * 1;
			totalbayar += jumlah;
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#grandTotalBayar").val(totalbayar); //Total Penjualan
		hitungKembalian();
	}

	function updateDiskonBarang() {
		var diskonBarang = $("#diskonBarang").val();
		for (var i = 0; i < barang.length; i++) {
			var cekDiskon = barang[i].cekDiskon;
			if (cekDiskon != 0) {
				barang[i].diskon = 0;
			} else {
				barang[i].diskon = diskonBarang;
			}
			$("#diskonBarang" + i).val(diskonBarang);
			$("#viewDiskonBarang" + i).html(toRp(diskonBarang));
			hitungtotal(i);
		}
	}

	function hitungKembalian() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			var kembalian = totalbayar - grandtotal;
			$("#kembalianView").html(toRp(kembalian));
			$("#kembalian").val(kembalian);
		} else {
			$("#kembalianView").html(toRp(0));
			$("#kembalian").val(0);
		}
	}

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		cekUsePoin = $('#userPoin' + id).val();
		var poin = $('#poin' + id).val();
		var saldo = $('#saldo' + id).val();
		var diskonBarang = $('#diskonBarang' + id).val();
		$('#diskonMember').val(0);
		$('#member').val(nama);
		$('#saldo').val(saldo);
		$('#point').val(poin);
		$('#poinMember').html(toRp(poin));
		$('#saldoMember').html(toRp(saldo));
		$('#diskonBarang').val(diskonBarang);
		$('#modal-member').modal('hide');
		$('[class="row statusMember col-md-12"').show("slow");
		updateDiskonBarang();
		calculatetotal();
	}

	$('#addLineBtn').on('click', function (event) {
		$('#pleaseWaitDialog').modal('show');
		var txtKode = $('#searchKodeBarang').val().trim();
		var jumlah = txtKode.split('*');
		if (jumlah.length > 1) {
			var arr = jumlah[1].split('_');
		} else {
			var arr = txtKode.split('_');
			jumlah[0] = 1;
		}
		var kode = arr[0];
		var countDetail = $('#detailTable').bootstrapTable('getData').length;
		var countAdd = 0;
		if (countAdd == 0) {
			$.ajax({
				url: "<?php echo base_url();?>cekKodeBarang/" + kode,
				method: 'GET',
				success: function (response) {
					resp = $.parseJSON(response);
					var countDetail = $('#detailTable').bootstrapTable('getData').length;
					var data = {
						kode: resp.barcode_kue,
						harga: resp.harga,
						nama: resp.nama_kue,
						id: resp.id_kue,
						qty: (jumlah[0] * 1),
						subtotal: (resp.harga * 1) * (jumlah[0] * 1),
						line: countDetail + 1
					};

					var existingData = $('#detailTable').bootstrapTable('getData');
					var duplicate = false;

					$.each(existingData, function (index, item) {
						if (item.kode == data.kode) {
							duplicate = true;
							item.qty = (item.qty * 1) + (jumlah[0] * 1);
							item.subtotal = (item.harga * 1) * (item.qty * 1);
						}
					});

					if (!duplicate) {
						$('#detailTable').bootstrapTable('append', data);
					} else {
						$('#detailTable').bootstrapTable('load', existingData);
					}
					calculatetotal();
					$('#txtKodeBarang').val('');
					$('#searchKodeBarang').val('');
					var elem = document.getElementById('detailTable');
					$('#detailTable').bootstrapTable('scrollTo', elem.scrollHeight);
				}, error: function (response, status) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Invalid article code.', 'warning');
				}
			})
		}
		$('#pleaseWaitDialog').modal('hide');
		$("#searchKodeBarang").focus();
	});

	$('#detailTable').bootstrapTable({
		classes: 'table table-striped table-condensed table-no-bordered text-nowrap',
		pagination: false,
		sidePagination: 'client',
		pageSize: 50,
		pageList: [5, 10, 25, 50, 100, 'All'],
		search: false,
		smartdisplay: false,
		showRefresh: false,
		showToggle: false,
		showColumns: false,
		height: '430',
		columns: [{
			title: '<i class="fa fa-trash fa-lg"></i>',
			valign: 'middle',
			width: '5%',
			formatter: function (value, row, index) {
				return '<button type="button" class="btn btn-danger btn-xs c-remove-row"><i class="fa fa-trash fa-lg"></i></button>';
			},
			events: {
				'click .c-remove-row': function (event, value, row, index) {
					$('#detailTable').bootstrapTable('remove', {field: 'id', values: [row.id]});
					$("#pemotongan").val(0);
					$("#viewPemotongan").html(0);
					calculatetotal();
				}
			},
			cellStyle: function (value, row, index, field) {
				return {
					classes: 'text-nowrap another-class',
					css: {"font-size": "20px"}
				};
			}
		}, {
			field: 'nama',
			title: 'Nama Barang',
			valign: 'middle',
			class: 'description',
			width: '30%',
			formatter: function (value, row, index) {
				return row.kode + " - " + value;
			},
			cellStyle: function (value, row, index, field) {
				return {
					classes: 'text-nowrap another-class',
					css: {"font-size": "20px"}
				};
			}
		}, {
			field: 'harga',
			title: 'Harga',
			valign: 'middle',
			width: '10%',
			align: 'right',
			halign: 'left',
			formatter: function (value, row, index) {
				return decimalFormatter(value);
			},
			cellStyle: function (value, row, index, field) {
				return {
					classes: 'text-nowrap another-class',
					css: {"font-size": "20px"}
				};
			}
		}, {
			field: 'qty',
			title: 'Jumlah',
			searchable: false,
			valign: 'middle',
			align: 'right',
			halign: 'left',
			width: '20%',
			formatter: function (value, row, index) {
				return '<input type="number" class="form-control qty" id="detail' + index + '" value="' + value + '" min="1"/>';
			},
			events: {
				'change .qty': function (event, value, row, index) {
					var qty = $(this).val();

					if (qty > 0) {
						row.qty = qty;
						row.subtotal = (row.harga * 1) * (row.qty * 1);
						$('label[id="subtotal' + index + '"]').html(decimalFormatter(row.subtotal));
					} else {
						row.qty = 1;
						$('input[id="detail' + index + '"]').val(row.qty);
					}
					calculatetotal();
				}
			},
			cellStyle: function (value, row, index, field) {
				return {
					classes: 'text-nowrap another-class',
					css: {"font-size": "20px"}
				};
			}
		}, {
			field: 'subtotal',
			title: 'Subtotal',
			valign: 'middle',
			halign: 'left',
			align: 'right',
			width: '20%',
			formatter: function (value, row, index) {
				return '<label id="subtotal' + index + '">' + decimalFormatter(value) + '</label>';
			},
			cellStyle: function (value, row, index, field) {
				return {
					classes: 'text-nowrap another-class',
					css: {"font-size": "20px"}
				};
			}
		}]
	});


	//payment type
	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;

		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			if (cekUsePoin == 0) {
				showAlert('', "Pembayaran poin tidak bisa digunakan", 'error');
			} else {
				$('[class="col-sm-12 form-group point"').show("slow");
				$('[class="col-sm-12 form-group saldo"').hide("slow");
			}
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	function removeat(idx) {
		barang.splice(idx, 1);
		$("#baris" + idx).remove();
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			barang = [];
		}
		$("#pemotongan").val(0);
		$("#viewPemotongan").html(0);
		calculatetotal();
	}

	function removeatPembayaran(idx) {
		bayar.splice(idx, 1);
		$("#barisPembayaran" + idx).remove();
		calculatetotalPembayaran();
	}

	//Payment Submit
	$('#btnSimpan').click(function () {
		$('#btnSimpan').attr('disabled');
		$('#pleaseWaitDialog').modal('show');
		$("#confirm-deposit-modal").modal('hide');
		$("#modal-payment").modal('hide');
		var barang = [];
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			barang.push({
				jumlah: item.qty,
				id: item.id,
				harga: item.harga
			});
		});
		if (totalbayar >= grandtotal) {
			$.ajax({
				url: "<?php echo base_url($page . '/create'); ?>",
				method: 'post',
				data: {
					idMember: $('#idMember').val(),
					tgl: $('#tgl').val(),
					diskonMember: $('#diskonMember').val(),
					idPegawai: $('#idPegawai').val(),
					pemotongan: $('#pemotongan').val(),
					totalPenjualan: $('#totalPenjualan').val(),
					grandtotal: $('#grandtotal').val(),
					kembalian: $('#kembalian').val(),
					type: 'payment',
					payment: bayar,
					barang: barang,
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Penjualan Berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page)?>/" + data['description'] + "/print";
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		} else {
			showAlert('', 'Pembayaran tidak mencukupi', 'error');
		}
		$('#btnSimpan').removeAttr('disabled');
	});

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		addPayment();
	});

	function addPayment() {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();
		var keterangan = $("#keterangan").val();
		var cek = false;
		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
			cek = true;
		} else if (namaJenis == 'poin' && cekUsePoin == 1) {
			var poin = $('#point').val();
			if (jumlahBayar > poin) {
				showAlert('', "Jumlah poin yang digunakan tidak boleh melebihi jumlah poin saat ini", 'error');
				cek = true;
			}
		} else if (namaJenis == 'saldo') {
			var saldo = $('#saldo').val();
			if (jumlahBayar > saldo) {
				showAlert('', "Jumlah saldo yang digunakan tidak boleh melebihi jumlah saldo saat ini", 'error');
				cek = true;
			}
		}
		if (cek != true) {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}
			if (namaJenis == 'poin' && cekUsePoin == 1) {
				jumlahBayar = jumlahBayar * 1000;
			} else {
				if (cek == 0) {
					obj.id = jenis;
					obj.jumlah = jumlahBayar;
					obj.keterangan = keterangan;
					bayar.push(obj);
					idx = bayar.length - 1;

					var newRow = "<tr id='barisPembayaran" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePembayaran' onclick='removeatPembayaran(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
						"<td width='20%'  style='text-align:right'><span id='viewKeterangan" + idx + "'>" + keterangan + "</span><input type='hidden' name='keteranganPayment[]' id='keteranganPayment" + idx + "' value='" + keterangan + "'></td>" +
						"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlahPembayaran[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
					$("#paymentDetail").append(newRow);
				} else {
					var jumlahrow = document.getElementById("paymentDetail").rows.length;
					for (var i = 0; i < jumlahrow; i++) {
						var cekID = $('#idPembayaran' + i).val();
						if (cekID == jenis) {
							var jumlahPembayaran = eval($('#jumlahPembayaran' + i).val());
							jumlahPembayaran = (jumlahPembayaran * 1) + (jumlahBayar * 1);
							$('#jumlahPembayaran' + i).val(jumlahPembayaran);
							$('#viewJumlah' + i).html(toRp(jumlahPembayaran));
							var idx = i;
						}
					}
				}
				hitungtotalPembayaran(idx);
				$('#jumlahPembayaran').val("");
				$('#keterangan').val("");
			}
		}
	}

	$('#btnSubmit').click(function () {
		$('#pleaseWaitDialog').modal('show');
		var jumlahrow = $('#detailTable').bootstrapTable('getData').length;
		var pemotongan = $('#pemotongan').val();
		var grandtotal = $('#grandtotal').val();
		var barang = [];
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			barang.push({
				jumlah: item.qty,
				id: item.id,
				harga: item.harga
			});
		});
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else if (pemotongan > grandtotal) {
			showAlert('', "Jumlah pemotongan lebih besar dari total penjualan", 'error');
		} else {
			if ($("#createForm").valid()) {
				$("#modal-payment").modal('hide');
				$.ajax({
					url: "<?php echo base_url($page . '/create'); ?>",
					method: 'post',
					data: {
						idMember: $('#idMember').val(),
						tgl: $('#tgl').val(),
						diskonMember: $('#diskonMember').val(),
						idPegawai: $('#idPegawai').val(),
						pemotongan: $('#pemotongan').val(),
						totalPenjualan: $('#totalPenjualan').val(),
						grandtotal: $('#grandtotal').val(),
						kembalian: $('#kembalian').val(),
						type: 'simpan',
						payment: bayar,
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', 'Penjualan Berhasil disimpan', 'success', function () {
								window.location = "<?php echo base_url($page)?>";
							});
						}
					}, error: function (xhr, text, status) {
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			}
		}
		$('#pleaseWaitDialog').modal('hide');
	});

	$('#btnPayment').click(function () {
		var jumlahrow = $('#detailTable').bootstrapTable('getData').length;
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else {
			$('#modal-payment').modal({
				show: 'true'
			});
		}
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});

	$("#searchKodeBarang").autocomplete({
		source: "<?php echo base_url('searchBarang'); ?>",
		select: function (event, ui) {
			var select = ui.item.label;
			var arr = select.split('_');
			$('#txtKodeBarang').val(arr[0].trim());
		},
		max: 10,
		scroll: true
	});

	$('#searchKodeBarang').keypress(function (event) {
		if (event.which == 13) {
			event.preventDefault();
			$('#addLineBtn').trigger('click');
		}
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});

	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});

	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});


	
</script>
