<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 13/07/2017
 * Time: 14.13
 */ ?>
<style>
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}

	th, td {
		text-align: center;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo strtoupper($page); ?>
			<small>Invoice</small>
		</h1>
	</section>
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="panel">
						<div class="cust-panel-head">
							<a href="<?php echo base_url($page . '/create') ?>" type="button" class="btn btn-danger btn-lg" id="btnBuat">
								<i class="fa fa-plus"></i> Transaksi Baru</a>
							<button type="button" class="btn btn-info btn-lg" id="btnPrint">
								<i class="fa fa-print"></i> PRINT
							</button>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<iframe src="<?php echo base_url($page . '/' . $detail['id_jual'] . '/invoice'); ?>" width="50%" id="invoiceFrame" name="invoiceFrame" height="400px">
								</iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>

	</section>
</div>
<!-- /.content -->
</div>
