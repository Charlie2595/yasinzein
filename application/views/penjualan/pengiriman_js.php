<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var bayar = [];
	var harga = "";
	var idx = 0;
	var totalbayar = 0;
	var grandtotal = 0;
	var cekUsePoin = 0;

	$(document).ready(function () {

	});

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function choosePegawai(id) {
		$('#idPegawai').val(id);
		var nama = $('#nama' + id).val();
		$('#namaPegawai').val(nama);
		$('#modal-pegawai').modal('hide');
	}

	$('#btnSubmit').click(function () {
		if ($('#pengirimForm').valid()) {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/store_send'); ?>",
				method: 'post',
				data: {
					idPenjualan: $('#idPenjualan').val(),
					tgl: $('#tgl').val(),
					jam: $('#jam').val(),
					idPegawai: $('#idPegawai').val(),
					nama: $('#nama').val(),
					notelp: $('#notelp').val(),
					alamat: $('#alamat').val(),
					tipeBayar: $('#tipeBayar').val(),
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Pengiriman Berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					console.log(status);
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		}
	});

	$('#pengirimForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
			jam: {
				required: true,
			},
			namaPegawai: {
				required: true,
			},
			nama: {
				required: true,
			},
			alamat: {
				required: true,
			},
			notelp: {
				required: true,
			}
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
			jam: {
				required: "Jam harus diisi",
			},
			namaPegawai: {
				required: "Pengirim harus diisi",
			},
			nama: {
				required: "Nama Member harus diisi",
			},
			alamat: {
				required: "Alamat Member harus diisi",
			},
			notelp: {
				required: "No Telp Member harus diisi",
			}
		}
	});

	//Timepicker
	$('.timepicker').timepicker({
		showInputs: true,
		showMeridian: false,
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '0D'
	});

</script>
