<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		Yasinzein
	</title>
	<style>
		body {
			font-family: "arial";
			letter-spacing: 1px;
		}
	</style>


	<style>
		@page {
			margin: 0px;
		}

		@media print {
			body {
				font-size: 12pt;
				width: 8cm;
			}

			table {
				font-size: 12pt;
			}
		}

		body {
			font: normal 'Helvetica Neue', Helvetica, Arial, sans-serif;
			margin: 0;
			padding: 1px;
		}

		hr {
			border: 1px inset;
		}
	</style>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// --><!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>
<body>
<?php if ($page == "pesanan"): ?>
	<strong><p align="center">PESANAN</strong>
<?php endif; ?>
<p align="center" style="font-size:12px">
	TOKO KUE TRADISIONAL<br/> NY. YASIN ZEIN<br/> JL. DHARMAHUSADA INDAH UTARA I/25 (B-44)<br/> TELP. 031-594733 SURABAYA<br/>
</p>
<hr>
<table width="100%">
	<tr style="font-size:15px">
		<td width="38%">Tanggal</td>
		<td width="2%">:</td>
		<td width="60%"><?php $phpdate = strtotime($detail['tgl']);
			echo date('d-m-Y H:i', $phpdate); ?></td>
	</tr>
	<tr style="font-size:15px">
		<td width="38%">Kasir | Nota</td>
		<td width="2%">:</td>
		<td width="65%"><?php echo $detail['pegawai'] . " | " . $detail['document'] ?></td>
		<?php if ($page == "pesanan") : ?>

		<?php endif; ?>
	</tr>
	<tr style="font-size:15px">
		<td width="38%">Pelanggan</td>
		<td width="2%">:</td>
		<td width="60%"><?php echo $detail['member'] ?></td>
	</tr>
</table>
<?php if ($page == "pesanan") : ?>
	<hr>
	<table width="100%">
		<tr style="font-size:15px">
			<td width="38%">Status</td>
			<td width="2%">:</td>
			<?php if ($detail['dikirim'] == 1 && $detail['id_pegawai_kirim'] != null) : ?>
				<td width="60%">Sudah Dikirim</td>
			<?php elseif ($detail['dikirim'] == 0 && $detail['tgl_terima'] == null) : ?>
				<td width="60%">Ambil Sendiri</td>
			<?php elseif ($detail['dikirim'] == 0 && row['tgl_terima'] != null) : ?>
				<td width="60%">Sudah Ambil</td>
			<?php else: ?>
				<td width="60%">Belum Dikirim</td>
			<?php endif; ?>
		</tr>
		<?php if ($detail['dikirim'] == 1 && $detail['id_pegawai_kirim'] != null) : ?>
			<tr style="font-size:15px">
				<td width="38%">Pengirim</td>
				<td width="2%">:</td>
				<td width="60%"><?php echo $detail['pengirim']; ?></td>
			</tr>
		<?php endif; ?>
		<?php if ($detail['dikirim'] == 1 && $detail['id_pegawai_kirim'] != null) : ?>
			<tr style="font-size:15px">
				<td width="38%">Tanggal/Jam Kirim</td>
				<td width="2%">:</td>
				<td width="60%"><?php echo $detail['tgl_kirim'] . " " . date('H:i', strtotime($detail['jam_kirim'])); ?></td>
			</tr>
		<?php endif; ?>
	</table>
<?php endif; ?>
<hr>
<?php $totalItem = 0;
$totalPembayaran = 0; ?>
<table width="100%">
	<?php foreach ($detail['detail'] as $detailPenjualan) : ?>
		<tr>
			<td colspan="25"><?php echo $detailPenjualan['nama_kue'] ?></td>
			<td width="10%"></td>
			<td width="10%" align="right"><?php echo $detailPenjualan['jumlah']; ?></td>
			<td width="20%" align="right"><?php echo number_format($detailPenjualan['harga'], 0, ".", ","); ?></td>
			<td width="25%" align="right"><?php echo number_format($detailPenjualan['jumlah'] * $detailPenjualan['harga'] - $detailPenjualan['diskon'], 0, ".", ",") ?></td>
			<?php $totalItem += $detailPenjualan['jumlah']; ?>
		</tr>
	<?php endforeach; ?>
</table>
<hr>
<table width="100%">
	<tr>
		<td width="45%">Total Item</td>
		<td width="10%" align="right"><?php echo $totalItem ?></td>
		<td width="45%" align="right"></td>
	</tr>
</table>
<hr>
<table width="100%">
	<tr>
		<td width="65%">TOTAL</td>
		<td width="5%">=</td>
		<td width="5%">Rp</td>
		<td width="25%" align="right"><?php echo number_format($detail['total'], 0, ".", ","); ?></td>
	</tr>
	<?php if ($detail['diskon_pembulatan'] > 0) : ?>
		<tr>
			<td width="65%">DISKON</td>
			<td width="5%">=</td>
			<td width="5%">Rp</td>
			<td width="25%" align="right"><?php echo number_format($detail['diskon_pembulatan'], 0, ".", ","); ?></td>
		</tr>
		<tr>
			<td width="65%">GRAND TOTAL</td>
			<td width="5%">=</td>
			<td width="5%">Rp</td>
			<td width="25%" align="right"><?php echo number_format($detail['total'] - ($detail['diskon_pembulatan'] * 1), 0, ".", ","); ?></td>
		</tr>
	<?php endif; ?>
	<?php if (count($detail['payment']) > 0 && $detail['status'] == 0): ?>
		<tr>
			<td>PEMBAYARAN UANG MUKA</td>
		</tr>
	<?php endif; ?>
	<?php foreach ($detail['payment'] as $pembayaran) : ?>
		<tr>
			<td width="65%"><?php echo $pembayaran['nama']; ?></td>
			<td width="5%">=</td>
			<td width="5%">Rp</td>
			<td width="25%" align="right"><?php echo number_format($pembayaran['total'], 0, ".", ","); ?></td>
			<?php $totalPembayaran += $pembayaran['total']; ?>
		</tr>
	<?php endforeach; ?>
	<?php if ($totalPembayaran < ($detail['total'] - ($detail['diskon_pembulatan'] * 1))) : ?>
		<tr>
			<td width="65%">SISA</td>
			<td width="5%">=</td>
			<td width="5%">Rp</td>
			<td width="25%" align="right"><?php echo number_format(($detail['total'] - ($detail['diskon_pembulatan'] * 1)) - $totalPembayaran, 0, ".", ","); ?></td>
		</tr>
	<?php endif; ?>
	<?php if ($detail['kembalian'] != 0) : ?>
		<tr>
			<td width="65%">KEMBALI</td>
			<td width="5%">=</td>
			<td width="5%">Rp</td>
			<td width="25%" align="right"><?php echo number_format($detail['kembalian'], 0, ".", ","); ?></td>
		</tr>
	<?php endif; ?>
	<tr>
		<td colspan="4"></td>
	</tr>
</table>
<hr>
<?php if (($page == "pesanan" && $detail['status'] == 1) || $detail['tipe_trx'] == 1): ?>
	<p align="center" style="font-size: 12px">
		TERIMA KASIH SILAHKAN DATANG KEMBALI
	</p>
<?php elseif ($totalPembayaran == 0): ?>
	<p align="center" style="font-size: 12px">
		BUKAN BUKTI PEMBAYARAN
	</p>
<?php endif; ?>
</body>
</html>
