<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var penjualan = []; //array penjualan
	var kode = "";
	var nama = "";
	var harga = "";
	var jumlah = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran

	$(document).ready(function () {
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		//cekSubmit();

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
	});

	function konvertnilai(elm) {
		var nilai = ($(elm).autoNumeric('get') * 1);
		$(elm).autoNumeric('destroy');
		$(elm).val(nilai);
	};

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;

		var diskon = $('#pemotongan').val() * 1;

		for (var i = 0; i < penjualan.length; i++) {
			var total = penjualan[i].total * 1;
			grandtotal += total;
		}

		$("#viewTotalBayar").html(toRp(grandtotal));
		$("#totalPenjualan").val(grandtotal);
		$("#viewGrandtotal").html(toRp(grandtotal - diskon));
		$("#grandtotal").val(grandtotal - diskon);
		$("#grandtotalPembelian").val(toRp(grandtotal - diskon));
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		$('#diskonMember').val(diskon);
		$('#member').val(nama);
		$('#modal-member').modal('hide');
		calculatetotal();
	}

	//Function untuk mencari history transaksi member
	function searchPenjualan() {
		var idMember = $('#idMember').val();
		$.ajax({
			url: "<?php echo base_url();?>searchPenjualan/" + idMember + "/b2b",
			success: function (data) {
				var mydata = $.parseJSON(data);
				var newRow = "";
				for (var i = 0; i < mydata.length; i++) {
					newRow += "<tr><td width='5%'><button type='button' class='btn btn-xs btn-primary' onclick='choosePenjualan(" + i + ");'><i class='fa fa-plus'></button></td>" +
						"<td><input type='hidden' id='document" + i + "' value='" + mydata[i].document + "'>" + mydata[i].document + "</td>" +
						"<td><input type='hidden' id='id_penjualan" + i + "' value='" + mydata[i].id_jual + "'><input type='hidden' id='tgl" + i + "' value='" + mydata[i].tgl + "'>" + mydata[i].tgl + "</td>" +
						"<td>" + mydata[i].pegawai + "</td>" +
						"<td><input type='hidden' id='total" + i + "' value='" + (mydata[i].total * 1 - ((mydata[i].diskon_member * mydata[i].total / 100) * 1 + mydata[i].diskon_pembulatan * 1)) + "'>" + toRp(mydata[i].total * 1 - ((mydata[i].diskon_member * mydata[i].total / 100) * 1 + mydata[i].diskon_pembulatan * 1)) + "</td></tr>";
				}
				$("#bodyPenjualan").html(newRow);
			}
		});
	}

	function choosePenjualan(id) {
		var idPenjualan = $('#id_penjualan' + id).val();
		var tgl = $('#tgl' + id).val();
		var document = $('#document' + id).val();
		var total = $('#total' + id).val();

		var obj = new Object();
		var cek = 0;
		for (var i = 0; i < penjualan.length; i++) {
			if (penjualan[i].id == idPenjualan) {
				cek++;
			}
		}
		if (cek == 0) {
			obj.id = idPenjualan;
			obj.total = total;
			penjualan.push(obj);
			idx = penjualan.length - 1;
			var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePenjualan' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
				"<td width='35%'>" + document + " </td>" +
				"<td width='35%'><input type='hidden' name='id[]' id='idPenjualan" + idx + "' value='" + idPenjualan + "'> " + tgl + " </td>" +
				"<td  width='20%'  style='text-align:right'><span id='total'>" + toRp(total * 1) + "</span><input type='hidden' name='total[]' value='" + total + "'></td></tr>";
			$("#bodyNota").append(newRow);
			// var jumlahrow=document.getElementById("bodyNota").rows.length;
		} else {
			showAlert('', "Penjualan sudah di masukkan sebelumnya", 'error');
		}
		$('#modal-nota').modal('hide');
		calculatetotal();
	}

	function removeat(idx) {
		penjualan.splice(idx, 1);
		$("#baris" + idx).remove();
		calculatetotal();
	}

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	//Payment Submit
	$('#btnSimpan').click(function () {
		grandtotal = $('#grandtotal').val();
		totalbayar = $('#grandTotalBayar').val();
		if (totalbayar >= grandtotal) {
			$("#type").val("payment");
			$(".input-uang").each(function (index) {
				konvertnilai(this);
			});
			// $('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/create'); ?>",
				method: 'post',
				data: {
					idMember: $('#idMember').val(),
					tgl: $('#tgl').val(),
					idPegawai: $('#idPegawai').val(),
					total: $('#totalPenjualan').val(),
					kembalian: $('#kembalian').val(),
					diskon: $('#pemotongan').val(),
					type: 'payment',
					penjualan: penjualan,
					payment: bayar,
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Nota Berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		} else {
			showAlert('', "Pembayaran kurang", 'error');
		}
	});

	$('#btnSubmit').click(function () {
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			showAlert('', "Pilih penjualan terlebih dahulu", 'error');
		} else {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/create'); ?>",
				method: 'post',
				data: {
					idMember: $('#idMember').val(),
					tgl: $('#tgl').val(),
					idPegawai: $('#idPegawai').val(),
					total: $('#totalPenjualan').val(),
					kembalian: $('#kembalian').val(),
					diskon: $('#pemotongan').val(),
					type: 'save',
					penjualan: penjualan,
					payment: bayar
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Nota Berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		}
	});

	$('#searchPenjualan').click(function () {
		var idMember = $('#idMember').val();
		if (idMember == "") {
			showAlert('', "Pilih member terlebih dahulu", 'error');
		} else {
			searchPenjualan();
			$('#modal-nota').modal({
				show: 'true'
			});
		}
	});

	$('.searchTable').DataTable();

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/1'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'email',
			title: 'Email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	//Timepicker
	$('.timepicker').timepicker({
		showInputs: true,
		showMeridian: false,
	});

	$('#btnPayment').click(function () {
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			showAlert('', "Pilih penjualan terlebih dahulu", 'error');
		} else {
			$('#modal-payment').modal({
				show: 'true'
			});
		}
	});

	//payment type
	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;

		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			if (cekUsePoin == 0) {
				showAlert('', "Pembayaran poin tidak bisa digunakan", 'error');
			} else {
				$('[class="col-sm-12 form-group point"').show("slow");
				$('[class="col-sm-12 form-group saldo"').hide("slow");
			}
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	function removeatPembayaran(idx) {
		bayar.splice(idx, 1);
		$("#barisPembayaran" + idx).remove();
		calculatetotalPembayaran();
	}

	function addPayment() {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();
		var keterangan = $("#keterangan").val();
		var cek = false;
		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
			cek = true;
		} else if (namaJenis == 'poin' && cekUsePoin == 1) {
			var poin = $('#point').val();
			if (jumlahBayar > poin) {
				showAlert('', "Jumlah poin yang digunakan tidak boleh melebihi jumlah poin saat ini", 'error');
				cek = true;
			}
		} else if (namaJenis == 'saldo') {
			var saldo = $('#saldo').val();
			if (jumlahBayar > saldo) {
				showAlert('', "Jumlah saldo yang digunakan tidak boleh melebihi jumlah saldo saat ini", 'error');
				cek = true;
			}
		}
		if (cek != true) {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}
			if (namaJenis == 'poin' && cekUsePoin == 1) {
				jumlahBayar = jumlahBayar * 1000;
			} else {
				if (cek == 0) {
					obj.id = jenis;
					obj.jumlah = jumlahBayar;
					obj.keterangan = keterangan;
					bayar.push(obj);
					idx = bayar.length - 1;

					var newRow = "<tr id='barisPembayaran" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePembayaran' onclick='removeatPembayaran(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
						"<td width='20%'  style='text-align:right'><span id='viewKeterangan" + idx + "'>" + keterangan + "</span><input type='hidden' name='keteranganPayment[]' id='keteranganPayment" + idx + "' value='" + keterangan + "'></td>" +
						"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlahPembayaran[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
					$("#paymentDetail").append(newRow);
				} else {
					var jumlahrow = document.getElementById("paymentDetail").rows.length;
					for (var i = 0; i < jumlahrow; i++) {
						var cekID = $('#idPembayaran' + i).val();
						if (cekID == jenis) {
							var jumlahPembayaran = eval($('#jumlahPembayaran' + i).val());
							jumlahPembayaran = (jumlahPembayaran * 1) + (jumlahBayar * 1);
							$('#jumlahPembayaran' + i).val(jumlahPembayaran);
							$('#viewJumlah' + i).html(toRp(jumlahPembayaran));
							var idx = i;
						}
					}
				}
				hitungtotalPembayaran(idx);
				$('#jumlahPembayaran').val("");
				$('#keterangan').val("");
			}
		}
	}

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		addPayment();
	});

	//hitung subtotal
	function hitungtotalPembayaran(id) {
		id = id * 1;
		var jumlah = eval($("#jumlahPembayaran" + id).val());
		bayar[id].jumlah = jumlah;

		calculatetotalPembayaran();
		hitungKembalian();
		cekSubmit();
	}

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function hitungKembalian() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			var kembalian = totalbayar - grandtotal;
			$("#kembalianView").html(toRp(kembalian));
			$("#kembalian").val(kembalian);
		} else {
			$("#kembalianView").html(toRp(0));
			$("#kembalian").val(0);
		}
	}

	//hitung total pembayaran
	function calculatetotalPembayaran() {
		totalbayar = 0;

		for (var i = 0; i < bayar.length; i++) {
			var jumlah = bayar[i].jumlah * 1;
			totalbayar += jumlah;
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#grandTotalBayar").val(totalbayar); //Total Penjualan
		hitungKembalian();
	}

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '0D'
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
			member: {
				required: true,
			}
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
			member: {
				required: "Member harus diisi",
			}
		}
	});

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		if (pemotongan == '' || pemotongan == null) {
			pemotongan = 0;
		}
		var total = $('#totalPenjualan').val();

		if (pemotongan > total) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();

		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}

	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-payment').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				addPayment();
			} else if ($('#modal-verifikasi').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				cekVerifikasi();
			}
		}
	});

</script>
