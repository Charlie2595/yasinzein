<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 13/07/2017
 * Time: 14.13
 */ ?>
<style>
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}

	th, td {
		text-align: center;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">DETAIL <?php echo strtoupper($page); ?></li>
		</ol>
	</section>
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="panel">
						<div class="cust-panel-head">
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<form class="form-horizontal" role="form">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Document :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo $detail['document']; ?></p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Tanggal :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo $detail['tgl']; ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Member :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo $detail['member']; ?></p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Pegawai :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo $detail['pegawai']; ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Total :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo "Rp. " . number_format($detail['total'], 0, ",", ","); ?></p>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Diskon :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo "Rp. " . number_format($detail['diskon'], 0, ",", ","); ?></p>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">LUNAS :</label>
												<div class="col-sm-9 col-lg-8"><p class="form-control-static">
														<?php if ($detail['lunas'] == 1) : ?>
															LUNAS
														<?php else: ?>
															BELUM LUNAS
														<?php endif; ?>
													</p></div>
											</div>
										</div>
									</div>
									<div class="row">
										<?php $detailNota = $detail['detail'];
										$detailPembayaran = $detail['payment']; ?>
										<?php if ($detailPembayaran != null) : ?>
										<div class="col-xs-12 col-lg-8">
											<?php else: ?>
											<div class="col-xs-12 col-lg-12">
												<?php endif; ?>
												<h2 style="text-align: center;" class="label-rouded label-info">DETAIL NOTA</h2>
												<div class="table-responsive">
													<table id="kegiatanTable" class="table table-bordered table-striped users">
														<thead>
														<tr>
															<th width="5%" style="text-align: center;">No</th>
															<th width="35%" style="text-align: center">Tanggal</th>
															<th width="15%" style="text-align: center">Total</th>
															<th width="20%" style="text-align: center">Diskon</th>
															<th width="15%" style="text-align: center">Grandtotal</th>
														</tr>
														</thead>
														<tbody>
														<?php
														$counter = 1; ?>
														<?php foreach ($detailNota AS $detailNota): ?>
															<tr>
																<td><?php echo $counter; ?></td>
																<td>
																	<a href="<?php echo base_url('b2b/' . $detailNota['id_jual']); ?>"><?php echo $detailNota['tgl']; ?></a>
																</td>
																<td style="text-align: right"><?php echo "Rp. " . number_format($detailNota['total'], 0, ",", ","); ?></td>
																<td style="text-align: right"><?php echo "Rp. " . number_format($detailNota['diskon_pembulatan'], 0, ",", ","); ?></td>
																<td style="text-align: right"><?php echo "Rp. " . number_format(($detailNota['total'] - $detailNota['diskon_pembulatan']), 0, ",", ","); ?></td>
															</tr>
															<?php $counter++; ?>
														<?php endforeach; ?>
														<tr>
															<th colspan="4">Diskon</th>
															<td style="text-align: right;"><?php echo "Rp. " . number_format($detail['diskon'], 0, ",", ","); ?></td>
														</tr>
														</tbody>
														<tfoot>
														<tr>
															<th colspan="4" style="text-align: center;" ;>Grandtotal</th>
															<th style="text-align: right;" ;><?php echo "Rp. " . number_format($detail['total'] - ($detail['diskon'] * 1), 0, ",", "."); ?></th>
														</tr>
														</tfoot>
													</table>
												</div>
											</div>
											<?php if ($detailPembayaran != null) : ?>
												<div class="col-xs-12 col-lg-4">
													<h2 style="text-align: center;" class="label-rouded label-success">DETAIL PEMBAYARAN</h2>
													<div class="table-responsive">
														<table id="kegiatanTable" class="table table-bordered table-striped users">
															<thead>
															<tr>
																<th style="width: 5%; text-align: center;">No</th>
																<th style="width: 30%; text-align: center;">Tipe Bayar</th>
																<th style="width: 30%; text-align: center;">Keterangan</th>
																<th style="width: 35%; text-align: center;">Jumlah</th>
															</tr>
															</thead>
															<tbody>
															<?php
															$counter = 1;
															$pembayaran = 0; ?>
															<?php foreach ($detailPembayaran AS $detailPembayaran): ?>
																<tr>
																	<td><?php echo $counter; ?></td>
																	<td><?php echo $detailPembayaran['nama']; ?></td>
																	<td><?php echo $detailPembayaran['nama']; ?></td>
																	<td style="text-align: right"><?php echo "Rp. " . number_format($detailPembayaran['total']); ?></td>
																</tr>
																<?php $counter++;
																$pembayaran += $detailPembayaran['total']; ?>
															<?php endforeach; ?>
															</tbody>
															<tfoot>
															<tr>
																<th colspan="3" style="text-align: center;" ;>Grandtotal</th>
																<th style="text-align: right;" ;><?php echo "Rp. " . number_format($pembayaran, 0, ",", "."); ?></th>
															</tr>
															</tfoot>
														</table>
													</div>
												</div>
											<?php endif; ?>
										</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>

	</section>
</div>
<!-- /.content -->
</div>
