<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'document',
		url: "<?php echo base_url('getAllNota'); ?>",
		columns: [{
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'total',
			title: 'Total',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'diskon',
			title: 'Diskon',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="<?php echo base_url();?>nota/' + row['id_nota'] + '" data-toggle="tooltip" title="Detail" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					if (row['lunas'] == 0) {
						btn += '<a href="<?php echo base_url();?>nota/' + row['id_nota'] + '/payment" data-toggle="tooltip" title="Pembayaran" class="btn btn-sm btn-warning"><i class="fa fa-money fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-money"></i></a> &nbsp;';
					}
				}

				return btn;
			}
		}]
	});
</script>
