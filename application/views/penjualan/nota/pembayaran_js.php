<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var bayar = [];
	var harga = "";
	var penjualan = [];
	var idx = 0;
	var totalbayar = 0;
	var grandtotal = 0;
	var cekUsePoin;

	$(document).ready(function () {
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		var count = document.getElementById("bodyNota").rows.length;
		for (var i = 0; i < count; i++) {
			var id = $('#idPenjualan' + i).val();
			var total = $('#total' + i).val() * 1;

			var obj = new Object();
			obj.id = id;
			obj.total = total;
			penjualan.push(obj);
			grandtotal += total;
		}
		calculatetotal();
		$('#grandtotal').val(grandtotal);
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(countJumlah)); //Total Bayar
		$('#viewGrandtotal').html(toRp(grandtotal));
		$('#grandtotalPembelian').html(toRp(grandtotal));

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});

		cekUsePoin = $('#cekPoin').val();
	});

	function konvertnilai(elm) {
		var nilai = ($(elm).autoNumeric('get') * 1);
		$(elm).autoNumeric('destroy');
		$(elm).val(nilai);
	};

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;

		var diskon = $('#pemotongan').val() * 1;

		for (var i = 0; i < penjualan.length; i++) {
			var total = penjualan[i].total * 1;
			grandtotal += total;
		}

		$("#totalPenjualan").val(grandtotal);
		$("#viewTotalBayar").html(toRp(grandtotal));
		$("#grandtotalPembayaran").html(toRp(totalbayar - grandtotal)); //Total Bayar
		$("#viewGrandtotal").html(toRp(grandtotal - diskon));
		$("#grandtotal").val(grandtotal - diskon);
		cekSubmit();
	}

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlahPembayaran").autoNumeric('get') * 1);
		bayar[id].jumlah = jumlah;

		var subtotal = (jumlah * 1) + (totalbayar * 1);
		totalbayar = subtotal;
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(countJumlah)); //Total Bayar
		$("#grandTotalBayar").val(subtotal);
		hitungKembalian();
		cekSubmit();
	}

	function hitungKembalian() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			var kembalian = totalbayar - grandtotal;
			$("#kembalianView").html(toRp(kembalian));
			$("#kembalian").val(kembalian);
		} else {
			$("#kembalianView").html(toRp(0));
			$("#kembalian").val(0);
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;
		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			$('[class="col-sm-12 form-group point"').show("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	$('#btnSimpan').click(function () {
		if (totalbayar >= grandtotal) {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/store_payment'); ?>",
				method: 'post',
				data: {
					idNota: $('#idNota').val(),
					grandtotal: $('#grandtotal').val(),
					total: $('#totalPenjualan').val(),
					idPenjualan: $('#idPenjualan').val(),
					kembalian: $('#kembalian').val(),
					diskon: $('#pemotongan').val(),
					type: 'payment',
					payment: bayar,
					penjualan: penjualan,
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', 'Pembayaran Berhasil disimpan', 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					console.log(status);
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		} else {
			showAlert('', "Pembayaran kurang", 'error');
		}
	});

	$('#confirmSubmit').click(function () {
		$('#deposit').val('SIMPAN');
		$('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page . '/store_payment'); ?>",
			method: 'post',
			data: {
				idNota: $('#idNota').val(),
				grandtotal: $('#grandtotal').val(),
				total: $('#totalPenjualan').val(),
				idPenjualan: $('#idPenjualan').val(),
				kembalian: $('#kembalian').val(),
				diskon: $('#pemotongan').val(),
				type: 'payment',
				deposit: 'SIMPAN',
				payment: bayar,
				penjualan: penjualan,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pembayaran Berhasil disimpan', 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$('#noConfirmSubmit').click(function () {
		$('#deposit').val('TIDAK');
		$('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page . '/store_payment'); ?>",
			method: 'post',
			data: {
				idNota: $('#idNota').val(),
				grandtotal: $('#grandtotal').val(),
				total: $('#totalPenjualan').val(),
				idPenjualan: $('#idPenjualan').val(),
				kembalian: $('#kembalian').val(),
				diskon: $('#pemotongan').val(),
				type: 'payment',
				deposit: 'TIDAK',
				payment: bayar,
				penjualan: penjualan,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pembayaran Berhasil disimpan', 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$('#btnSubmit').click(function () {
		$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
	});

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		if (pemotongan == '' || pemotongan == null) {
			pemotongan = 0;
		}
		var total = $('#totalPenjualan').val();

		if (pemotongan > total) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();

		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}

	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-payment').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				addPayment();
			} else if ($('#modal-verifikasi').is(':visible') && !$('#pleaseWaitDialog').is(':visible')) {
				cekVerifikasi();
			}
		}
	});

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		var keterangan = $('#keterangan').val();
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();

		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
			cek = true;
		} else if (namaJenis == 'poin' && cekUsePoin == 1) {
			var poin = $('#point').val();
			if (jumlahBayar > poin) {
				showAlert('', "Jumlah poin yang digunakan tidak boleh melebihi jumlah poin saat ini", 'error');
				cek = true;
			}
		} else if (namaJenis == 'saldo') {
			var saldo = $('#saldo').val();
			if (jumlahBayar > saldo) {
				showAlert('', "Jumlah saldo yang digunakan tidak boleh melebihi jumlah saldo saat ini", 'error');
				cek = true;
			}
		}
		if (cek != true) {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}
			if (namaJenis == 'poin' && cekUsePoin == 1) {
				jumlahBayar = jumlahBayar * 1000;
			} else {
				if (cek == 0) {
					obj.id = jenis;
					obj.jumlah = jumlahBayar;
					obj.keterangan = keterangan;
					bayar.push(obj);
					idx = bayar.length - 1;

					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePembayaran' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
						"<td width='20%'  style='text-align:right'><span id='viewKeterangan" + idx + "'>" + keterangan + "</span><input type='hidden' name='keteranganPayment[]' id='keteranganPayment" + idx + "' value='" + keterangan + "'></td>" +
						"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlahPembayaran[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
					$("#paymentDetail").append(newRow);
				} else {
					var jumlahrow = document.getElementById("paymentDetail").rows.length;
					for (var i = 0; i < jumlahrow; i++) {
						var cekID = $('#idPembayaran' + i).val();
						if (cekID == jenis) {
							var jumlah = eval($('#jumlah' + i).val());
							jumlah = (jumlah * 1) + (jumlahBayar * 1);
							$('#jumlah' + i).val(jumlah);
							$('#viewJumlah' + i).html(toRp(jumlah));
							var idx = i;
						}
					}
				}
				hitungtotal(idx);
				$('#jumlahPembayaran').val("");
				$('#keterangan').val("");
			}
		}
	});

	function removeat(idx) {
		var jumlah = $('#jumlahPembayaran' + idx).val() * 1;
		totalbayar = totalbayar - jumlah;
		if (totalbayar) {
			totalbayar = 0;
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(countJumlah)); //Total Bayar
		$("#grandTotalBayar").val(toRp(totalbayar));
		bayar.splice(idx, 1);
		$("#baris" + idx).remove();
		hitungKembalian();
	}

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});
</script>
