<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 13/07/2017
 * Time: 14.13
 */ ?>
<style>
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}

	th, td {
		text-align: center;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">DETAIL <?php echo strtoupper($page); ?></li>
		</ol>
	</section>
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<div class="col-xs-12 col-lg-12">
					<div class="box">
						<div class="box-body">
							<form class="form-horizontal" role="form">
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Document :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['document']; ?></p>
											</div>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Total :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo "Rp. " . number_format($detail['total'], 0, ",", ","); ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Tanggal :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['tgl']; ?></p></div>
										</div>
										<?php if ($detail['tipe_trx'] == 2) : ?>
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Jam :</label>
												<div class="col-sm-9 col-lg-8">
													<p class="form-control-static"><?php echo $detail['jam']; ?></p>
												</div>
											</div>
										<?php endif; ?>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">LUNAS :</label>
											<div class="col-sm-9 col-lg-8"><p class="form-control-static">
													<?php if ($detail['lunas'] == 1) : ?>
														LUNAS
													<?php else: ?>
														BELUM LUNAS
													<?php endif; ?>
												</p></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Member :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['member']; ?></p>
											</div>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Pegawai :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['pegawai']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<?php if ($detail['tipe_trx'] == 3) : ?>
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">NOTA :</label>
												<div class="col-sm-9 col-lg-8"><p class="form-control-static">
														<?php if ($detail['id_nota'] != null) : ?>
															<?php echo $detail['id_nota']; ?>
														<?php else: ?>
															-
														<?php endif; ?>
													</p></div>
											</div>
										</div>
									</div>
								<?php endif; ?>
								<?php if ($detail['tipe_trx'] != 1 OR $detail['tipe_trx'] != 3) : ?>
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label class="control-label col-sm-3 col-lg-4">Dikirim :</label>
												<div class="col-sm-9 col-lg-8"><p class="form-control-static">
														<?php if ($detail['dikirim'] == 1) : ?>
															SUDAH DIKIRIM OLEH <?php echo $detail['pengirim'] ?>
														<?php elseif ($detail['dikirim'] == 0 && $detail['tipe_trx'] == 2): ?>
															AMBIL SENDIRI
														<?php else: ?>
															BELUM DIKIRIM
														<?php endif; ?>
													</p></div>
											</div>
										</div>
										<div class="col-md-5">
											<?php if ($detail['tipe_trx'] == 2 && $detail['dikirim'] == 0 && $detail['tgl_terima'] != null): ?>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Tanggal Terima :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['tgl_terima'] ?></p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Jam Terima :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['jam_terima'] ?></p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Keterangan :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['keterangan'] ?></p>
													</div>
												</div>
											<?php elseif ($detail['tipe_trx'] == 2 && $detail['dikirim'] == 1 && $detail['tgl_kirim'] != null): ?>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Tanggal Kirim :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['tgl_kirim'] ?></p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Jam Kirim :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['jam_kirim'] ?></p>
													</div>
												</div>
												<div class="form-group">
													<label class="control-label col-sm-3 col-lg-4">Alamat :</label>
													<div class="col-sm-9 col-lg-8">
														<p class="form-control-static"><?php echo $detail['alamat'] ?></p>
													</div>
												</div>
											<?php endif; ?>
										</div>
									</div>
								<?php endif; ?>
								<div class="row">
									<?php $detailPembayaran = $detail['payment'];
									$detailBarang = $detail['detail']; ?>
									<?php if ($detailPembayaran != null) : ?>
									<div class="col-xs-12 col-lg-8">
										<?php else: ?>
										<div class="col-xs-12 col-lg-12">
											<?php endif; ?>
											<h2 style="text-align: center;" class="label-rouded label-info">DETAIL BARANG</h2>
											<div class="table-responsive">
												<table id="kegiatanTable" class="table table-bordered table-striped users">
													<thead>
													<tr>
														<th width="5%" style="text-align: center;">No</th>
														<th width="35%" style="text-align: center">Nama Barang</th>
														<th width="20%" style="text-align: center">Harga</th>
														<th width="15%" style="text-align: center">Jumlah</th>
														<th width="20%" style="text-align: center">Subtotal</th>
													</tr>
													</thead>
													<tbody>
													<?php
													$counter = 1; ?>
													<?php foreach ($detailBarang AS $detailBarang): ?>
														<tr>
															<td><?php echo $counter; ?></td>
															<td><?php echo $detailBarang['nama_kue']; ?></td>
															<td><?php echo $detailBarang['harga']; ?></td>
															<td><?php echo $detailBarang['jumlah']; ?></td>
															<td style="text-align: right;"><?php echo "Rp. " . number_format(($detailBarang['harga'] * $detailBarang['jumlah']), 0, ",", ","); ?></td>
														</tr>
														<?php $counter++; ?>
													<?php endforeach; ?>
													<tr>
														<th colspan="4">Diskon</th>
														<td style="text-align: right;"><?php echo "Rp. " . number_format($detail['diskon_pembulatan'], 0, ",", ","); ?></td>
													</tr>
													</tbody>
													<tfoot>
													<tr>
														<th colspan="4" style="text-align: center;" ;>Grandtotal</th>
														<th style="text-align: right;" ;><?php echo "Rp. " . number_format($detail['total'] - ($detail['diskon_pembulatan'] * 1), 0, ",", "."); ?></th>
													</tr>
													</tfoot>
												</table>
											</div>
										</div>
										<?php if ($detailPembayaran != null) : ?>
											<div class="col-xs-12 col-lg-4">
												<h2 style="text-align: center;" class="label-rouded label-success">DETAIL PEMBAYARAN</h2>
												<div class="table-responsive">
													<table id="kegiatanTable" class="table table-bordered table-striped users">
														<thead>
														<tr>
															<th style="width: 5%; text-align: center;">No</th>
															<th style="width: 35%; text-align: center;">Tipe Bayar</th>
															<th style="width: 30%; text-align: center;">Keterangan</th>
															<th style="width: 30%; text-align: center;">Jumlah</th>
														</tr>
														</thead>
														<tbody>
														<?php
														$counter = 1;
														$pembayaran = 0; ?>
														<?php foreach ($detailPembayaran AS $detailPembayaran): ?>
															<tr>
																<td><?php echo $counter; ?></td>
																<td><?php echo $detailPembayaran['nama']; ?></td>
																<td><?php echo $detailPembayaran['keterangan']; ?></td>
																<td style="text-align: right"><?php echo "Rp. " . number_format($detailPembayaran['total']); ?></td>
															</tr>
															<?php $counter++;
															$pembayaran += $detailPembayaran['total']; ?>
														<?php endforeach; ?>
														</tbody>
														<tfoot>
														<tr>
															<th colspan="3" style="text-align: center;" ;>Grandtotal</th>
															<th style="text-align: right;" ;><?php echo "Rp. " . number_format($pembayaran, 0, ",", "."); ?></th>
														</tr>
														</tfoot>
													</table>
												</div>
											</div>
										<?php endif; ?>
									</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	</div>

	</section>
</div>
<!-- /.content -->
</div>
