<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var barang = []; //array barang
	var kode = "";
	var nama = "";
	var harga = "";
	var jumlah = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran
	var table;

	$(document).ready(function () {
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		$('.statusMember').hide();

		$(".pengiriman").hide();

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});

		showMember();
	});

	function showMember() {
		$('#memberTable').bootstrapTable('refresh', {
			silent: true
		});
		$('#modal-member').modal({show: 'true'});
	};

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/0'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	$('#barangTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getBarang'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseBarang(' + row['id_kue'] + ',`barangModal`,1)"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="id_kue' + row['id_kue'] + '" value="' + row['id_kue'] + '">'
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode Kue',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="barcode_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="nama_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="harga' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	$('#newMember').click(function () {
		var url = '<?php echo base_url('member/create')?>';
		window.open(url, '_blank');
		$('#modal-member').modal('hide');
	});

	shortcut.add("f1", function () {
		$("#searchKodeBarang").focus();
	});

	shortcut.add("f2", function () {
		$('#modal-barang').modal({show: 'true'});
	});

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	function konvertnilai(elm) {
		var nilai = ($(elm).autoNumeric('get') * 1);
		$(elm).autoNumeric('destroy');
		$(elm).val(nilai);
	};

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;
		var totalItem = 0;
		var diskon = 0;

		for (var i = 0; i < barang.length; i++) {
			var jumlah = barang[i].jumlah * 1;
			var harga = barang[i].harga * 1;
			var diskonBarang = barang[i].diskonBarang * 1;
			grandtotal += ((jumlah * harga) - diskonBarang);
			totalItem += jumlah;
		}

		$("#viewTotalBayar").html(toRp(grandtotal)); //Total Bayar
		$("#totalPenjualan").val(grandtotal); //Total Penjualan

		diskon = grandtotal * (($('#diskonMember').val() * 1) / 100); //Hitung Diskon
		$('#viewDiskon').html(toRp(diskon)); //Menampilkan Diskon
		$("#viewTotalItem").html("(" + totalItem + " Item)");
		var pemotongan = $('#pemotongan').val(); //Pemotongan harga
		grandtotal = grandtotal - diskon - pemotongan; //Hitung Grandtotal
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#viewGrandtotal").html(toRp(grandtotal));
		$("#grandtotal").val(grandtotal);
		$("#grandtotalPembelian").val(toRp(grandtotal));
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlah" + id).val());
		var harga = barang[id].harga * 1;
		var diskonBarang = barang[id].diskonBarang * 1;
		barang[id].jumlah = jumlah;

		var subtotal = (jumlah * harga) - diskonBarang;
		$("#subtotal" + id).html(toRp(subtotal));
		calculatetotal();
	}

	function updateDiskonBarang() {
		var diskonBarang = $("#diskonBarang").val();
		for (var i = 0; i < barang.length; i++) {
			var cekDiskon = barang[i].cekDiskon;
			if (cekDiskon != 0) {
				barang[i].diskon = 0;
			} else {
				barang[i].diskon = diskonBarang;
			}
			$("#diskonBarang" + i).val(diskonBarang);
			$("#viewDiskonBarang" + i).html(toRp(diskonBarang));
			hitungtotal(i);
		}
	}

	//hitung subtotal
	function hitungtotalPembayaran(id) {
		id = id * 1;
		var jumlah = eval($("#jumlahPembayaran" + id).val());
		bayar[id].jumlah = jumlah;

		calculatetotalPembayaran();
		hitungKembalian();
		//cekSubmit();
	}

	//hitung total pembayaran
	function calculatetotalPembayaran() {
		totalbayar = 0;

		for (var i = 0; i < bayar.length; i++) {
			var jumlah = bayar[i].jumlah * 1;
			totalbayar += jumlah;
		}

		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#grandTotalBayar").val(totalbayar); //Total Penjualan
		hitungKembalian();
	}

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		cekUsePoin = $('#userPoin' + id).val();
		var poin = $('#poin' + id).val();
		var saldo = $('#saldo' + id).val();
		var diskonBarang = $('#diskonBarang' + id).val();
		console.log(cekUsePoin);
		$('#saldoMember').html(toRp(saldo));
		$('#poinMember').html(toRp(poin));
		$('#diskonMember').val(diskon);
		$('#member').val(nama);
		$('#saldo').val(saldo);
		$('#diskonBarang').val(diskonBarang);
		$('#point').val(poin);
		$('#modal-member').modal('hide');
		updateDiskonBarang();
		calculatetotal();
		$('.statusMember').show("slow");
	}

	function changeDatePengiriman(){
		var tgl = $("#tgl").val();
		var time = $("#jam").val();
		$("#tglKirim").val(tgl);
		$("#jamKirim").val(time);
	}

	//Function untuk mencari history transaksi member
	function searchHistory() {
		var idMember = $('#idMember').val();
		$.ajax({
			url: "<?php echo base_url();?>searchHistory/" + idMember,
			success: function (data) {
				$("#historyBody").html("");
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var newRow = "<tr id='baris" + i + "'><td width='5%'><button type='button' class='btn btn-primary' onclick='addHistory(" + mydata[i].id_jual + ");'><i class='fa fa-plus'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idJual[]' id='idJual" + i + "' value='" + mydata[i].id_jual + "'> " +
						"<a onclick='detailHistory(" + mydata[i].id_jual + ")'>" + mydata[i].document + " <a></td>" +
						"<td>" + mydata[i].tgl + "</td>" +
						"<td>" + toRp(mydata[i].total * 1) + "</td></tr>";
					$("#historyBody").append(newRow);
				}
			}
		});
	}

	function detailHistory(id) {
		$.ajax({
			url: "<?php echo base_url();?>searchDetailHistory/" + id,
			success: function (data) {
				$("#historyDetailBody").html("");
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var newRow = "<tr id='baris" + i + "'>" +
						"<td>" + mydata[i].barcode_kue + " - " + mydata[i].nama_kue + "</td>" +
						"<td>" + mydata[i].jumlah + "</td>" +
						"<td>" + toRp(mydata[i].harga) + "</td></tr>";
					$("#historyDetailBody").append(newRow);
				}

				$('#modal-detailHistory').modal({
					show: 'true'
				});
			}
		});
	}

	function addHistory(id) {
		var diskonBarang = 0;
		$.ajax({
			url: "<?php echo base_url();?>searchDetailHistory/" + id,
			success: function (data) {
				console.log(data);
				$("#bodyNota").html("");
				barang = [];
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var obj = new Object();
					cekDiskon = mydata[i].diskon;
					if (cekDiskon != 0) {
						diskonBarang = 0;
					}
					var subtotal = (mydata[i].jumlah * 1) * (mydata[i].harga * 1) - (diskonBarang * 1);
					obj.id = mydata[i].id_kue;
					obj.kode = mydata[i].barcode_kue;
					obj.harga = mydata[i].harga;
					obj.nama = mydata[i].nama_kue;
					obj.diskonBarang = diskonBarang;
					barang.push(obj);
					idx = barang.length - 1;
					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='25%'>" +
						"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + mydata[i].id_kue + "'> " +
						" " + mydata[i].barcode_kue + " - " + mydata[i].nama_kue + " </td>" +
						"<td  width='20%'  style='text-align:right'><span id='harga'>" + toRp(mydata[i].harga * 1) + "</span><input type='hidden' name='harga[]' value='" + mydata[i].harga + "'></td>" +
						"<td  width='5%'  style='text-align:right'><span id='viewDiskonBarang" + idx + "'>" + toRp(diskonBarang * 1) + "</span><input type='hidden' name='diskonBarang[]' id='diskonBarang" + idx + "' value='" + diskonBarang + "'></td>" +
						"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + mydata[i].jumlah + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
						"<td width='20%' style='text-align:right'><span id='subtotal" + idx + "'>" + toRp(subtotal) + " </span></td></tr>";
					$("#bodyNota").append(newRow);
					hitungtotal(idx);
					var objDiv = document.getElementById("detailTable");
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				$('#modal-history').modal('hide');
			}
		});
	}

	function cekKodeBarang() {
		var txtKode = $('#searchKodeBarang').val();
		if (txtKode.length >= 5) {
			var jumlah = txtKode.split('*');
			if (jumlah.length > 1) {
				var arr = jumlah[1].split('_');
			} else {
				var arr = txtKode.split('_');
				jumlah[0] = 1;
			}
			var kode = arr[0];
			$.ajax({
				url: "<?php echo base_url();?>cekKodeBarang/" + kode,
				success: function (data) {
					if (data == 'null') {
						showAlert('', "Kode Barang tidak ditemukan.", 'error');
						error = true;
					} else {
						chooseBarang(data, "cekKodeBarang", jumlah[0]);
					}
				}
			});
			$('#txtKodeBarang').val("");
			$('#searchKodeBarang').val("");
		}
	}

	function chooseBarang(id, type, qty) {
		var diskonBarang = $('#diskonBarang').val();
		if (type == "barangModal") {
			kode = $('#barcode_kue' + id).val();
			nama = $('#nama_kue' + id).val();
			harga = $('#harga' + id).val();
			cekDiskon = $('#cekDiskon' + id).val();
			qty = qty;
		} else if (type == "cekKodeBarang") {
			var mydata = $.parseJSON(id);
			kode = mydata.barcode_kue;
			harga = mydata.harga;
			cekDiskon = mydata.diskon;
			nama = mydata.nama_kue;
			id = mydata.id_kue;
			qty = qty;
		}
		var obj = new Object();
		var cek = 0;
		for (var i = 0; i < barang.length; i++) {
			if (barang[i].id == id || barang[i].kode == kode) {
				cek++;
			}
		}
		if (cek == 0) {
			if (cekDiskon != 0) {
				diskonBarang = 0;
			}
			var subtotal = 1 * (harga - diskonBarang);
			obj.id = id;
			obj.kode = kode;
			obj.harga = harga;
			obj.cekDiskon = cekDiskon;
			obj.diskonBarang = diskonBarang;
			obj.nama = nama;
			obj.jumlah = qty;
			barang.push(obj);
			idx = barang.length - 1;
			var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></i></button></td>" +
				"<td width='25%'>" +
				"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + id + "'><input type='hidden' name='cekDiskon[]' id='cekDiskon" + idx + "' value='" + cekDiskon + "'>" +
				" " + kode + " - " + nama + " </td>" +
				"<td  width='20%'  style='text-align:right'><span id='harga'>" + toRp(harga * 1) + "</span><input type='hidden' name='harga[]' value='" + harga + "'></td>" +
				"<td  width='10%'  style='text-align:right'><span id='viewDiskonBarang" + idx + "'>" + toRp(diskonBarang * 1) + "</span><input type='hidden' name='diskonBarang[]' id='diskonBarang" + idx + "' value='" + diskonBarang + "'></td>" +
				"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + qty + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
				"<td width='20%' style='text-align:right'><span id='subtotal" + idx + "'>" + toRp(subtotal) + " </span></td></tr>";
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			$("#bodyNota").append(newRow);
			var objDiv = document.getElementById("bodyNota");
			objDiv.scrollTop = objDiv.scrollHeight;
		} else {
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			for (var i = 0; i < jumlahrow; i++) {
				var cekID = $('#idBarang' + i).val();
				if (cekID == id) {
					var jumlah = $('#jumlah' + i).val() * 1;
					jumlah = jumlah + (qty * 1);
					$('#jumlah' + i).val(jumlah);
					var idx = i;
				}
			}

		}
		$('#modal-barang').modal('hide');
		hitungtotal(idx);
	}

	function removeat(idx) {
		//console.log(idx);
		barang.splice(idx, 1);
		$("#baris" + idx).remove();
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			barang = [];
		}
		$("#pemotongan").val(0);
		$("#viewPemotongan").html(0);
		calculatetotal();
	}

	function removeatPembayaran(idx) {
		//console.log(idx);
		bayar.splice(idx, 1);
		$("#barisPembayaran" + idx).remove();
		calculatetotalPembayaran();
	}

	//payment type
	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;
		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			$('[class="col-sm-12 form-group point"').show("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		var diskon = $('#diskonMember').val();
		var total = $('#grandtotal').val();
		if (total == "") {
			total = 0;
		}
		if (pemotongan > (total - diskon)) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();
			hitungKembalian();
		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}

	//Payment Submit
	$('#btnSimpan').click(function () {
		$('#pleaseWaitDialog').modal('show');
		$("#type").val("payment");
		$("#confirm-deposit-modal").modal('hide');
		$("#modal-payment").modal('hide');
		$('#deposit').val('TIDAK');
		$.ajax({
			url: "<?php echo base_url($page . '/create'); ?>",
			method: 'post',
			data: {
				idMember: $('#idMember').val(),
				tgl: $('#tgl').val(),
				diskonMember: $('#diskonMember').val(),
				idPegawai: $('#idPegawai').val(),
				pemotongan: $('#pemotongan').val(),
				totalPenjualan: $('#totalPenjualan').val(),
				jam: $('#jam').val(),
				kirim: $('#kirim').val(),
				tglKirim: $('#tglKirim').val(),
				jamKirim: $('#jamKirim').val(),
				alamat: $('#alamat').val(),
				grandtotal: $('#grandtotal').val(),
				type: 'payment',
				payment: bayar,
				barang: barang,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pesanan Berhasil disimpan', 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	function hitungKembalian() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			var kembalian = totalbayar - grandtotal;
			$("#kembalianView").html(toRp(kembalian));
			$("#kembalian").val(kembalian);
		} else {
			$("#kembalianView").html(toRp(0));
			$("#kembalian").val(0);
		}
	}

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		addPayment();
	});

	function addPayment() {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();
		var keterangan = $("#keterangan").val();

		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
		} else {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}
			if (cek == 0) {
				obj.id = jenis;
				obj.jumlah = jumlahBayar;
				obj.keterangan = keterangan;
				bayar.push(obj);
				idx = bayar.length - 1;

				var newRow = "<tr id='barisPembayaran" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePembayaran' onclick='removeatPembayaran(" + idx + ");'><i class='fa fa-trash'></button></td>" +
					"<td width='35%'>" +
					"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
					"<td width='20%'  style='text-align:right'><span id='viewKeterangan" + idx + "'>" + keterangan + "</span><input type='hidden' name='keteranganPayment[]' id='keteranganPayment" + idx + "' value='" + keterangan + "'></td>" +
					"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlahPembayaran[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
				$("#paymentDetail").append(newRow);
			} else {
				var jumlahrow = document.getElementById("paymentDetail").rows.length;
				for (var i = 0; i < jumlahrow; i++) {
					var cekID = $('#idPembayaran' + i).val();
					if (cekID == jenis) {
						var jumlahPembayaran = eval($('#jumlahPembayaran' + i).val());
						jumlahPembayaran = (jumlahPembayaran * 1) + (jumlahBayar * 1);
						$('#jumlahPembayaran' + i).val(jumlahPembayaran);
						$('#viewJumlah' + i).html(toRp(jumlahPembayaran));
						var idx = i;
					}
				}
			}
			hitungtotalPembayaran(idx);
			$('#jumlahPembayaran').val("");
		}
	}

	$('#btnSubmit').click(function () {
		if ($('#createForm').valid()) {
			$('#modal-payment').modal({
				show: 'true'
			});
		}
	});

	$('#showHistory').click(function () {
		var idMember = $('#idMember').val();
		if (idMember == "") {
			showAlert('', "Pilih member terlebih dahulu", 'error');
		} else {

			searchHistory();
			$('#modal-history').modal({
				show: 'true'
			});
		}
	});

	$("#kirim").on("click", function () {
		if ($("#kirim").is(':checked')) {
			$(".pengiriman").show("slow");
		} else {
			$(".pengiriman").hide("slow");
		}
	});

	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});
	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});
	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});

	$('#btnPayment').click(function () {
		//var jumlahrow=document.getElementById("bodyNota").rows.length;
		if ($('#createForm').valid()) {
			$('#modal-payment').modal({
				show: 'true'
			});
		}
	});

	//Timepicker
	$('.timepicker').timepicker({
		showInputs: true,
		showMeridian: false,
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '+1D'
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
			member: {
				required: true,
			}
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
			member: {
				required: "Member harus diisi",
			}
		}
	});

	$("#searchKodeBarang").autocomplete({
		source: "<?php echo base_url('searchBarang'); ?>",
		select: function (event, ui) {
			var select = ui.item.label;
			var arr = select.split('_');
			$('#txtKodeBarang').val(arr[0]);
		}
	});
</script>
