<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<style>
	.ui-autocomplete {
		max-height: 300px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	}

	/* IE 6 doesn't support max-height
     * we use height instead, but this forces the menu to always be this tall
     */
	* html .ui-autocomplete {
		height: 400px;
	}

	.fixed_header {
		width: 100%;
		border-collapse: collapse;
	}

	.fixed_header tbody {
		display: block;
		width: 100%;
		overflow: auto;
		height: 300px;
	}

	.fixed_header thead tr {
		display: block;
	}

	.fixed_header th, .fixed_header td {
		padding: 5px;
		text-align: left;
		width: 200px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			INPUT
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">INPUT <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/create'); ?>">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="row col-md-12">
										<div class="col-md-4">
											<div class="form-group">
												<label><h4>Tanggal Pesanan:</h4></label>
												<div class="input-group">
													<input type="text" class="form-control tgl pull-right" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime(date('d-m-Y') . "+1 days")); ?>" onchange="changeDatePengiriman()">
													<span class="input-group-btn">
														<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<h4><label>Jam :</label></h4>
												<div class="input-group bootstrap-timepicker">
													<input type="text" class="form-control pull-right timepicker" id="jam" name="jam" placeholder="Jam" readonly value="07:00" onchange="changeDatePengiriman()">
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat"><i class="fa fa-clock-o"></i></button>
										</span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label><h4>Member :</h4></label>
												<div class="input-group">
													<input type="hidden" id="idMember" name="idMember">
													<input type="hidden" id="diskonMember" name="diskonMember" value="0">
													<input type="hidden" id="diskonBarang" name="diskonBarang" value="0">
													<input type="text" class="form-control" id="member" placeholder="Member" name="member" onclick="showMember()" readonly>
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-member"><i class="fa fa-search"></i></button>
										</span>
												</div>
											</div>
										</div>
										<input type="hidden" name="idPegawai" id="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
										<!-- <div class="col-sm-4">
											<div class="form-group">
												<label><h4>Pegawai :</h4></label>
												<div class="input-group">
													<input type="text" class="form-control" value="<?php echo $_SESSION['nama'] ?>" readonly>
													
													<span class="input-group-btn">
														<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
													</span>
												</div>
											</div>
										</div> -->
										<div class="col-sm-2">
											<div class="form-group">
												<div class="checkbox">
													<label>
														<h4>
															<input type="checkbox" id="kirim" value="kirim" name="kirim"> Di kirim ?
														</h4>
													</label>
												</div>
											</div>
										</div>
										<div class="col-sm-3 pengiriman">
											<div class="input-group">
												<button type="button" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal-pengiriman">
													<i class="fa fa-search"> Detail Pengiriman</i></button>
											</div>
										</div>
										<div class="statusMember col-md-6">
											<div class="col-sm-4">
												<div class="form-group">
													<label>Point :
														<span id="poinMember" class="pull-right">0</span></label>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group">
													<label>Saldo :
														<span id="saldoMember" class="pull-right">0</span></label>
												</div>
											</div>
										</div>
									</div>

									<!-- Detail -->
									<ul class="nav nav-tabs">
									</ul>
									<section>
										<div class="col-sm-4">
											<div class="form-group">
												<input type="text" class="form-control autocomplete" id="searchKodeBarang" placeholder="Scan atau Kode Barang" name="searchKodeBarang" onchange="cekKodeBarang();">
												<input type="hidden" id="txtKodeBarang" name="txtKodeBarang">
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-barang">
													<i class="fa fa-search"> Cari </i></button>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" id="showHistory">
													<i class="fa fa-search"> History </i></button>
											</div>
										</div>
									</section>

									<!-- /.box-body -->
									<div class="box-body">
										<table class="table table-bordered fixed_header" id="detailTable">
											<thead>
											<tr>
												<th width="5%" style="text-align: center">Action</th>
												<th width="25%" style="text-align: center">Nama Barang</th>
												<th width="20%" style="text-align: center">Harga</th>
												<th width="5%" style="text-align: center">Diskon</th>
												<th width="20%" style="text-align: center">Jumlah</th>
												<th width="20%" style="text-align: center">Subtotal</th>
											</tr>
											</thead>
											<tbody id="bodyNota">
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2>
													<label id="viewTotalItem"><h4>(0 Item)</h4></label></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right">0</span></h2></div>
												<input type="hidden" name="totalPenjualan" value="0" id="totalPenjualan">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<a data-toggle="modal" data-target="#modal-verifikasi">
														<h3 class="list-group-item-heading">DISKON</h3></a></div>
												<div class="col-sm-6"><h3 class="list-group-item-heading pull-right">
														<span id="viewPemotongan" class="pull-right">0</span></h3></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h2 class="list-group-item-heading">GRAND TOTAL</h2></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right">0</span></h2></div>
												<input type="hidden" name="grandtotal" value="0" id="grandtotal">
												<input type="hidden" name="pemotongan" id="pemotongan" value="0">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> PEMBAYARAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>

								</section>
							</div>
						</div>
						<input type="hidden" name="type" id="type" value="">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<!-- /.box-footer -->
						<!-- Modal Payment -->
						<div class="modal fade" id="modal-payment">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">Pembayaran</h4>
									</div>
									<div class="modal-body">
										<div class="col-sm-12 form-group">
											<label class="col-sm-4 control-label">Grandtotal :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="grandtotalPembelian" placeholder="Total" name="grandtotalPembelian" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group">
											<label class="col-sm-4 control-label">Tipe Pembayaran :</label>
											<div class="col-sm-8">
												<select class="form-control select2" id="payment" name="payment" style="width: 100%;">
													<?php foreach ($payment as $payment) : ?>
														<option value="<?php echo $payment['id_masterbayar'] ?>"><?php echo $payment['nama'] ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="col-sm-12 form-group point">
											<label class="col-sm-4 control-label">Point :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="point" placeholder="Point" name="point" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group saldo">
											<label class="col-sm-4 control-label">Saldo :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="saldo" placeholder="Saldo" name="saldo" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group jumlah">
											<label class="col-sm-4 control-label">Jumlah :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control input-uang" id="jumlahPembayaran" placeholder="Jumlah Pembayaran" name="jumlahPembayaran">
											</div>
										</div>
										<div class="col-sm-12 form-group jumlah">
											<label class="col-sm-4 control-label">Keterangan :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan">
											</div>
										</div>
										<div class="col-sm-12">
											<center>
												<button type="button" class="btn btn-info btn-sm btn-block" id="btnTambah">
													<i class="fa fa-plus"></i> Bayar
												</button>
											</center>
										</div>
										<table class="table table-bordered" id="paymentTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th style="text-align: center"><i class="fa fa-trash"></th>
												<th style="text-align: center">Jenis Pembayaran</th>
												<th style="text-align: center">Keterangan</th>
												<th style="text-align: center">Jumlah</th>
											</tr>
											</thead>
											<tbody id="paymentDetail">
											</tbody>
											<tfoot>
											<tr>
												<th colspan="3" style="text-align: center;">Total Pembayaran</th>
												<th style="text-align: right">
													<span id="grandtotalPembayaran" class="pull-right">0</span></th>
											</tr>
											<tr>
												<th colspan="3" style="text-align: center;">Kembalian</th>
												<th style="text-align: right">
													<span id="kembalianView" class="pull-right">0</span></th>
											</tr>
											<input type="hidden" name="kembalian" id="kembalian" value="0">
											<input type="hidden" name="grandTotalBayar" id="grandTotalBayar" value="0">
											</tfoot>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-info" id="btnSimpan">Save</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
						<div class="modal fade" id="modal-pengiriman">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">Pembayaran</h4>
									</div>
									<div class="modal-body">
										<div class="form-group col-sm-12 ">
											<label class="col-sm-4 control-label">Tanggal Kirim:</label>
											<div class="input-group col-sm-8">
												<input type="text" class="form-control tgl pull-right" id="tglKirim" name="tglKirim" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y'); ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
											<!-- /.input group -->
										</div>
										<div class="form-group col-sm-12">
											<label class="col-sm-4 control-label">Jam Kirim:</label>
											<div class="input-group bootstrap-timepicker  col-sm-8">
												<input type="text" class="form-control pull-right timepicker" id="jamKirim" name="jamKirim" placeholder="Jam" readonly>
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-clock-o"></i></button>
									</span>
											</div>
										</div>
										<div class="col-sm-12 form-group">
											<label class="col-sm-4 control-label">Alamat Kirim:</label>
											<div class="col-sm-8 input-group">
												<input type="text" class="form-control" id="alamat" placeholder="Alamat" name="alamat">
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
					</form>
					<!-- /.box -->
				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
	</div>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-member">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Member</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<button class="btn btn-success" id="newMember">Create member</button>
				</div>
				<table class="table table-bordered" id="memberTable" style="width:100%">
					<thead>
					<tr>
						<th width="3%"></th>
						<th style="text-align: center;">Kode</th>
						<th style="text-align: center;">Nama</th>
						<th style="text-align: center;">No HP</th>
						<th style="text-align: center;">Keterangan</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-barang">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Barang</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="barangTable" style="width:100%">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- Modal Pemotongan -->
<div class="modal fade" id="modal-pemotongan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pemotongan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-2 control-label">Jumlah :</label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-uang" id="jumlahPemotongan" value="0" placeholder="Jumlah Pemotongan">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnPemotongan">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-history">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">History</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="detailTable" style="width:100%">
					<thead class="alert">
					<tr>
						<th width="3%"></th>
						<th width="40%" style="text-align: center">Document</th>
						<th width="40%" style="text-align: center">Tanggal</th>
						<th width="20%" style="text-align: center">Total</th>
					</tr>
					</thead>
					<tbody id="historyBody">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-detailHistory">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Detail History</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="detailTable" style="width:100%">
					<thead class="alert">
					<tr>
						<th width="40%" style="text-align: center">Kode</th>
						<th width="20%" style="text-align: center">Jumlah</th>
						<th width="20%" style="text-align: center">Harga</th>
					</tr>
					</thead>
					<tbody id="historyDetailBody">
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-verifikasi">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Username :</label>
					<div class="col-sm-9">
						<input type="text" class="form-control pull-right" id="username" name="username" placeholder="Username">
					</div>
				</div>
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Password :</label>
					<div class="col-sm-9">
						<input type="password" class="form-control pull-right" id="password" name="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnVerifikasi">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
