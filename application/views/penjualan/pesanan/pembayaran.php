<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<style>
	.ui-autocomplete {
		max-height: 300px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	}

	/* IE 6 doesn't support max-height
     * we use height instead, but this forces the menu to always be this tall
     */
	* html .ui-autocomplete {
		height: 400px;
	}

	.fixed_header {
		width: 100%;
		border-collapse: collapse;
	}

	.fixed_header tbody {
		display: block;
		width: 100%;
		overflow: auto;
		height: 400px;
	}

	.fixed_header thead tr {
		display: block;
	}

	.fixed_header th, .fixed_header td {
		padding: 5px;
		text-align: left;
		width: 200px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Pembayaran
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">Pembayaran <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="pembayaranForm" method="post" action="<?php echo base_url($page . '/store_payment'); ?>">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Tanggal Pesanan :</h4></label>
											<div class="input-group date">
												<input type="text" class="form-control pull-right" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo $data['tgl_pesanan']; ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Member :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" id="member" placeholder="Member" name="member" readonly value="<?php echo $data['member']; ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label><h4>Pegawai :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" value="<?php echo $data['pegawai']; ?>" readonly>
												<input type="hidden" name="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
									</span>
											</div>
										</div>
									</div>

									<!-- /.box-body -->
									<div class="box-body">
										<table class="table table-bordered fixed_header" id="detailTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th width="30%" style="text-align: center">Nama Barang</th>
												<th width="20%" style="text-align: center">Harga</th>
												<th width="20%" style="text-align: center">Diskon</th>
												<th width="10%" style="text-align: center">Jumlah</th>
												<th width="20%" style="text-align: center">Subtotal</th>
											</tr>
											</thead>
											<tbody id="bodyNota">
											<?php $counter = 1;
											$countItem = 0;
											$detailpenjualan = $data['detail']; ?>
											<?php foreach ($detailpenjualan as $detailSO) : ?>
												<tr>
													<td style="text-align: left"><?php echo $detailSO['nama_kue']; ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format($detailSO['harga'], 0, ",", ","); ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format($detailSO['diskon'], 0, ",", ","); ?></td>
													<td style="text-align: right"><?php echo $detailSO['jumlah'];
														$countItem = $countItem + ($detailSO['jumlah'] * 1); ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format(($detailSO['harga'] * $detailSO['jumlah'] - $detailSO['diskon']), 0, ",", ","); ?></td>
												</tr>
												<?php $counter++; ?>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h4 class="list-group-item-heading">TOTAL</h4>
													<label id="viewTotalItem"><h4>(<?php echo $countItem ?> Item)</h4>
													</label></div>
												<input type="hidden" name="total" id="total" value="<?php echo $data['total']; ?>">
												<div class="col-sm-6"><h4 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right"><?php echo number_format($data['total'], 0, ".", "."); ?></span>
													</h4></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h4 class="list-group-item-heading">DISKON MEMBER</h4></div>
												<div class="col-sm-6"><h4 class="list-group-item-heading pull-right">
														<input type="hidden" name="diskonMember" id="diskonMember" value="<?php echo $data['diskon_member']; ?>">
														<span id="viewDiskon" class="pull-right"><?php echo number_format((($data['total'] * 1) * ($data['diskon_member'] / 100)), 0, ".", "."); ?></span>
													</h4></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<a data-toggle="modal" data-target="#modal-verifikasi">
														<h4 class="list-group-item-heading">PEMOTONGAN</h4></a></div>
												<input type="hidden" name="pemotongan" id="pemotongan" value="<?php echo $data['diskon_pembulatan']; ?>">
												<div class="col-sm-6"><h4 class="list-group-item-heading pull-right">
														<span id="viewPemotongan" class="pull-right"><?php echo number_format($data['diskon_pembulatan'], 0, ".", "."); ?></span>
													</h4></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h4 class="list-group-item-heading">GRAND TOTAL</h4></div>
												<div class="col-sm-6"><h4 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right"><?php echo number_format($data['total'] - ($data['total'] * $data['diskon_member'] / 100) - $data['diskon_pembulatan'], 0, ".", "."); ?></span>
													</h4></div>
												<input type="hidden" name="grandtotal" value="<?php echo $data['total'] - ($data['total'] * $data['diskon_member'] / 100) - $data['diskon_pembulatan']; ?>" id="grandtotal">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-success btn-lg btn-block" id="btnSubmit" data-toggle="modal" data-target="#modal-payment">
													<i class="fa fa-money"></i> PEMBAYARAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>
								</section>
							</div>
						</div>
						<input type="hidden" name="type" id="type" value="">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<!-- /.box-footer -->

						<!-- Modal Payment -->
						<div class="modal fade" id="modal-payment">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title">Pembayaran</h4>
									</div>
									<div class="modal-body">
										<div class="col-sm-12 form-group">
											<label class="col-sm-4 control-label">Grandtotal :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="grandtotalPembelian" placeholder="Total" name="grandtotalPembelian" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group">
											<label class="col-sm-4 control-label">Tipe Pembayaran :</label>
											<div class="col-sm-8">
												<select class="form-control select2" id="payment" name="payment" style="width: 100%;">

													<?php foreach ($payment as $payment) : ?>
														<option value="<?php echo $payment['id_masterbayar'] ?>"><?php echo $payment['nama'] ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="col-sm-12 form-group point">
											<label class="col-sm-4 control-label">Point :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="point" placeholder="Point" name="point" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group saldo">
											<label class="col-sm-4 control-label">Saldo :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="saldo" placeholder="Saldo" name="saldo" readonly>
											</div>
										</div>
										<div class="col-sm-12 form-group jumlah">
											<label class="col-sm-4 control-label">Jumlah :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control input-uang" id="jumlahPembayaran" placeholder="Jumlah Pembayaran" name="jumlahPembayaran">
											</div>
										</div>
										<div class="col-sm-12 form-group jumlah">
											<label class="col-sm-4 control-label">Keterangan :</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan">
											</div>
										</div>
										<div class="col-sm-12">
											<center>
												<button type="button" class="btn btn-info btn-sm btn-block" id="btnTambah">
													<i class="fa fa-plus"></i> Bayar
												</button>
											</center>
										</div>
										<table class="table table-bordered searchTable" id="paymentTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th style="text-align: center"><i class="fa fa-trash"></th>
												<th style="text-align: center">Jenis Pembayaran</th>
												<th style="text-align: center">Keterangan</th>
												<th style="text-align: center">Jumlah</th>
											</tr>
											</thead>
											<?php $counter = 0;
											$grandtotal = 0; ?>
											<tbody id="paymentDetail">
											<?php foreach ($data['payment'] as $payment) : ?>
												<tr id=<?php echo "barisPembayaran" . $counter ?>>
													<td width="5%">
														<button type="button" class="btn btn-danger removePembayaran" onclick="removeat(<?php echo $counter ?>);">
															<i class="fa fa-trash"></i></button>
													</td>
													<td width="35%">
														<input type="hidden" name="idPembayaran[]" id=<?php echo "idPembayaran" . $counter; ?> value="<?php echo $payment['id_tipe_bayar']; ?>"><?php echo $payment['nama']; ?>
													</td>
													<td width="30%">
														<input type="hidden" name="keteranganPayment[]" id=<?php echo "keteranganPayment" . $counter; ?> value="<?php echo $payment['keterangan']; ?>"><?php echo $payment['keterangan']; ?>
													</td>
													<td width="30%" style="text-align: right">
														<input type="hidden" name="jumlahPembayaran[]" id=<?php echo "jumlahPembayaran" . $counter; ?> value="<?php echo $payment['total']; ?>"><span id=<?php echo "viewJumlah" . $counter ?>><?php echo number_format(($payment['total'] * 1), 0, ",", "."); ?></span>
													</td>
												</tr>
												<?php $counter++;
												$grandtotal += ($payment['total'] * 1) ?>
											<?php endforeach; ?>
											</tbody>
											<tfoot>
											<tr>
												<th colspan="3" style="text-align: center;">Total Pembayaran</th>
												<th style="text-align: right">
													<span id="grandtotalPembayaran" class="pull-right">0</span></th>
											</tr>
											<tr>
												<th colspan="3" style="text-align: center;">Kembalian</th>
												<th style="text-align: right">
													<span id="kembalianView" class="pull-right">0</span></th>
											</tr>
											<input type="hidden" name="kembalian" id="kembalian" value="0">
											<input type="hidden" name="grandTotalBayar" id="grandTotalBayar" value="0">
											</tfoot>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
										<button type="button" class="btn btn-info" id="btnSimpan">Save</button>
										<div id="confirm-deposit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-hidde n="true">×</button>
														<h4 class="modal-title">Konfirmasi Penyimpanan Pembayaran</h4>
													</div>
													<div class="modal-body">
														<p>Apakah anda yakin ingin menyimpan sisa saldo ini?</p>
													</div>
													<div class="modal-footer">
														<button type="button" id="confirmSubmit" class="btn btn-success waves-effect">
															<i class="fa fa-check"></i> Ya
														</button>
														<button type="button" id="noConfirmSubmit" class="btn btn-default waves-effect">Tidak</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
						<input type="hidden" id="idPenjualan" name="idPenjualan" value="<?php echo $data['id_jual']; ?>">
						<input type="hidden" name="deposit" value="" id="deposit">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
					</form>
					<!-- /.box -->
				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
	</div>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-pemotongan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pemotongan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-2 control-label">Jumlah :</label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-uang" id="jumlahPemotongan" value="0" placeholder="Jumlah Pemotongan">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnPemotongan">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-verifikasi">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Username :</label>
					<div class="col-sm-9">
						<input type="text" class="form-control pull-right" id="username" name="username" placeholder="Username">
					</div>
				</div>
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Password :</label>
					<div class="col-sm-9">
						<input type="password" class="form-control pull-right" id="password" name="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnVerifikasi">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
