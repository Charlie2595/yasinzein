<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Pengiriman
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">Pengiriman <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="pengirimForm" method="post" action="#">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9">
								<div class="box-header">
									<div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<h4><label>Tanggal :</label></h4>
												<div class="input-group">
													<input type="text" class="form-control pull-right tgl" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo set_value('tglKirim', date('d-m-Y')); ?>">
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
										</span>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<h4><label>Jam :</label></h4>
												<div class="input-group bootstrap-timepicker">
													<input type="text" class="form-control pull-right timepicker" id="jam" name="jam" placeholder="Jam" readonly>
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat"><i class="fa fa-clock-o"></i></button>
										</span>
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<h4><label>Member :</label></h4>
												<div class="input-group">
													<input type="text" class="form-control" id="member" placeholder="Member" name="member" readonly value="<?php echo $data['member']; ?>">
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat"><i class="fa fa-search"></i></button>
										</span>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<h4><label>Pengirim :</label></h4>
												<div class="input-group" data-toggle="modal" data-target="#modal-pegawai">
													<input type="text" class="form-control" placeholder="Pegawai Pengirim" id="namaPegawai" readonly name="namaPegawai">
													<input type="hidden" name="idPegawai" id="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
													<span class="input-group-btn">
											<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
										</span>
												</div>
											</div>
										</div>
									</div>
									<div class="row detail">
										<div class="col-md-3">
											<div class="form-group">
												<label>Nama</label>
												<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="<?php echo $data['member']; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<label>No Telp :</label>
												<input type="text" class="form-control" id="notelp" name="notelp" placeholder="No Telepon" value="<?php echo $data['nohp']; ?>">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label>Alamat :</label>
												<textarea class="form-control" rows="2" placeholder="Alamat" id="alamat" name="alamat"><?php if ($data['alamat_kirim'] == null) {
														echo $data['alamat_rumah'];
													} else {
														echo $data['alamat_kirim'];
													} ?></textarea>
											</div>
										</div>
									</div>

									<!-- /.box-body -->
									<div class="box-body">
										<table class="table table-bordered" id="detailTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th width="30%" style="text-align: center">Nama Barang</th>
												<th width="20%" style="text-align: center">Harga</th>
												<th width="20%" style="text-align: center">Diskon</th>
												<th width="10%" style="text-align: center">Jumlah</th>
												<th width="20%" style="text-align: center">Subtotal</th>
											</tr>
											</thead>
											<tbody id="bodyNota">
											<?php $counter = 1;
											$datapenjualan = $data['detail']; ?>
											<?php foreach ($datapenjualan as $dataSO) : ?>
												<tr>
													<td style="text-align: left"><?php echo $dataSO['nama_kue']; ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format($dataSO['harga'], 0, ",", ","); ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format($dataSO['diskon'], 0, ",", ","); ?></td>
													<td style="text-align: right"><?php echo $dataSO['jumlah']; ?></td>
													<td style="text-align: right"><?php echo "Rp. " . number_format(($dataSO['harga'] * $dataSO['jumlah'] - $dataSO['diskon']), 0, ",", ","); ?></td>
												</tr>
												<?php $counter++; ?>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2>
												</div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right"><?php echo number_format($data['total'], 0, ".", "."); ?></span>
													</h2></div>
												<input type="hidden" name="total" id="total" value="<?php echo $data['total']; ?>">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="form-group col-sm-12">
													<h3>Tipe Pembayaran</h3>
													<select class="form-control" id="tipeBayar" name="tipeBayar">
														<option value="Sudah Lunas">Sudah Lunas</option>
														<option value="Tagih Ditempat">Tagih ditempat</option>
													</select>
												</div>
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> SIMPAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>
								</section>
							</div>
						</div>
						<!-- /.box-footer -->
						<input type="hidden" name="idPenjualan" id="idPenjualan" value="<?php echo $data['id_jual']; ?>">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
					</form>
					<!-- /.box -->
				</div>
				<!--/.col (right) -->
			</div>
			<!-- /.row -->
		</section>
	</div>
	<!-- /.content -->
</div>

<div class="modal fade" id="modal-pegawai">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pegawai</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="pegawaiTable" style="width:100%">
					<thead class="alert">
					<tr>
						<th width="3%"></th>
						<th width="20%" style="text-align: center">Nama</th>
						<th width="20%" style="text-align: center">No HP</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($pegawai as $list) : ?>
						<tr>
							<td>
								<button type="button" class="btn btn-xs btn-primary" onclick="choosePegawai(<?php echo $list['id']; ?>)">
									<i class="fa fa-plus"></i></button>
								<input type="hidden" id="nama<?php echo $list['id']; ?>" value="<?php echo $list['nama']; ?>">
							</td>
							<td><?php echo $list['nama']; ?></td>
							<td><?php echo $list['nohp']; ?></td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>





