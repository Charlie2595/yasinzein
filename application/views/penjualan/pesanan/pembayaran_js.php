<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var bayar = [];
	var harga = "";
	var idx = 0;
	var totalbayar = 0;
	var grandtotal = 0;

	$(document).ready(function () {
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		grandtotal = $('#grandtotal').val();
		var count = document.getElementById("paymentDetail").rows.length;
		for (var i = 0; i < count; i++) {
			var id = $('#idPembayaran' + i).val();
			var jumlahPembayaran = $('#jumlahPembayaran' + i).val() * 1;
			var keterangan = $('#keteranganPayment' + i).val() * 1;

			var obj = new Object();
			obj.id = id;
			obj.jumlah = jumlahPembayaran;
			obj.keterangan = keterangan;
			bayar.push(obj);
			totalbayar += jumlahPembayaran;
			calculatetotalPembayaran();
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$('#grandTotalBayar').val(totalbayar);

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
		var objDiv = document.getElementById("bodyNota");
		objDiv.scrollTop = objDiv.scrollHeight;

	});

	function calculatetotalPembayaran() {
		totalbayar = 0;

		for (var i = 0; i < bayar.length; i++) {
			var jumlah = bayar[i].jumlah * 1;
			totalbayar += jumlah;
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#grandTotalBayar").val(totalbayar); //Total Penjualan
		hitungKembalian();
	}

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});
	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});
	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});

	//hitung grandtotal
	function calculatetotal() {
		var diskon = 0;
		var pemotongan = $('#pemotongan').val() * 1;
		var diskon = $('#diskonMember').val() * 1;
		var total = $('#total').val() * 1;
		diskon = total * diskon / 100;

		grandtotal = total - (pemotongan + diskon);
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#viewGrandtotal").html(toRp(grandtotal));
		$("#grandtotal").val(grandtotal);
		cekSubmit();
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlahPembayaran" + id).val());
		bayar[id].jumlah = jumlah;

		calculatetotalPembayaran();
		hitungKembalian();
		cekSubmit();
	}

	function hitungKembalian() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;

		if (totalbayar >= grandtotal) {
			var kembalian = totalbayar - grandtotal;
			$("#kembalianView").html(toRp(kembalian));
			$("#kembalian").val(kembalian);
		} else {
			$("#kembalianView").html(toRp(0));
			$("#kembalian").val(0);
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;
		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			$('[class="col-sm-12 form-group point"').show("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	$('#btnSimpan').click(function () {
		if (totalbayar >= grandtotal) {
			$("#confirm-deposit-modal").modal('show');
		} else {
			showAlert('', "Pembayaran kurang", 'error');
		}
	});

	$('#confirmSubmit').click(function () {
		$("#confirm-deposit-modal").modal('hide');
		$('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page . '/store_payment'); ?>",
			method: 'post',
			data: {
				idPenjualan: $('#idPenjualan').val(),
				diskonMember: $('#diskonMember').val(),
				pemotongan: $('#pemotongan').val(),
				grandtotal: $('#grandtotal').val(),
				total: $('#total').val(),
				kembalian: $('#kembalian').val(),
				payment: bayar,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pembayaran Berhasil disimpan', 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$('#noConfirmSubmit').click(function () {
		$("#confirm-deposit-modal").modal('hide');
		$('#pleaseWaitDialog').modal('show');
		$('#deposit').val('TIDAK');
		$.ajax({
			url: "<?php echo base_url($page . '/store_payment'); ?>",
			method: 'post',
			data: {
				idPenjualan: $('#idPenjualan').val(),
				diskonMember: $('#diskonMember').val(),
				pemotongan: $('#pemotongan').val(),
				grandtotal: $('#grandtotal').val(),
				total: $('#total').val(),
				deposit: 'TIDAK',
				kembalian: $('#kembalian').val(),
				payment: bayar,
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pembayaran Berhasil disimpan', 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				console.log(status);
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$('#btnSubmit').click(function () {
		$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
	});

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		var diskon = $('#diskonMember').val() * 1;
		var total = $('#total').val() * 1;
		if (pemotongan > (total - diskon)) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();
		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		addPayment();
	});

	function addPayment() {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();
		var keterangan = $("#keterangan").val();

		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
		} else {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}

			if (cek == 0) {
				obj.id = jenis;
				obj.jumlah = jumlahBayar;
				obj.keterangan = keterangan;
				bayar.push(obj);
				idx = bayar.length - 1;

				var newRow = "<tr id='barisPembayaran" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
					"<td width='35%'>" +
					"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
					"<td width='20%'  style='text-align:right'><span id='viewKeterangan" + idx + "'>" + keterangan + "</span><input type='hidden' name='keteranganPayment[]' id='keteranganPayment" + idx + "' value='" + keterangan + "'></td>" +
					"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlah[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
				$("#paymentDetail").append(newRow);
			} else {
				var jumlahrow = document.getElementById("paymentDetail").rows.length;
				for (var i = 0; i < jumlahrow; i++) {
					var cekID = $('#idPembayaran' + i).val();
					if (cekID == jenis) {
						jumlah = (jumlah * 1) + (jumlahBayar * 1);
						$('#jumlahPembayaran' + i).val(jumlah);
						$('#viewJumlah' + i).html(toRp(jumlah));
						var idx = i;
					}
				}
			}
			hitungtotal(idx);
			$('#jumlahPembayaran').val("");

		}
	}

	function removeat(idx) {
		var jumlah = $('#jumlahPembayaran' + idx).val();
		totalbayar = totalbayar - jumlah;
		if (totalbayar < 0) {
			totalbayar = 0;
		}
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(totalbayar)); //Total Bayar
		$("#grandTotalBayar").val(toRp(totalbayar));
		$("#barisPembayaran" + idx).remove();
		hitungKembalian();
		bayar.splice(idx, 1);
	}

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});
</script>
