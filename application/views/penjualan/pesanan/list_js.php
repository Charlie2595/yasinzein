<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-receive' + id).modal('show');
	}

	function save(id) {
		$.ajax({
			url: "<?php echo base_url($page . '/receive'); ?>",
			method: 'post',
			data: {
				idPenjualan: $('#idPenjualan').val(),
				jamTerima: $('#jamTerima').val(),
				tglTerima: $('#tglTerima').val(),
				keterangan: $('#keterangan').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pesanan berhasil diterima', 'success', function () {
						$('#pesananModalReceive').modal('hide');
						$('#listTable').bootstrapTable('refresh', {
							silent: true
						});
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	};

	function deleted(id) {
		$.ajax({
			url: "<?php echo base_url($page . '/hapus'); ?>",
			method: 'post',
			data: {
				idPenjualanHapus: $('#idPenjualanHapus').val(),
				alasan: $('#alasan').val(),
			},
			success: function (data) {
				console.log(data);
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', 'Pesanan berhasil dihapus', 'success', function () {
						$('#pesananModalHapus').modal('hide');
						$('#listTable').bootstrapTable('refresh', {
							silent: true
						});
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	};

	$('#pesananReceiveForm').validate({
		errorElement: 'div',
		rules: {
			keterangan: {
				required: true,
			},
			tglTerima: {
				required: true,
			},
			jamTerima: {
				required: true,
			}
		},

		messages: {
			keterangan: {
				required: "Keterangan harus diisi",
			},
			tglTerima: {
				required: "Tanggal harus diisi",
			},
			jamTerima: {
				required: "Jam harus diisi",
			}
		}
	});

	$('#pesananModalHapus').on('hidden.bs.modal', function (event) {
		$('#idPenjualanHapus').val("");
		$('#alasan').val("");
	});

	$('#pesananHapusForm').validate({
		errorElement: 'div',
		rules: {
			alasan: {
				required: true,
			}
		},

		messages: {
			alasan: {
				required: "Alasan harus diisi",
			}
		}
	});

	$('#pesananModalReceive').on('hidden.bs.modal', function (event) {
		$('#idPenjualan').val("");
		$('#jamTerima').val("<?php echo date('H:i'); ?>");
		$('#tglTerima').val("<?php echo date('d-m-Y'); ?>");
		$('#keterangan').val("");
	});

	function terimaEvent(event, value, row, index) {
		$('#idPenjualan').val(row.id);
		$('#pesananReceiveForm').data('id', row.id);
		$('#pesananModalReceive').modal('show');
	}

	function hapusPesanan(event, value, row, index) {
		$('#idPenjualanHapus').val(row.id);
		$('#pesananHapusForm').data('id', row.id);
		$('#pesananModalHapus').modal('show');
	}

	//Timepicker
	$('.timepicker').timepicker({
		showInputs: true,
		showMeridian: false,
	});

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '0D'
	});

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortName: 'document',
		sortOrder: 'desc',
		url: "<?php echo base_url('getAllPesanan'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";
				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['status'] == 1) {
						btn += '<a class="btn btn-sm btn-default" disable ><i class="fa fa-trash fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a href="javascript:void(0)" data-toggle="tooltip" title="Hapus Pesanan" class="btn btn-sm btn-danger hapus"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';
					}
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '" data-toggle="tooltip" title="Detail Pesanan" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['status'] == 0) {
						btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '/edit" data-toggle="tooltip" title="Ubah pesanan" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a class="btn btn-sm btn-default" disable ><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
					}
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['dikirim'] == 1 && row['id_pegawai_kirim'] == null) {
						btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '/send" data-toggle="tooltip" title="Pengiriman pesanan" class="btn btn-sm btn-warning"><i class="fa fa-send fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-send"></i></a> &nbsp;';
					}

					if (row['dikirim'] == 0 && row['tgl_terima'] == null) {
						btn += '<a href="javascript:void(0)" class="btn btn-sm btn-danger waves-effect waves-light terima" data-toggle="tooltip" title="Pesanan Diambil"><i class="fa fa-sign-out fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a disable class="btn btn-default" data-placement="top"><i class="fa fa-sign-out"></i></a> &nbsp;'
					}

					if (row['dikirim'] == 1 && row['id_pegawai_kirim'] == null) {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-check"></i></a> &nbsp;';
					} else if (row['dikirim'] == 0 && row['tgl_terima'] == null) {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-check"></i></a> &nbsp;';
					} else if (row['lunas'] == 0) {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-check"></i></a> &nbsp;';
					} else if (row['status'] == 1) {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-check"></i></a> &nbsp;';
					} else {
						btn += '<a href="#" data-toggle="tooltip" title="Pesanan selesai" class="btn btn-sm btn-warning selesai"><i class="fa fa-check fa-lg"></i></a> &nbsp;';
					}

					btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '/print" data-toggle="tooltip" title="Print pesanan" class="btn btn-sm btn-primary"><i class="fa fa-print fa-lg"></i></a> &nbsp;';
				}

				return btn;
			},
			events: {
				'click .terima': terimaEvent,
				'click .selesai': function (e, value, row, index) {
					showConfirm('', 'Pesanan selesai?', 'warning', function () {
						$.ajax({
							url: "<?php echo base_url();?>pesanan/" + row['id'] + "/selesai",
							success: function (data) {
								var data = $.parseJSON(data);
								if (data['status'] == 'error') {
									showAlert('', data['description'], 'error');
								} else {
									showAlert('', data['description'], 'success', function (e) {
										$('#listTable').bootstrapTable('refresh');
									});
								}
							},
							error: function (xhr, text, status) {
								if (xhr.status == 422) {
									showAlert('', xhr.responseJSON.join(' '), 'error');
								}
							}
						});
					});
				},
				'click .hapus': hapusPesanan,
			}
		}, {
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal Buat',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return moment(value).format('DD-MM-YYYY HH:mm');
					// return value;
				}
			}
		}, {
			field: 'tgl_pesanan',
			title: 'Tanggal Pesan',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return moment(value + " " + row.jam_pesanan).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'total',
			title: 'Grand Total',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'totalQty',
			title: 'Total Item',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else if (value == 2) {
					return '<label class="badge bg-red">BELUM DIPROSES</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'dikirim',
			title: 'Dikirim',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1 && row['id_pegawai_kirim'] != null) {
					return '<label class="badge bg-green">SUDAH DIKIRIM</label>';
				} else if (value == 0 && row['tgl_terima'] == null) {
					return '<label class="badge bg-blue">AMBIL SENDIRI</label>';
				} else if (value == 0 && row['tgl_terima'] != null) {
					return '<label class="badge bg-red">SUDAH DIAMBIL</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM DIKIRIM</label>';
				}
			}
		}, {
			field: 'status',
			title: 'Status',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">SUDAH SELESAI</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM SELESAI</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});
</script>
