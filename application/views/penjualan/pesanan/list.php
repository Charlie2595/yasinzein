<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Master
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master <?php echo $page; ?> </li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> List <?php echo strtoupper($page); ?></h3>
						<a href="<?php echo base_url('pesanan/create') ?>" class="btn btn-success pull-right">
							<i class="fa fa-plus"></i> Input Pesanan</a>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">

						</table>
					</div>

				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="pesananModalReceive" tabindex="-1" role="dialog" aria-labelledby="receiveModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="receiveModalLabel">Pengambilan Pesanan</h4>
				</div>
				<div class="modal-body">
					<div class="form-group col-sm-12 ">
						<label class="col-sm-4 control-label">Tanggal Kirim :</label>
						<div class="input-group col-sm-8">
							<input type="text" class="form-control tgl pull-right" id="tglTerima" name="tglTerima" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y'); ?>">
							<span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
                            </span>
						</div>
					</div>
					<div class="form-group col-sm-12">
						<label class="col-sm-4 control-label">Jam Kirim :</label>
						<div class="input-group bootstrap-timepicker  col-sm-8">
							<input type="text" class="form-control pull-right timepicker" id="jamTerima" name="jamTerima" placeholder="Jam" readonly>
							<span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat"><i class="fa fa-clock-o"></i></button>
                            </span>
						</div>
					</div>
					<div class="col-sm-12 form-group">
						<label class="col-sm-4 control-label">Keterangan :</label>
						<div class="col-sm-8 input-group">
							<textarea class="form-control" id="keterangan" name="keterangan" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idPenjualan" id="idPenjualan">
					<button class="btn btn-info pull-right" onclick="save();"><i class="fa fa-save"></i> Simpan</button>
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Tidak</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="pesananModalHapus" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="hapusModalLabel">Hapus Pesanan</h4>
				</div>
				<div class="modal-body">
					<div class="col-sm-12 form-group">
						<label class="col-sm-4 control-label">Alasan :</label>
						<div class="col-sm-8 input-group">
							<textarea class="form-control" id="alasan" name="alasan" required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="idPenjualanHapus" id="idPenjualanHapus">
					<button class="btn btn-info pull-right" onclick="deleted();"><i class="fa fa-save"></i> Simpan
					</button>
					<button type="button" class="btn btn-danger waves-effect pull-left" data-dismiss="modal">Tidak</button>
				</div>
			</div>
		</div>
	</div>
</div>
