<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-receive' + id).modal('show');
	}

	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'document',
		url: "<?php echo base_url('getAllHistoryPesanan'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";
				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '" data-toggle="tooltip" title="Detail Pesanan" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '/print" data-toggle="tooltip" title="Print pesanan" class="btn btn-sm btn-primary"><i class="fa fa-print fa-lg"></i></a> &nbsp;';
				}

				return btn;
			},
		}, {
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal Buat',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return moment(value).format('DD-MM-YYYY HH:mm');
					// return value;
				}
			}
		}, {
			field: 'tgl_pesanan',
			title: 'Tanggal Pesan',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return moment(value + " " + row.jam_pesanan).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'total',
			title: 'Grand Total',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'totalQty',
			title: 'Total Item',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else if (value == 2) {
					return '<label class="badge bg-red">BELUM DIPROSES</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'dikirim',
			title: 'Dikirim',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1 && row['id_pegawai_kirim'] != null) {
					return '<label class="badge bg-green">SUDAH DIKIRIM</label>';
				} else if (value == 0 && row['tgl_terima'] == null) {
					return '<label class="badge bg-blue">AMBIL SENDIRI</label>';
				} else if (value == 0 && row['tgl_terima'] != null) {
					return '<label class="badge bg-red">SUDAH DIAMBIL</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM DIKIRIM</label>';
				}
			}
		}, {
			field: 'status',
			title: 'Status',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">SUDAH SELESAI</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM SELESAI</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});
</script>
