<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */ ?>
<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'document',
		url: "<?php echo base_url('getAllB2B'); ?>",
		columns: [{
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal Buat',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					// return value;
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'member',
			title: 'Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'lunas',
			title: 'Lunas',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else if (value == 2) {
					return '<label class="badge bg-red">BELUM DIPROSES</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'dikirim',
			title: 'Dikirim',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">SUDAH DIKIRIM</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM DIKIRIM</label>';
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";
				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner') {
					btn += '<a href="<?php echo base_url();?>b2b/' + row['id'] + '" data-toggle="tooltip" title="Detail B2B" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';
				}

				if ('<?php echo $_SESSION['jabatan'] ?>' === 'Owner' || '<?php echo $_SESSION['jabatan'] ?>' === 'Kasir') {
					if (row['id_nota'] == null && row['lunas'] == 0) {
						btn += '<a href="<?php echo base_url();?>b2b/' + row['id'] + '/edit" data-toggle="tooltip" title="Ubah B2B" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
					} else {
						btn += '<a class="btn btn-default" disable ><i class="fa fa-pencil"></i></a> &nbsp;';
					}
				}

				// if('<?php echo $_SESSION['jabatan'] ?>' === 'Owner'){
				// 	if(row['dikirim'] == 0){
				// 		btn += '<a href="<?php echo base_url();?>b2b/'+ row['id']+'/send" data-toggle="tooltip" title="Ubah" class="btn btn-sm btn-warning"><i class="fa fa-send fa-lg"></i></a> &nbsp;';
				// 	} else {
				//  	btn += '<a class="btn btn-default" disable ><i class="fa fa-send"></i></a> &nbsp;';
				// 	}
				// }

				return btn;
			}
		}]
	});
</script>
