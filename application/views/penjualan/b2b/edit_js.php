<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var barang = []; //array barang
	var kode = "";
	var nama = "";
	var harga = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran

	$(document).ready(function () {
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		//cekSubmit();
		var detailArray = <?php echo json_encode($data['detail']); ?>;
		$.each(detailArray, function (index, item) {
			var countDetail = $('#detailTable').bootstrapTable('getData').length;
			var countAdd = 0;
			if (countAdd == 0) {
				$.ajax({
					url: "<?php echo base_url();?>cekKodeBarang/" + item.barcode_kue,
					method: 'GET',
					success: function (response) {
						resp = $.parseJSON(response);
						var countDetail = $('#detailTable').bootstrapTable('getData').length;
						var data = {
							kode: resp.barcode_kue,
							harga: resp.harga,
							nama: resp.nama_kue,
							id: resp.id_kue,
							qty: item.jumlah,
							subtotal: (resp.harga * 1) * (item.jumlah * 1),
							line: countDetail + 1
						};

						var existingData = $('#detailTable').bootstrapTable('getData');
						var duplicate = false;

						$.each(existingData, function (index, item) {
							if (item.kode == data.kode) {
								duplicate = true;
								item.qty++;
							}
						});

						if (!duplicate) {
							$('#detailTable').bootstrapTable('append', data);
						} else {
							$('#detailTable').bootstrapTable('load', existingData);
						}
						calculatetotal();
						$('#txtKodeBarang').val('');
						$('#searchKodeBarang').val('');
						var elem = document.getElementById('detailTable');
						$('#detailTable').bootstrapTable('scrollTo', elem.scrollHeight);
					}, error: function (response, status) {
						showAlert('', 'Invalid article code.', 'warning');
					}
				})
			}
		});

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});

		// var count = document.getElementById("paymentDetail").rows.length;
		// for (var i = 0; i < count; i++) {
		// 	var id = $('#idPembayaran' + i).val();
		// 	var jumlah = $('#jumlahPembayaran' + i).val() * 1;

		// 	var obj = new Object();
		// 	obj.id = id;
		// 	obj.jumlah = jumlah;
		// 	totalbayar += jumlah;
		// 	bayar.push(obj);
		// }
		// $("#grandTotalBayar").val(totalbayar);

		// var count=document.getElementById("bodyNota").rows.length;
		// for(var i = 0 ; i<count; i++) {
		// 	var id = $('#idBarang'+i).val();
		// 	var harga = $('#harga'+i).val();
		// 	var diskonBarang = $('#diskonBarang'+i).val();

		// 	var obj=new Object();
		// 	obj.id=id;
		// 	obj.harga=harga;
		// 	obj.diskonBarang=diskonBarang;
		// 	barang.push(obj);
		// 	idx=barang.length-1;
		// 	hitungtotal(idx);
		// }
		calculatetotal();
		// hitungKembalian();
		cekSubmit();
	});

	function chooseBarang(kode) {
		$('#searchKodeBarang').val(kode);
		$('#addLineBtn').trigger('click');
		$('#modal-barang').modal('hide');
	}

	$('#addLineBtn').on('click', function (event) {
		var txtKode = $('#searchKodeBarang').val().trim();
		var jumlah = txtKode.split('*');
		if (jumlah.length > 1) {
			var arr = jumlah[1].split('_');
		} else {
			var arr = txtKode.split('_');
			jumlah[0] = 1;
		}
		var kode = arr[0];
		var countDetail = $('#detailTable').bootstrapTable('getData').length;
		var countAdd = 0;
		if (countAdd == 0) {
			$.ajax({
				url: "<?php echo base_url();?>cekKodeBarang/" + kode,
				method: 'GET',
				success: function (response) {
					resp = $.parseJSON(response);
					var countDetail = $('#detailTable').bootstrapTable('getData').length;
					var data = {
						kode: resp.barcode_kue,
						harga: resp.harga,
						nama: resp.nama_kue,
						id: resp.id_kue,
						qty: (jumlah[0] * 1),
						subtotal: (resp.harga * 1) * (jumlah[0] * 1),
						line: countDetail + 1
					};

					var existingData = $('#detailTable').bootstrapTable('getData');
					var duplicate = false;

					$.each(existingData, function (index, item) {
						if (item.kode == data.kode) {
							duplicate = true;
							item.qty = (item.qty * 1) + (jumlah[0] * 1);
							item.subtotal = (item.harga * 1) * (item.qty * 1);
						}
					});

					if (!duplicate) {
						$('#detailTable').bootstrapTable('append', data);
					} else {
						$('#detailTable').bootstrapTable('load', existingData);
					}
					calculatetotal();
					$('#txtKodeBarang').val('');
					$('#searchKodeBarang').val('');
					var elem = document.getElementById('detailTable');
					$('#detailTable').bootstrapTable('scrollTo', elem.scrollHeight);
				}, error: function (response, status) {
					showAlert('', 'Invalid article code.', 'warning');
				}
			})
		}
	});

	$('#detailTable').bootstrapTable({
		classes: 'table table-striped table-condensed table-no-bordered text-nowrap',
		pagination: false,
		sidePagination: 'client',
		pageSize: 50,
		pageList: [5, 10, 25, 50, 100, 'All'],
		search: false,
		smartdisplay: false,
		showRefresh: false,
		showToggle: false,
		showColumns: false,
		height: '430',
		columns: [{
			title: '<i class="fa fa-trash fa-lg"></i>',
			valign: 'middle',
			width: '5%',
			formatter: function (value, row, index) {
				return '<button type="button" class="btn btn-danger btn-xs c-remove-row"><i class="fa fa-trash fa-lg"></i></button>';
			},
			events: {
				'click .c-remove-row': function (event, value, row, index) {
					$('#detailTable').bootstrapTable('remove', {field: 'id', values: [row.id]});
					$("#pemotongan").val(0);
					$("#viewPemotongan").html(0);
					calculatetotal();
				}
			}
		}, {
			field: 'nama',
			title: 'Nama Barang',
			valign: 'middle',
			class: 'description',
			width: '30%',
			formatter: function (value, row, index) {
				return row.kode + " - " + value;
			}
		}, {
			field: 'harga',
			title: 'Harga',
			valign: 'middle',
			width: '10%',
			align: 'right',
			halign: 'left',
			formatter: function (value, row, index) {
				return decimalFormatter(value);
			}
		}, {
			field: 'qty',
			title: 'Jumlah',
			searchable: false,
			valign: 'middle',
			align: 'right',
			halign: 'left',
			width: '10%',
			formatter: function (value, row, index) {
				return '<input type="number" class="form-control qty" id="detail' + index + '" value="' + value + '" min="1"/>';
			},
			events: {
				'change .qty': function (event, value, row, index) {
					var qty = $(this).val();

					if (qty > 0) {
						row.qty = qty;
						row.subtotal = (row.harga * 1) * (row.qty * 1);
						$('label[id="subtotal' + index + '"]').html(decimalFormatter(row.subtotal));
					} else {
						row.qty = 1;
						$('input[id="detail' + index + '"]').val(row.qty);
					}
					calculatetotal();
				}
			}
		}, {
			field: 'subtotal',
			title: 'Subtotal',
			valign: 'middle',
			halign: 'left',
			align: 'right',
			width: '10%',
			formatter: function (value, row, index) {
				return '<label id="subtotal' + index + '">' + decimalFormatter(value) + '</label>';
			}
		}]
	});

	function konvertnilai(elm) {
		var nilai = ($(elm).autoNumeric('get') * 1);
		$(elm).autoNumeric('destroy');
		$(elm).val(nilai);
	};

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;
		var diskon = 0;
		var totalItem = 0;

		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			totalItem += item.qty * 1;
			var jumlah = item.qty * 1;
			var harga = item.harga * 1;
			var diskonBarang = 0;
			grandtotal += ((jumlah * harga) - diskonBarang);
		});

		$("#viewTotalBayar").html(toRp(grandtotal)); //Total Bayar
		$("#viewTotalItem").html("(" + totalItem + " Item)");
		$("#total").val(grandtotal); //Total Bayar
		$("#totalPenjualan").val(grandtotal); //Total Penjualan

		diskon = grandtotal * (($('#diskonMember').val() * 1) / 100); //Hitung Diskon
		$('#viewDiskon').html(toRp(diskon)); //Menampilkan Diskon
		$('#totaldiskonMember').val(diskon); //Menyimpan total diskon

		var pemotongan = $('#pemotongan').val(); //Pemotongan harga
		grandtotal = grandtotal - diskon - pemotongan; //Hitung Grandtotal

		$("#viewGrandtotal").html(toRp(grandtotal));
		$("#grandtotal").val(grandtotal);
		$("#grandtotalPembelian").val(toRp(grandtotal));
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlah" + id).val());
		var harga = barang[id].harga * 1;
		var diskonBarang = barang[id].diskonBarang * 1;
		barang[id].jumlah = jumlah;

		var subtotal = (jumlah * harga - diskonBarang);
		$("#subtotal" + id).html(toRp(subtotal));
		calculatetotal();
	}

	function diskonBarang() {
		var diskonBarang = $("#diskonBarang").val();
		for (var i = 0; i < barang.length; i++) {
			var cekDiskon = barang[i].cekDiskon;
			if (cekDiskon != 0) {
				barang[i].diskon = 0;
			} else {
				barang[i].diskon = diskonBarang;
			}
			$("#diskonBarang" + i).val(diskonBarang);
			$("#viewDiskonBarang" + i).html(toRp(diskonBarang));
			hitungtotal(i);
		}
	}

	function cekSubmit() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		cekUsePoin = $('#userPoin' + id).val();
		var poin = $('#poin' + id).val();
		var saldo = $('#saldo' + id).val();
		var diskonBarang = $('#diskonBarang' + id).val();
		$('#diskonMember').val(0);
		$('#member').val(nama);
		$('#saldo').val(saldo);
		$('#point').val(poin);
		$('#poinMember').html(toRp(poin));
		$('#saldoMember').html(toRp(saldo));
		$('#diskonBarang').val(diskonBarang);
		$('#modal-member').modal('hide');
		$('[class="row statusMember col-md-12"').show("slow");
		updateDiskonBarang();
		calculatetotal();
	}

	function updateDiskonBarang() {
		var diskonBarang = $("#diskonBarang").val();
		for (var i = 0; i < barang.length; i++) {
			var cekDiskon = barang[i].cekDiskon;
			if (cekDiskon != 0) {
				barang[i].diskon = 0;
			} else {
				barang[i].diskon = diskonBarang;
			}
			$("#diskonBarang" + i).val(diskonBarang);
			$("#viewDiskonBarang" + i).html(toRp(diskonBarang));
			hitungtotal(i);
		}
	}

	//Function untuk mencari history transaksi member
	function searchHistory() {
		var idMember = $('#idMember').val();
		$.ajax({
			url: "<?php echo base_url();?>searchHistory/" + idMember,
			success: function (data) {
				$("#bodyNota").html("");
				barang = [];
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var obj = new Object();
					var subtotal = mydata[i].jumlah * mydata[i].harga;
					obj.id = mydata[i].id_kue;
					obj.kode = mydata[i].barcode_kue;
					obj.harga = mydata[i].harga;
					obj.nama = mydata[i].nama_kue;
					barang.push(obj);
					idx = barang.length - 1;
					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + mydata[i].id_kue + "'> " +
						" " + mydata[i].barcode_kue + " - " + mydata[i].nama_kue + " </td>" +
						"<td  width='20%'  style='text-align:right'><span id='harga'>" + toRp(mydata[i].harga * 1) + "</span><input type='hidden' name='harga[]' value='" + mydata[i].harga + "'></td>" +
						"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + mydata[i].jumlah + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
						"<td width='20%' style='text-align:right'><span id='subtotal" + idx + "'>" + toRp(subtotal) + " </span></td></tr>";
					$("#bodyNota").append(newRow);
					hitungtotal(idx);
				}
			}
		});
	}

	function removeat(idx) {
		barang.splice(idx, 1);
		$("#baris" + idx).remove();
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			barang = [];
		}
		$("#pemotongan").val(0);
		$("#viewPemotongan").html(0);
		calculatetotal();
	}

	$('#btnSubmit').click(function () {
		var jumlahrow = $('#detailTable').bootstrapTable('getData').length;
		var pemotongan = $('#pemotongan').val();
		var total = $('#total').val();
		var barang = [];
		console.log(total);
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			barang.push({
				jumlah: item.qty,
				id: item.id,
				harga: item.harga
			});
		});
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else if (pemotongan > total) {
			showAlert('', "Jumlah pemotongan lebih besar dari total penjualan", 'error');
		} else {
			if ($("#createForm").valid()) {
				$.ajax({
					url: "<?php echo base_url($page . '/edit'); ?>",
					method: 'post',
					data: {
						idPenjualan: $('#idPenjualan').val(),
						tgl: $('#tgl').val(),
						diskonMember: $('#diskonMember').val(),
						idPegawai: $('#idPegawai').val(),
						pemotongan: $('#pemotongan').val(),
						total: $('#total').val(),
						grandtotal: $('#grandtotal').val(),
						type: 'simpan',
						payment: bayar,
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', 'Pesanan Berhasil diubah', 'success', function () {
								window.location = "<?php echo base_url($page)?>";
							});
						}
					}, error: function (xhr, text, status) {
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			}
		}
	});

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/0'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'email',
			title: 'Email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	$('#barangTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getBarang'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseBarang(`' + row['barcode_kue'] + '`)"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="id_kue' + row['id_kue'] + '" value="' + row['id_kue'] + '">'
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode Kue',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="barcode_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="nama_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="harga' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '0D'
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});

	$("#searchKodeBarang").autocomplete({
		source: "<?php echo base_url('searchBarang'); ?>",
		select: function (event, ui) {
			var select = ui.item.label;
			var arr = select.split('_');
			$('#txtKodeBarang').val(arr[0]);
		}
	});

	$('#searchKodeBarang').keypress(function (event) {
		if (event.which == 13) {
			event.preventDefault();
			// cekKodeBarang();
			$('#addLineBtn').trigger('click');
		}
	});

	shortcut.add("f1", function () {
		$("#searchKodeBarang").focus();
	});

	shortcut.add("f2", function () {
		$('#modal-barang').modal({show: 'true'});
	});

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	shortcut.add("f8", function () {
		$('#btnSubmit').trigger('click');
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});
	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});
	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});


	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		var diskon = $('#diskonMember').val();
		var total = $('#total').val();
		if (pemotongan > (total - diskon)) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();
		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}
</script>
