<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<!--  <style>
 .ui-autocomplete {
   max-height: 300px;
   overflow-y: auto;
   /* prevent horizontal scrollbar */
   overflow-x: hidden;
 }
 /* IE 6 doesn't support max-height
  * we use height instead, but this forces the menu to always be this tall
  */
 * html .ui-autocomplete {
   height: 400px;
 }
.fixed_header{
   width: 100%;
   border-collapse: collapse;
}

.fixed_header tbody{
 display:block;
 width: 100%;
 overflow: auto;
 height: 350px;
}

.fixed_header thead tr {
  display: block;
}

.fixed_header th, .fixed_header td {
 padding: 5px;
 text-align: left;
 width: 200px;
}
 </style> -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Ubah Pesanan
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">Pembayaran <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<!--/.col (left) -->
				<!-- right column -->
				<!-- Horizontal Form -->
				<div class="box box-info">
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/edit'); ?>">
						<!-- Main -->
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Tanggal :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control pull-right tgl" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($data['tgl'])); ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Member :</h4></label>
											<div class="input-group">
												<input type="hidden" name="idMember" id="idMember" value="<?php echo $data['id_member'] ?>">
												<input type="hidden" name="diskonBarang" id="diskonBarang" value="<?php echo $data['diskon_barang'] ?>">
												<input type="text" class="form-control" id="member" placeholder="Member" name="member" readonly value="<?php echo $data['member']; ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-member"><i class="fa fa-search"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label><h4>Pegawai :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" value="<?php echo $_SESSION['nama'] ?>" readonly>
												<input type="hidden" name="idPegawai" id="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
									</span>
											</div>
										</div>
									</div>
									<ul class="nav nav-tabs">
									</ul>
									<section>
										<div class="col-sm-4">
											<div class="input-group">
												<input type="text" class="form-control autocomplete" id="searchKodeBarang" placeholder="Scan atau Kode Barang" name="searchKodeBarang" onchange="cekKodeBarang();">
												<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-flat" id="addLineBtn"><i class="fa fa-plus"></i></button>
									</span>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-barang">
													<i class="fa fa-search"> Cari </i></button>
											</div>
										</div>
									</section>
									<div class="box-body">
										<table id="detailTable">
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2>
													<label id="viewTotalItem">(0 Item)</label></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right"><?php echo number_format($data['total'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="total" id="total" value="<?php echo $data['total']; ?>">
												<input type="hidden" name="diskonMember" id="diskonMember" value="<?php echo $data['diskon_member']; ?>">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<a data-toggle="modal" data-target="#modal-verifikasi">
														<h3 class="list-group-item-heading">DISKON</h3></a></div>
												<input type="hidden" name="pemotongan" id="pemotongan" value="<?php echo $data['diskon_pembulatan']; ?>">
												<div class="col-sm-6"><h3 class="list-group-item-heading pull-right">
														<span id="viewPemotongan" class="pull-right"><?php echo number_format($data['diskon_pembulatan'], 0, ",", "."); ?></span></h3></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h2 class="list-group-item-heading">GRAND TOTAL</h2></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right"><?php echo number_format($data['total'] - ($data['total'] * $data['diskon_member'] / 100) - $data['diskon_pembulatan'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="grandtotal" value="" id="grandtotal">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> SIMPAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>
								</section>
							</div>
						</div>
						<input type="hidden" name="type" id="type" value="B2B">
						<input type="hidden" name="idPenjualan" id="idPenjualan" value="<?php echo $data['id_jual']; ?>">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
					</form>
				</div>
		</section>
	</div>
</div>
<div class="modal fade" id="modal-pemotongan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pemotongan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-2 control-label">Jumlah :</label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-uang" id="jumlahPemotongan" placeholder="Jumlah Pemotongan">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnPemotongan">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade modal-lg" id="modal-member" style="width: 100%">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Member</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="memberTable">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-barang">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Barang</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered searchTable" id="barangTable" style="width:100%">
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal Pemotongan -->
<div class="modal fade" id="modal-verifikasi">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Username :</label>
					<div class="col-sm-9">
						<input type="text" class="form-control pull-right" id="username" name="username" placeholder="Username">
					</div>
				</div>
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Password :</label>
					<div class="col-sm-9">
						<input type="password" class="form-control pull-right" id="password" name="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnVerifikasi">Simpan</button>
			</div>
		</div>
	</div>
</div>
