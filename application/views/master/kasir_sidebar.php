<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $pageTitle; ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.css') ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>">
	<!-- Theme style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/AdminLTE.css') ?>">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/skins/_all-skins.min.css') ?>">
	<!-- Morris chart -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/morris.js/morris.css') ?>">
	<!-- DataTables -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
	<!-- iCheck for checkboxes and radio inputs -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/iCheck/all.css') ?>">
	<!-- Date Picker -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>">
	<!-- Daterange picker -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-daterangepicker/daterangepicker.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap-table/bootstrap-table.min.css') ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/timepicker/bootstrap-timepicker.min.css') ?>">
	<link href="<?php echo base_url('assets/css/DateTimePicker.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/select2//dist/css/select2.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/animate.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/bootstrapValidator.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/jquery.autocomplete.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/custom_style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/jquery-ui-1.12.1/jquery-ui.structure.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/jquery-ui-1.12.1/jquery-ui.theme.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/pace-1.0.0/pace.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/sweetalert2-1.0.0/dist/sweetalert2.css') ?>" rel="stylesheet">
</head>
<body class="hold-transition skin-green sidebar-mini <?php if (isset($collapse)) {
	echo $collapse;
} ?>">
<div id="wrapper">
	<header class="main-header">
		<!-- Logo -->
		<a href="<?php echo base_url('home') ?>" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>T</b>K</span>
			<!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Toko</b>KUE</span> </a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
				<span class="sr-only">Toggle navigation</span> </a>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<!-- User Account: style can be found in dropdown.less -->
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="hidden-xs"><?php echo $_SESSION['nama']; ?></span> </a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								<p>
									<?php echo $_SESSION['nama']; ?> - <?php echo $_SESSION['jabatan']; ?>
								</p>
							</li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-right">
									<a href="<?php echo base_url('logout') ?>" class="btn btn-default btn-flat">Sign out</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar">
			<!-- Sidebar user panel -->
			<!-- sidebar menu: : style can be found in sidebar.less -->
			<ul class="sidebar-menu" data-widget="tree">
				<li class="treeview <?php echo $masterActive ?? ''; ?> waves-effect">
					<a href="#"> <i class="fa fa-laptop text-aqua"></i> <span>Master</span>
						<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span> </a>
					<ul class="treeview-menu">
						<li class="<?php echo $supplierActive ?? ''; ?>">
							<a href="<?php echo base_url('supplier'); ?>">Supplier</a></li>
						<li class="<?php echo $memberActive ?? ''; ?>">
							<a href="<?php echo base_url('member'); ?>">Member</a></li>
						<li class="<?php echo $barangActive ?? ''; ?>">
							<a href="<?php echo base_url('barang'); ?>">Barang</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $penjualanActive ?? ''; ?> waves-effect">
				<li class="<?php echo $penjualanActive ?? ''; ?>">
					<a href="<?php echo base_url('pos'); ?>"><i class="fa fa-dollar text-red"></i><span>POS</span></a>
				</li>
				</li>
				<li class="treeview <?php echo $pesananActive ?? ''; ?> waves-effect">
				<li class="<?php echo $pesananActive ?? ''; ?>">
					<a href="<?php echo base_url('pesanan'); ?>"><i class="fa fa-cart-plus text-red"></i><span>Pesanan Kue</span></a>
				</li>
				</li>
				<li class="treeview <?php echo $barangMasukActive ?? ''; ?> waves-effect">
				<li class="<?php echo $barangMasukActive ?? ''; ?>">
					<a href="<?php echo base_url('masuk'); ?>"><i class="fa fa-briefcase text-red"></i><span>Input Stok</span></a>
				</li>
				<li class="<?php echo $returCabangActive ?? ''; ?>">
					<a href="<?php echo base_url('retur_barang'); ?>"><i class="fa fa-briefcase text-red"></i><span>Retur Barang Masuk</span></a>
				</li>
				</li>
				<li class="treeview <?php echo $inputKueActive ?? ''; ?> waves-effect">
					<a href="#"> <i class="fa fa-laptop text-aqua"></i> <span>Input Kue</span>
						<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span> </a>
					<ul class="treeview-menu">
						<li class="<?php echo $b2bActive ?? ''; ?>"><a href="<?php echo base_url('b2b'); ?>">B2B</a>
						</li>
						<li class="<?php echo $cabangActive ?? ''; ?>">
							<a href="<?php echo base_url('cabang'); ?>">Cabang</a></li>
						<li class="<?php echo $returActive ?? ''; ?>">
							<a href="<?php echo base_url('retur'); ?>">Retur Cabang</a></li>
					</ul>
				</li>
				<li class="treeview <?php echo $laporanActive ?? ''; ?> waves-effect">
					<a href="#"> <i class="fa fa fa-line-chart text-aqua"></i> <span>Laporan</span>
						<span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span> </a>
					<ul class="treeview-menu">
						<li class="<?php echo $reportPembayaranDPActive ?? ''; ?>">
							<a href="<?php echo base_url('laporan/pembayaranDP'); ?>">Pembayaran DP</a></li>
						<li class="<?php echo $reportBarangActive ?? ''; ?>">
							<a href="<?php echo base_url('laporan/barang'); ?>">Barang</a></li>
						<li class="<?php echo $reportDepartemenActive ?? ''; ?>">
							<a href="<?php echo base_url('laporan/departemen'); ?>">Departemen</a></li>
						<li class="<?php echo $reportSupplierActive ?? ''; ?>">
							<a href="<?php echo base_url('laporan/supplier'); ?>">Supplier</a></li>
						<li class="<?php echo $reportStokBarangActive ?? ''; ?>">
							<a href="<?php echo base_url('laporan/stokBarang'); ?>">Stok Barang</a></li>
						<!-- <li class="<?php echo $reportPesananActive ?? ''; ?>"><a href="<?php echo base_url('laporan/pesanan'); ?>">Pesanan</a></li> -->
					</ul>
				</li>
			</ul>
		</section>
		<!-- /.sidebar -->
	</aside>
	<div id="page-wrapper">
		<?php
		echo $content;
		?>
		<div class="modal" id="pleaseWaitDialog" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Processing...</h4>
					</div>
					<div class="modal-body">
						<div class="progress">
							<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="main-footer">
			<strong>Copyright &copy; 2018 </strong>
		</footer>
	</div>
</div>

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/jquery/dist/jquery.min.js') ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/jquery-ui-1.12.1/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- <script src="<?php echo base_url('assets/js/jquery-1.12.4.min.js') ?>"></script> -->
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js') ?>"></script>

<script src="<?php echo base_url('assets/pace-1.0.0/pace.js') ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/chart.js/Chart.min.js') ?>"></script>

<script src="<?php echo base_url('assets/select2/dist/js/select2.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<!-- DataTables -->
<!-- <script src="<?php echo base_url('assets/datatables.net/js/jquery.dataTables.min.js') ?>"></script> -->
<script src="<?php echo base_url('assets/newdatatables/datatables.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<!-- datepicker -->
<script src="<?php echo base_url('assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap-table/bootstrap-table.min.js') ?>"></script>

<script src="<?php echo base_url('assets/timepicker/bootstrap-timepicker.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/js/adminlte.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/numeral.min.js') ?>"></script>

<script src="<?php echo base_url('assets/sweetalert2-1.0.0/dist/sweetalert2.min.js') ?>"></script>

<script src="<?php echo base_url('assets/js/shortcut.js') ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js') ?>"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url('assets/iCheck/icheck.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/moment.js') ?>"></script>

<script src="<?php echo base_url('assets/autoNumeric/autoNumeric.js') ?>"></script>
<script src="<?php echo base_url('assets/js/year-select.js') ?>"></script>
<?php foreach ($js_source as $key => $value) : ?>
	<script src="<?php echo $value; ?>"></script>
<?php endforeach; ?>
<?php echo $js ?? ''; ?>
</body>
</html>
