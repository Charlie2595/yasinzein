<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title><?php echo $pageTitle ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css') ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css') ?>">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url('assets/Ionicons/css/ionicons.min.css') ?>">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets//css/AdminLTE.min.css') ?>">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url('assets/iCheck/square/blue.css') ?>">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>	<![endif]-->
</head>
<body class="hold-transition login-page">
<?php
echo $content;
?>

<script src="<?php echo base_url('assets/js/jquery-1.12.4.min.js') ?>"></script>
<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/iCheck/icheck.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.validate.js') ?>"></script>
<script src="<?php echo base_url('assets/jquery-slimscroll/jquery.slimscroll.js') ?>"></script>
<?php foreach ($js_source as $key => $value) : ?>
	<script src="<?php echo $value; ?>"></script>
<?php endforeach; ?>
<?php echo $js ?? ''; ?>
</body>
</html>
