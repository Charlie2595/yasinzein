<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.32
 */
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Retur
			<small>Barang Masuk</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master Retur Barang Masuk</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> List Retur Barang Masuk</h3>
						<a href="<?php echo base_url($page . '/create') ?>" class="btn btn-success pull-right">
							<i class="fa fa-plus"></i> Buat Retur</a>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">
						</table>
					</div>

				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
