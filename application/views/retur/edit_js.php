<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var barang = []; //array barang
	var kode = "";
	var nama = "";
	var harga = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var total = 0;
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran

	$(document).ready(function () {
		total = 0;
		$('.select2').select2();

		var detailArray = <?php echo json_encode($data['detail']); ?>;
		$.each(detailArray, function (index, item) {
			var countDetail = $('#detailTable').bootstrapTable('getData').length;
			var countAdd = 0;
			barang = [];
			if (countAdd == 0) {
				$.ajax({
					url: "<?php echo base_url();?>cekKodeBarang/" + item.barcode_kue,
					method: 'GET',
					success: function (response) {
						resp = $.parseJSON(response);
						var countDetail = $('#detailTable').bootstrapTable('getData').length;
						var data = {
							id_masukdetail: item.id_masuk_detail,
							kode: resp.barcode_kue,
							harga: item.harga_beli,
							nama: resp.nama_kue,
							id: resp.id_kue,
							qty: item.jumlah,
							retur: item.retur,
							subtotal: (item.harga_beli * 1) * (item.jumlah * 1),
							line: countDetail + 1
						};
						total += (parseInt(item.harga_beli) * parseInt(item.jumlah));
						var existingData = $('#detailTable').bootstrapTable('getData');
						var duplicate = false;

						$.each(existingData, function (index, item) {
							if (item.kode == data.kode) {
								duplicate = true;
								item.qty++;
							}
						});
						if (!duplicate) {
							barang.push(data);
							$('#detailTable').bootstrapTable('append', data);
						} else {
							barang.push(existingData);
							$('#detailTable').bootstrapTable('load', existingData);
						}
						$("#total").val(total);
						$("#viewTotalBayar").html(toRp(total));
						$('#txtKodeBarang').val('');
						$('#searchKodeBarang').val('');
						hitungRetur();
						var elem = document.getElementById('detailTable');
						$('#detailTable').bootstrapTable('scrollTo', elem.scrollHeight);
					}, error: function (response, status) {
						showAlert('', 'Invalid article code.', 'warning');
					}
				})
			}
		});
		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});

		cekSubmit();
	});

	function chooseBarang(kode) {
		$('#searchKodeBarang').val(kode);
		$('#addLineBtn').trigger('click');
		$('#modal-barang').modal('hide');
	}

	$('#detailTable').bootstrapTable({
		classes: 'table table-striped table-condensed table-no-bordered text-nowrap',
		pagination: false,
		sidePagination: 'client',
		pageSize: 50,
		pageList: [5, 10, 25, 50, 100, 'All'],
		search: false,
		smartdisplay: false,
		showRefresh: false,
		showToggle: false,
		showColumns: false,
		height: '430',
		columns: [{
			field: 'nama',
			title: 'Nama Barang',
			valign: 'middle',
			class: 'description',
			width: '30%',
			formatter: function (value, row, index) {
				return row.kode + " - " + value;
			}
		}, {
			field: 'harga',
			title: 'Harga',
			valign: 'middle',
			width: '10%',
			align: 'right',
			halign: 'left',
			formatter: function (value, row, index) {
				return decimalFormatter(value);
			}
		}, {
			field: 'qty',
			title: 'Jumlah',
			valign: 'middle',
			width: '10%',
			align: 'right',
			halign: 'left',
			formatter: function (value, row, index) {
				return decimalFormatter(value);
			}
		}, {
			field: 'retur',
			title: 'Retur',
			searchable: false,
			valign: 'middle',
			align: 'right',
			halign: 'left',
			width: '10%',
			formatter: function (value, row, index) {
				return '<input type="number" name="jumlahRetur[]" class="form-control qtyRetur" id="jumlahRetur' + index + '" value="' + value + '" min="0" max="' + (row.qty * 1) + '" data-msg-number="Jumlah harus diisi angka/numerik." data-msg-max="Jumlah retur tidak boleh melebihi jumlah beli.">';
			},
			events: {
				'change .qtyRetur': function (event, value, row, index) {
					hitungRetur();
				}
			}
		}], onPostBody: function (data) {
			data.forEach(function (element, index) {
				hitungRetur();
			});
		},
	});

	function hitungRetur() {
		grandtotal = 0;
		var diskon = 0;
		var totalItem = 0;
		var totalItemRetur = 0;
		var total = parseInt($('#total').val());
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			console.log(item);
			totalItemRetur += parseInt($("#jumlahRetur" + index).val());
			totalItem += parseInt(item.qty);
			var retur = parseInt($("#jumlahRetur" + index).val());
			var harga = parseInt(item.harga);
			grandtotal += (retur * harga);
			barang[index].retur = retur;
		});
		$('#grandtotal').val(total - grandtotal);
		$('#totalItem').val(parseInt(totalItem) - parseInt(totalItemRetur));
		$('#viewTotalItem').html("(" + totalItem + " Item)");
		$('#viewTotalItemRetur').html("(" + totalItemRetur + " Item)");
		$('#viewRetur').html(toRp(grandtotal));
		$('#viewGrandtotal').html(toRp(total - grandtotal));
	}

	function konvertnilai(elm) {
		var nilai = ($(elm).autoNumeric('get') * 1);
		$(elm).autoNumeric('destroy');
		$(elm).val(nilai);
	};

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function cekSubmit() {
		totalbayar = $('#grandTotalBayar').val() * 1;
		grandtotal = $('#grandtotal').val() * 1;
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseMember(id) {
		$('#idMember').val(id);
		var nama = $('#namaMemberModal' + id).val();
		var diskon = $('#diskonMember' + id).val();
		cekUsePoin = $('#userPoin' + id).val();
		var poin = $('#poin' + id).val();
		var saldo = $('#saldo' + id).val();
		var diskonBarang = $('#diskonBarang' + id).val();
		$('#diskonMember').val(0);
		$('#member').val(nama);
		$('#saldo').val(saldo);
		$('#point').val(poin);
		$('#poinMember').html(toRp(poin));
		$('#saldoMember').html(toRp(saldo));
		$('#diskonBarang').val(diskonBarang);
		$('#modal-member').modal('hide');
		$('[class="row statusMember col-md-12"').show("slow");
	}

	//Function untuk mencari history transaksi member
	function searchHistory() {
		var idMember = $('#idMember').val();
		$.ajax({
			url: "<?php echo base_url();?>searchHistory/" + idMember,
			success: function (data) {
				$("#bodyNota").html("");
				barang = [];
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var obj = new Object();
					var subtotal = mydata[i].jumlah * mydata[i].harga;
					obj.id = mydata[i].id_kue;
					obj.kode = mydata[i].barcode_kue;
					obj.harga = mydata[i].harga;
					obj.nama = mydata[i].nama_kue;
					barang.push(obj);
					idx = barang.length - 1;
					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + mydata[i].id_kue + "'> " +
						" " + mydata[i].barcode_kue + " - " + mydata[i].nama_kue + " </td>" +
						"<td  width='20%'  style='text-align:right'><span id='harga'>" + toRp(mydata[i].harga * 1) + "</span><input type='hidden' name='harga[]' value='" + mydata[i].harga + "'></td>" +
						"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + mydata[i].jumlah + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
						"<td width='20%' style='text-align:right'><span id='subtotal" + idx + "'>" + toRp(subtotal) + " </span></td></tr>";
					$("#bodyNota").append(newRow);
				}
			}
		});
	}

	$('#btnSubmit').click(function () {
		var jumlahrow = $('#detailTable').bootstrapTable('getData').length;
		var pemotongan = $('#pemotongan').val();
		var grandtotal = $('#grandtotal').val();
		var barang = [];
		$.each($('#detailTable').bootstrapTable('getData'), function (index, item) {
			barang.push({
				retur: item.retur,
				id: item.id,
				id_masukdetail: item.id_masukdetail
			});
		});
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else if (pemotongan > grandtotal) {
			showAlert('', "Jumlah pemotongan lebih besar dari total penjualan", 'error');
		} else {
			if ($("#createForm").valid()) {
				$.ajax({
					url: "<?php echo base_url($page . '/edit'); ?>",
					method: 'post',
					data: {
						idMasuk: $('#idMasuk').val(),
						tgl: $('#tgl').val(),
						idPegawai: $('#idPegawai').val(),
						totalItem: $('#totalItem').val(),
						total: $('#total').val(),
						grandtotal: $('#grandtotal').val(),
						type: 'simpan',
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', 'Retur Barang Masuk Berhasil diubah', 'success', function () {
								window.location = "<?php echo base_url($page)?>";
							});
						}
					}, error: function (xhr, text, status) {
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			}
		}
	});

	$('#memberTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getMember/0'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseMember(' + row['id_member'] + ')"><i class="fa fa-plus"></i></button>'
					+ '<input type="hidden" id="namaMemberModal' + row['id_member'] + '" value="' + row['nama'] + '">'
					+ '<input type="hidden" id="diskonMember' + row['id_member'] + '" value="' + row['diskon'] + '">'
					+ '<input type="hidden" id="userPoin' + row['id_member'] + '" value="' + row['use_poin'] + '">'
					+ '<input type="hidden" id="saldo' + row['id_member'] + '" value="' + row['saldo'] + '">'
					+ '<input type="hidden" id="poin' + row['id_member'] + '" value="' + row['poin'] + '">'
					+ '<input type="hidden" id="diskonBarang' + row['id_member'] + '" value="' + row['diskon_barang'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'email',
			title: 'Email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	$('#barangTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getBarang'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseBarang(`' + row['barcode_kue'] + '`)"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="id_kue' + row['id_kue'] + '" value="' + row['id_kue'] + '">'
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode Kue',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="barcode_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="nama_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="harga' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
		startDate: '0D'
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});

	$("#searchKodeBarang").autocomplete({
		source: "<?php echo base_url('searchBarang'); ?>",
		select: function (event, ui) {
			var select = ui.item.label;
			var arr = select.split('_');
			$('#txtKodeBarang').val(arr[0]);
		}
	});

	$('#searchKodeBarang').keypress(function (event) {
		if (event.which == 13) {
			event.preventDefault();
			// cekKodeBarang();
			$('#addLineBtn').trigger('click');
		}
	});

	shortcut.add("f1", function () {
		$("#searchKodeBarang").focus();
	});

	shortcut.add("f2", function () {
		$('#modal-barang').modal({show: 'true'});
	});

	shortcut.add("f3", function () {
		$('#modal-pemotongan').modal({show: 'true'});
	});

	shortcut.add("f8", function () {
		$('#btnSubmit').trigger('click');
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});
	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});
	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});


	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}
</script>
