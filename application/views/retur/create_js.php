<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var penjualan = []; //array penjualan
	var kode = "";
	var nama = "";
	var harga = "";
	var jumlah = "";
	var count = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var barang = []; //array barang

	$(document).ready(function () {
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
	});

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseSupplier(id) {
		$('#idSupplier').val(id);
		var nama = $('#namaSupplierModal' + id).val();
		$('#supplier').val(nama);
		$('#modal-supplier').modal('hide');
	}

	$('#historyTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		// url: "<?php echo base_url('getHistoryMasuk'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return "<button type='button' class='btn btn-xs btn-primary' onclick='chooseBarangMasuk(" + index + ");'><i class='fa fa-plus'></i></button><input type='hidden' id='id_masuk" + index + "' value='" + row['id_masuk'] + "'><input type='hidden' id='total" + index + "' value='" + row['jumlah'] + "'><input type='hidden' id='tgl" + index + "' value='" + row['tgl'] + "'>";
			}
		}, {
			field: 'document',
			title: 'Document',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'jumlah',
			title: 'Jumlah',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	//Function untuk mencari history transaksi member
	function searchBarangMasuk() {
		var idSupplier = $('#idSupplier').val();
		$('#historyTable').bootstrapTable('refresh', {
			url: "<?php echo base_url('searchBarangMasuk');?>",
			query: {
				idSupplier: $('#idSupplier').val()
			}
		});

		$('#depositTable').bootstrapTable('resetWidth');
	}

	function chooseBarangMasuk(id) {
		var idMasuk = $('#id_masuk' + id).val();
		var total = $('#total' + id).val();
		detailBarangMasuk(idMasuk);
		$('#totalBarang').val(total);
		$('#modal-nota').modal('hide');
	}

	function detailBarangMasuk(id) {
		$.ajax({
			url: "<?php echo base_url();?>detailBarangMasuk/" + id,
			success: function (data) {
				var mydata = $.parseJSON(data);
				var newRow = "";
				$('#idBarangMasuk').val(id);
				for (var i = 0; i < mydata.length; i++) {
					console.log(mydata[i]);
					var obj = new Object();
					obj.idDetail = mydata[i].id;
					obj.idBarang = mydata[i].id_kue;
					obj.harga = mydata[i].harga_beli;
					obj.jumlah = mydata[i].jumlah;
					barang.push(obj);

					newRow += "<tr><td><input type='hidden' name='idDetail[]' value='" + mydata[i].id + "'><input type='hidden' name='idBarang[]' id='idBarang" + i + "' value='" + mydata[i].id_kue + "'>" + mydata[i].barcode_kue + " - " + mydata[i].nama + "</td>" +
						"<td>" + toRp(mydata[i].harga_beli) + "</td>" +
						"<td>" + (mydata[i].jumlah * 1) + "</td>" +
						"<td><input type='number' style='width=100%;' name='jumlahRetur[]' id='jumlahRetur" + i + "' value='" + mydata[i].retur * 1 + "' min='0' max='" + (mydata[i].jumlah * 1 - mydata[i].retur * 1) + "' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max='Jumlah retur tidak boleh melebihi jumlah beli.' onchange='hitungRetur();'></td></tr>";
				}
				$("#bodyNota").html(newRow);
				hitungRetur();
			}
		});
	}

	function hitungRetur() {
		grandtotal = 0;
		var diskon = 0;
		var totalItem = 0;
		var totalItemRetur = 0;
		var total = 0;
		for (var i = 0; i < barang.length; i++) {
			var retur = parseInt($("#jumlahRetur" + i).val());
			totalItemRetur += retur;
			totalItem += parseInt(barang[i].jumlah);
			var harga = parseInt(barang[i].harga);
			grandtotal += (retur * harga);
			barang[i].retur = retur;
			total += parseInt(barang[i].jumlah) * parseInt(barang[i].harga);
		}
		$('#total').val(total);
		$('#viewTotalBayar').html(toRp(total));
		$('#totalItem').val(parseInt(totalItem) - parseInt(totalItemRetur));
		$('#viewTotalItem').html("(" + totalItem + " Item)");
		$('#viewTotalItemRetur').html("(" + totalItemRetur + " Item)");
		$('#viewRetur').html(toRp(grandtotal));
		$('#grandtotal').val(total - grandtotal);
		$('#viewGrandtotal').html(toRp(total - grandtotal));
	}

	$('#btnSubmit').click(function () {
		if ($('#createForm').valid()) {
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			var cek = 0;
			for (var i = 0; i < jumlahrow; i++) {
				if ($('#jumlahRetur' + i).val() > 0) {
					cek++
				}
			}
			if (cek > 0) {
				// $('#pleaseWaitDialog').modal('show');
				$.ajax({
					url: "<?php echo base_url($page . '/create'); ?>",
					method: 'post',
					data: {
						idSupplier: $('#idSupplier').val(),
						tgl: $('#tgl').val(),
						idPegawai: $('#idPegawai').val(),
						idBarangMasuk: $('#idBarangMasuk').val(),
						grandtotal: $('#grandtotal').val(),
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'success', function () {
								window.location = "<?php echo base_url($page); ?>"
							});
						}
					}, error: function (xhr, text, status) {
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			} else {
				showAlert('', 'Isi jumlah barang yang ingin diretur', 'error');
			}
		}
	});

	$('#searchBarangMasuk').click(function () {
		var idSupplier = $('#idSupplier').val();
		if (idSupplier == "") {
			showAlert('', "Pilih supplier terlebih dahulu", 'error');
		} else {
			searchBarangMasuk();
			$('#modal-nota').modal({
				show: 'true'
			});
		}
	});

	$('#supplierTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getSupplier'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseSupplier(' + row['id_sup'] + ')"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="namaSupplierModal' + row['id_sup'] + '" value="' + row['nama'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'No Telp',
			field: 'nohp',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Alamat',
			field: 'alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Email',
			field: 'email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Keterangan',
			field: 'ket',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});


	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('#tgl').datepicker({
		autoclose: true
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
			member: {
				required: true,
			}
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
			member: {
				required: "Member harus diisi",
			}
		}
	});
</script>
