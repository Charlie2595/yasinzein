<script type="text/javascript">
	$('#confirmSubmit').click(function () {
		$('#createForm').submit();
	});

	$("#buttonModal").click(function () {
		if ($("#createForm").valid()) {
			$('#confirm-edit-modal').modal('show');
		}
	});

	//Date picker
	$('#tgllahir').datepicker({
		autoclose: true
	})

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			nama: {
				required: true,
			},
			tgllahir: {
				required: true,
			},
			alamat: {
				required: true,
			},
		},

		messages: {
			tgllahir: {
				required: "Tanggal harus diisi",
			},
			nama: {
				required: "Nama harus diisi",
			},
			alamat: {
				required: "Alamat harus diisi",
			},
		}
	});

</script>
