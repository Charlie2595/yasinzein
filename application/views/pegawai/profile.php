<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Buat
			<small>Pegawai</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url('pegawai'); ?>">Pegawai</a></li>
			<li class="active">Buat Pegawai</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Pegawai Form</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url('pegawai/create'); ?>">
						<div class="box-body">
							<div class="form-group">
								<label for="inputNama" class="col-sm-4 control-label">Nama :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama">
								</div>
							</div>
							<div class="form-group">
								<label for="inputTanggal" class="col-sm-4 control-label">Tanggal Lahir :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control pull-right" id="tgllahir" name="tgllahir">
								</div>
							</div>
							<div class="form-group">
								<label for="inputAlamat" class="col-sm-4 control-label">Alamat :</label>
								<div class="col-sm-4">
									<textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat..."></textarea>
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="buttonModal">
									<i class="fa fa-save"></i> Simpan
								</button>
								<div id="confirm-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Pembuatan Pemasukan</h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin membuat pemasukan ini?</p>
											</div>
											<div class="modal-footer">
												<button type="button" id="confirmSubmit" class="btn btn-success waves-effect">
													<i class="fa fa-check"></i> Ya
												</button>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
