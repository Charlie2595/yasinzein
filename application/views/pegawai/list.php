<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Master
			<small>Pegawai</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Master Pegawai</li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> List Pegawai</h3>
						<a href="<?php echo base_url('pegawai/create') ?>" class="btn btn-success pull-right">
							<i class="fa fa-plus"></i> Tambah Pegawai</a>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Nama</th>
								<th>Tanggal Lahir</th>
								<th>Alamat</th>
								<th>Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php $counter = 1; ?>
							<?php foreach ($list as $list) : ?>
								<tr>
									<td><?php echo $list['nama']; ?></td>
									<td><?php echo $list['Jabatan']; ?></td>
									<td><?php echo $list['nohp']; ?></td>
									<td><?php echo $list['ktp']; ?></td>
									<td class="p-t-8 p-b-6">
										<a href="<?php echo base_url('pegawai/' . $list['id_pegawai']); ?>" class="btn btn-default"><i class="fa fa-info-circle"></i> Detail
										</a>
										<a href="<?php echo base_url('pegawai/' . $list['id_pegawai'] . '/edit'); ?>" class="btn btn-info"><i class="fa fa-info-circle"></i> Ubah
										</a>
										<button type="button" class="btn btn-danger waves-effect waves-light" id="buttonModal<?php echo $counter; ?>" onclick="showModal(<?php echo $counter; ?>);">
											<i class="fa fa-trash"></i> Hapus
										</button>
									</td>
								</tr>
								<div id="confirm-edit-modal<?php echo $counter; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Menghapus Pegawai</h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin membuat Mengapus ini?</p>
											</div>
											<div class="modal-footer">
												<a href="<?php echo base_url('pegawai/' . $list['id_pegawai'] . '/delete'); ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Ya
												</a>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
								<?php $counter++; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>

				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
