<script type="text/javascript">
	$('#confirmSubmit').click(function () {
		$('#createForm').submit();
	});

	$("#buttonModal").click(function () {
		if ($("#createForm").valid()) {
			$('#confirm-edit-modal').modal('show');
		}
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			nama: {
				required: true,
			},
			hp: {
				required: true,
				remote: {
					url: "<?php echo base_url();?>cekHPMember",
					type: "post",
					data:
						{
							hp: function () {
								return $("#hp").val();
							},
							oldHP: function () {
								return $("#oldHP").val();
							}
						}
				}
			},
		},

		messages: {
			kode: {
				required: "Tanggal harus diisi",
			},
			nama: {
				required: "Nama harus diisi",
			},
			hp: {
				required: "no HP harus diisi",
			},
		}
	});

</script>
