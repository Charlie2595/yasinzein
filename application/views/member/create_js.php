<script type="text/javascript">
	$('#confirmSubmit').click(function () {
		$('#pleaseWaitDialog').modal('show');
		$.ajax({
			url: "<?php echo base_url($page . '/create'); ?>",
			method: 'post',
			data: {
				nama: $('#nama').val(),
				email: $('#email').val(),
				rumah: $('#rumah').val(),
				hp: $('#hp').val(),
				ktp: $('#ktp').val(),
				diskon: 0,
				diskonBarang: 0,
				instansi: $('#instansi').val(),
				kantor: $('#kantor').val(),
				keterangan: $('#keterangan').val(),
				jenisMember: $('#jenisMember').val(),
				usepoin: $('#usepoin').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				if (data['status'] == 'error') {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'error');
				} else {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', data['description'], 'success', function () {
						window.location = "<?php echo base_url($page); ?>"
					});
				}
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$("#buttonModal").click(function () {
		if ($("#createForm").valid()) {
			$('#confirm-edit-modal').modal('show');
		}
	});

	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue'
	})

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			nama: {
				required: true,
			},
			hp: {
				required: true,
				remote: {
					url: "<?php echo base_url();?>cekHPMember",
					type: "post",
					data:
						{
							hp: function () {
								return $("#hp").val();
							}
						}
				}
			},
			jenisMember: {
				required: true,
			}
		},

		messages: {
			nama: {
				required: "Nama harus diisi",
			},
			jenisMember: {
				required: "Jenis Member harus diisi",
			},
			hp: {
				required: "No HP harus diisi",
				remote: jQuery.validator.format("{0} sudah digunakan.")
			}
		}
	});

</script>
