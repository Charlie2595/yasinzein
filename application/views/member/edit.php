<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			UBAH
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">UBAH <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo strtoupper($page); ?> FORM</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post" action="<?php echo base_url($page . '/edit'); ?>">
						<div class="box-body">
							<div class="form-group">
								<div class="col-sm-6">
									<label class="col-sm-3"><h4>Saldo</h4></label>
									<div class="col-md-9">
										<div class="input-group">
											<input type="text" class="form-control pull-right" id="kode" name="kode" placeholder="Kode" disabled="" value="<?php echo $detail['saldo']; ?>">
											<span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-barcode"></i></button>
                      </span>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<label class="col-sm-3"><h4>Poin</h4></label>
									<div class="col-md-9">
										<div class="input-group">
											<input type="text" class="form-control pull-right" id="kode" name="kode" placeholder="Kode" disabled="" value="<?php echo $detail['poin']; ?>">
											<span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat"><i class="fa fa-barcode"></i></button>
                      </span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Use Poin</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="radio" name="poin" value="1" class="minimal" <?php echo $detail['use_poin'] == '1' ? 'checked' : ''; ?>> YA
										<input type="radio" name="poin" value="0" class="minimal" <?php echo $detail['use_poin'] == '0' ? 'checked' : ''; ?>> TIDAK
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Jenis Member </h4></label>
								<div class="col-sm-9">
									<select id="jenisMember" name="jenisMember" class="form-control select2" style="width: 100%;">
										<option selected="selected" disabled="">Pilih Jenis Member</option>
										<option value="0" <?php echo $detail['jenis_member'] == '0' ? 'selected' : ''; ?>>Regular</option>
										<option value="1" <?php echo $detail['jenis_member'] == '1' ? 'selected' : ''; ?>>B2B</option>
										<option value="2" <?php echo $detail['jenis_member'] == '2' ? 'selected' : ''; ?>>Cabang</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Nama</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="nama" name="nama" placeholder="Nama" value="<?php echo $detail['nama']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
                    </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Email</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="email" class="form-control pull-right" id="email" name="email" placeholder="Email" value="<?php echo $detail['email']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-envelope"></i></button>
                    </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>No HP</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="hp" name="hp" placeholder="No Handphone" value="<?php echo $detail['nohp']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="glyphicon glyphicon-phone"></i></button>
                    </span> <input type="hidden" name="oldHP" id="oldHP" value="<?php echo $detail['nohp']; ?>">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>KTP</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="ktp" name="ktp" placeholder="KTP" value="<?php echo $detail['ktp']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-credit-card"></i></button>
                    </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Instansi</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<input type="text" class="form-control pull-right" id="instansi" name="instansi" placeholder="Instansi" value="<?php echo $detail['instansi']; ?>">
										<span class="input-group-btn">
                      <button type="button" class="btn btn-info btn-flat"><i class="fa fa-institution "></i></button>
                    </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Alamat Rumah</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<textarea id="rumah" name="rumah" class="form-control pull-right" placeholder="Alamat Rumah" rows="2" cols="110"><?php echo $detail['alamat_rumah']; ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Alamat Kantor</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<textarea id="kantor" name="kantor" class="form-control pull-right" placeholder="Alamat Kantor" rows="2" cols="110"><?php echo $detail['alamat_kantor']; ?></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3"><h4>Keterangan</h4></label>
								<div class="col-md-9">
									<div class="input-group">
										<textarea id="keterangan" name="keterangan" class="form-control pull-right" placeholder="Keterangan" rows="2" cols="110"><?php echo $detail['ket']; ?></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="buttonModal">
									<i class="fa fa-save"></i> Simpan
								</button>
								<div id="confirm-edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title">Konfirmasi Perubahan Data <?php echo($page); ?></h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda yakin ingin mengubah <?php echo($page); ?> ini?</p>
											</div>
											<div class="modal-footer">
												<button type="button" id="confirmSubmit" class="btn btn-success waves-effect">
													<i class="fa fa-check"></i> Ya
												</button>
												<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tidak</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<input type="hidden" name="id" value="<?php echo $detail['id_member']; ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
