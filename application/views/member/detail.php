<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 13/07/2017
 * Time: 14.13
 */ ?>
<style>
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}

	th, td {
		text-align: center;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Detail
			<small><?php echo strtoupper($page); ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo($page); ?></a></li>
			<li class="active">DETAIL <?php echo strtoupper($page); ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12 col-lg-12">
				<div class="box">
					<div class="cust-panel-head">
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<form class="form-horizontal" role="form">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Tanggal Buat :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['tgl_buat']; ?></p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Tanggal Transaksi :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['tgl_transaksi']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Name :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['nama']; ?></p></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Jenis Member :</label>
											<div class="col-sm-9 col-lg-8"><p class="form-control-static">
													<?php if ($detail['jenis_member'] == 1) : ?>
														B2B
													<?php elseif ($detail['jenis_member'] == 2): ?>
														CABANG
													<?php else: ?>
														POS & PESANAN
													<?php endif; ?>
												</p></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Email :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['email']; ?></p></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">KTP :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['ktp']; ?></p></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Saldo :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['saldo']; ?></p></div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Poin :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['poin']; ?></p></div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Alamat Rumah :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['alamat_rumah']; ?></p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Alamat Kantor :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['alamat_kantor']; ?></p>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Instansi :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['instansi']; ?></p>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label col-sm-3 col-lg-4">Keterangan :</label>
											<div class="col-sm-9 col-lg-8">
												<p class="form-control-static"><?php echo $detail['ket']; ?></p></div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
