<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		url: "<?php echo base_url('getAllMember'); ?>",
		columns: [{
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				btn += '<a href="<?php echo base_url();?>member/' + row['id_member'] + '" data-toggle="tooltip" title="Detail Member" class="btn btn-sm btn-primary"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';

				btn += '<a href="<?php echo base_url();?>member/' + row['id_member'] + '/edit" data-toggle="tooltip" title="Edit Member" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';

				btn += '<a href="#" class="btn btn-sm btn-danger waves-effect waves-light delete" data-toggle="tooltip" title="Delete Member"><i class="fa fa-trash fa-lg"></i></a> &nbsp;';

				return btn;
			},
			events: {
				'click .delete': function (e, value, row, index) {
					showConfirm('', 'Are you sure you want to delete this member?', 'warning', function () {
						$.ajax({
							url: "<?php echo base_url();?>member/" + row['id_member'] + "/delete",
							success: function (data) {
								var data = $.parseJSON(data);
								if (data['status'] == 'error') {
									showAlert('', data['description'], 'error');
								} else {
									showAlert('', data['description'], 'success', function (e) {
										$('#listTable').bootstrapTable('refresh');
									});
								}
							},
							error: function (xhr, text, status) {
								if (xhr.status == 422) {
									showAlert('', xhr.responseJSON.join(' '), 'error');
								}
							}
						});
					});
				}
			}
		}, {
			field: 'nama',
			title: 'Nama',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'nohp',
			title: 'No HP',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'saldo',
			title: 'Saldo',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'poin',
			title: 'Poin',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'alamat_rumah',
			title: 'Alamat Rumah',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'jenis_member',
			title: 'Jenis Member',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return 'B2B';
				} else if (value == 2) {
					return 'CABANG';
				} else {
					return 'POS & PESANAN';
				}
			}
		}, {
			field: 'tgl_transaksi',
			title: 'Tanggal Transaksi',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

</script>
