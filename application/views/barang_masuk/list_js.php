<script type="text/javascript">
	$(document).ready(function () {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#listTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		onPostBody: function () {
			$('[data-toggle="tooltip"]').tooltip();
		},
		sortOrder: 'desc',
		sortName: 'document',
		url: "<?php echo base_url('getAllBarangMasuk'); ?>",
		columns: [{
			field: 'document',
			title: 'Document',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return moment(value).format('DD-MM-YYYY HH:mm');
				}
			}
		}, {
			field: 'jumlah',
			title: 'Jumlah',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'total',
			title: 'Grandtotal (Rp)',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value) {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'lunas',
			title: 'Status',
			sortable: true,
			halign: "left",
			align: "right",
			formatter: function (value, row, index, field) {
				if (value == 1) {
					return '<label class="badge bg-green">LUNAS</label>';
				} else {
					return '<label class="badge bg-yellow">BELUM LUNAS</label>';
				}
			}
		}, {
			field: 'supplier',
			title: 'Supplier',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'pegawai',
			title: 'Pegawai',
			sortable: true,
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Actions',
			width: '9%',
			formatter: function (value, row, index) {
				var btn = "";

				btn += '<a href="<?php echo base_url();?>masuk/' + row['id_masuk'] + '" data-toggle="tooltip" title="Detail" class="btn btn-sm btn-info"><i class="fa fa-info-circle fa-lg"></i></a> &nbsp;';

				if (row['lunas'] == 1) {
					btn += '<a class="btn btn-default" disable ><i class="fa fa-money"></i></a> &nbsp;';
				} else {
					btn += '<a href="<?php echo base_url();?>masuk/' + row['id_masuk'] + '/edit" data-toggle="tooltip" title="Edit Barang Masuk" class="btn btn-sm btn-success"><i class="fa fa-pencil fa-lg"></i></a> &nbsp;';
					btn += '<a href="<?php echo base_url();?>masuk/' + row['id_masuk'] + '/payment" data-toggle="tooltip" title="Pembayaran" class="btn btn-sm btn-warning"><i class="fa fa-money fa-lg"></i></a> &nbsp;';
				}
				return btn;
			}
		}]
	});
</script>
