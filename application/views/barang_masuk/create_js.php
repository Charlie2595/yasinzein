<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>
<script type="text/javascript">
	var barang = []; //array barang
	var kode = "";
	var nama = "";
	var harga = "";
	var idx = "";
	var totalbayar = 0; //Total pembayaran
	var grandtotal = 0; //Total transaksi setelah diskon
	var bayar = []; //array pembayaran
	var cekUsePoin = "";

	$(document).ready(function () {
		//CKEDITOR.replace('txtEditor');
		$('.select2').select2();
		$('[class="col-sm-12 form-group point"').hide();
		$('[class="col-sm-12 form-group saldo"').hide();
		cekSubmit();

		$('.input-uang').autoNumeric('init', {
			vMin: -999999999999,
			mDec: 6,
			aSep: ".",
			aDec: ",",
			aPad: false
		});
	});

	shortcut.add("f1", function () {
		$("#searchKodeBarang").focus();
	});

	shortcut.add("f2", function () {
		$('#modal-barang').modal({show: 'true'});
	});

	shortcut.add("f3", function () {
		$('#modal-verifikasi').modal({show: 'true'});
	});

	//hitung grandtotal
	function calculatetotal() {
		grandtotal = 0;
		var diskon = 0;
		var totalItem = 0;
		for (var i = 0; i < barang.length; i++) {
			totalItem += barang[i].jumlah * 1;
			var jumlah = barang[i].jumlah * 1;
			var hargaBeli = barang[i].hargaBeli * 1;
			grandtotal += (jumlah * hargaBeli);
		}
		$("#viewTotalItem").html("(" + totalItem + " Item)");
		$("#viewTotalBayar").html(toRp(grandtotal));
		$("#totalPenjualan").val(grandtotal); //Total Penjualan

		var pemotongan = $('#pemotongan').val();
		grandtotal = grandtotal - pemotongan;
		countJumlah = totalbayar - grandtotal;
		$("#grandtotalPembayaran").html(toRp(countJumlah)); //Total Bayar
		$("#viewGrandtotal").html(toRp(grandtotal));
		$("#grandtotal").val(grandtotal);
		$("#grandtotalPembelian").val(toRp(grandtotal));
	}

	//hitung subtotal
	function hitungtotal(id) {
		id = id * 1;
		var jumlah = eval($("#jumlah" + id).val());
		var harga = barang[id].hargaBeli * 1;
		barang[id].jumlah = jumlah;
		var subtotal = jumlah * harga;
		$("#subtotal" + id).html(toRp(subtotal));
		calculatetotal();
	}

	//hitung subtotal
	function hitungtotalPembayaran(id) {
		id = id * 1;
		var jumlah = $("#jumlahPembayaran").autoNumeric('get') * 1;
		bayar[id].jumlah = jumlah;

		var subtotal = (jumlah * 1) + (totalbayar * 1);
		totalbayar = subtotal;
		$("#grandtotalPembayaran").html(toRp(subtotal));
		$("#grandTotalBayar").val(subtotal);
		cekSubmit();
	}

	function hitungKembalian() {
		var total = ($('#total').autoNumeric('get') * 1);
		var jumlahPembayaran = ($('#jumlahPembayaran').autoNumeric('get') * 1);

		if (jumlahPembayaran >= total) {
			var kembalian = jumlahPembayaran - total;
			$("#kembalian").val(toRp(kembalian));
		} else {
			$("#kembalian").val(0);
		}
	}

	function cekSubmit() {
		if (totalbayar >= grandtotal) {
			$("#btnSimpan").removeAttr("disabled");
		} else {
			$("#btnSimpan").attr("disabled", "disabled");
		}
	}

	function toRp(amount, decimalSeparator, thousandsSeparator, nDecimalDigits) {
		var num = parseFloat(amount); //convert to float
		//default values
		decimalSeparator = decimalSeparator || ',';
		thousandsSeparator = thousandsSeparator || '.';
		nDecimalDigits = nDecimalDigits == null ? 0 : nDecimalDigits;

		var fixed = num.toFixed(nDecimalDigits); //limit or add decimal digits
		//separate begin [$1], middle [$2] and decimal digits [$4]
		var parts = new RegExp('^(-?\\d{1,3})((?:\\d{3})+)(\\.(\\d{' + nDecimalDigits + '}))?$').exec(fixed);

		if (parts) { //num >= 1000 || num < = -1000
			return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + (parts[4] ? decimalSeparator + parts[4] : '');
		} else {
			return fixed.replace('.', decimalSeparator);
		}
	}

	function chooseSupplier(id) {
		$('#idSupplier').val(id);
		var nama = $('#namasupplier' + id).val();
		$('#supplier').val(nama);
		$('#barangTable').bootstrapTable('refresh', {
			url: "<?php echo base_url('getBarang'); ?>",
			query: {
				supplier: $('#idSupplier').val(),
			}
		});
		$('#barangTable').bootstrapTable('resetWidth');
		$('#modal-supplier').modal('hide');
	}

	function cekKodeBarang() {
		var txtKode = $('#searchKodeBarang').val().trim();
		if (txtKode.length >= 5) {
			var jumlah = txtKode.split('*');
			if (jumlah.length > 1) {
				var arr = jumlah[1].split('_');
			} else {
				jumlah[0] = 1;
				var arr = txtKode.split('_');
			}
			var kode = arr[0];
			$.ajax({
				url: "<?php echo base_url();?>cekKodeBarang/" + kode,
				success: function (data) {
					if (data == 'null') {
						showAlert('', "Kode Barang tidak ditemukan.", 'error');
						error = true;
					} else {
						chooseBarang(data, "cekKodeBarang", jumlah[0]);
					}
				}
			});
			$('#txtKodeBarang').val("");
			$('#searchKodeBarang').val("");
		}
	}

	function chooseBarang(id, type, qty) {
		if (type == "barangModal") {
			kode = $('#barcode_kue' + id).val();
			nama = $('#nama_kue' + id).val();
			harga = $('#harga' + id).val();
			qty = qty;
		} else if (type == "cekKodeBarang") {
			var mydata = $.parseJSON(id);
			kode = mydata.barcode_kue;
			nama = mydata.nama_kue;
			id = mydata.id_kue;
			harga = mydata.harga;
			qty = qty;
		}

		var obj = new Object();
		var cek = 0;
		for (var i = 0; i < barang.length; i++) {
			if (barang[i].id == id || barang[i].kode == kode) {
				cek++;
			}
		}
		if (cek == 0) {
			var subtotal = 1 * harga;
			obj.id = id;
			obj.kode = kode;
			obj.nama = nama;
			obj.jumlah = qty;
			obj.hargaJual = harga;
			obj.hargaBeli = 0;
			barang.push(obj);
			idx = barang.length - 1;

			var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
				"<td width='35%'>" +
				"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + id + "'> " +
				" " + kode + " - " + nama + " </td>" +
				"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + qty + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
				"<td width='20%'><input type='text' id='hargaBeli" + idx + "' style='text-align:right'  name='hargaBeli[]' onkeyup='changeBeli(" + idx + ")' value='0' class='form-control' style='display:block'>" +
				"<td width='20%'><input type='text' id='hargaJual" + idx + "' style='text-align:right'  name='hargaJual[]' onkeyup='changeJual(" + idx + ")' value='" + decimalFormatter(harga) + "' class='form-control' style='display:block'>" +
				"</tr>";
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			$("#bodyNota").append(newRow);
		} else {
			var jumlahrow = document.getElementById("bodyNota").rows.length;
			for (var i = 0; i < jumlahrow; i++) {
				var cekID = $('#idBarang' + i).val();
				if (cekID == id) {
					var jumlah = $('#jumlah' + i).val() * 1;
					jumlah = jumlah + (qty * 1);
					$('#jumlah' + i).val(jumlah);
					var idx = i;
				}
			}
		}
		$('#modal-barang').modal('hide');
		hitungtotal(idx);
	}

	//payment type
	$('#payment').on('select2:select', function (e) {
		var data = e.params.data;

		if (data.text == "saldo") {
			$('[class="col-sm-12 form-group saldo"').show("slow");
			$('[class="col-sm-12 form-group point"').hide("slow");
		} else if (data.text == "poin") {
			if (cekUsePoin == 0) {
				showAlert('', "Pembayaran poin tidak bisa digunakan", 'error');
			} else {
				$('[class="col-sm-12 form-group point"').show("slow");
				$('[class="col-sm-12 form-group saldo"').hide("slow");
			}
		} else {
			$('[class="col-sm-12 form-group point"').hide("slow");
			$('[class="col-sm-12 form-group saldo"').hide("slow");
		}
	});

	$('#showHistory').click(function () {
		var idSupplier = $('#idSupplier').val();
		if (idSupplier == "") {
			showAlert('', "Pilih supplier terlebih dahulu", 'error');
		} else {
			searchHistory();
			$('#modal-history').modal({
				show: 'true'
			});
		}
	});

	function searchHistory() {
		var idSupplier = $('#idSupplier').val();
		$.ajax({
			url: "<?php echo base_url();?>searchHistoryBarangMasuk/" + idSupplier,
			success: function (data) {
				$("#bodyNota").html("");
				barang = [];
				var mydata = $.parseJSON(data);
				for (var i = 0; i < mydata.length; i++) {
					var obj = new Object();
					var subtotal = mydata[i].jumlah * mydata[i].harga;
					obj.id = mydata[i].id_kue;
					obj.kode = mydata[i].barcode_kue;
					obj.nama = mydata[i].nama_kue;
					obj.hargaJual = mydata[i].harga_jual;
					obj.hargaBeli = mydata[i].harga_beli;
					barang.push(obj);
					idx = barang.length - 1;
					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removeBarang' onclick='removeat(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idBarang[]' id='idBarang" + idx + "' value='" + mydata[i].id_kue + "'> " +
						" " + mydata[i].barcode_kue + " - " + mydata[i].nama_kue + " </td>" +
						"<td  width='20%'><input type='number' min='1' id='jumlah" + idx + "' onkeyup='hitungtotal(" + idx + ");' onchange='hitungtotal(" + idx + ");' name='jumlah[]' value='" + mydata[i].jumlah + "' class='form-control' style='display:block' required data-msg-required='Jumlah harus diisi' data-rule-number='true' data-msg-number='Jumlah harus diisi angka/numerik.' data-msg-max = 'Jumlah tidak boleh melebihi jumlah stok saat ini' data-msg-min = 'Jumlah harus diisi min 1'></td>" +
						"<td width='20%'><input type='text' style='text-align:right' id='hargaBeli" + idx + "' name='hargaBeli[]' value='" + decimalFormatter(mydata[i].harga_beli) + "' class='form-control' style='display:block' onkeyup='changeBeli(" + idx + ")'>" +
						"<td width='20%'><input type='text' style='text-align:right' id='hargaJual" + idx + "' name='hargaJual[]' value='" + decimalFormatter(mydata[i].harga_jual) + "' class='form-control' style='display:block' onkeyup='changeJual(" + idx + ")'>" +
						"</tr>";
					$("#bodyNota").append(newRow);
					hitungtotal(idx);
				}
			}
		});
	}

	function removeat(idx) {
		barang.splice(idx, 1);
		$("#baris" + idx).remove();
		$("#pemotongan").val(0);
		$("#viewPemotongan").html(0);
		calculatetotal();
	}

	function decimalFormatter(number) {
		return numeral(String(number)).format('0,0');
	}

	function changeJual(idx) {
		var price = $("#hargaJual" + idx).val();
		barang[idx].hargaJual = numeral(price).value();
		$("#hargaJual" + idx).val(numeral(price).format('0,0'));
		hitungtotal(idx);
	};

	function changeBeli(idx) {
		var price = $("#hargaBeli" + idx).val();
		barang[idx].hargaBeli = numeral(price).value();
		$("#hargaBeli" + idx).val(numeral(price).format('0,0'));
		hitungtotal(idx);
	};

	//Payment Submit
	$('#btnSimpan').click(function () {
		if (totalbayar >= grandtotal) {
			$("#type").val("payment");
			$.ajax({
				url: "<?php echo base_url($page . '/create'); ?>",
				method: "POST",
				data: {
					tgl: $('#tgl').val(),
					idSupplier: $('#idSupplier').val(),
					idPegawai: $('#idPegawai').val(),
					total: $('#totalPenjualan').val(),
					pemotongan: $('#pemotongan').val(),
					barang: barang
				},
				success: function (data) {
					var data = $.parseJSON(data);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		} else {
			showAlert('', "Pembayaran kurang", 'error');
		}
	});

	//Tambah Pembayaran
	$('#btnTambah').click(function () {
		var jenis = $('#payment').val();
		var namaJenis = $("#payment :selected").text();
		var jumlahBayar = $('#jumlahPembayaran').autoNumeric('get') * 1;
		totalbayar = $("#grandTotalBayar").val();
		var grandtotal = $("#grandtotal").val();
		var cek = false;
		if (jenis == null || jumlahBayar == 0 || jenis == "" || namaJenis == "") {
			showAlert('', "Pilih jenis bayar terlebih dahulu/masukkan jumlah pembayaran", 'error');
			cek = true;
		} else if (namaJenis == 'poin' && cekUsePoin == 1) {
			var poin = $('#point').val();
			if (jumlahBayar > poin) {
				showAlert('', "Jumlah poin yang digunakan tidak boleh melebihi jumlah poin saat ini", 'error');
				cek = true;
			}
		} else if (namaJenis == 'saldo') {
			var saldo = $('#saldo').val();
			if (jumlahBayar > saldo) {
				showAlert('', "Jumlah saldo yang digunakan tidak boleh melebihi jumlah saldo saat ini", 'error');
				cek = true;
			}
		}
		if (cek != true) {
			var obj = new Object();
			var cek = 0;
			for (var i = 0; i < bayar.length; i++) {
				if (bayar[i].id == jenis) {
					cek++;
				}
			}
			if (namaJenis == 'poin' && cekUsePoin == 1) {
				jumlahBayar = jumlahBayar * 1000;
			}
			if ((totalbayar * 1 + jumlahBayar * 1) > grandtotal) {
				showAlert('', "Total bayar tidak boleh melebihi grandtotal", 'error');
			} else {
				if (cek == 0) {
					obj.id = jenis;
					obj.jumlah = jumlahBayar;
					bayar.push(obj);
					idx = bayar.length - 1;

					var newRow = "<tr id='baris" + idx + "'><td width='5%'><button type='button' class='btn btn-danger removePembayaran' onclick='removeatPembayaran(" + idx + ");'><i class='fa fa-trash'></button></td>" +
						"<td width='35%'>" +
						"<input type='hidden' name='idPembayaran[]' id='idPembayaran" + idx + "' value='" + jenis + "'> " + namaJenis + " </td>" +
						"<td width='20%'  style='text-align:right'><span id='viewJumlah" + idx + "'>" + toRp(jumlahBayar * 1) + "</span><input type='hidden' name='jumlahPembayaran[]' id='jumlahPembayaran" + idx + "' value='" + jumlahBayar + "'></td></tr>";
					$("#paymentDetail").append(newRow);
				} else {
					var jumlahrow = document.getElementById("paymentDetail").rows.length;
					for (var i = 0; i < jumlahrow; i++) {
						var cekID = $('#idPembayaran' + i).val();
						if (cekID == jenis) {
							var jumlahPembayaran = eval($('#jumlahPembayaran' + i).val());
							jumlahPembayaran = (jumlahPembayaran * 1) + (jumlahBayar * 1);
							$('#jumlahPembayaran' + i).val(jumlahPembayaran);
							$('#viewJumlah' + i).html(toRp(jumlahPembayaran));
							var idx = i;
						}
					}
				}
				hitungtotalPembayaran(idx);
				$('#jumlahPembayaran').val("");
			}
		}
	});


	$('#btnSubmit').click(function () {
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		var pemotongan = $('#pemotongan').val();
		var grandtotal = $('#grandtotal').val();
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else if (pemotongan > grandtotal) {
			showAlert('', "Jumlah pemotongan lebih besar dari total penjualan", 'error');
		} else {
			$("#type").val("simpan");
			if ($("#createForm").valid()) {
				//$('#pleaseWaitDialog').modal('show');
				$.ajax({
					url: "<?php echo base_url($page . '/create'); ?>",
					method: 'post',
					data: {
						tgl: $('#tgl').val(),
						idSupplier: $('#idSupplier').val(),
						idPegawai: $('#idPegawai').val(),
						total: $('#totalPenjualan').val(),
						pemotongan: $('#pemotongan').val(),
						barang: barang,
					},
					success: function (data) {
						var data = $.parseJSON(data);
						if (data['status'] == 'error') {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'error');
						} else {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', data['description'], 'success', function () {
								window.location = "<?php echo base_url($page); ?>"
							});
						}
					}, error: function (xhr, text, status) {
						if (xhr.status == 422) {
							$('#pleaseWaitDialog').modal('hide');
							showAlert('', xhr.responseJSON.join('\n'), 'error');
						}
					}
				});
			}
		}
	});

	$('#btnPayment').click(function () {
		var jumlahrow = document.getElementById("bodyNota").rows.length;
		if (jumlahrow < 1) {
			showAlert('', "Pilih barang terlebih dahulu", 'error');
		} else {
			$('#modal-payment').modal({
				show: 'true'
			});
		}
	});

	$('#barangTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getBarang'); ?>",
		queryParams: function (p) {
			p.supplier = $('#idSupplier').val();

			return p;
		},
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseBarang(' + row['id_kue'] + ',`barangModal`,1)"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="namasupplier' + row['id_sup'] + '" value="' + row['nama'] + '">'
			}
		}, {
			field: 'barcode_kue',
			title: 'Kode Kue',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="barcode_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'nama_kue',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="nama_kue' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'harga',
			title: 'Harga',
			formatter: function (value, row, index, field) {
				if (value) {
					return '<input type="hidden" id="harga' + row['id_kue'] + '" value="' + value + '">' + value;
				}
			}
		}, {
			field: 'stok',
			title: 'Stok',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			field: 'produksi',
			title: 'Produksi',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});

	$('#supplierTable').bootstrapTable({
		classes: 'table table-striped table-no-bordered table-condensed text-nowrap',
		striped: true,
		pagination: true,
		search: true,
		showRefresh: true,
		showColumns: true,
		sortOrder: 'desc',
		url: "<?php echo base_url('getSupplier'); ?>",
		columns: [{
			title: '',
			formatter: function (value, row, index, field) {
				return '<button type="button" class="btn btn-xs btn-primary" onclick="chooseSupplier(' + row['id_sup'] + ')"><i class="fa fa-plus"></i></button>' +
					'<input type="hidden" id="namasupplier' + row['id_sup'] + '" value="' + row['nama'] + '">'
			}
		}, {
			field: 'nama',
			title: 'Nama',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'No Telp',
			field: 'nohp',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Alamat',
			field: 'alamat',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Email',
			field: 'email',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}, {
			title: 'Keterangan',
			field: 'ket',
			formatter: function (value, row, index, field) {
				if (value) {
					return value;
				}
			}
		}]
	});


	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			tgl: {
				required: true,
			},
		},

		messages: {
			tgl: {
				required: "Tanggal harus diisi",
			},
		}
	});

	$("#searchKodeBarang").autocomplete({
		source: "<?php echo base_url('searchBarang'); ?>",
		select: function (event, ui) {
			var select = ui.item.label;
			var arr = select.split('_');
			$('#txtKodeBarang').val(arr[0]);
		}
	});

	//Date picker
	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('#tgl').datepicker({
		autoclose: true
	});

	document.addEventListener("keydown", function (event) {
		if (event.which == 13) {
			if ($('#modal-pemotongan').is(':visible')) {
				savePemotongan();
			} else if ($('#modal-verifikasi').is(':visible')) {
				cekVerifikasi();
			} else if ($('#modal-payment').is(':visible')) {
				addPayment();
			} else {
				$('#grandtotalPembelian').val(toRp($('#grandtotal').val()));
			}
		}
	});

	$('#modal-verifikasi').on('shown.bs.modal', function () {
		$('#username').focus();
	});
	$('#modal-pemotongan').on('shown.bs.modal', function () {
		$('#jumlahPemotongan').focus();
	});
	$('#modal-payment').on('shown.bs.modal', function () {
		$('#jumlahPembayaran').focus();
	});


	$('#btnVerifikasi').click(function () {
		cekVerifikasi();
	});

	function cekVerifikasi() {
		var username = $("#username").val();
		var password = $("#password").val();
		$.ajax({
			url: "<?php echo base_url();?>verifikasiPassword",
			data: {"password": password, "username": username},
			type: "POST",
			success: function (data) {
				if (data == 'true') {
					$('#modal-pemotongan').modal({show: 'true'});
					$('#modal-verifikasi').modal('hide');
				} else {
					showAlert('', "Username dan password salah", 'error');
				}
			}
		});
	}

	//Diskon Pemotongan
	$('#btnPemotongan').click(function () {
		savePemotongan();
	});

	function savePemotongan() {
		var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
		var diskon = $('#diskonMember').val();
		var total = $('#total').val();
		if (pemotongan > (total - diskon)) {
			showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
		} else {
			if (pemotongan == "") {
				pemotongan = 0;
			}
			$('#pemotongan').val(pemotongan);
			$('#viewPemotongan').html(toRp(pemotongan));
			calculatetotal();

		}
		$('#jumlahPemotongan').val(0);
		$('#modal-pemotongan').modal('hide');
	}

</script>
