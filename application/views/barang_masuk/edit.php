<?php
/**
 * Created by PhpStorm.
 * User: CHARLIE
 * Date: 24/03/2018
 * Time: 14.14
 */
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Buat
			<small>Barang Masuk</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url($page); ?>">Barang Masuk</a></li>
			<li class="active">BUAT Barang Masuk</li>
		</ol>
	</section>

	<div class="col-sm-12 no-padding">
		<section class="content">
			<div class="row">
				<div class="box box-info">
					<form class="form-horizontal" id="createForm" method="post">
						<div class="row no-margin">
							<div class="col-sm-9 no-padding">
								<div class="box-header">
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Tanggal :</h4></label>
											<div class="input-group date">
												<input type="text" class="form-control pull-right" id="tgl" name="tgl" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($data['tgl'])); ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-calendar"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label><h4>Supplier :</h4></label>
											<div class="input-group">
												<input type="hidden" id="idSupplier" name="idSupplier" value="<?php echo $data['id_sup'] ?>">
												<input type="text" class="form-control" id="supplier" placeholder="Supplier" name="supplier" data-toggle="modal" data-target="#modal-supplier" readonly value="<?php echo $data['supplier'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-supplier"><i class="fa fa-search"></i></button>
									</span>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<label><h4>Pegawai :</h4></label>
											<div class="input-group">
												<input type="text" class="form-control" value="<?php echo $_SESSION['nama'] ?>" readonly>
												<input type="hidden" id="idPegawai" name="idPegawai" value="<?php echo $_SESSION['idLogin'] ?>">
												<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat"><i class="fa fa-user"></i></button>
									</span>
											</div>
										</div>
									</div>

									<ul class="nav nav-tabs">
									</ul>
									<section>
										<div class="col-sm-4">
											<div class="input-group">
												<input type="text" class="form-control autocomplete" id="searchKodeBarang" placeholder="Scan atau Kode Barang" name="searchKodeBarang" onchange="cekKodeBarang();">
												<input type="hidden" id="txtKodeBarang" name="txtKodeBarang">
												<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-flat" id="addLineBtn"><i class="fa fa-plus"></i></button>
									</span>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-barang">
													<i class="fa fa-search"> Cari </i></button>
											</div>
										</div>
										<div class="col-sm-2">
											<div class="form-group">
												<button type="button" class="btn btn-block btn-warning" id="showHistory">
													<i class="fa fa-search"> History </i></button>
											</div>
										</div>
									</section>

									<div class="box-body">
										<table class="table table-bordered" id="detailTable" style="width:100%">
											<thead class="alert">
											<tr>
												<th width="5%" style="text-align: center">Action</th>
												<th width="35%" style="text-align: center">Nama Barang</th>
												<th width="20%" style="text-align: center">Jumlah</th>
												<th width="20%" style="text-align: center">Harga Beli</th>
												<th width="20%" style="text-align: center">Harga Jual</th>
											</tr>
											</thead>
											<?php $counter = 0;
											$grandtotal = 0; ?>
											<tbody id="bodyNota">
											<?php foreach ($data['detail'] as $detail) : ?>
												<tr id=<?php echo "baris" . $counter ?>>
													<td width="5%">
														<button type="button" class="btn btn-danger removeBarang" onclick="removeat(<?php echo $counter ?>);">
															<i class="fa fa-trash"></i></button>
													</td>
													<td width="35%">
														<input type="hidden" name="idBarang[]" id=<?php echo "idBarang" . $counter; ?> value="<?php echo $detail['id_kue']; ?>">
														<input type="hidden" name="kode[]" id=<?php echo "kode" . $counter; ?> value="<?php echo $detail['barcode_kue']; ?>">
														<input type="hidden" name="nama[]" id=<?php echo "nama" . $counter; ?> value="<?php echo $detail['nama_kue']; ?>">
														<?php echo $detail['barcode_kue'] . " - " . $detail['nama_kue']; ?>
													</td>
													<td width="20%" style="text-align: right">
														<input type="number" class="form-control" style="display:block" required
															   name="jumlah[]" data-msg-required="Jumlah harus diisi" data-rule-number="true" data-msg-number="Jumlah harus diisi angka/numerik." data-msg-max="Jumlah tidak boleh melebihi jumlah stok saat ini" data-msg-min="Jumlah harus diisi min 1"
															   id=<?php echo "jumlah" . $counter; ?>
															   value="<?php echo $detail['jumlah']; ?>" onkeyup="hitungtotal(<?php echo $counter; ?>)"
															   onchange="hitungtotal(<?php echo $counter; ?>)">
													</td>
													<td width="20%" style="text-align: right">
														<input type="text" class="form-control" style="display:block" required
															   name="hargaBeli[]"
															   id=<?php echo "hargaBeli" . $counter; ?>
															   value="<?php echo $detail['harga_beli']; ?>" onkeyup="changeBeli(<?php echo $counter; ?>)"
															   onchange="changeBeli(<?php echo $counter; ?>)">
													</td>
													<td width="20%" style="text-align: right">
														<input type="text" class="form-control" style="display:block" required
															   name="hargaJual[]"
															   id=<?php echo "hargaJual" . $counter; ?>
															   value="<?php echo $detail['harga_jual']; ?>" onkeyup="changeJual(<?php echo $counter; ?>)"
															   onchange="changeJual(<?php echo $counter; ?>)">
													</td>
												</tr>
												<?php $counter++; ?>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<section class="panel panel-primary fixed-sidebar-right">
									<ul class="list-group">
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6"><h2 class="list-group-item-heading">TOTAL</h2>
													<label id="viewTotalItem"><h4>(0 Item)</h4></label></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewTotalBayar" class="pull-right"><?php echo number_format($data['total'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="totalPenjualan" id="totalPenjualan" value="<?php echo $data['total']; ?>">
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<a data-toggle="modal" data-target="#modal-verifikasi">
														<h3 class="list-group-item-heading">DISKON</h3></a></div>
												<input type="hidden" name="pemotongan" id="pemotongan" value="<?php echo $data['pemotongan']; ?>">
												<div class="col-sm-6"><h3 class="list-group-item-heading pull-right">
														<span id="viewPemotongan" class="pull-right"><?php echo number_format($data['pemotongan'], 0, ",", "."); ?></span>
													</h3></div>
											</div>
										</li>
										<li class="list-group-item">
											<div class="row">
												<div class="col-sm-6">
													<h2 class="list-group-item-heading">GRAND TOTAL</h2></div>
												<div class="col-sm-6"><h2 class="list-group-item-heading pull-right">
														<span id="viewGrandtotal" class="pull-right"><?php echo number_format($data['total'] - $data['pemotongan'], 0, ",", "."); ?></span>
													</h2></div>
												<input type="hidden" name="grandtotal" id="grandtotal" value="<?php echo($data['total'] - $data['pemotongan']); ?>">
											</div>
										</li>
										<li class="list-group-item">
											<center>
												<button type="button" class="btn btn-info btn-lg btn-block" id="btnSubmit">
													<i class="fa fa-save"></i> SIMPAN
												</button>
											</center>
										</li>
										<li class="list-group-item">
											<center>
												<a href="<?php echo base_url($page); ?>">
													<button type="button" class="btn btn-danger btn-lg btn-block" id="btnCancel">
														<i class="fa fa-chevron-left"></i> KEMBALI
													</button>
												</a>
											</center>
										</li>
									</ul>

								</section>
							</div>
						</div>
						<input type="hidden" name="type" id="type" value="">
						<input type="hidden" name="idMasuk" id="idMasuk" value="<?php echo $data['id_masuk']; ?>">
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
					</form>
				</div>
			</div>
		</section>
	</div>
</div>

<div class="modal fade" id="modal-supplier">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">supplier</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered" id="supplierTable">
					<thead class="alert">
					<tr>
						<th width="3%"></th>
						<th style="text-align: center">Nama</th>
						<th style="text-align: center">No HP</th>
						<th style="text-align: center">Alamat</th>
						<th style="text-align: center">Email</th>
						<th style="text-align: center">Keterangan</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-barang">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Barang</h4>
			</div>
			<div class="modal-body">
				<table class="table table-bordered" id="barangTable" style="width:100%">
					<thead class="alert">
					<tr>
						<th width="3%"></th>
						<th width="40%" style="text-align: center">Kode</th>
						<th width="20%" style="text-align: center">Nama</th>
						<th width="20%" style="text-align: center">Harga</th>
						<th width="20%" style="text-align: center">Stok</th>
						<th width="20%" style="text-align: center">Produksi</th>
					</tr>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<!-- Modal Pemotongan -->
<div class="modal fade" id="modal-verifikasi">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Verifikasi</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Username :</label>
					<div class="col-sm-9">
						<input type="text" class="form-control pull-right" id="username" name="username" placeholder="Username">
					</div>
				</div>
				<div class="col-sm-12 form-group">
					<label class="col-sm-3 control-label">Password :</label>
					<div class="col-sm-9">
						<input type="password" class="form-control pull-right" id="password" name="password" placeholder="Password">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnVerifikasi">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-pemotongan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Pemotongan</h4>
			</div>
			<div class="modal-body">
				<div class="col-sm-12 form-group">
					<label class="col-sm-2 control-label">Jumlah :</label>
					<div class="col-sm-10">
						<input type="text" class="form-control input-uang" id="jumlahPemotongan" value="0" placeholder="Jumlah Pemotongan">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" id="btnPemotongan">Simpan</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
