<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Laporan
			<small>Penjualan Stok Barang</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Laporan Stok Barang</li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<div class="row">
							<div class="col-sm-3  form-group">
								<label for="member">Barang</label>
								<div class="form-group">
									<select id="cbobarang" name="barang" class="form-control select2">
										<option value="" selected="">ALL</option>
										<?php foreach ($barang as $barang) : ?>
											<?php if ($idbarang != null): ?>
												<option value="<?php echo $barang['id_kue'] ?>" <?php echo $barang['id_kue'] == $idbarang ? 'selected' : ''; ?>> <?php echo $barang['nama_kue'] ?></option>
											<?php else : ?>
												<option value="<?php echo $barang['id_kue'] ?>"><?php echo $barang['nama_kue'] ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3  form-group">
								<label for="member">Departemen</label>
								<div class="form-group">
									<select id="cbodepartemen" name="departemen" class="form-control select2">
										<option value="" selected="">ALL</option>
										<?php foreach ($departemen as $departemen) : ?>
											<option value="<?php echo $departemen['nama'] ?>"><?php echo $departemen['nama'] ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="col-sm-4 form-group">
								<label for="tgl">Date</label>
								<div class="input-group col-sm-12">
									<input type="text" class="form-control tgl pull-right input-sm" id="tglAwal" name="tglAwal" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAwal)); ?>">
									<div class="input-group-addon">to</div>
									<input type="text" class="form-control tgl pull-right input-sm" id="tglAkhir" name="tglAkhir" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAkhir)); ?>">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-block btn-warning" id="searchBtn">
										<i class="fa fa-search"> Cari </i></button>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-3 form-group">
								<label for="totalBarang">Jumlah Barang</label>
								<div class="input-group col-sm-12">
									<input class="form-control text-right input-sm" id="totalBarang" readonly="" name="totalBarang" value="0"/>
								</div>
							</div>
						</div>
					</div>
					<div class="box-body">
						<table id="listTable" class="table table-bordered table-striped">

						</table>
					</div>
				</div><!--end of whitebox -->
			</div>
		</div>
		</form>
	</section>
</div>
