<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Laporan
			<small>Penjualan</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Laporan Penjualan</li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
					</div>
					<div class="box-body">
						<div class="well well-sm">
							<div class="row">
								<div class="col-sm-4 form-group">
									<label for="supplier">supplier</label>
									<div class="form-group">
										<select id="supplier" name="supplier" class="form-control input-sm">
											<option value="" selected="">ALL</option>
											<?php foreach ($supplier as $supplier) : ?>
												<option value="<?php echo $supplier['id_sup'] ?>"><?php echo $supplier['nama'] ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-sm-4 form-group">
									<label for="tgl">Date</label>
									<div class="input-group col-sm-12">
										<input type="text" class="form-control tgl pull-right input-sm" id="tglAwal" name="tglAwal" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAwal)); ?>">
										<div class="input-group-addon">to</div>
										<input type="text" class="form-control tgl pull-right input-sm" id="tglAkhir" name="tglAkhir" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAkhir)); ?>">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-warning" id="searchBtn">
											<i class="fa fa-search"> Cari </i></button>
									</div>
								</div>
							</div>
						</div>
						<table id="listTable" class="table table-bordered table-striped">
							<div>
								<div class="col-sm-2 form-group">
									<label for="totalBarang">Jumlah Barang Masuk</label>
									<div class="input-group col-sm-12">
										<input class="form-control text-right input-sm" id="totalBarang" readonly="" name="totalBarang" value="0"/>
									</div>
								</div>
								<div class="col-sm-2 form-group">
									<label for="grandtotal">Jumlah Retur Barang Masuk</label>
									<div class="input-group col-sm-12">
										<input class="form-control text-right input-sm" id="countRetur" readonly="" name="countRetur" value="0"/>
									</div>
								</div>
								<div class="col-sm-2 form-group">
									<label for="totalBarang">Jumlah Penjualan</label>
									<div class="input-group col-sm-12">
										<input class="form-control text-right input-sm" id="countTransaksi" readonly="" name="countTransaksi" value="0"/>
									</div>
								</div>
							</div>
						</table>
					</div>
				</div><!--end of whitebox -->
			</div>
		</div>
		</form>
	</section>
</div>
