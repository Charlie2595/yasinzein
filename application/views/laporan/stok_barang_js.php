<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('.select2').select2();
	var countQty = 0;
	$('#listTable').bootstrapTable({
		classes: 'table table-no-bordered table-condensed table-hover',
		pagination: false,
		pageSize: 50,
		pageList: [10, 25, 50, 'ALL'],
		sortOrder: 'asc',
		search: false,
		showRefresh: false,
		showColumns: false,
		showFooter: false,
		stickyHeader: true,
		stickyHeaderOffsetY: $('nav.header').height() + 'px',
		rowStyle: function rowStyle(value, row, index) {
			var obj = {};
            obj.classes = 'text-nowrap';
            if((parseInt(value.masuk ? value.masuk : 0) - parseInt(value.transaksi ? value.masuk : 0)) !== parseInt(value.retur ? value.masuk : 0)){
                obj.classes += ' danger';
            }
			return obj;
        },
		queryParams: function (p) {
			p.tglAwal = $('#tglAwal').val();
			p.tglAkhir = $('#tglAkhir').val();
			p.barang = $('#cbobarang').val();
			p.departemen = $('#cbodepartemen').val();

			return p;
		},
		columns: [{
			field: 'nama_kue',
			title: 'Kue',
			sortable: true,
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'departemen',
			title: 'Departemen',
			sortable: true,
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'masuk',
			title: 'Barang Masuk',
			sortable: true,
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'transaksi',
			title: 'Penjualan',
			sortable: true,
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				countQty += value * 1;
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'retur',
			title: 'Retur',
			sortable: true,
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'stok',
			title: 'Stok Akhir',
			sortable: true,
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}],
		onPostBody: function () {
			$('#totalBarang').val(numeral(String(countQty)).format('0,0'));
			countQty = 0;
		}
	});

	$('#searchBtn').on('click', function (e) {
		$('#listTable').bootstrapTable('refresh', {
			url: "<?php echo base_url();?>searchLaporanStokBarang",
			query: {
				barang: $('#cbobarang').val(),
				tglAwal: $('#tglAwal').val(),
				tglAkhir: $('#tglAkhir').val(),
				departemen: $('#cbodepartemen').val(),
			}
		});
		$('#listTable').bootstrapTable('resetWidth');
	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
	});

	document.addEventListener("DOMContentLoaded", function (event) {
		$('#searchBtn').trigger('click');
	});
</script>
