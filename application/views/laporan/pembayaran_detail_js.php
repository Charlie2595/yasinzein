<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	var year = "";

	$(function () {
		$('#searchBtn').click(function () {
			$('#searchBarang').submit();
		});
		$('.select2').select2();
		var d = new Date();
		year = d.getFullYear();

		$('.yearselect').yearselect({
			start: 2000,
			end: year
		});
		var grandtotal = 0;
	});

	$('#listTable').DataTable({
		"paging": true,
		"searching": true,
	});

	$('#monthTable').DataTable({
		"paging": false,
		"searching": false,
		"scrollX": true,
	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
	});

	function strPad(input, length, string) {
		string = string || '0';
		input = input + '';
		return input.length >= length ? input : new Array(length - input.length + 1).join(string) + input;
	}

	$('#dateTable').bootstrapTable({
		classes: 'table table-no-bordered table-condensed table-hover',
		pagination: false,
		pageSize: 50,
		pageList: [10, 25, 50, 'ALL'],
		sortOrder: 'asc',
		search: false,
		showRefresh: false,
		showColumns: false,
		showFooter: false,
		stickyHeader: true,
		stickyHeaderOffsetY: $('nav.header').height() + 'px',
		rowStyle: function rowStyle(value, row, index) {
			var obj = {};
			obj.classes = 'text-nowrap';
			return obj;
		},
		queryParams: function (p) {
			p.tglAwal = $('#tglAwal').val();
			p.tglAkhir = $('#tglAkhir').val();
			p.payment = $('#cbopayment').val();

			return p;
		},
		columns: [{
			field: 'document',
			title: 'Document',
			sortable: true,
			width: '5%',
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			width: '10%',
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'nama',
			title: 'Jenis Pembayaran',
			sortable: true,
			width: '10%',
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			//     field : 'total',
			//     title : 'Total Penjualan',
			//     sortable : true,
			//     width : '5%',
			//     halign : 'left',
			//     align : 'right',
			//     formatter : function(value,row,index){
			//         return numeral(String(value)).format('0,0');
			//     }
			// },  {
			//     field : 'kembalian',
			//     title : 'Kembalian',
			//     sortable : true,
			//     width : '5%',
			//     halign : 'left',
			//     align : 'right',
			//     formatter : function(value,row,index){
			//         return numeral(String(value)).format('0,0');
			//     }
			// },  {
			field: 'grandtotal',
			title: 'Grand Total',
			sortable: true,
			width: '5%',
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				grandtotal += row.grandtotal * 1;
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'keterangan',
			title: 'Keterangan',
			sortable: true,
			width: '5%',
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				return value + (row.keteranganPenjualan == null ? "" : row.keteranganPenjualan == " " ? "" : row.keteranganPenjualan) + (row.last_document == null ? "" : row.last_document == " " ? "" : row.last_document);
			}
		}],
		onPostBody: function () {
			$('#grandtotal').val(numeral(String(grandtotal)).format('0,0'));
			grandtotal = 0;
		}
	});

	$('#searchBtn').on('click', function (e) {
		$('#dateTable').bootstrapTable('refresh', {
			url: "<?php echo base_url();?>searchPembayaranDetailByDate",
			query: {
				tglAwal: $('#tglAwal').val(),
				payment: $('#cbopayment').val(),
				tglAkhir: $('#tglAkhir').val()
			}
		});
		$('#dateTable').bootstrapTable('resetWidth');
	});

	document.addEventListener("DOMContentLoaded", function (event) {
		$('#searchBtn').trigger('click');
	});
</script>
