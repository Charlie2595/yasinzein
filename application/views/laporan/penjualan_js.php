<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('#member').select2();

	var grandtotal = 0;
	var countQty = 0;

	$('#listTable').bootstrapTable({
		classes: 'table table-no-bordered table-condensed table-hover text-nowrap',
		pagination: false,
		pageSize: 50,
		pageList: [10, 25, 50, 'ALL'],
		sortOrder: 'asc',
		search: true,
		showRefresh: true,
		showColumns: true,
		showFooter: false,
		stickyHeader: true,
		stickyHeaderOffsetY: $('nav.header').height() + 'px',
		rowStyle: function rowStyle(value, row, index) {
			var obj = {};
			obj.classes = 'text-nowrap';
			return obj;
		},
		queryParams: function (p) {
			p.tglAwal = $('#tglAwal').val();
			p.tglAkhir = $('#tglAkhir').val();
			p.member = $('#member').val();

			return p;
		},
		columns: [{
			field: 'tipe_trx',
			title: 'Jenis Transaksi',
			sortable: true,
			width: '5%',
			halign: 'left',
			align: 'right',
			formatter: function (value, row, index) {
				if (value == 1) {
					return 'POS';
				} else if (value == 2) {
					return 'Pesanan';
				} else if (value == 3) {
					return 'B2B';
				} else if (value == 4) {
					return 'Cabang';
				}
			}
		}, {
			field: 'document',
			title: 'Document',
			width: "15%",
			sortable: true,
			formatter: function (value, row, index) {
				if (row['tipe_trx'] == 1) {
					return '<a href="<?php echo base_url();?>pos/' + row['id'] + '">' + value + '</a>';
				} else if (row['tipe_trx'] == 2) {
					return '<a href="<?php echo base_url();?>pesanan/' + row['id'] + '">' + value + '</a>';
				} else if (row['tipe_trx'] == 3) {
					return '<a href="<?php echo base_url();?>b2b/' + row['id'] + '">' + value + '</a>';
				} else if (row['tipe_trx'] == 4) {
					return '<a href="<?php echo base_url();?>cabang/' + row['id'] + '">' + value + '</a>';
				}
			}
		}, {
			field: 'tgl',
			title: 'Tanggal',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'nama',
			title: 'Member',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "10%",
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			//     field : 'diskon_member',
			//     title : 'Diskon Member (Rp)',
			//     sortable : true,
			//     halign : 'left',
			//     align : 'right',
			//     width : "10%",
			//     formatter : function(value,row,index){
			//         console.log(row);
			//         if(value == 0){
			//             return 0;
			//         } else {
			//             return numeral(String(row['total'])/value*1).format('0,0');
			//         }
			//     }
			// },  {
			field: 'pemotongan',
			title: 'Diskon',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'total',
			title: 'Total (Rp)',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				grandtotal += row.grandtotal * 1;
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'grandtotal',
			title: 'Grandtotal (Rp)',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'totalQty',
			title: 'Total Barang',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				countQty += value * 1;
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'keterangan',
			title: 'Keterangan',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				return (value == null ? "" : value == " " ? "" : value)
			}
		}],
		onPostBody: function () {
			countTransaksi = $('#listTable').bootstrapTable('getData').length;
			$('#grandtotal').val(numeral(String(grandtotal)).format('0,0'));
			$('#totalBarang').val(numeral(String(countQty)).format('0,0'));
			$('#countTransaksi').val(numeral(String(countTransaksi)).format('0,0'));
			countTransaksi = 0;
			grandtotal = 0;
			countQty = 0;
		}
	});

	$('#searchBtn').on('click', function (e) {
		$('#listTable').bootstrapTable('refresh', {
			url: "<?php echo base_url();?>searchPenjualanMember",
			query: {
				member: "",
				tglAwal: $('#tglAwal').val(),
				tglAkhir: $('#tglAkhir').val()
			}
		});
		$('#listTable').bootstrapTable('resetWidth');

	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
	});

	document.addEventListener("DOMContentLoaded", function (event) {
		$('#searchBtn').trigger('click');
	});
</script>
