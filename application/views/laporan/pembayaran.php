<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Laporan
			<small>Pembayaran</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"> Laporan Pembayaran</li>
		</ol>
	</section>

	<?php if (isset($_SESSION['createSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['createSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php elseif (isset($_SESSION['deleteSucceedMsg'])) : ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable m-b-10">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<?php echo $_SESSION['deleteSucceedMsg']; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title m-b-5"> Laporan Pembayaran</h3>
					</div>
					<ul class="nav nav-tabs">
						<li class="active"><a href="#harian" data-toggle="tab">Harian</a></li>
						<li><a href="#bulanan" data-toggle="tab">Bulanan</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="harian">
							<div class="box-header">
								<div class="row">
									<div class="col-sm-3 form-group">
										<label for="member">Tipe Pembayaran</label>
										<div class="form-group">
											<select id="cbopayment" name="payment" class="form-control select2" style="width: 100%">
												<option value="all">ALL</option>
												<?php foreach ($payment as $payment) : ?>
													<?php if ($idPayment != null): ?>
														<option value="<?php echo $payment['id_masterbayar'] ?>" <?php echo $payment['id_masterbayar'] == $idPayment ? 'selected' : ''; ?>> <?php echo $payment['nama'] ?></option>
													<?php else : ?>
														<option value="<?php echo $payment['id_masterbayar'] ?>"><?php echo $payment['nama'] ?></option>
													<?php endif; ?>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="col-sm-4 form-group">
										<label for="tgl">Date</label>
										<div class="input-group col-sm-12">
											<input type="text" class="form-control tgl pull-right input-sm" id="tglAwal" name="tglAwal" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAwal)); ?>">
											<div class="input-group-addon">to</div>
											<input type="text" class="form-control tgl pull-right input-sm" id="tglAkhir" name="tglAkhir" placeholder="Tanggal" readonly value="<?php echo date('d-m-Y', strtotime($tglAkhir)); ?>">
										</div>
									</div>
									<div class="col-sm-5">
										<div class="btn-group pull-right">
											<button type="button" class="btn btn-warning" id="searchBtn">
												<i class="fa fa-search"> Cari </i></button>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3 form-group">
										<label for="grandtotal">Total Pembayaran</label>
										<div class="input-group col-sm-12">
											<input class="form-control text-right input-sm" id="grandtotal" readonly="" name="grandtotal" value="0"/>
										</div>
									</div>
								</div>
							</div>
							<div class="box-body">
								<table id="dateTable">

								</table>
							</div>
						</div>
						<div class="tab-pane" id="bulanan">
							<div class="box-header">
								<section>
									<div class="col-sm-1">
										<div class="form-group">
											<h5 style="font-weight: bold;">TAHUN:</h5>
										</div>
									</div>
									<div class="col-sm-5">
										<div class="form-group col-sm-5">
											<select class="yearselect select2" style="width: 100%" id="year" name="year"></select>
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group">
											<button type="button" class="btn btn-block btn-warning" id="searchBtnYear" onclick="searchPembayaranByYear()">
												<i class="fa fa-search"> Cari </i></button>
										</div>
									</div>
								</section>
							</div>
							<div class="box-body">
								<table id="yearTable" class="table table-bordered table-striped" style="width: 100%">
									<thead>
									<tr>
										<th>Jenis Pembayaran</th>
										<th>Januari</th>
										<th>Februari</th>
										<th>Maret</th>
										<th>April</th>
										<th>Mei</th>
										<th>Juni</th>
										<th>Juli</th>
										<th>Agustus</th>
										<th>September</th>
										<th>Oktober</th>
										<th>November</th>
										<th>Desember</th>
									</tr>
									</thead>
									<tbody id="yearBody">

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!--end of whitebox -->
			</div>
		</div>
	</section>
</div>
