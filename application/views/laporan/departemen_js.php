<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('.select2').select2();

	$('#searchBtn').on('click', function (e) {
		$.ajax({
			url: "<?php echo base_url();?>searchLaporanDepartemen",
			method: 'get',
			data: {
				tglAwal: $('#tglAwal').val(),
				tglAkhir: $('#tglAkhir').val(),
				departemen: $('#cbodepartemen').val(),
			},
			success: function (data) {
				var data = $.parseJSON(data);
				console.log(data);
				var countItem = 0;
				$("#tableBody").html("");
				$.each(data, function (key, value) {
					var newRow = "<tr>" +
						"<th colspan='4' style='text-align:center; background-color:#4CAF50'>" + value['departemen'] + "</th>" +
						"</tr>" +
						"<tr>" +
						"<th>Nama</th>" +
						"<th>Stok</th>" +
						"<th>Total</th>" +
						"<th>Retur</th>" +
						"</tr>";
					$.each(value['detail'], function (index, detail) {
						newRow += "<tr>" +
							"<td>" + detail['nama_kue'] + "</td>" +
							"<td>" + detail['stok'] + "</td>" +
							"<td>" + detail['jumlah'] + "</td>" +
							"<td>" + detail['retur'] + "</td>" +
							"</tr>";
						countItem = countItem + (detail['jumlah'] * 1);
					});
					newRow += "<tr><td colspan='4'></td></tr>";
					$("#tableBody").append(newRow);
					$('#totalBarang').val(numeral(String(countItem)).format('0,0'));
				});
			}, error: function (xhr, text, status) {
				if (xhr.status == 422) {
					$('#pleaseWaitDialog').modal('hide');
					showAlert('', xhr.responseJSON.join('\n'), 'error');
				}
			}
		});
	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
	});

	document.addEventListener("DOMContentLoaded", function (event) {
		$('#searchBtn').trigger('click');
	});
</script>
