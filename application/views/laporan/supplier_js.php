<script type="text/javascript">
	function showModal(id) {
		$('#confirm-edit-modal' + id).modal('show');
	}

	$('#supplier').select2();

	var countRetur = 0;
	var countQty = 0;
	var countTransaksi = 0;

	$('#listTable').bootstrapTable({
		classes: 'table table-no-bordered table-condensed table-hover text-nowrap',
		pagination: false,
		pageSize: 50,
		pageList: [10, 25, 50, 'ALL'],
		sortOrder: 'asc',
		search: true,
		showRefresh: true,
		showColumns: true,
		showFooter: false,
		stickyHeader: true,
		stickyHeaderOffsetY: $('nav.header').height() + 'px',
		rowStyle: function rowStyle(value, row, index) {
			var obj = {};
			obj.classes = 'text-nowrap';
			return obj;
		},
		queryParams: function (p) {
			p.tglAwal = $('#tglAwal').val();
			p.tglAkhir = $('#tglAkhir').val();
			p.member = $('#member').val();

			return p;
		},
		columns: [{
			field: 'nama_kue',
			title: 'Nama Kue',
			sortable: true,
			halign: 'left',
			align: 'left',
			width: "15%",
			formatter: function (value, row, index) {
				return value;
			}
		}, {
			field: 'masuk',
			title: 'Barang Masuk',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "10%",
			formatter: function (value, row, index) {
				if (value == 0) {
					return 0;
				} else {
					return numeral(String(value)).format('0,0');
				}
			}
		}, {
			field: 'transaksi',
			title: 'Transaksi',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				return numeral(String(value)).format('0,0');
			}
		}, {
			field: 'retur',
			title: 'Retur',
			sortable: true,
			halign: 'left',
			align: 'right',
			width: "15%",
			formatter: function (value, row, index) {
				countQty += row.masuk * 1;
				countTransaksi += row.transaksi * 1;
				countRetur += row.retur * 1;
				return numeral(String(value)).format('0,0');
			}
		}],
		onPostBody: function () {
			$('#countTransaksi').val(numeral(String(countTransaksi)).format('0,0'));
			$('#totalBarang').val(numeral(String(countQty)).format('0,0'));
			$('#countTransaksi').val(numeral(String(countTransaksi)).format('0,0'));
			countTransaksi = 0;
			countRetur = 0;
			countQty = 0;
		}
	});

	$('#searchBtn').on('click', function (e) {
		$('#listTable').bootstrapTable('refresh', {
			url: "<?php echo base_url();?>searchSupplier",
			query: {
				supplier: $('#supplier').val(),
				tglAwal: $('#tglAwal').val(),
				tglAkhir: $('#tglAkhir').val()
			}
		});
		$('#listTable').bootstrapTable('resetWidth');

	});

	$.fn.datepicker.defaults.format = "dd-mm-yyyy";
	$('.tgl').datepicker({
		autoclose: true,
	});

	// document.addEventListener("DOMContentLoaded", function(event) {
	//     $('#searchBtn').trigger('click');
	// });
</script>
