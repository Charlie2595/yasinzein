<script type="text/javascript">
	$('#btnSubmit').click(function () {
		if ($('#createForm').valid()) {
			$('#pleaseWaitDialog').modal('show');
			$.ajax({
				url: "<?php echo base_url($page . '/edit'); ?>",
				method: 'post',
				data: {
					id: $('#id').val(),
					nama: $('#nama').val()
				},
				success: function (data) {
					var data = $.parseJSON(data);
					console.log(data['status']);
					if (data['status'] == 'error') {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'error');
					} else {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', data['description'], 'success', function () {
							window.location = "<?php echo base_url($page); ?>"
						});
					}
				}, error: function (xhr, text, status) {
					console.log(status);
					if (xhr.status == 422) {
						$('#pleaseWaitDialog').modal('hide');
						showAlert('', xhr.responseJSON.join('\n'), 'error');
					}
				}
			});
		}
	});

	$('#createForm').validate({
		errorElement: 'div',
		rules: {
			nama: {
				required: true,
			}
		},

		messages: {
			nama: {
				required: "Nama harus diisi",
			}
		}
	});

</script>
