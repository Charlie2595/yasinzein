<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			UBAH
			<small><?php echo strtoupper($page) ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-dashboard"></i> HOME</a></li>
			<li><a href="<?php echo base_url($page); ?>"><?php echo strtoupper($page); ?></a></li>
			<li class="active">BUAT <?php echo strtoupper($page); ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<!--/.col (left) -->
			<!-- right column -->
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title"><?php echo strtoupper($page); ?> Form</h3>
					</div>
					<form class="form-horizontal" id="createForm" method="post">
						<div class="box-body">
							<div class="form-group">
								<label for="inputNama" class="col-sm-4 control-label">Nama :</label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="nama" placeholder="Nama" name="nama" value="<?php echo $detail['nama'] ?>">
								</div>
							</div>
						</div>
						<!-- /.box-body -->

						<div class="box-footer">
							<div class="col-sm-offset-3 col-sm-9">
								<button type="button" class="btn btn-info waves-effect waves-light m-t-10 pull-right" id="btnSubmit">
									<i class="fa fa-save"></i> Simpan
								</button>
							</div>
						</div>
						<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
						<input type="hidden" name="id" id="id" value="<?php echo $detail['id_masterbayar'] ?>"/>
						<!-- /.box-footer -->
					</form>
				</div>
				<!-- /.box -->
			</div>
			<!--/.col (right) -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
