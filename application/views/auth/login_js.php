<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 06/09/2017
 * Time: 23.11
 */ ?>
<script>
	$(function () {
		$("#btnLogin").click(function () {
			if ($("#loginform").valid()) {
				$('#loginform').submit();
			}
		});

		$('#loginForm').validate({
			rules: {
				txtUsername: {
					required: true,
				},

				txtPassword: {
					required: true,
				},
			},
			messages: {
				txtUsername: {
					required: "Username harus diisi",
				},

				txtPassword: {
					required: "Password harus diisi",
				},
			}
		});
	});
</script>
