<?php
/**
 * Created by PhpStorm.
 * User: Charlie
 * Date: 02/03/2017
 * Time: 14:09
 */
?>
<div class="login-box">
	<div class="login-logo">
		<h1 class="m-b-15 login-head">LOG IN</h1>
	</div>
	<div class="login-box-body">
		<form method="post" action="<?php echo base_url('authenticate') ?>" class="form-horizontal form-material" id="loginForm">
			<?php if (isset($_SESSION['formError']['errMsg'])) : ?>
				<div class="alert alert-danger">
					<?php echo $_SESSION['formError']['errMsg']; ?>
				</div>
			<?php endif; ?>
			<?php if (isset($_SESSION['logoutMsg'])) : ?>
				<div class="alert alert-success">
					<?php echo $_SESSION['logoutMsg']; ?>
				</div>
			<?php endif; ?>

			<div class="form-group has-feedback">
				<div class="col-xs-12">
					<label>Username :</label>
				</div>
				<div class="col-xs-12">
					<input class="form-control" style="display:block" type="text" placeholder="username" name="txtUsername" value="<?php echo set_value('txtUsername'); ?>">
					<span class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
			</div>

			<div class="form-group has-feedback">
				<div class="col-xs-12">
					<label>Password :</label>
				</div>
				<div class="col-xs-12">
					<input class="form-control" style="display:block" type="password" placeholder="password" name="txtPassword">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
			</div>

			<div class="form-group text-center m-t-20">
				<div class="col-xs-12">
					<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="btnLogin" id="btnLogin">Log In</button>
				</div>
			</div>
			<input type="hidden" name="<?= $csrf['name']; ?>" value="<?= $csrf['hash']; ?>"/>
		</form>
		<p class="text-center">2018 &copy; SP</p>
	</div>
</div>
