function showAlert(title,text,type,callback){
    swal({
        title:title,
        text:text,
        type:type,
        html:true,
        allowOutsideClick:false,
        allowEscapeKey:false,
    }, callback);
}

function showConfirm(title,text,type,callback){
    swal({
        title:title,
        text: text,
        type: type,
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        closeOnCancel: true,
        allowOutsideClick:false,
        allowEscapeKey:false,
    }, callback);
}

function decimalFormatter(number){
    return numeral(String(number)).format('0,0');
}

$('#modal-verifikasi').on('shown.bs.modal', function () {
    $('#username').focus();
});

$('#modal-pemotongan').on('shown.bs.modal', function () {
    $('#jumlahPemotongan').focus();
});

$('#modal-payment').on('shown.bs.modal', function () {
    $('#jumlahPembayaran').focus();
});

$('#btnVerifikasi').click(function () {
    cekVerifikasi();
});

function cekVerifikasi() {
    
    var username = $("#username").val();
    var password = $("#password").val();
    $.ajax({
        url: "<?php echo base_url();?>verifikasiPassword",
        data: {"password": password, "username": username},
        type: "POST",
        success: function (data) {
            if (data == 'true') {
                if (pemotongan == "") {
                    pemotongan = 0;
                }
                $('#pemotongan').val(pemotongan);
                $('#viewPemotongan').html(toRp(pemotongan));
                calculatetotal();
                
                $('#jumlahPemotongan').val(0);
            } else {
                showAlert('', "Username dan password salah", 'error');
            }
        }
    });
}

function savePemotongan() {
    var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
    if (pemotongan == '' || pemotongan == null) {
        pemotongan = 0;
    }
    var diskon = $('#diskonMember').val();
    var total = $('#calDiskonTotal').autoNumeric('get') * 1;
    if (pemotongan > (total - diskon)) {
        showAlert('', "Jumlah pemotongan harga tidak boleh lebih dari total biaya", 'error');
    } else {
        var total = $('#calDiskonTotal').autoNumeric('get') * 1;
        var grandtotal = $('#calDiskonGrandtotal').autoNumeric('get') * 1;
        var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;
        $("#viewDiskonTotal").val(toRp(total));
        $("#viewDiskonGrandtotal").val(toRp(grandtotal));
        $("#viewjumlahPemotongan").val(toRp(pemotongan));
        $('#modal-verifikasi').modal({show: 'true'});
        $('#modal-pemotongan').modal('hide');
    }
    $('#modal-pemotongan').modal('hide');
}

//Diskon Pemotongan
$('#btnPemotongan').click(function () {
    savePemotongan();
});

function calDiskon(){
    var total = $('#calDiskonTotal').autoNumeric('get') * 1;
    var pemotongan = $('#jumlahPemotongan').autoNumeric('get') * 1;

    var grandtotal = total - pemotongan;

    $("#calDiskonGrandtotal").val(toRp(grandtotal));
}

